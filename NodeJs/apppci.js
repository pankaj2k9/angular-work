var redis = require("redis");
var SSE = require('sse-nodejs');
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

// io.set('heartbeat timeout', 20000); 
// io.set('heartbeat interval', 2000);

var client = redis.createClient(
	6380, 
	'surgecloud.redis.cache.windows.net', 
	{
    	auth_pass: 'QMb/7DyNrG6LRbKWrwruMgqYO3NBATlZbo8aIwG1JQ4=',
    	tls: { servername: 'surgecloud.redis.cache.windows.net' }
	},
	{
		retry_strategy: function retry(options) 
		{
	        console.log('Options: ' + options);
	        if (options.error.code === 'ECONNREFUSED') {
	            console.log('connection refused');
	        }
	        if (options.total_retry_time > 1000 * 60 * 60) {
                console.log('Retry time exhausted');
	            return new Error('Retry time exhausted');
            }
            
            console.log('trying to reconnect to Redis (Retry Strategy)');
	        return Math.max(options.attempt * 100, 2000);
	    }
	}
);
console.log(Date());
console.log('Redis Client Created : ' + client.connected + ' : ' + client.connection_id);

client.on('connect', function connect() {
    // console.log(Date());
    console.log(Date() + ': Connecting to Redis');
  });

client.on('ready', function connect() {
    // console.log(Date());
    console.log(Date() + ': Redis connection is established');
  });
client.on('Error', function error(err) {
    // console.log(Date());
    console.log(Date() + ': Redis connection error');
  });

client.on('reconnecting', function reconnecting() {
    //console.log(Date());
    console.log(Date() + ': Redis connection re-established');
  });

client.on('end', function reconnecting() {
    //console.log(Date());
    console.log(Date() + ': Redis connection closed');
  }); 

client.on('warning', function reconnecting() {
    // console.log(Date());
    console.log(Date() + ': Redis connection idle');
  }); 
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    next();
});

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function (socket) {
	Object.keys(io.sockets.sockets).forEach(function(id) { console.log(Date() + ": **ID**:",id) })
    
    console.log(Date() +': live Connections (io.ON) : ' + io.engine.clientsCount);

    var intervalId = null;
    socket.on('subscribe', (data) => {
		// console.log(Date() + ': ' + data);
		// console.log('User connected : ' + socket.id);
        // console.log('Total Users : ' + io.sockets.clients() );
        
        intervalId = setInterval(() => {  data.forEach((element) => { global.getKey(element, socket);  });
        }, 1000);
        // console.log(Date() + ': io.sockets.connected[socket.id].connected : ' + io.sockets.connected[socket.id].connected + ', intervalId : ' + intervalId + ', socket.id : ' + socket.id + ' Live Connections : ' + io.engine.clientsCount);
    });

    socket.on('disconnect', function () {
        //console.log(Date());
        console.log(Date() + ': User disconnected : ' + socket.id);
        //console.log(Date() + ': Total Users : ' + io.sockets.clients() );
        console.log(Date() + ': Socket Disconnected (before Clear Interval) : ' + socket.id + ' : ' + socket.disconnected);
	    //io.sockets.sockets.forEach(function(s) { s.disconnect(true); });
	    //Object.keys(io.sockets.sockets).forEach(function(s) { io.sockets.sockets[s].disconnect(true); });
        delete io.sockets[socket.id];
        delete io.sockets.sockets[socket.id];
        // console.log(Date() + ': IntervalId : ' + intervalId );
        clearInterval(intervalId);
        // console.log(Date() + ': Socket Disconnected (after Clear Interval): ' + socket.id + ' : ' + socket.disconnected);
	    // console.log(Date() + ': live Connections (io.disconnect): ' + io.engine.clientsCount);
        // socket.disconnect(true);
       	Object.keys(io.sockets.sockets).forEach(function(id) { console.log(Date() + ': **ID**:', id) })
    });
});

getKey = (key, socket) => {
    //if(client.connected && io.sockets[socket.id].connected)
    if(client.connected && io.sockets.connected[socket.id] && io.sockets.connected[socket.id].connected)
    //if(client.connected)
    {
    	// console.log('io.sockets.connected[socket.id].connected : ' + io.sockets.connected[socket.id].connected + ', socket.connected : ' + socket.connected + ', socket.id : ' + socket.id + ' Live Connections : ' + io.engine.clientsCount);
        //console.log('live Connections (GetKey): ' + io.engine.clientsCount);
        client.hgetall(key, (error, reply) => {
            //console.log(reply);
            if (!error && reply) {
                socket.emit('data', reply);
            };
        });
    }
    else
    if (!client.connected)
    {
        // console.log(Date());
        console.log(Date() + ': From Get Key, Redis disconnected, waiting for reconnect : ' + socket.id );
    }
    // else if(!io.sockets[socket.id].connected) { 
    //     console.log(Date());
    //     console.log('From Get Key, Angular Client disconnected: ' + socket.id );
    // }
}

http.listen(3000, function () {
    // console.log(Date());
    console.log(Date() + ': Listening on *:3000');
});
