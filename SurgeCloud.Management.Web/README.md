# surgecloud

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## data format for tree map and spidar and radar 
<!-- <app-tree-map-chart [data]="data" [treeConfigurations] ="treeConfigDatas" (selectTreeEvent)="onNotifyEventFromTreeData($event)"></app-tree-map-chart> -->
<!-- <app-swim-lane-chart></app-swim-lane-chart> -->
<app-spider-radar-chart [dataMap]="dataMapSpider" [spidarData] ="spiderChartData"></app-spider-radar-chart>

dataMapSpider =[
    {
      "name": "French Southern Territories",
      "series": [
        {
          "value": 6242,
          "name": "2016-09-20T05:45:33.775Z"
        },
        {
          "value": 4312,
          "name": "2016-09-21T21:30:26.649Z"
        },
        {
          "value": 2326,
          "name": "2016-09-16T17:27:13.851Z"
        },
        {
          "value": 4515,
          "name": "2016-09-18T06:14:45.158Z"
        },
        {
          "value": 2612,
          "name": "2016-09-18T10:28:51.458Z"
        }
      ]
    },
    {
      "name": "Greece",
      "series": [
        {
          "value": 5986,
          "name": "2016-09-20T05:45:33.775Z"
        },
        {
          "value": 3539,
          "name": "2016-09-21T21:30:26.649Z"
        },
        {
          "value": 5445,
          "name": "2016-09-16T17:27:13.851Z"
        },
        {
          "value": 3081,
          "name": "2016-09-18T06:14:45.158Z"
        },
        {
          "value": 6068,
          "name": "2016-09-18T10:28:51.458Z"
        }
      ]
    },
    {
      "name": "Sao Tome and Principe",
      "series": [
        {
          "value": 3984,
          "name": "2016-09-20T05:45:33.775Z"
        },
        {
          "value": 4407,
          "name": "2016-09-21T21:30:26.649Z"
        },
        {
          "value": 6069,
          "name": "2016-09-16T17:27:13.851Z"
        },
        {
          "value": 6638,
          "name": "2016-09-18T06:14:45.158Z"
        },
        {
          "value": 2204,
          "name": "2016-09-18T10:28:51.458Z"
        }
      ]
    },
    {
      "name": "Antigua and Barbuda",
      "series": [
        {
          "value": 6852,
          "name": "2016-09-20T05:45:33.775Z"
        },
        {
          "value": 2805,
          "name": "2016-09-21T21:30:26.649Z"
        },
        {
          "value": 2188,
          "name": "2016-09-16T17:27:13.851Z"
        },
        {
          "value": 4477,
          "name": "2016-09-18T06:14:45.158Z"
        },
        {
          "value": 3066,
          "name": "2016-09-18T10:28:51.458Z"
        }
      ]
    },
    {
      "name": "New Zealand",
      "series": [
        {
          "value": 4598,
          "name": "2016-09-20T05:45:33.775Z"
        },
        {
          "value": 3195,
          "name": "2016-09-21T21:30:26.649Z"
        },
        {
          "value": 6337,
          "name": "2016-09-16T17:27:13.851Z"
        },
        {
          "value": 6983,
          "name": "2016-09-18T06:14:45.158Z"
        },
        {
          "value": 5942,
          "name": "2016-09-18T10:28:51.458Z"
        }
      ]
    }
  ]

  
  colorScheme = {
    domain: ['#000000', '#A10A28', '#C7B42C']
  };

  data 
  treeConfigDatas:treeMapChartConfig ;
  spiderChartData:spiderradarChartConfig
  
  ngOnInit()
  {

    this.data = single;
    this.treeConfigDatas = new treeMapChartConfig(500,500,this.colorScheme,true,true);
    this.spiderChartData = new spiderradarChartConfig(800 ,500,true,
      true,
      true,
      true,
      null ,
      true,
       false,
      "Country",
      true,
      "date",
      true,
      "10%",
      10,
      10,
       true,
      10,
      3,
      this.colorScheme,
      true,
      true ,
      true,
      10,
      20,
      10,
      20,
      "test" )
  }

  onNotifyEventFromTreeData(data)
  {
   console.log("data from tree component " + data.name +" " + data.value);
  }
}

export var single = [
  {
    "name": "Germany",
    "value": 40632
  },
  {
    "name": "United States",
    "value": 49737
  },
  {
    "name": "France",
    "value": 36745
  },
  {
    "name": "United Kingdom",
    "value": 36240
  },
  {
    "name": "Spain",
    "value": 33000
  },
  {
    "name": "Italy",
    "value": 35800
  }
];

