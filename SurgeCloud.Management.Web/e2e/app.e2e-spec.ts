import { SurgecloudPage } from './app.po';

describe('surgecloud App', () => {
  let page: SurgecloudPage;

  beforeEach(() => {
    page = new SurgecloudPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
