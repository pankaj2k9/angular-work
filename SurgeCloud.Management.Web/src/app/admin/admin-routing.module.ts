import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent, CompanyResolverService, AccessGuard, HeaderComponent} from '../app-common';
import { AuthenticationGuard } from '../authentication.guard';
import { CustomerOnboardingComponent } from './customer-onboarding';
import { TenantManagementComponent } from './tenant-management/tenant-management.component';
import { SubscriptionManagementComponent } from './subscription-management/subscription-management.component';
import { AssetOnboardingComponent } from './asset-onboarding';
import { DeviceManagementComponent } from './device-management/device-management.component';
import { GeoFencingComponent } from '../operation/geo-fencing';
import { AssetDashboardComponent } from '../operation/asset-dashboard';
import { AlarmConsoleComponent } from '../operation/alarm-console';
import { RuleEngineComponent } from '../operation/rule-engine';
import { GeneratorMonitoringComponent } from '../operation/generator-monitoring/generator-monitoring.component';
import { DynamicDashboardComponent } from './dynamic-dashboard/dynamic-dashboard.component';


import {GeoFenceCreateComponent} from '../operation/geo-fence-create/geo-fence-create.component';
import {CreateUserComponent} from "./customer-onboarding/create-user";
import {LoginResolver} from "../login-resolver.service";
import {CreateRouteComponent} from "../operation/create-route/create-route.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {FeatureComponent} from "./dashboard/feature/feature.component";
import {GoogleMapComponent} from "./dashboard/google-map/google-map.component";


const routes: Routes = [
  {
    path: ':type',
    component: HomeComponent,
    // resolve: {
    //   company: CompanyResolverService
    // },
    canActivate: [AuthenticationGuard],

    children: [
      {
        path: '',
        component: DashboardComponent,
        children: [
          {
            path: 'feature',
            component: FeatureComponent,
            resolve: { userData: LoginResolver }
          },
          {
            path: 'map',
            component: GoogleMapComponent
          },
          {
            path: 'customer-onboarding',
            component: CustomerOnboardingComponent,
            // canActivate: [AccessGuard]
          },
          {
            path: 'asset-onboarding',
            component: AssetOnboardingComponent,
            // canActivate: [AccessGuard]
          },
          {
            path: 'dynamic-dashboard',
            component: DynamicDashboardComponent,
            // canActivate: [AccessGuard]
          }
        ],
      },
      {
        path: 'tenant-management',
        component: TenantManagementComponent
      },
      {
        path: 'subscription-management',
        component: SubscriptionManagementComponent
      },
      {
        path: 'device-management',
        component: DeviceManagementComponent
      },
      {
        path: 'geo-fencing',
        component: GeoFencingComponent,
        // canActivate: [AccessGuard]
      },
      {
        path: 'asset-dashboard/:assetId',
        component: AssetDashboardComponent,
        // canActivate: [AccessGuard]
      },
      {
        path: 'asset-dashboard',
        component: AssetDashboardComponent,
        // canActivate: [AccessGuard]
      },
      {
        path: 'alarm',
        component: AlarmConsoleComponent,
        // canActivate: [AccessGuard]
      },
      {
        path: 'rule-engine',
        component: RuleEngineComponent,
        // canActivate: [AccessGuard]
      },
      {
        path: 'alarm/:assetId',
        component: AlarmConsoleComponent,
        // canActivate: [AccessGuard]
      },
      {
        path: 'alarm/:assetId/:gwSNO',
        component: AlarmConsoleComponent,
        // canActivate: [AccessGuard]
      },
      {
        path: 'generator-monitoring',
        component: GeneratorMonitoringComponent,

        // canActivate: [AccessGuard]
      },
      {
        path: 'geo-fence-create',
        component: GeoFenceCreateComponent,
        // canActivate: [AccessGuard]
      },
      {
        path: 'update-profile',
        component: CreateUserComponent,
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'create-route',
        component: CreateRouteComponent,
        // canActivate: [AccessGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {

}
