import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { SharedModule } from '../shared-module';
import { CustomerOnboardingModule } from './customer-onboarding';
import { TenantManagementModule } from './tenant-management';
import { SubscriptionManagementModule } from './subscription-management';
import { AssetOnboardingModule } from './asset-onboarding';

import { DeviceManagementModule } from './device-management';
import { DynamicDashboardModule } from './dynamic-dashboard';
import {DashboardRoutingModule} from "./dashboard/dashboard-routing.module";
import {TranslateModule} from "@ngx-translate/core";


@NgModule({
  imports: [
    CommonModule,
    DashboardModule,
    DashboardRoutingModule,
    // MaterialModule,
    SharedModule,
    CustomerOnboardingModule,
    AdminRoutingModule,
    TenantManagementModule,
    SubscriptionManagementModule,
    AssetOnboardingModule,
    DeviceManagementModule,
    DynamicDashboardModule,
    TranslateModule
  ],
  declarations: [],
  exports: [TranslateModule],
})
export class AdminModule { }
