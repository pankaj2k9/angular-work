import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {AssetOnboardingComponent} from "./asset-onboarding.component";
const routes: Routes = [
  {
    path: '',
    component: AssetOnboardingComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssetOnboardingRoutingModule {
}
