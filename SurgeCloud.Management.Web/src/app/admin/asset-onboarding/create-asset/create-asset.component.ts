import { Component, OnInit, OnDestroy, Input, Output, NgZone, DoCheck, ViewChild } from '@angular/core';
import { Asset, AssetType, Facility, Device, MapDeviceAsset, MapDeviceFacility } from '../asset-onboarding';
import { AssetOnboardingService } from '../asset-onboarding.service';
import { Subject } from 'rxjs/Subject';
import { NotificationsService } from 'angular2-notifications';
import { AppService } from '../../../app.service';
import {Router, ActivatedRoute, Event} from '@angular/router';
import { AdalService } from '../../../shared-module';
import * as _ from 'underscore';
import { isUndefined } from "util";
import { Subscription } from "rxjs/Subscription";
import {TranslateService,  LangChangeEvent} from "@ngx-translate/core";

@Component({
  selector: 'app-create-asset',
  templateUrl: './create-asset.component.html',
  styleUrls: ['./create-asset.component.scss']
})
export class CreateAssetComponent implements OnInit, OnDestroy, DoCheck {

  public asset: Asset = new Asset({ status: true });
  public avilableaAssetType: AssetType[] = [];
  @Output() public createDeviceOrFacility: Subject<{
    asset: Asset, selectedDevice: Device, selectedFacility:
    Facility, isForDevice: Boolean, isEditMode: Boolean
  }> = new Subject();
  private availableFacility: Facility[] = [];
  private availableDevice: Device[] = [];
  public selectedDevice: Device;
  public selectedFacility: Facility;
  private logoFile: any;
  private isEditMode: Boolean = false;
  private availableAsset: Asset[] = [];
  private assetIdIndex: Asset;
  private existingAssest: any;
  public user: any;
  public activeCompanyInfoSubscription: any;
  private routerParam: Subscription;
  private langChange:Subscription;
  public Lang:string;
  loadTypes = [];
  assetDefaultImage = "assets/images/default_asset.svg#def_asset";
  @ViewChild('onboardingForm') assetFormData: HTMLElement;
  @Input() set returningBack(back: boolean) {
    //console.log("returningBack", back);
    if (back) {
      if (!isUndefined(this.assetOnboardingService.assetSingleData)) {
        this.asset = this.assetOnboardingService.assetSingleData;
      }
    }
  }
  allDevicesAndFacilitiesLoaded = { devices: false, facilities: false };
  allDevicesLoaded: Subject<boolean> = new Subject<boolean>();
  allFacilitiesLoaded: Subject<boolean> = new Subject<boolean>();
  allDeviceLoadedSubscription: Subscription;
  allFacilitiesLoadedSubscription: Subscription;
  constructor(private assetOnboardingService: AssetOnboardingService, private appService: AppService,
    private _notificationService: NotificationsService,
    private adalService: AdalService, private _zone: NgZone, private route: ActivatedRoute,
    private router: Router,private translate: TranslateService) {
    this.allDeviceLoadedSubscription = this.allDevicesLoaded.subscribe((data: boolean) => {
      this.allDevicesAndFacilitiesLoaded.devices = data;
      this.callGetAssetAvailable();
    });
    this.allFacilitiesLoadedSubscription = this.allFacilitiesLoaded.subscribe((data: boolean) => {
      this.allDevicesAndFacilitiesLoaded.facilities = data;
      this.callGetAssetAvailable();
    });

  }
  callGetAssetAvailable() {
    //console.log(this.allDevicesAndFacilitiesLoaded);
    if (this.allDevicesAndFacilitiesLoaded.devices == true && this.allDevicesAndFacilitiesLoaded.facilities == true) {
      const dataForEdit = this.assetOnboardingService.onboardingDataForEdit;
      if (dataForEdit && dataForEdit.asset) {
        this.getAssetAvailable(); //will be called after getting all devices & facilities
        //console.log('getAssetAvailable called');
      }
    }
  }
  ngOnInit() {

    this.activeCompanyInfoSubscription = this.appService.activeCompanyInfo.subscribe(
      (company) => {
        this.isEditMode = false;
        this.resetForm();
      }
    );
    this.user = this.adalService.userInfo;
    const data = this.assetOnboardingService.onboardingData;
    //console.log('onboardingData:', data);
    if (data) {
      if (data.asset) {
        this.asset = data.asset;
      }
      if (data.facility) {
        this.selectedFacility = data.facility;
      }
      if (data.device) {
        this.selectedDevice = data.device;
      }
      if (data.isEditMode) {
        this.isEditMode = true;
        this.existingAssest = this.asset;
        //console.log('getAssetAvailable call 1');
        this.getAssetAvailable();
      }
    }

    const dataForEdit = this.assetOnboardingService.onboardingDataForEdit;
    if (dataForEdit && dataForEdit.asset) {
      this.isEditMode = true;
      this.existingAssest = dataForEdit.asset;
      //console.log(this.existingAssest);
      //this.getAssetAvailable(); //will be called after getting all devices & facilities
    }

    this.appService.activeCompanyInfo.subscribe(
      (company) => {
        this.getAvailableFacility();
        //console.log('call1-------');
        this.getAvailableDevice();
      }
    );


    this.getAssetType();

    //console.log(this.appService.getActiveCompany());
    if (this.appService.getActiveCompany()) {
      this.getAvailableFacility();
      //console.log('call2-------');
      this.getAvailableDevice();
    }
    this.appService.getLoadType().subscribe((res) => {



        let avilableaLaodType:any[] = [];
        if (res) {
          res.forEach(loadtype => {
            avilableaLaodType.push(loadtype);
          });
        }
        avilableaLaodType.forEach(data => {
          let name=this.sentenceToKey(data.name);
          this.translate.stream(name).subscribe(res => {
            if(res.charAt(0) !== res.charAt(0).toLowerCase()){
              this.loadTypes.push({
                name: res,
                loadId:data.loadId});
            }
          });

        });
        console.log(this.loadTypes);



      //console.log(res);
      ///res.forEach((item) => {
       // this.loadTypes.push(item);
      //});
      //console.log(this.loadTypes);
    });

  }
  ngDoCheck() {
    if (!this.assetFormData['pristine']) {
      this.assetOnboardingService.assetSingleData = this.asset;
    }
  }
  ngOnDestroy() {
    if (this.assetOnboardingService.onboardingDataForEdit) {
      this.assetOnboardingService.onboardingDataForEdit.asset = null;
    }
    this.resetForm();
    this.allDeviceLoadedSubscription.unsubscribe();
    this.allFacilitiesLoadedSubscription.unsubscribe();
  }

  getAssetType() {

    this.assetOnboardingService.getAssetType().subscribe(
      (data) => {

            let avilableaAssetType:AssetType[] = [];
            if (data) {
              data.forEach(assettype => {
                let model = new AssetType(assettype.name, assettype.status);
                avilableaAssetType.push(model);
              });
            }
            avilableaAssetType.forEach(data => {
              let name=this.sentenceToKey(data.name);
              this.translate.stream(name).subscribe(res => {
                if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
                  this.avilableaAssetType.push({
                    name: res,
                    status: data.status
                  });
                }
              });

            });





      }
    );
  }

  getAvailableFacility() {
    this.assetOnboardingService.getFacility(this.appService.getActiveCompany().companyId).subscribe(
      (data) => {
        data.forEach(facility => {
          let model = new Facility(facility.logo, facility.name, facility.status, facility.address,
            facility.timezone, facility.facilityType, facility.facilityId);
          this.availableFacility.push(model);
        });
        this.allFacilitiesLoaded.next(true);
        if (this.selectedFacility) {
          const indexForFacility = _.indexOf(this.availableFacility, _.findWhere(this.availableFacility,
            { facilityId: this.selectedFacility.facilityId }));
          this.selectedFacility = this.availableFacility[indexForFacility];
        }
      }
    );
  }

  getAvailableDevice() {
    //console.log('getAvailableDevice', this.appService.getActiveCompany());
    //const companyId =
    this.assetOnboardingService.getDevice(this.appService.getActiveCompany().companyId).subscribe(
      (data) => {
        //console.log(data);
        data.forEach(device => {
          let model = new Device(this.appService.getActiveCompany().companyId,
            this.appService.getActiveCompany().name, device.logo, device.gwSNO,
            device.status, device.iotHubName, device.pVer, device.gwOSBuild, device.gwAppBuild,
            device.gwDCAver, device.gwSIMICCID, device.gwIMEI, device.gwRSSI, device.gwWANIP,
            device.gwTimezone, device.latitude, device.longitude, device.altitude, device.gwDescription, device.controllerId);
          this.availableDevice.push(model);
        });
        this.allDevicesLoaded.next(true);
        if (this.selectedDevice) {
          const indexFordevice = _.indexOf(this.availableDevice, _.findWhere(this.availableDevice,
            { controllerId: this.selectedDevice.controllerId }));
          this.selectedDevice = this.availableDevice[indexFordevice];
        }
      }
    );

  }

  getNewDeviceOrFacility(isForDevice = false) {
    //console.log(isForDevice);
    this.createDeviceOrFacility.next({
      asset: this.asset, selectedDevice: this.selectedDevice,
      selectedFacility: this.selectedFacility, isForDevice: isForDevice, isEditMode: this.isEditMode
    });
  }


  changeImage(image: { file, data }) {
    const pattern = new RegExp('image/');
    if (image.file && pattern.test(image.file.type)) {
      this.asset.logo = image.data;
      this.logoFile = image.file;
    }
  }

  createAsset(isValid: boolean) {
    if (isValid) {
      let logoName = 'https://bsdevst01.blob.core.windows.net/images/asset_default.png';
      if (this.logoFile) {
        const splittedName = (this.appService.getActiveCompany().name.replace(/[^\w\s]/gi, '')).split(' ');
        const addOn = (this.asset.assetId.replace(/[^\w\s]/gi, '')).split(' ');
        splittedName.push(addOn.join('_'));
        const randomNumber: any = Math.random().toFixed(3);
        splittedName.push((randomNumber * 1000).toString());

        logoName = `asset_${splittedName.join('_')}.${this.logoFile.name.split('.').pop()}`;
      }

      const data = {
        aId: this.asset.aId,
        assetId: this.asset.assetId,
        companyId: this.appService.getActiveCompany().companyId,
        companyName: this.appService.getActiveCompany().name,
        assetType: this.asset.assetType,
        logo: this.isEditMode ? this.asset.logo : logoName,
        make: this.asset.make,
        model: this.asset.model,
        serialNumber: this.asset.serialNumber,
        unitNumber: this.asset.unitNumber,
        vin: this.asset.vin,
        softwareId: this.asset.softwareId,
        calibrationId: this.asset.calibrationId,
        customerEquipmentGroup: this.asset.customerEquipmentGroup,
        customerEquipmentId: this.asset.customerEquipmentId,
        capacity: this.asset.capacity,
        loadType: this.asset.loadType,
        status: this.asset.status ? 'active' : 'Inactive',
        createdDate: this.asset.createdDate
      };

      if (this.isEditMode) {
        data['createdBy'] = this.asset.createdBy;
        data['updatedBy'] = this.user.profile.name;
      } else {
        data['createdBy'] = this.user.profile.name;
        data['updatedBy'] = this.asset.updatedBy;
      }
      //console.log(this.user);
      //console.log(this.asset);
      //console.log(data);
      this.checkExistingDevice((isValid) => {
        if (isValid) {
          if (this.logoFile) {
            this.uploadLogo(data, logoName);
          } else {
            this.saveAsset(data);
          }
        }
      });
    }
  }
  uploadLogo(asset, logoName) {
    if (this.logoFile) {
      this.assetOnboardingService.uploadLogo(this.logoFile, logoName)
        .subscribe(
          (data) => {
            asset.logo = data._body;
            this.saveAsset(asset)
          },
          (error) => {
            console.log(error);
          },
          () => {
          }
        );
    }
  }

  saveAsset(asset) {
    this.assetOnboardingService.createAsset(asset, this.isEditMode).subscribe(
      (res) => {
        this.appService.showSuccessMessage(
          'Success',
          `Asset is ${!this.isEditMode ? 'created' : 'updated'} and redirecting to Dashboard`
        );
        const savedAsset = new Asset(res);
        const mapDeviceModel = new MapDeviceAsset(
          this.selectedDevice.controllerId,
          this.selectedDevice.gatewaySno,
          savedAsset.aId,
          savedAsset.assetId,
          this.appService.getActiveCompany().companyId,
          this.appService.getActiveCompany().name
        );
        const mapFacilityModel = new MapDeviceFacility(
          this.selectedDevice.controllerId,
          this.selectedDevice.gatewaySno,
          this.selectedFacility.facilityId,
          this.selectedFacility.facilityName,
          this.appService.getActiveCompany().companyId,
          this.appService.getActiveCompany().name
        );
        this.mapDeviceAsset([mapDeviceModel]);
        this.mapFacilityAsset([mapFacilityModel]);
        this.resetForm();
        this.selectedFacility = null;
        this.selectedDevice = null;
        if (this.assetOnboardingService.onboardingDataForEdit && this.assetOnboardingService.onboardingDataForEdit.asset) {
          this.router.navigate(['admin/tenant-management']);
        } else {
          this.router.navigate([this.appService.getLandingPage()]);
        }

      },
      (error) => {
        this.appService.showErrorMessage('Error', (error.json() && error.json().message || error.json().ErrorMessage));
      }
    );
  }


  mapDeviceAsset(data: MapDeviceAsset[]) {
    this.assetOnboardingService.mapDeviceAsset(data).subscribe(
      (res) => {
      });
  }

  mapFacilityAsset(data: MapDeviceFacility[]) {
    this.assetOnboardingService.mapFacilityAsset(data).subscribe(
      (res) => {
      });
  }

  resetForm() {
    this.asset = new Asset({ status: true });
    this.selectedDevice = null;
    this.selectedFacility = null;
    if (this.assetOnboardingService.onboardingData) {
      this.assetOnboardingService.onboardingData.asset = null;
    }
  }

  getAssetAvailable() {
    this.assetOnboardingService.getAsset(this.appService.getActiveCompany().companyId).subscribe(
      (data) => {
        //console.log(data);
        this.availableAsset = [];
        data.forEach((assetData) => {
          // console.log(assetData);
          let model = new Asset(assetData);
          this.availableAsset.push(model);
        });
        if (this.existingAssest) {
          this.assetIdIndex = _.findWhere(this.availableAsset, { assetId: this.existingAssest.assetId });
          if (this.assetIdIndex) {
            this.getSelectedAsset(this.assetIdIndex);
          }
        }
      }
    )
  };

  getSelectedAsset(selectedAsset: Asset) {
    this.asset = selectedAsset;
    //console.log(this.asset);
    this.getDeviceAndFacilityByAsset(selectedAsset.assetId);
  }

  cancel() {
    if (this.appService.changeRoute === 'admin/asset-onboarding') {
      this.router.navigate([this.appService.userProfileDetails.landingPage]);
      this.appService.changeRoute = '';
    } else {
      this.router.navigate([this.appService.changeRoute]);
    }
    // this.router.navigate(['/']);
  }

  getDeviceAndFacilityByAsset(assetId: string) {
    //console.log('getDeviceAndFacilityByAsset', assetId);
    this.assetOnboardingService.getDeviceByAsset(this.appService.getActiveCompany().companyId, assetId).subscribe(
      (res) => {
        //console.log(res);
        if (res) {
          this._zone.run(() => {
            if (res[0] && res[0].facilityId) {
              const indexForFacility = _.indexOf(this.availableFacility, _.findWhere(this.availableFacility,
                { facilityId: res[0].facilityId }));
              this.selectedFacility = this.availableFacility[indexForFacility];
              //console.log('selectedFacility', this.selectedFacility);
            }
            if (res[0] && res[0].controllerId) {
              const indexFordevice = _.indexOf(this.availableDevice, _.findWhere(this.availableDevice,
                { controllerId: res[0].controllerId }));
              this.selectedDevice = this.availableDevice[indexFordevice];
              //console.log('selectedDevice', this.selectedDevice);
            }
          });

        }
      }
    );
  }


  checkExistingDevice(callback) {
    this.assetOnboardingService.checkAsset(this.appService.getActiveCompany().companyId, this.selectedDevice.gatewaySno,
      this.asset.assetId).subscribe(
        (res) => {
          callback(res);
        }, (error) => {
          this.appService.showErrorMessage('Error', (error.json() && error.json().message || error.json().ErrorMessage));
        }
      );
  }
  assetTypeOnChange(assetType: string) {
    //console.log(assetType);
    assetType = assetType.replace(/\s/g, '');
    assetType = assetType.toLowerCase();
    this.assetDefaultImage = 'assets/icons/assettype/' + assetType + '.svg';
    //console.log(this.assetDefaultImage);
  }

  sentenceToKey(str){
    let strr=str.toLowerCase().trim().split(/\s+/).join('_');
    return strr;

  }
}
