import { Component, OnInit, OnDestroy, ElementRef, Output, EventEmitter } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { Device, IotHub, Timezone, Asset } from '../asset-onboarding';
import { AssetOnboardingService } from '../asset-onboarding.service';
import { Subject } from 'rxjs/Subject';
import { AppService } from '../../../app.service';
import { AdalService } from '../../../shared-module';
import * as _ from 'underscore';
import { Router } from '@angular/router';
import {NotificationsService} from "angular2-notifications/dist";


declare const google: any;
@Component({
  selector: 'app-create-device',
  templateUrl: './create-device.component.html',
  styleUrls: ['./create-device.component.scss']
})
export class CreateDeviceComponent implements OnInit, OnDestroy {

  private address: any;
  private device: Device = new Device('', '', '', '', true, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
  private iotHub: IotHub[] = [];
  private timezone: Timezone[] = [];
  private elevator: any;
  @Output() public deviceOrFacilityCreated: Subject<{ data: Device, isForDevice: Boolean, cancel: Boolean }> = new Subject();
  @Output() public back: EventEmitter<string> = new EventEmitter();
  public availbleDevice: Device[] = [];
  public geoCoder: any;
  private addressname: string;
  private logoFile: any;
  private isEditMode: Boolean = false;
  private existingDeviceEdit: any;
  private deviceSelected: any;
  public user: any;
  constructor(private assetOnboardingService: AssetOnboardingService, private elementRef: ElementRef,
    private mapsAPILoader: MapsAPILoader,
    private adalService: AdalService,
    private appService: AppService, private router: Router,
    private notificationService: NotificationsService) {
    this.deviceSelected = '';
  }

  ngOnInit() {
    this.getIotHub();
    this.getTimezone();
    this.user = this.adalService.userInfo;
    this.mapsAPILoader.load().then(() => {
      const autocomplete: any = new google.maps.places.Autocomplete(this.elementRef.nativeElement.querySelector('input[name=addressName]'),
        {
          types: ['address']
        });
      autocomplete.addListener('place_changed', () => {
        const place: any = autocomplete.getPlace();
        if (place.geometry === undefined || place.geometry === null) {
          return;
        }
        this.setAddress(place);
      });
      if (!this.elevator) {
        this.elevator = new google.maps.ElevationService;
      }
      if (!this.geoCoder) {
        this.geoCoder = new google.maps.Geocoder;
      }
    });

    const data = this.assetOnboardingService.onboardingDataForEdit;
    if (data && data.index === 2 && data.device) {
      this.isEditMode = true;
      this.existingDeviceEdit = data.device;
      this.getAvailableDevice();
    }


  }

  ngOnDestroy() {
    if (this.assetOnboardingService.onboardingDataForEdit) {
      this.assetOnboardingService.onboardingDataForEdit.device = null;
    }
  }

  getIotHub() {
    this.assetOnboardingService.getIotHub().subscribe(
      (data) => {
        data.forEach((iothub) => {
          this.iotHub.push(new IotHub(iothub.iotHubUri, iothub.name, iothub.status));
        });
      }
    );
  }

  getTimezone() {
    this.assetOnboardingService.getTimezone().subscribe(
      (data) => {
        data.forEach((timezone: Timezone) => {
          this.timezone.push(new Timezone(timezone.id, timezone.displayName,
            timezone.daylightName, timezone.standardName, timezone.supportsDaylightSavingTime));
        });
      }
    );
  }

  setAddress(place: any) {
    this.address = {
      latitude: place.geometry.location.lat(),
      longitude: place.geometry.location.lng(),
      altitude: 0
    };
    this.getAltitude(place.geometry.location.lat(), place.geometry.location.lng());
  }

  getAltitude(lat, lng) {
    this.elevator.getElevationForLocations({
      'locations': [{ lat: lat, lng: lng }]
    }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.address.altitude = results[0].elevation;
        } else {
          console.log('No result found');
        }
      } else {
        console.log('Elevation service failed due to: ' + status);
      }
    });
  }

  changeImage(image: { file, data }) {
    const pattern = new RegExp('image/');
    if (image.file && pattern.test(image.file.type)) {
      this.device.logo = image.data;
      this.logoFile = image.file;
    }
  }

  saveDevice(isValid: Boolean): void {
    if (isValid) {
      let logoName = '';
      const splittedName = (this.device.gatewaySno.replace(/[^\w\s]/gi, '')).split(' ');
      const randomNumber: any = Math.random().toFixed(3);
      splittedName.push((randomNumber * 1000).toString());

      if (this.logoFile) {
        logoName = `device_${splittedName.join('_')}.${this.logoFile.name.split('.').pop()}`;
      }

      const data = {
        companyId: this.appService.getActiveCompany().companyId,
        companyName: this.appService.getActiveCompany().name,
        controllerId: this.device.controllerId, // Added pkey
        IotHubName: this.device.iotHub,
        pVer: this.device.productVersion,
        gwSNO: this.device.gatewaySno,
        gwDescription: this.device.description,
        gwOSBuild: this.device.osBuild,
        gwAppBuild: this.device.appBuild,
        gwDCAver: this.device.dcaVersion,
        gwSIMICCID: this.device.simiccid,
        gwIMEI: this.device.imei,
        gwRSSI: this.device.rssi,
        gwTimezone: this.device.timezone,
        gwWANIP: this.device.wanip,
        Latitude: this.address && this.address.latitude ? this.address.latitude : null,
        Longitude: this.address && this.address.longitude ? this.address.longitude : null,
        Altitude: this.address && this.address.altitude ? this.address.altitude : null,
        Logo: this.isEditMode ? this.device.logo : 'https://bsdevst01.blob.core.windows.net/images/device_default.png',
        status: this.device.status ? 1 : 0
      };
      if (this.isEditMode) {
        data['updatedBy'] = this.user.profile.name;
      } else {
        data['createdBy'] = this.user.profile.name;
      }
      if (this.logoFile) {
        this.uploadLogo(data, logoName)
      } else {
        //console.log('Device Update');
        this.createDeviceAPI(data);
      }
    }
  }

  uploadLogo(data, logoName) {
    if (this.logoFile) {
      this.assetOnboardingService.uploadLogo(this.logoFile, logoName)
        .subscribe(
        (res) => {
          data.Logo = res._body;
          this.createDeviceAPI(data);
        },
        (error) => {
          console.log(error);
        },
        () => {
        }
        );
    }
  }

  createDeviceAPI(data) {
    this.assetOnboardingService.createDevice(data, this.isEditMode).subscribe(
      (response) => {
        const model = new Device(
          this.appService.getActiveCompany().companyId,
          this.appService.getActiveCompany().name,
          response.logo,
          response.gwSNO,
          response.status,
          response.iotHubName,
          response.pVer,
          response.gwOSBuild,
          response.gwAppBuild,
          response.gwDCAver,
          response.gwSIMICCID,
          response.gwIMEI,
          response.gwRSSI,
          response.gwWANIP,
          response.gwTimezone,
          response.latitude,
          response.longitude,
          response.altitude,
          response.gwDescription,
          response.controllerId);
        if (this.assetOnboardingService.onboardingDataForEdit && this.assetOnboardingService.onboardingDataForEdit.index === 2) {
          this.notificationService.success('Success', 'Device update successful.');
          this.router.navigate(['admin/device-management']);
        } else {
          this.deviceOrFacilityCreated.next({ data: model, isForDevice: true, cancel: false });
        }
      },
      (error) => {
        this.appService.showErrorMessage('Error', (error.json() && error.json().message || error.json().ErrorMessage));
      }
    );
  }

  getAvailableDevice() {
    this.assetOnboardingService.getDevice(this.appService.getActiveCompany().companyId)
      .subscribe(
      (response) => {
        this.availbleDevice = [];
        response.forEach(
          (deviceAvailable) => {
            const model = new Device(
              deviceAvailable.companyId,
              deviceAvailable.companyName,
              deviceAvailable.logo,
              deviceAvailable.gwSNO,
              deviceAvailable.status,
              deviceAvailable.iotHubName,
              deviceAvailable.pVer,
              deviceAvailable.gwOSBuild,
              deviceAvailable.gwAppBuild,
              deviceAvailable.gwDCAver,
              deviceAvailable.gwSIMICCID,
              deviceAvailable.gwIMEI,
              deviceAvailable.gwRSSI,
              deviceAvailable.gwWANIP,
              deviceAvailable.gwTimezone,
              deviceAvailable.latitude,
              deviceAvailable.longitude,
              deviceAvailable.altitude,
              deviceAvailable.gwDescription,
              deviceAvailable.controllerId);
            this.availbleDevice.push(model);
          }
        );
        if (this.existingDeviceEdit) {
          this.deviceSelected = _.findWhere(this.availbleDevice, { gatewaySno: this.existingDeviceEdit.gwSNO });
          this.setSelectedDeive(this.deviceSelected);
        }
      }
      );
  }

  setSelectedDeive(selected: Device) {
    this.device = selected;
    this.address = {
      latitude: selected.latitude,
      longitude: selected.longitude,
      altitude: selected.altitude
    };
    this.getAddress(parseFloat(this.device.latitude), parseFloat(this.device.longitude));
  }

  getAddress(lat: any, lng: any) {
    const latlng = { lat: parseFloat(lat), lng: parseFloat(lng) };
    if (this.geoCoder) {
      this.geoCoder.geocode({ 'location': latlng }, (results, status) => {
        if (status === 'OK') {
          if (results[1]) {
            this.addressname = results[1].formatted_address;
          }
        }
      });
    }
  }

  resetForm() {
    this.device = new Device('', '', '', '', true, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
    this.addressname = '';
    this.deviceSelected = '';
  }

  cancel() {
    if (this.assetOnboardingService.onboardingDataForEdit && this.assetOnboardingService.onboardingDataForEdit.device) {
      this.router.navigate([this.appService.changeRoute]);
    } else if (this.appService.ManagementAddEdit.value) {
      this.router.navigate([this.appService.changeRoute]);
    } else {
      this.deviceOrFacilityCreated.next({ data: null, isForDevice: false, cancel: true });
    }
  }

}
