import {SubscriptionTopicsModel} from "../../../shared-module/components/subscription-topics/subscription-topics-model";

export class CompanyAndRoleDataModel {
  public companyId: string;
  public companyName: string;
  public roleId: string;
  public groupId: string;
  public topics: SubscriptionTopicsModel[];
  constructor(companyId: string, companyName: string, roleId: string, groupId: string, topics: SubscriptionTopicsModel[]) {
    this.companyId = companyId;
    this.companyName = companyName;
    this.roleId = roleId;
    this.groupId = groupId;
    this.topics = topics;
  }
}
