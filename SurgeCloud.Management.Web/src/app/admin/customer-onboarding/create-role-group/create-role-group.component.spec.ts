import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateRoleGroupComponent } from './create-role-group.component';

describe('CreateRoleGroupComponent', () => {
  let component: CreateRoleGroupComponent;
  let fixture: ComponentFixture<CreateRoleGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateRoleGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateRoleGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
