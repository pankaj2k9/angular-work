import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef, HostListener, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerOnboardingService } from '../customer-onboarding.service';
import { Organization, RoleGroup } from '../../../app-common';
import { AppStartService } from '../../../app-start.service';
import { CustomerOnboardingStatus } from '../customer-onboarding';
import * as _ from 'underscore';
import { AppService } from '../../..//app.service';
import { CreateRoleGroupService } from "./create-role-group.service";
import { CompanyAndRoleDataModel } from "./company-and-role-data-model";
import { SubscriptionTopicsModel } from "../../../shared-module/components/subscription-topics/subscription-topics-model";
import { AdalService } from "../../../shared-module";
import * as jQuery from 'jquery';

@Component({
  selector: 'app-create-role-group',
  templateUrl: './create-role-group.component.html',
  styleUrls: ['./create-role-group.component.scss']
})
export class CreateRoleGroupComponent implements OnInit, OnDestroy {
  private _organization: any;
  private _features: any[];
  private _roleGroup: RoleGroup;
  public roleGroup = {
    displalyName: '',
    users: [],
    description: '',
    logo: '',
    status: true
  };
  public isEditMode = false;

  public selected = false;
  public savedRoleData: any;
  public featureSelected = 1;
  public availableFeatures: any[] = [];
  public subscriptionTopics = [];

  public roleEdit: Boolean = false;
  public isValidDescription: Boolean = true;

  public data: CustomerOnboardingStatus;
  @Output() public status: EventEmitter<CustomerOnboardingStatus> = new EventEmitter();
  @Output() public back: EventEmitter<string> = new EventEmitter();
  public notifiyStatus: CustomerOnboardingStatus;
  public onboardingContentMinHeight: any;
  public tableBodyMaxHeight: any;
  activeRoleFeatureTabId = 'Operational';
  tabBoxHeight: object;
  selectedTabId: string;

  companyRoleData: object;
  groupId: string;
  @HostListener('window:resize') onWindowResize() {
    if (window.innerWidth < 600) {
      this.tabBoxHeight = { height: 'auto' };
    }
    else {
      this.setTabBoxHeight(this.selectedTabId);
    }
  }
  constructor(private router: Router,
    private onboardingService: CustomerOnboardingService, private appStartService: AppStartService, public appService: AppService,
    private changeDetectorRef: ChangeDetectorRef, private createRoleGroupService: CreateRoleGroupService, private adalService: AdalService) {
  }

  ngOnInit() {
    this.availableFeatures = this.appStartService.features;
    this.availableFeatures.map((feature) => {
      feature.enabled = false;
    });
    this._features = _.sortBy(_.where(this.availableFeatures, { controlLevel: 2 }), 'controlLevel');
    this.data = this.onboardingService.onboardingData;
    this._organization = this.data.organization || this.appService.getActiveCompany();
    console.log('Organization data', this._organization);
    this.changeFeature();
    this._roleGroup = this.onboardingService.getSavedRoleGroup();
    if (this.onboardingService.onboardEdit.roleGroup) {
      this.roleEdit = true;
      this.getRoleGroup();
      // this.roleGroup.displalyName = this.onboardingService.onboardEdit.roleGroup.Name;
    }
    const windowHeight = window.innerHeight;
    const headerHeight = document.getElementById('appHeader').offsetHeight;
    const footerHeight = document.getElementById('appFooter').offsetHeight;
    const justifyHeight = jQuery('.nav.nav-tabs.justify-content-start').outerHeight();
    this.onboardingContentMinHeight = windowHeight - headerHeight - footerHeight - justifyHeight;
    this.tableBodyMaxHeight = this.onboardingContentMinHeight - 215;
  }

  ngOnDestroy() {
    if (this.onboardingService.onboardEdit) {
      this.onboardingService.onboardEdit.roleGroup = null;
    }
    this.companyRoleData = undefined;
  }

  checkRoleGroupDescription() {
    if (this.isOnlySpecialCharacters(this.roleGroup.description) || this.hasRepeatedLetters(this.roleGroup.description)) {
      this.isValidDescription = false;
    } else {
      this.isValidDescription = true;
    }
  }

  isOnlySpecialCharacters(str) {
    return /^[^a-zA-Z0-9]+$/.test(str);
  }

  hasRepeatedLetters(str) {
    let result = false;
    const regex = /^[a-zA-Z0-9]+$/i;
    for (let i = 2; i < str.length; i++) {
      if (str[i] === str[i - 1]) {
        if (str[i - 1] === str[i - 2] && !(regex.test(str[i]))) {
          result = true;
          return result;
        }
      }
    }
    return result;
  }

  saveRoleGroup(isValid: Boolean) {
    let roleGroup = {
      DisplayName: this.roleGroup.displalyName,
      Description: this.roleGroup.description,
      Status: this.roleGroup.status ? 1 : 0,
      CompanyId: this._organization.companyId,
      CompanyName: this._organization.name,
      Id: this.onboardingService.onboardEdit.roleGroup ? this.onboardingService.onboardEdit.roleGroup.roleId : null
    };
    this.onboardingService.saveRoleGroup(roleGroup, this.roleEdit).subscribe(
      (data) => {
        this.savedRoleData = new RoleGroup(data.description, data.displayName, data.id, data.status);
        this.companyRoleData = data;
        this.groupId = this._organization.groupId;
        this.isEditMode = true;
        this.changeFeature();
      },
      (error) => {
        this.notifiyStatus = new CustomerOnboardingStatus();
        this.notifiyStatus.index = 2;
        this.notifiyStatus.success = false;
        this.notifiyStatus.message = error.json().message;
        this.notifyErrorMessage('Error', this.notifiyStatus.message);
        this.status.next(this.notifiyStatus);
      }
    )

  }
  goToNextTab() {
    //if(this.featureSelected < 3) {
    this.featureSelected = this.featureSelected + 1;
    //}
    if (this.featureSelected == 2) {
      this.changeFeature(2, 'operational-content')
    }
    if (this.featureSelected == 3) {
      this.changeFeature(3, 'subscription-content');
      this.setTabBoxHeight(this.selectedTabId);
    }
    if (this.featureSelected > 3) {

    }
    this.saveFeature();
  }

  saveFeature() {
    if (this.featureSelected < 4) {
      let availableFeature = _.where(this.availableFeatures, { enabled: true });
      let enabledFeature: any = [];

      availableFeature.forEach(feature => {
        let model = {
          roleId: this.savedRoleData.id,
          featureId: feature.featureId,
          canAccess: 1
        }
        enabledFeature.push(model);
      });
      if (this.featureSelected == 3) {
        this.onboardingService.MapRolesFeatures(enabledFeature).subscribe(
          (data) => {
            // if (this.savedRoleData && this.savedRoleData.id) {
            //   if (this.selected) {
            this.notifiyStatus = new CustomerOnboardingStatus();
            this.notifiyStatus.index = 2;
            this.notifiyStatus.success = true;
            this.notifiyStatus.roleGroup = this.savedRoleData;
            if (!this.data.organization) {
              //this.router.navigate(['admin/subscription-management']);
            } else {
              //this.status.next(this.notifiyStatus);
            }
            // }
            // } else {
            //   this.isEditMode = false;
            //   this.isFeatureSaved = true;
            // }
          },
          (error) => {
            this.notifiyStatus = new CustomerOnboardingStatus();
            this.notifiyStatus.index = 2;
            this.notifiyStatus.success = false;
            this.notifiyStatus.message = error.json();
            this.notifyErrorMessage('Error', error.json().ErrorMessage);
            this.status.next(this.notifiyStatus);
          }
        );
      }
    }
    else {
      if (this.roleEdit) {
        this.appService.showSuccessMessage('Success', 'Role group updated successfully');
        this.router.navigate(['admin/subscription-management']);
      } else {
        this.appService.showSuccessMessage('Success', 'Role group saved successfully');
        this.notifiyStatus.roleGroupCreated = true;
        this.status.next(this.notifiyStatus);
      }
    }

  }

  cancel() {
    if (this.onboardingService.onboardEdit.roleGroup) {
      this.router.navigate([this.appService.changeRoute]);
    } else if (this.appService.ManagementAddEdit.value) {
      this.router.navigate([this.appService.changeRoute]);
    } else {
      this.back.next('user');
    }
  }

  checkForm() {
    this.selected = (_.where(this.availableFeatures, { enabled: true })).length > 0 ? true : false;
  }
  changeFeature(featureSelected: number = 1, sectionId: string = 'admin-content') {
    this.featureSelected = featureSelected;
    this.selectedTabId = sectionId;

    if (this._features) {
      this._features.forEach((feature) => {
        const selectedFeature = _.findWhere(this.availableFeatures, { featureId: feature.featureId });
        selectedFeature.enabled = feature.enabled;
      });
    }
    if (this.featureSelected === 1) {
      this._features = _.sortBy(_.where(this.availableFeatures, { controlLevel: 1 }), 'controlLevel');
      this.setTabBoxHeight(sectionId);
    } else if (this.featureSelected === 2) {
      let availableFeaturesTemp = [];
      this.availableFeatures.forEach((item) => {
        if (item.controlLevel === 2 || item.controlLevel === 3) {
          availableFeaturesTemp.push(item);
        }
      });
      this._features = _.sortBy(availableFeaturesTemp, 'controlLevel');
      this.setTabBoxHeight(sectionId);
    } else {
      //this.loadTopicOptionsData();
    }
    //this.changeDetectorRef.detectChanges();
  }

  setTabBoxHeight(sectionId: string) {
    setTimeout(() => {
      if (document.getElementById(sectionId) != null) {
        const heightVal = document.getElementById(sectionId).offsetHeight + 13; //static 13 is used for creating a empty space below of the tab ul and also for increasing the height of the section
        if (window.innerWidth < 600) {
          this.tabBoxHeight = { height: 'auto' };
        } else {
          this.tabBoxHeight = { height: heightVal + 'px' };
        }
      }
    });
  }

  notifyErrorMessage(title, message = '') {
    this.appService.showErrorMessage(title, message);
  }

  getRoleGroup() {
    this.onboardingService.getRoleGroup(this.appService.getActiveCompany().companyId).subscribe(
      (res) => {
        let role = _.findWhere(res, { id: this.onboardingService.onboardEdit.roleGroup.roleId });
        this.roleGroup.displalyName = role.displayName;
        this.roleGroup.description = role.description;
        this.roleGroup.status = role.status ? true : false;
        this.getRoleFeature(role.id);
      }
    );
  }

  getRoleFeature(roleId: string) {
    this.onboardingService.GetRoleFeature(roleId).subscribe(
      (res) => {
        let availFeature = _.pluck(this.availableFeatures, 'featureId');
        if (res) {
          res.forEach(feature => {
            if (availFeature.indexOf(feature.featureId) > -1) {
              this.availableFeatures[availFeature.indexOf(feature.featureId)].enabled = true;
            }
          });
          this.selected = true;
        }
      });
  }
  addFeatureToRoleTabLinkOnClick(tabId: string) {
    this.activeRoleFeatureTabId = tabId;
  }
}
