import { Injectable } from '@angular/core';
import { HttpService } from "../../../shared-module/services/http/http.service";
import { AppService } from "../../../app.service";
import { Observable } from "rxjs/Observable";
import { LocalStorageService } from "angular-2-local-storage";

@Injectable()
export class CreateRoleGroupService {
  constructor(private httpService: HttpService,
    private localStorageService: LocalStorageService) {
  }
  getSubscriptionTopics(companyId: string) {
    return this.httpService.get(`device/api/iot/gettopicbasic/${companyId}`).map((res) => {
      return res.json();
    });
  }
  getTopicEmailSubscription(companyId: string, roleId: string, updateProfile = false) {
    let emailTopicApi = `users/api/users/gettopicemailsubscription/${companyId}?roleId=${roleId}`;
    if (updateProfile) {
      const userDetails = JSON.parse(this.localStorageService.get('userDetails'));
      emailTopicApi = emailTopicApi + '&userId=' + userDetails.userId;
    }
    return this.httpService.get(emailTopicApi).map((res) => {
      return res.json();
    }).catch((error: any) => {
      return Observable.throw(error);
    });
  }
  createTopicEmailSubscription(data) {
    return this.httpService.post(`users/api/users/CreateTopicEmailSubscription`, data).map((res) => {
      return res.json();
    }).catch((error: any) => {
      return Observable.throw(error);
    });
  }
  updateTopicEmailSubscription(data) {
    //console.log("updateTopicEmailSubscription", data);
    return this.httpService.post(`users/api/users/UpdateTopicEmailSubscription`, data).map((res) => {
      return res.json();
    }).catch((error: any) => {
      return Observable.throw(error);
    });
  }
  getTopicSmsSubscription(companyId: string, roleId: string, updateProfile = false) {
    let smsTopicApi = `users/api/users/gettopicsmssubscription/${companyId}?roleId=${roleId}`;
    if (updateProfile) {
      const userDetails = JSON.parse(this.localStorageService.get('userDetails'));
      smsTopicApi = smsTopicApi + '&userId=' + userDetails.userId;
    }
    return this.httpService.get(smsTopicApi).map((res) => {
      return res.json();
    }).catch((error: any) => {
      return Observable.throw(error);
    });
  }
  createTopicSmsSubscription(data) {
    return this.httpService.post(`users/api/users/CreateTopicSmsSubscription`, data).map((res) => {
      return res.json();
    }).catch((error: any) => {
      return Observable.throw(error);
    });
  }
  updateTopicSmsSubscription(data) {
    return this.httpService.post(`users/api/users/UpdateTopicSmsSubscription`, data).map((res) => {
      return res.json();
    }).catch((error: any) => {
      return Observable.throw(error);
    });
  }
  getTopicPushSubscription(companyId: string, roleId: string, updateProfile = false) {
    let pushTopicApi = `users/api/users/gettopicpushsubscription/${companyId}?roleId=${roleId}`;
    if (updateProfile) {
      const userDetails = JSON.parse(this.localStorageService.get('userDetails'));
      pushTopicApi = pushTopicApi + '&userId=' + userDetails.userId;
    }
    return this.httpService.get(pushTopicApi).map((res) => {
      return res.json();
    }).catch((error: any) => {
      return Observable.throw(error);
    });
  }
  createTopicPushSubscription(data) {
    return this.httpService.post(`users/api/users/CreateTopicPushSubscription`, data).map((res) => {
      return res.json();
    }).catch((error: any) => {
      return Observable.throw(error);
    });
  }
  updateTopicPushSubscription(data) {
    return this.httpService.post(`users/api/users/UpdateTopicPushSubscription`, data).map((res) => {
      return res.json();
    }).catch((error: any) => {
      return Observable.throw(error);
    });
  }
  getSuggestedWordsForTemplating() {
    return this.httpService.get(`device/api/iot/getconfigdatapoints`).map((res) => {
      return res.json();
    }).catch((error: any) => {
      return Observable.throw(error);
    });
  }
}
