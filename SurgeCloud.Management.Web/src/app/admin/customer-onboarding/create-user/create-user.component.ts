import { Component, OnInit, OnDestroy, Input, EventEmitter, Output, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Users } from '../../../app-common';
import { CustomerOnboardingService } from '../customer-onboarding.service'
import { Subject } from 'rxjs/Subject';
import { Organization, RoleGroup } from '../../../app-common';
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import { CustomerOnboardingStatus } from '../customer-onboarding';
import * as _ from 'underscore';
import { AppService } from '../../../app.service';
import { AdalService } from "../../../shared-module";
import { DomSanitizer } from "@angular/platform-browser";
import { AppStartService } from "../../../app-start.service";
import { CreateRoleGroupService } from "../create-role-group/create-role-group.service";


@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit, OnDestroy {
  public user: Users = new Users();
  singleUserData = { name: '' };
  public roleGroup: any;
  public getFeaturesSubscriber;
  public changeStatus: Subject<Boolean> = new Subject();
  public saveUserData: Subject<Users> = new Subject();
  private _organization: any;
  private _roleGroup: RoleGroup;
  private logoFile: File;
  private formatedExpiredDate: any;
  private isEditMode: Boolean = false;
  private availableUsers: Users[] = [];
  private modelDate: Object;
  public userIndex: any;
  public subscriptionTopics = [];
  public appAvailableFeatures = [];
  public data: CustomerOnboardingStatus;
  @Output() public status: EventEmitter<CustomerOnboardingStatus> = new EventEmitter();
  @Output() public back: EventEmitter<string> = new EventEmitter();
  @Output() public createRole: EventEmitter<any> = new EventEmitter();
  public notifiyStatus: CustomerOnboardingStatus;

  todayDate: Date = new Date();
  private myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'mm/dd/yyyy',
    disableUntil: {
      year: this.todayDate.getFullYear(),
      month: this.todayDate.getMonth() + 1,
      day: this.todayDate.getDate()
    }
  };
  userUpdateProfile = false;// this flag will be true, if user click on update profile
  languages: Array<string>;
  units: Array<string>;
  private timezone = [];
  private companies = [];
  allLandingPages = [];
  private landingPages = [];
  companyRoleData: object;
  groupId: string;
  private activeCompanyInfoSubscription: any;
  constructor(private router: Router,
    private onboardingservice: CustomerOnboardingService,
    public appService: AppService, private activatedRoute: ActivatedRoute, private changeDetectorRef: ChangeDetectorRef, private sanitizer: DomSanitizer, private appStartService: AppStartService, private createRoleGroupService: CreateRoleGroupService, private adalService: AdalService) {
    this.userIndex = '';
    if (this.onboardingservice.userEditMode) {
      this.userUpdateProfile = true;
      this.appService.getPushNotification.next(true);
      this.onboardingservice.userEditMode = false;
      this.user = this.appService.userProfileDetails;
      if (this.user['timezone'] == null) {
        this.user['timezone'] = '';
      }
      if (!this.user['defaultLanguage']) {
        this.user['defaultLanguage'] = '';
      }
      if (!this.user['measurementUnit']) {
        this.user['measurementUnit'] = '';
      }
      if (!this.user['defaultCompanyId']) {
        this.user['defaultCompanyId'] = '';
      }
      if (this.user.expirationDate) {
        this.user.licenseDetailsAvailable = true;
        const expDate = new Date(this.user.expirationDate);
        this.formatedExpiredDate = this.user.expirationDate;
        this.user.modelDate = { date: { year: expDate.getFullYear(), month: expDate.getMonth() + 1, day: expDate.getDate() } };
      }

      this.appService.getUserRoleData().subscribe((res) => {
        this.user['roleGroup'] = res.roleId;
        this.user['roleName'] = res.roleName;
        const data = {
          companyId: this.user.companyId,
          companyName: this.user.companyName,
          id: this.user.roleGroup
        };
        this.companyRoleData = data;
        this.groupId = this.user.groupId;
        //this.companyAndRoleDataForSubscription = new CompanyAndRoleDataModel(this.user.companyId, this.user.companyName, this.user.roleGroup, this.user.groupId, this.subscriptionTopics);
        //this.loadTopicOptionsData();
      });
      //});
    }
    else {
      const data = {
        companyId: this.user.companyId,
        companyName: this.user.companyName,
        id: this.user.roleGroup
      };
      this.companyRoleData = data;
      this.groupId = this.user.groupId;
    }
    this.languages = this.appService.getLanguages();
    this.units = this.appService.getUnits();
    this.appService.getTimezone().subscribe(
      (data) => {
        this.timezone = data;
        this.changeDetectorRef.detectChanges();
      }
    );

    this.companies = this.appService.companies;
    // this.appService.getLandingPages().subscribe((res) => {
    //   this.allLandingPages = res;
    //   this.landingPages = res;
    // });
  }

  ngOnInit() {
    this.data = this.onboardingservice.onboardingData;
    const existingUserId = this.onboardingservice.onboardEdit.userId;
    this._organization = this.data.organization;
    this._organization = this.appService.getActiveCompany();

    if (this.onboardingservice.onboardingData && this.onboardingservice.onboardingData.organization) {
      if (!this.onboardingservice.isCreatingNewOrg) {
        this.newDefaultOrganization(this.onboardingservice.onboardingData.organization['name']);
      }
      this._organization = this.onboardingservice.onboardingData.organization;
    } else {
      this._organization = this.onboardingservice.selectedOrganization ? JSON.parse(JSON.stringify(this.onboardingservice.selectedOrganization)) : this._organization;
    }
    this.getGetMembers(this._organization);
    if (this.data && this.data.roleGroup) {
      this.user.roleGroup = this.data.roleGroup.id;
    }

    if (this.data.isEditMode) {
      this.isEditMode = true;
      this.getGetMembers(this.onboardingservice.selectedOrganization ? JSON.parse(JSON.stringify(this.onboardingservice.selectedOrganization)) : this._organization);
    }

    this._roleGroup = this.data.roleGroup;
    if (this.onboardingservice.selectedOrganization) {
      this.onboardingservice.getRoleGroup(this.onboardingservice.selectedOrganization.companyId).subscribe(
        (data) => {
          this.roleGroup = data;
          if (this.data && this.data.roleGroup) {
            this.user.roleGroup = this.data.roleGroup.id;
          }
        }
      );
    } else if (this._organization) {
      this.onboardingservice.getRoleGroup(this._organization.companyId).subscribe(
        (data) => {
          this.roleGroup = data;
          if (this.data && this.data.roleGroup) {
            this.user.roleGroup = this.data.roleGroup.id;
          }
        }
      );
    } else {
      if (!this.userUpdateProfile) {
        this.router.navigate(['/admin/subscription-management']);
      }
    }

    this.getFeaturesSubscriber = this.appStartService.getFeatures().subscribe((res) => {
      this.appAvailableFeatures = res;
      this.checkAvailableAccessForPointName(res);
    });

    this.activeCompanyInfoSubscription = this.appService.activeCompanyInfo.subscribe(
      (company) => {
        this._organization = company;
        this.getGetMembers(this._organization);
      }
    );
  }

  ngOnDestroy() {
    if (this.getFeaturesSubscriber)
      this.getFeaturesSubscriber.unsubscribe();
    if (this.onboardingservice.onboardEdit) {
      this.onboardingservice.onboardEdit.userId = null;
    }
    if (this.activeCompanyInfoSubscription)
      this.activeCompanyInfoSubscription.unsubscribe();
  }

  newDefaultOrganization(id) {
    this.onboardingservice.getDefaultOrganization(id).subscribe((response) => {
      this.companies = response;
    }, (err) => {
      console.log(err);
    });
  }

  checkAvailableAccessForPointName(features) {
    this.landingPages = [];
    console.log(this.user.roleGroup);
    if (!this.user.roleGroup) {
      features.forEach(feature => {
        this.appService.availableFeatures.forEach(availablefearute => {
          if (availablefearute.featureId === feature.featureId && availablefearute.canAccess === 1 && feature.controlLevel === 2) {
            if (feature.name.toLowerCase() === 'Dashboard') {
              this.landingPages.push(feature);
            }
          }
        });
      });
    }
  }

  onDateChanged(event: IMyDateModel) {
    if (event.jsdate) {
      this.formatedExpiredDate = event.jsdate.toISOString();
    }
  }

  onBlurInputEmail(value) {
    if (!value.includes('@surgecloud.bluesurge.com')) {
      this.user.username = value + '@surgecloud.bluesurge.com';
    }
  }

  createRoleGroup() {
    this.appService.ManagementAddEdit.value = false;
    this.createRole.next({ user: this.user, isEditMode: this.isEditMode });
  }

  changeImage(image: { file, data }) {
    const pattern = new RegExp('image/');
    if (image.file && pattern.test(image.file.type)) {
      this.user.logo = image.data;
      this.logoFile = image.file;
    }
  }
  defaultCompanyChooseChange(companyId) {
    let defaultCompanyName = '';
    this.companies.forEach((value, key) => {
      if (value['companyId'] == companyId) {
        defaultCompanyName = value['name'];
        return false;
      }
    });
    this.user['defaultCompanyName'] = defaultCompanyName;
  }

  saveUser(form) {
    if (form.valid) {
      let logoName = 'https://bsdevst01.blob.core.windows.net/images/user_default.png';
      let splittedName = [];
      if (!this.userUpdateProfile) {
        splittedName = (this._organization.name.replace(/[^\w\s]/gi, '')).split(' ');
      } else {
        splittedName = (this.user.companyName.replace(/[^\w\s]/gi, '')).split(' ');
      }
      const addOn = (this.user.displayName.replace(/[^\w\s]/gi, '')).split(' ');
      splittedName.push(addOn.join('_'));
      const randomNumber: any = Math.random().toFixed(3);
      splittedName.push((randomNumber * 1000).toString());

      if (this.logoFile) {
        logoName = `user_${splittedName.join('_')}.${this.logoFile.name.split('.').pop()}`;
      }
      if ((this.onboardingservice.userEditOwnProfile && !this.logoFile) || (this.onboardingservice.adminEditUserProfile && !this.logoFile)) {
        logoName = this.user.logo;
      }

      const user: any = {
        UserId: !this.userUpdateProfile ? this.user.Id : this.user['userId'],
        EmailId: this.user.emailId,
        Username: this.user.username,
        DisplayName: this.user.displayName,
        GivenName: this.user.givenName,
        Surname: this.user.surname,
        JobTitle: this.user.jobTitle,
        Logo: logoName,
        GroupId: !this.userUpdateProfile ? this._organization.groupId : this.user.groupId,
        CompanyName: !this.userUpdateProfile ? this._organization.name : this.user.companyName,
        CompanyId: !this.userUpdateProfile ? this._organization.companyId : this.user.companyId,
        PhoneNumber: this.user.phoneNumber,
        RoleGroup: this.user.roleGroup,
        IsActive: this.user.status ? 1 : 0,
        defaultLanguage: this.user['defaultLanguage'],
        measurementUnit: this.user['measurementUnit'],
        timezone: this.user['timezone'],
        defaultCompanyId: this.user['defaultCompanyId'],
        landingPage: this.user['landingPage'],
        defaultCompanyName: this.user['defaultCompanyName'],
        emailViewed: this.appService.userProfileDetails['emailViewed'],
        pushViewed: this.appService.userProfileDetails['pushViewed']
      };
      if (this.onboardingservice.userEditOwnProfile) { //user edit his own profile
        user.IsActive = this.user['isActive'];
      } else if (this.onboardingservice.adminEditUserProfile) { //admin edit user profile
        if (this.user.password) {
          user.Password = this.user.password;
        }
      } else {
        user.Password = this.user.password;
      }

      if (this.user.licenseDetailsAvailable) {
        user.DrivingLicenseNumber = this.user.drivingLicenseNumber;
        user.ExpirationDate = this.formatedExpiredDate;
        user.DriverId = this.user.driverId;
      } else {
        user.ExpirationDate = '';
      }

      if (this.logoFile) {
        this.uploadLogo(user, logoName);
      } else {
        this.createUser(user);
      }
    }
  }

  uploadLogo(user, logoName) {
    if (this.logoFile) {
      this.onboardingservice.uploadLogo(this.logoFile, logoName)
        .subscribe(
          (data) => {
            user.logo = data._body;
            this.createUser(user)
          },
          (error) => {
            this.notifiyStatus = new CustomerOnboardingStatus();
            this.notifiyStatus.index = 0;
            this.notifiyStatus.success = false;
            this.notifiyStatus.message = error.json();
            this.notifyErrorMessage('Error', error.json());
            this.status.next(this.notifiyStatus);
          },
          () => {
          }
        );
    }
  }

  createUser(userData) {
    let editMode = false;
    if (this.isEditMode) {
      editMode = true;
    }
    if (this.userUpdateProfile) {
      editMode = true;
    }
    this.onboardingservice.saveUser(userData, editMode).subscribe(
      (data) => {
        //this.onboardingservice.userEditOwnProfile = false;
        //this.onboardingservice.adminEditUserProfile = false;
        if (data.landingPage === 'admin/dashboard')
          data.landingPage = 'admin/feature';
        this.appService.userProfileDetails = data;
        let redirectName = this.appService.userProfileDetails.landingPage ? this.appService.userProfileDetails.landingPage.split('/')[this.appService.userProfileDetails.landingPage.split('/').length - 1] : 'Dashboard';
        this.notifySuccessMessage(
          'Success',
          `User is ${editMode ? 'updated' : 'created'}  and redirecting to ${redirectName}`,
        );
        let addUserToRoleGroup = new Array();

        if (this.onboardingservice.userEditOwnProfile) {
          addUserToRoleGroup.push('no');
        }
        if (this.onboardingservice.adminEditUserProfile) {
          addUserToRoleGroup.push('no');
        }
        if (this._organization.groupId === this.user.roleGroup) {
          addUserToRoleGroup.push('no');
        }
        if (addUserToRoleGroup.indexOf('no') == -1 && editMode === false) {
          this.onboardingservice.addUserToGroup(data.userId, this._organization.groupId).subscribe(
            (data) => {
              this.resetAddEditUserFlags();
            },
            (error) => {
              this.showError(error);
            }
          );
        }

        this.onboardingservice.updateRoleUserMap(data.userId, this.user.roleGroup).subscribe(
          (data) => {
            if (!this.onboardingservice.userEditOwnProfile && !this.onboardingservice.adminEditUserProfile) {
              this.onboardingservice.mapUserWithSubscription(data.roleId, data.userId).subscribe((response) => {
              });
            }
            if (this.onboardingservice.onboardNew.for === 2 || this.onboardingservice.onboardEdit.userId) {
              this.router.navigate(['admin/subscription-management']);
              this.resetAddEditUserFlags();
            } else {
              this.router.navigate([this.appService.userProfileDetails.landingPage]);
              this.resetAddEditUserFlags();
            }
          },
          (error) => {
            this.showError(error);
          }
        );

      },
      (error) => {
        this.showError(error);
      }
    );
  }
  resetAddEditUserFlags() {
    this.onboardingservice.adminEditUserProfile = false;
    this.onboardingservice.userEditOwnProfile = false;
  }
  clearDriving(clear) {
    if (clear) {
      this.user.drivingLicenseNumber = '';
      this.user.expirationDate = '';
      this.user.driverId = '';
      this.user.modelDate = null;
    }
  }

  cancel() {
    if (this.onboardingservice.onboardingData.organization) {
      this.user = new Users();
      this.onboardingservice.onboardingData.User = this.user;
      this.back.next('organization');
    }
    else if (this.onboardingservice.adminEditUserProfile || this.appService.changeRoute === 'admin/subscription-management') {
      this.router.navigate([this.appService.changeRoute]);
    } else {
      if (this.appService.changeRoute === 'admin/tenant-management' || this.router.url === '/user/update-profile') {
        this.router.navigate([this.appService.userProfileDetails.landingPage]);
        this.appService.changeRoute = '';
      } else {
        this.router.navigate([this.appService.changeRoute]);
      }
    }
  }


  getGetMembers(org) {
    org = org ? org : this._organization;
    if (!org)
      return;
    this.onboardingservice.getGetMembers(org.groupId)
      .finally(() => {
      })
      .subscribe(
        (data) => {
          this.availableUsers = [];
          data.forEach(user => {
            this.availableUsers.push(new Users(user));
          });
          this.availableUsers = _.sortBy(this.availableUsers, 'displayName');
          if (this.onboardingservice.onboardEdit.userId) {
            this.isEditMode = true;
            const selectIndex = _.indexOf(this.availableUsers, _.findWhere(this.availableUsers,
              { Id: this.onboardingservice.onboardEdit.userId }));
            this.userIndex = selectIndex;
            this.setUserSelected(selectIndex);
          } else if (this.data && this.data.User && this.data.isEditMode) {
            this.isEditMode = true;
            const selectIndex = _.indexOf(this.availableUsers, _.findWhere(this.availableUsers, { Id: this.data.User.Id }));
            this.userIndex = selectIndex;
            this.setUserSelected(selectIndex);
          }
        }
      );
  }

  showError(error) {
    this.notifiyStatus = new CustomerOnboardingStatus();
    this.notifiyStatus.index = 1;
    this.notifiyStatus.success = false;
    this.notifiyStatus.message = error.json();
    this.notifyErrorMessage('Error', error.json());
    this.status.next(this.notifiyStatus);
  }


  setUserSelected(index: any) {
    index = Number(index);
    if (index >= 0) {
      let selectedUser = this.availableUsers[index];
      this.user = selectedUser;
      //const newUser = new Users(this.user);
      //this.filterLandingPages(this.user['roleGroup']);
      if (!this.data.isEditMode) {
        this.setRoleGroup();
      }
      if (selectedUser.expirationDate) {
        selectedUser.licenseDetailsAvailable = true;
        let expDate = new Date(selectedUser.expirationDate);
        this.formatedExpiredDate = selectedUser.expirationDate;
        selectedUser.modelDate = { date: { year: expDate.getFullYear(), month: expDate.getMonth() + 1, day: expDate.getDate() } };
      } else {
        selectedUser.licenseDetailsAvailable = false;
        this.clearDriving(true);
      }

    }
    else {
      this.user = new Users();
      this.user.licenseDetailsAvailable = false;
      this.clearDriving(true);
    }

  }

  setRoleGroup() {
    this.onboardingservice.GetRoleUserMapping(this.user.Id).subscribe(
      (data) => {
        if (data[0]) {
          this.user.roleGroup = data[0].roleId;
          this.filterLandingPages(this.user.roleGroup);
        }
      },
      (error) => {
        this.showError(error);
      }
    )
  }

  filterLandingPages(roleGroupId: string) {
    this.onboardingservice.GetRoleFeature(roleGroupId).subscribe((res) => {
      const tempLandingPages = [];
      this.appAvailableFeatures.forEach(feature => {
        res.forEach(availablefearute => {
          if (availablefearute.featureId === feature.featureId && availablefearute.canAccess === 1 && feature.controlLevel === 2) {
            tempLandingPages.push(feature);
          }
        });
      });
      this.landingPages = tempLandingPages;
    });
  }

  onRoleGroupChange(roleGroupId: string) {
    this.filterLandingPages(roleGroupId);
  }

  resetForm(form: any) {
    this.user = new Users();
    this.user.licenseDetailsAvailable = false;
    this.clearDriving(true);
    this.userIndex = '';
  }

  notifyErrorMessage(title, error) {
    if (error && error.ErrorMessage) {
      this.appService.showErrorMessage(title, error.ErrorMessage);
    }
    if (error.isError) {
      this.appService.showErrorMessage(title, error.message);
    }
  }

  notifySuccessMessage(title, message) {
    this.appService.showSuccessMessage(title, message);
  }

  subscriptionTopicsChanged(data) {
    setTimeout(() => {
      this.subscriptionTopics = data;
    });
  }
}
