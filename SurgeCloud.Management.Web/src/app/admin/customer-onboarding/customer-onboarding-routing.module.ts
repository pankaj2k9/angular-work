import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {CustomerOnboardingComponent} from "./customer-onboarding.component";
import {CreateUserComponent} from "./create-user";
const routes: Routes = [
  {
    path: '',
    component: CustomerOnboardingComponent,
  },
  {
    path: 'update-profile',
    component: CreateUserComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class CustomerOnboardingRoutingModule {
}
