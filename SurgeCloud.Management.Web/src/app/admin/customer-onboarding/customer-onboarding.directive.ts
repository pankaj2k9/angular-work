import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appCustomerOnboarding]'
})
export class CustomerOnboardingDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
