import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { OrganizationModule } from './organization';
// import { RoleGroupsModule } from './role-groups';
import { CoreModule } from '../../core';
import { SharedModule } from '../../shared-module';

import { CustomerOnboardingComponent } from './customer-onboarding.component';
// import { MaterialModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomerOnboardingDirective } from './customer-onboarding.directive';
import { CustomerOnboardingService } from './customer-onboarding.service';
import { MyDatePickerModule } from 'mydatepicker';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CreateOrganizationComponent } from './create-organization';
import { CreateRoleGroupComponent } from './create-role-group';
import { CreateUserComponent } from './create-user';
import { AngularSvgIconModule } from "angular-svg-icon";
import { ModalModule } from "ngx-bootstrap";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import {CustomerOnboardingRoutingModule} from "./customer-onboarding-routing.module";
import {TranslateModule,TranslateLoader,TranslateCompiler,TranslateParser,MissingTranslationHandler} from "@ngx-translate/core";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {HttpLoaderFactory} from "../../app.module";

@NgModule({
  imports: [
    CommonModule,
    // MaterialModule,
    FormsModule,
    SharedModule,
    MyDatePickerModule,
    SimpleNotificationsModule,
    NgbModule,
    CoreModule,
    AngularSvgIconModule,
    ModalModule.forRoot(),
    ReactiveFormsModule,
    AngularMultiSelectModule,
    CustomerOnboardingRoutingModule,
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    CustomerOnboardingComponent,
    CustomerOnboardingDirective,
    CreateOrganizationComponent,
    CreateRoleGroupComponent,
    CreateUserComponent
  ],
  exports: [CustomerOnboardingComponent],
  providers: []
})
export class CustomerOnboardingModule { }
