import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {DashboardComponent} from "./dashboard.component";
import {FeatureComponent} from "./feature/feature.component";
import {LoginResolver} from "../../login-resolver.service";
import {GoogleMapComponent} from "./google-map/google-map.component";

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'feature',
        component: FeatureComponent,
        resolve: { userData: LoginResolver }
      },
      {
        path: 'map',
        component: GoogleMapComponent
      },
      {
        path: 'customer-onboarding',
        loadChildren: '../customer-onboarding/customer-onboarding.module#CustomerOnboardingModule'
      },
      {
        path: 'asset-onboarding',
        loadChildren: '../asset-onboarding/asset-onboarding.module#AssetOnboardingModule'
      },
      {
        path: 'dynamic-dashboard',
        loadChildren: '../dynamic-dashboard/dynamic-dashboard.module#DynamicDashboardModule'
      }
    ]
  }
]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {
}
