import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, OnChanges, SimpleChanges } from '@angular/core';
import { DashboardService } from './dashboard.service';
import { AppService } from '../../app.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from "rxjs/Subscription";


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  private routerParam: Subscription;
  constructor(public appService:AppService, private route: ActivatedRoute) {

  }

  ngOnInit() {


  }
  ngOnDestroy() {

  }



}
