import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardService } from './dashboard.service';
import { GoogleMapComponent } from './google-map/google-map.component';
import { RouterModule } from '@angular/router';
import { FeatureComponent } from './feature/feature.component';
import { SharedModule } from '../../shared-module';
import { DashboardRoutingModule } from "./dashboard-routing.module";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    DashboardRoutingModule
  ],
  declarations: [
    DashboardComponent,
    GoogleMapComponent,
    FeatureComponent
  ],
  exports: [
    DashboardComponent,
    GoogleMapComponent,
    FeatureComponent
  ],
  providers: [DashboardService]
})
export class DashboardModule { }
