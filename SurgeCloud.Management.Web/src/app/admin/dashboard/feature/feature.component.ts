import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

// import { DashboardService } from '../dashboard.service';
import { INavigation } from '../../../app';
import { AppService } from '../../../app.service';
import { AppStartService } from '../../../';
import * as _ from 'underscore';
import {AdalService} from "../../../shared-module";
import {TranslateService,  LangChangeEvent} from "@ngx-translate/core";

@Component({
  selector: 'app-feature',
  templateUrl: './feature.component.html',
  styleUrls: ['./feature.component.scss']
})
export class FeatureComponent implements OnInit {

  private features: INavigation[] = [];
  private cardColors: string[] = [];
  private svgIcons: string[] = [];
  // over:boolean[];

  public currentIcon: string;
  public showError: Boolean = false;

  constructor(public appStartService: AppStartService, private route: ActivatedRoute,
    private router: Router, private sanitizer: DomSanitizer, public appService: AppService, public adalService: AdalService,private translate: TranslateService) {
    this.cardColors = this.appService.getFeatureDashboardCardColors();
    this.svgIcons = this.appService.getFeatureDashboardSvgIcons();
  }
  ngOnInit() {
    this.route.data.subscribe((res) => {
       //console.log(res['userData'].landingPage);
      if(res['userData'].landingPage != '') {
        //console.log('applyRedirectionAtLogin', this.adalService.applyRedirectionAtLogin);
        if(this.adalService.applyRedirectionAtLogin) {
          if(res['userData'].landingPage == 'admin/dashboard') {
            this.router.navigate(['/admin/feature']);
          } else {
            this.adalService.applyRedirectionAtLogin = false;
            this.router.navigate(['/'+res['userData'].landingPage]);
          }
        }
        //this.router.navigate(['/'+res['userData'].landingPage]);
      }
    });
    this.appService.changeRoute = this.router.url;
    let i = 0, tempData = _.sortBy(this.appStartService.feature, 'sortOrder');
    let availableFeatures = [];
    for (let feature of tempData) {
      feature.imageIcon = this.sanitizer.bypassSecurityTrustResourceUrl(`${feature.imageIcon}${this.svgIcons[i]}_01.svg#Vector_Smart_Object?version=1`);
      feature.imageHoverIcon = this.sanitizer.bypassSecurityTrustResourceUrl(`${feature.imageHoverIcon}${this.svgIcons[i]}_02.svg#Vector_Smart_Object?version=2`);
      feature.color = this.cardColors[i++];
      feature.enabled = false; // hover
      availableFeatures.push(feature);
    }
    //console.log('before:', availableFeatures);



      let features = this.appService.getAccessibleFeatures(availableFeatures);
      this.appService.feature.subscribe(
        (res) => {
          features = this.appService.getAccessibleFeatures(availableFeatures);
          this.showError = true;
          features.forEach(data => {
            let name=this.sentenceToKey(data.name);
            this.translate.stream(name).subscribe(res => {
              console.log(res);
              if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
                this.features.push({
                  controlLevel: data.controlLevel,
                  featureId: data.featureId,
                  name: res,
                  sortOrder: data.sortOrder,
                  status: data.status,
                  imageIcon: data.imageIcon,
                  imageHoverIcon: data.imageHoverIcon,
                  color: data.color,
                  url: data.url,
                  enabled: data.enabled
                });
              }

            });

          });
        });


    //console.log('features::', this.features);

  }

  featureMouseOver(feature) {
    feature.enabled = true;
    feature.imageIcon['changingThisBreaksApplicationSecurity'] = feature.imageIcon['changingThisBreaksApplicationSecurity'].split('version=')[0] + 'version=';
    feature.imageIcon['changingThisBreaksApplicationSecurity'] = feature.imageIcon['changingThisBreaksApplicationSecurity'] + new Date().getMilliseconds();
  }

  featureMouseOut(feature) {
    feature.enabled = false;
    feature.imageHoverIcon['changingThisBreaksApplicationSecurity'] = feature.imageHoverIcon['changingThisBreaksApplicationSecurity'].split('version=')[0] + 'version=';
    feature.imageHoverIcon['changingThisBreaksApplicationSecurity'] = feature.imageHoverIcon['changingThisBreaksApplicationSecurity'] + new Date().getMilliseconds();
  }

  goto(feature: INavigation) {
    this.router.navigate([feature.url]);
  }

  sentenceToKey(str){
    let strr=str.toLowerCase().trim().split(/\s+/).join('_');
    return strr;

  }


}
