import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { FormsModule } from '@angular/forms';
import { DeviceManagementComponent } from './device-management.component';
import { DeviceManagementService } from './device-management.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { DeviceModule, DeviceComponent } from './device';
import { IotHubModule, IotHubComponent } from './iothub';
import {SharedModule} from '../../shared-module/shared-module.module';
import {AngularSvgIconModule} from 'angular-svg-icon';
import {DeviceManagementRoutingModule} from "./device-management-routing.module";

@NgModule({
  imports: [
    DeviceManagementRoutingModule,
    CommonModule,
    SimpleNotificationsModule,
    FormsModule,
    NgbModule,
    DeviceModule,
    IotHubModule,
    SharedModule,
    AngularSvgIconModule
  ],
  declarations: [
    DeviceManagementComponent,
  ],
  exports: [
    DeviceManagementComponent
  ],
  providers: [
    DeviceManagementService
  ],
  entryComponents : [
    DeviceComponent,
    IotHubComponent
  ]
})

export class DeviceManagementModule { }
