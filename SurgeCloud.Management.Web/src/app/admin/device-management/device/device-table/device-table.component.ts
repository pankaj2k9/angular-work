import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import { Device } from '../device';
import { DeviceManagementService } from '../../device-management.service';
import { Router } from '@angular/router';
import { AppService } from '../../../../../app';
import { AssetOnboardingService } from '../../../asset-onboarding';
import { ArrayFilterPipe } from '../../../../shared-module';
import {Subscription} from 'rxjs/Subscription';
import * as jQuery from 'jquery';

@Component({
  selector: 'app-device-table',
  templateUrl: './device-table.component.html',
  styleUrls: ['./device-table.component.scss']
})
export class DeviceTableComponent implements OnInit, OnDestroy {
  @Input() data: { deviceList: Device[], filterObject: any };

  device: Device[];

  private paginationOrgInfo = {
    devicesortOrder: 'asc', devicesortBy: 'gwSNO',
    numberRowPerPage: 5
  };
  public statusClass: string[] = [
    'inactive',
    'moving'
  ];
  public mainBodyHeight: any;
  private arrayFilterPipe: ArrayFilterPipe = new ArrayFilterPipe();
  toggleFilterSearch = false;
  filterValues = {};
  public filterWidthData = [{ 'width': 16, 'id': 'gwSNO', 'prm': true },
  { 'width': 8, 'id': 'assetId', 'prm': true },
  { 'width': 18, 'id': 'createdDate', 'prm': true },
  { 'width': 18, 'id': 'latestSyncDate', 'prm': true },
  { 'width': 10, 'id': 'gwWANIP', 'prm': true },
  { 'width': 10, 'id': 'gwRSSI', 'prm': true },
  { 'width': 10, 'id': 'gwDCAver', 'prm': true },
  { 'width': 10, 'id': 'status', 'prm': true }];
  pieChartClickSubscription: Subscription;
  constructor(private deviceManagementService: DeviceManagementService, private router: Router,
    private onboardingSerive: AssetOnboardingService, private appService: AppService) {
    this.paginationOrgInfo.numberRowPerPage = this.deviceManagementService.numberOfRecordPerPage;
    this.pieChartClickSubscription = this.appService.pieChartClick.subscribe((data: object) => {
      const status = data['name'] === 'Active' ? 1 : 0;
      this.device = this.arrayFilterPipe.transform(this.data.deviceList, { status: status });
    });
  }

  ngOnInit() {
    if (this.data.filterObject && this.data.deviceList.length > 0) {
      let filter;
      if (typeof this.data.deviceList[0].status === 'number') {
        filter = this.data.filterObject;
      } else {
        filter = { status: this.data.filterObject.status === 0 ? false : true };
      }
      this.device = this.arrayFilterPipe.transform(this.data.deviceList, filter);
    } else {
      this.device = this.data.deviceList;
    }

    this.appService.setAllOptionPagination('dmDeviceTable');
    this.resetFilterData();
    this.mainBodyHeight = jQuery(document).height() - 392;
  }

  ngOnDestroy() {
    this.pieChartClickSubscription.unsubscribe();
  }

  editDevice(device: any) {
    this.onboardingSerive.onboardingDataForEdit = { index: 2, device: device };
    this.appService.ManagementAddEdit = {
      'value' : true,
      'id' : 'device_overview'
    };
    this.router.navigate(['admin/asset-onboarding'])
  }

  resetFilterData() {
    this.toggleFilterSearch = false;
    this.filterValues = {
      'gwSNO': '',
      'assetId': '',
      'createdDate': '',
      'latestSyncDate': '',
      'gwWANIP': '',
      'gwRSSI': '',
      'gwDCAver': '',
      'status': ''
    };
  }

  updateData(event) {
    this.device = event;
    this.appService.appExportData.next(event);
    this.appService.setAllOptionPagination('dmDeviceTable');
  }
}
