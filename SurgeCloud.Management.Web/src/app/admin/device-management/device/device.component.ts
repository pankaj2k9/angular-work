import { Component, OnInit, ViewChild, ComponentFactoryResolver, Input } from '@angular/core';
import { DeviceService } from './device.service';
import { Device } from './device';
import { AppService } from '../../../../app';
import { ArrayFilterPipe } from '../../../shared-module';
import { DeviceTableComponent } from './';
import { DeviceImageComponent } from './';
import { DeviceDirective } from './device.directive';
import { DeviceItem } from './device-item';
import { IWizardItem } from '../../../shared-module';
import { AssetOnboardingService } from '../../asset-onboarding';
import { Router } from '@angular/router';
import * as _ from 'underscore';
import {PieChartComponent, PieChatConfig, SingleChartData} from '../../../core';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.scss']
})

export class DeviceComponent implements OnInit {
  pieChartConfigOptions: PieChatConfig;
  device: Device[] = [];

  ArrayFilterPipe = new ArrayFilterPipe();

  deviceItem: DeviceItem[] = [
    new DeviceItem(DeviceTableComponent, {}),
    new DeviceItem(DeviceImageComponent, {})];

  currentIndex = 0;

  public wizardItems: IWizardItem[] = [
    {
      name: 'Device Table',
      icon: 'glyphicon-th-list glyphicon gi-1-5x',
      disabled: false,
      index: 0,
      success: false,
      error: false,
      active: true
    },
    {
      name: 'Device Image',
      icon: 'glyphicon-th glyphicon gi-1-5x',
      disabled: false,
      index: 1,
      success: false,
      error: false,
      active: false
    },
    {
      name: 'Device Add',
      icon: 'plus-icon',
      disabled: false,
      index: 2,
      success: false,
      error: false,
      active: false
    }
  ];
  filterObject: any = null;
  @ViewChild(DeviceDirective) deviceHost: DeviceDirective;
  chartData: SingleChartData[] = [];
  filterStatus: boolean = this.filterObject === null ? false : true;
  lastUpdated: any;
  public headers = [{
    'title': 'Device ID',
    'dataKey': 'gwSNO',
    'width': 100
  }, {
    'title': 'Asset ID',
    'dataKey': 'assetId',
    'width': 70
  }, {
    'title': 'Provisioned Date',
    'dataKey': 'createdDate|date',
    'width': 90
  }, {
    'title': 'Last Active Time',
    'dataKey': 'latestSyncDate|date',
    'width': 90
  }, {
    'title': 'WAN IP',
    'dataKey': 'gwWANIP',
    'width': 70
  }, {
    'title': 'RSSI',
    'dataKey': 'gwRSSI',
    'width': 60
  }, {
    'title': 'DCA Version',
    'dataKey': 'gwDCAver',
    'width': 70
  }, {
    'title': 'Status',
    'dataKey': 'status',
    'width': 50
  }];

  constructor(private deviceService: DeviceService, public onboardingService: AssetOnboardingService,
    private componentFactoryResolver: ComponentFactoryResolver, private appService: AppService,
    public router: Router,private translate: TranslateService) {
    const colorScheme = { domain: ['#94d67e', '#ee7f32', '#ef4846', '#6f7588', '#eb3c33', '#5acbf9', '#cf4385',
        '#935bcd', '#f3af2f', '#a6d9fd', '#aaaaaa', '#f17cb0', '#b2912f', '#b276b2', '#decf3f' ] };
    this.pieChartConfigOptions = new PieChatConfig(
      [200, 200],
      true,
      colorScheme,
      false,
      false,
      false,
      false,
      true,
      true
    );
  }

  ngOnInit() {
    if (this.appService.getActiveCompany()) {
      this.getDevice(this.appService.getActiveCompany().companyId);
    }
    this.appService.activeCompanyInfo.subscribe((company) => {
      if (company) {
        this.getDevice(company.companyId);
      }
    });

  }
  getDevice(companyId: string) {
    this.lastUpdated = Date.now();
    this.deviceService.getDevice(companyId).subscribe(
      (data) => {
        this.device = [];
        this.chartData = [];
        if (data) {
          data.forEach((dev) => {
            this.device.push(new Device(dev));
          })
        }
        //console.log(data);
        //console.log(this.device);
        if (this.device.length > 0) {
          const activeCount = Math.ceil((this.ArrayFilterPipe.transform(this.device, { status: 1 }).length / this.device.length) * 100) +
            Math.ceil((this.ArrayFilterPipe.transform(this.device, { status: true }).length / this.device.length) * 100);
          const inactiveCount = Math.ceil((this.ArrayFilterPipe.transform(this.device, { status: 0 }).length / this.device.length) * 100) +
            Math.ceil((this.ArrayFilterPipe.transform(this.device, { status: false }).length / this.device.length) * 100);
          this.chartData.push({ name: 'Active', value: activeCount },
            { name: 'Inactive', value: inactiveCount });
          this.translate.stream("generic_active").subscribe(res => {
            if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
              this.chartData[0].name = res;
            }
          });
          this.translate.stream("generic_inactive").subscribe(res => {
            if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
              this.chartData[1].name = res;
            }
          });
        }
        this.loadComponent();
      }
    );
  }

  loadComponent() {
    const boardingItem = this.deviceItem[this.currentIndex];
    //console.log(this.device);
    boardingItem.data = { deviceList: this.device, filterObject: this.filterObject };

    const componentFactory = this.componentFactoryResolver
      .resolveComponentFactory(boardingItem.component);

    const viewContainerRef = this.deviceHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);
    (<{ data: any }>componentRef.instance).data = boardingItem.data;

  }

  changeComponent(index) {
    if (index <= 1) {
      this.wizardItems[this.currentIndex].active = false;
      this.currentIndex = index;
      this.wizardItems[this.currentIndex].active = true;
      this.loadComponent();
    }

    if (index === 2) {
      this.onboardingService.onboardingDataForEdit = { index: 2, device: null };
      this.appService.ManagementAddEdit = {
        'value': true,
        'id': 'device_overview'
      };
      this.router.navigate(['/admin/asset-onboarding']);
    }

  }
  applyStatusFilter(filterData: object) {
    this.appService.emitEventOnPieChartClick(filterData);
  }
}
