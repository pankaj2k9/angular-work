import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeviceComponent } from './device.component';
import { DeviceTableComponent } from './device-table/device-table.component';
import { DeviceImageComponent } from './device-image/device-image.component';
import { DeviceDirective } from './device.directive';
import { DeviceService } from './device.service';
import { DataTableModule } from 'angular2-datatable';
import { CoreModule } from '../../../core';
import { SharedModule } from '../../../shared-module/shared-module.module';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    CoreModule,
    SharedModule
  ],
  declarations: [DeviceComponent,
   DeviceTableComponent,
   DeviceImageComponent,
   DeviceDirective
   ],
   exports: [DeviceComponent,
   DeviceTableComponent,
   DeviceImageComponent
   ],
   providers: [
   DeviceService
   ],
   entryComponents: [
   DeviceTableComponent,
   DeviceImageComponent
   ],
})

export class DeviceModule { }
