import { Injectable } from '@angular/core';
import { HttpService } from '../../../shared-module';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DeviceService {

  constructor(public httpService: HttpService) { }

  getDevice(data?: string): Observable<any> {

    return this.httpService.get(`device/api/iot/getdevicebycompany/${data}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }
}
