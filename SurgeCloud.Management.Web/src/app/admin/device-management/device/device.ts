export class Device {
    public gwSNO: string;
    public assetId: string;
    public provitionedDate: Date;
    public lastActivetime: Date;
    public status: number | boolean;
    public gwWANIP: string;
    public gwDCAver: string;
    public gwRSSI: number;
    public createdDate: Date;
    public latestSyncDate: Date;
    public logo: number;
    constructor(private d: any = {}) {
        this.gwSNO = d.gwSNO || '';
        this.assetId = d.assetId || '';
        this.provitionedDate = d.provitionedDate ? new Date(d.provitionedDate) : null;
        this.lastActivetime = d.lastActivetime ? new Date(d.lastActivetime) : null;
        this.latestSyncDate = d.latestSyncDate ? d.latestSyncDate : null;
        this.createdDate = d.createdDate ? d.createdDate : null;
        this.status = d.status || 0;
        this.gwWANIP = d.gwWANIP || '';
        this.gwRSSI = d.gwRSSI || '';
        this.gwDCAver = d.gwDCAver || '';
        this.logo = d.logo;
    }
}
