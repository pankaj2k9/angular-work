import { Component, OnInit, Input } from '@angular/core';
import { IotHub } from '../iothub';
import { DeviceManagementService } from '../../device-management.service';
import { IotHubService } from '../iothub.service';
import { Device } from '../../device/device';
import { AppService } from '../../../../../app';
import { Subscription } from "rxjs/Subscription";
import { ArrayFilterPipe } from "../../../../shared-module";
import * as jQuery from 'jquery';

@Component({
  selector: 'app-iothub-table',
  templateUrl: './iothub-table.component.html',
  styleUrls: ['./iothub-table.component.scss']
})
export class IotHubTableComponent implements OnInit {

  @Input() iothub: IotHub[];

  private paginationOrgInfo = {
    iothubsortOrder: 'asc', iothubsortBy: 'IotHubName',
    numberRowPerPage: 5
  };
  deviceView = false;
  device: Device[] = [];
  companyId: string;

  toggleFilterSearch: boolean = false;
  filterValues = {};
  public mainBodyHeight: any;
  public filterWidthData = [{ 'width': 50, 'id': 'iotHubName', 'prm': true },
  { 'width': 50, 'id': 'deviceCount', 'prm': true }];

  private paginationDevInfo = {
    devicesortOrder: 'asc', devicesortBy: 'gwSNO',
    numberRowPerPage: 10
  };
  pieChartClickSubscription: Subscription;
  private iothubList: Array<object>;
  private arrayFilterPipe: ArrayFilterPipe = new ArrayFilterPipe();
  constructor(private deviceManagementService: DeviceManagementService,
    private appService: AppService, private iotHubService: IotHubService) {
    this.paginationOrgInfo.numberRowPerPage = this.deviceManagementService.numberOfRecordPerPage;
    this.pieChartClickSubscription = this.appService.pieChartClick.subscribe((data: object) => {
      const status = data['name'] == 'Active' ? 1 : 0;
      this.iothub = this.arrayFilterPipe.transform(this.iothubList, { iotHubName: data['name'] });
    });
  }

  ngOnInit() {
    this.iothubList = this.iothub;
    this.companyId = this.appService.getActiveCompany().companyId;
    this.appService.activeCompanyInfo.subscribe((company) => {
      if (company) {
        this.companyId = company.companyId;
        this.deviceView = false;
      }
    });
    this.appService.setAllOptionPagination('dmIotHubTable');
    this.resetFilterData();
    this.mainBodyHeight = jQuery(document).height() - 392;
  }

  ngOnDestroy() {
    this.pieChartClickSubscription.unsubscribe();
  }

  showDevice(iothubname: string) {
    if (iothubname) {
      this.deviceView = true;
      this.iotHubService.getDevice(this.companyId, iothubname).subscribe(
        (data) => {
          this.device = [];
          if (data) {
            data.forEach((dev) => {
              this.device.push(new Device(dev));
            });
          }
        });
    } else {
      this.device = [];
    }
  }

  resetFilterData() {
    this.toggleFilterSearch = false;
    this.filterValues = {
      'iotHubName': '',
      'deviceCount': ''
    };
  }

  updateData(event) {
    this.iothub = event;
    this.appService.appExportData.next(event);
  }
}
