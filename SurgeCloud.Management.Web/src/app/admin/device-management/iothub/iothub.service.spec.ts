import { TestBed, inject } from '@angular/core/testing';

import { IotHubService } from './iothub.service';

describe('IotHubService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IotHubService]
    });
  });

  it('should ...', inject([IotHubService], (service: IotHubService) => {
    expect(service).toBeTruthy();
  }));
});
