import { Component, OnInit, Input, Output, ViewChild, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { ColorOptions } from '../dynamic-dashboard-constant';

@Component({
  selector: 'app-content-theme',
  templateUrl: './content-theme.component.html',
  styleUrls: ['./content-theme.component.scss']
})
export class ContentThemeComponent implements OnInit {

  private toggleColorOptions: Boolean = false;
  private selectedBackgroundOption = '';
  private toggleBorderWidth: Boolean = false;
  private colorPickerColor = 'white';
  private selectedThemeTypes = [];
  private themeTypes = ['Background Color', 'Border Color', 'Border Width', 'Gradient'];
  private positionTypes = ['top', 'bottom', 'left', 'right'];
  private defaultColorOptions = ColorOptions;
  private selectedPosition = 'top';
  private copyColorOptions = {};
  @ViewChild('themeModal') public themeModal: ModalDirective;
  @Input() colorOptions;
  @Output() notifyColorOptions = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
    this.copyColorOptions = JSON.parse(JSON.stringify(this.colorOptions));
    document.getElementsByTagName('body')[0].addEventListener('click', () => {
    });
  }



  onColorPickerOption(name) {
  }

  onChangeColor(event, index) {
    if (this.selectedBackgroundOption === 'bgColor') {
      this.colorOptions.backgroundColor = event;
    } else if (this.selectedBackgroundOption === 'brColor') {
      this.colorOptions.borderColor = event;
    } else if (this.selectedBackgroundOption === 'gradient') {
      this.colorOptions.backgroundColor = `linear-gradient(to ${this.selectedPosition}, ${this.colorOptions.gradient.first} , ${this.colorOptions.gradient.second})`;
    }
  }

  onChangePosition(value) {
    this.colorOptions.backgroundColor = `linear-gradient(to ${this.selectedPosition}, ${this.colorOptions.gradient.first} , ${this.colorOptions.gradient.second})`;
  }

  openModal(event, index) {
    event.stopPropagation();
    this.toggleColorOptions = false;
    this.themeModal.show();
    this.setSelectedOption(index);
  }

  setSelectedOption(index) {
    this.selectedThemeTypes = [];
    this.selectedThemeTypes[index] = true;
    if (index == 0) {
      this.selectedBackgroundOption = 'bgColor';
    } else if (index == 1) {
      this.selectedBackgroundOption = 'brColor';
    } else if (index == 2) {
      this.selectedBackgroundOption = 'border';
    } else if (index == 3) {
      this.selectedBackgroundOption = 'gradient';
    }
  }

  colorPickerClicked(event) {
    event.stopPropagation();
  }

  reset() {
    this.colorOptions = JSON.parse(JSON.stringify(this.copyColorOptions));
    console.log(this.copyColorOptions);
    this.notifyColorOptions.emit(this.colorOptions);
  }

  cancel() {
    this.colorOptions = this.defaultColorOptions;
    this.notifyColorOptions.emit(this.copyColorOptions);
    this.themeModal.hide();
  }

  saveTheme() {
    this.themeModal.hide();
    this.copyColorOptions = JSON.parse(JSON.stringify(this.colorOptions));
    this.notifyColorOptions.emit(this.colorOptions);
  }

}
