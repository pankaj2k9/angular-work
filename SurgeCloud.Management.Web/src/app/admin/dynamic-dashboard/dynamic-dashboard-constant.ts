import { concat } from "rxjs/observable/concat";

export const chartType = {
    'IMAGE': 'image',
    'TABLE': 'table',
    'WIDGET': 'widget',
    'PIECHART': 'dPieChart',
    'BARCHART': 'dBarChart',
    'LINECHART': 'dLineChart',
    'CURVECHART': 'dCurveChart',
    'BUBBLECHART': 'dBubbleChart',
    'AREAFILLEDCHART': 'dAreaFilledChart',
    'ANGLEVECTORCHART': 'dAngleVectorChart',
    'THERMOMETER': 'thermometer',
    'GUAGE': 'guage'
}
export const graphics = [
    {
        'name': 'Widget',
        'id': 'widget',
        'config': { 'dragHandle': '.handle', 'col': 51, 'row': 1 },
        'sizex': 128,
        'sizey': 60
    },
    {
        'name': 'Image',
        'id': 'image',
        'config': { 'dragHandle': '.handle', 'col': 1, 'row': 2 },
        'sizex': 30,
        'sizey': 30
    },
    {
        'name': 'Table',
        'id': 'table',
        'config': { 'dragHandle': '.handle', 'col': 51, 'row': 1 },
        'sizex': 129,
        'sizey': 10
    }
];

export const items = [
    {
        'name': 'Pie Chart',
        'id': 'dPieChart',
        'headerSrc': 'assets/images/piechart.png',
        'config': { 'dragHandle': '.handle', 'col': 51, 'row': 26 },
        'sizex': 26,
        'sizey': 40
    },
    {
        'name': 'Bar Chart',
        'id': 'dBarChart',
        'config': { 'dragHandle': '.handle', 'col': 26, 'row': 1 },
        'sizex': 76,
        'sizey': 60
    },
    {
        'name': 'Line Chart',
        'id': 'dLineChart',
        'config': { 'dragHandle': '.handle', 'col': 26, 'row': 1 },
        'sizex': 85,
        'sizey': 50
    },
    {
        'name': 'Curve Chart',
        'id': 'dCurveChart',
        'config': { 'dragHandle': '.handle', 'col': 26, 'row': 1 },
        'sizex': 85,
        'sizey': 50
    },
    {
        'name': 'Bubble Chart',
        'id': 'dBubbleChart',
        'config': { 'dragHandle': '.handle', 'col': 26, 'row': 1 },
        'sizex': 80,
        'sizey': 60
    },
    {
        'name': 'Area Filled Chart',
        'id': 'dAreaFilledChart',
        'config': { 'dragHandle': '.handle', 'col': 26, 'row': 1 },
        'sizex': 90,
        'sizey': 70
    },
    {
        'name': 'Angle Vector Chart',
        'id': 'dAngleVectorChart',
        'config': { 'dragHandle': '.handle', 'col': 26, 'row': 1 },
        'sizex': 80,
        'sizey': 60
    },
    {
        'name': 'Thermometer',
        'id': 'thermometer',
        'config': { 'dragHandle': '.handle', 'col': 26, 'row': 1 },
        'sizex': 23,
        'sizey': 33
    },
    {
        'name': 'Guage',
        'id': 'guage',
        'config': { 'dragHandle': '.handle', 'col': 26, 'row': 1 },
        'sizex': 30,
        'sizey': 20
    }
];

export const ngGridConfig = {
    'margins': [3],
    'draggable': true,
    'resizable': true,
    'max_cols': 0,
    'max_rows': 0,
    'visible_cols': 0,
    'visible_rows': 0,
    'min_cols': 1,
    'min_rows': 1,
    'col_width': 2,
    'row_height': 2,
    'cascade': 'up',
    'min_width': 5,
    'min_height': 5,
    'fix_to_grid': false,
    'auto_style': true,
    'auto_resize': false,
    'maintain_ratio': false,
    'prefer_new': false,
    'zoom_on_drag': false,
    'limit_to_screen': true
};

export const pieChartDefaultData = [
    { name: 'Active Users', value: 5 },
    { name: 'Inactive Users', value: 10 }
];

export const PieChartConfig = {
    'view': [200, 200],
    'showLegend': true,
    'colorScheme': {
        domain: ['#52ba9b', '#f48b37', '#ef4846', '#348ea9', '#f3af2f', '#cf4385', '#a6d9fd',
            '#935bcd', '#4da338', '#aaaaaa', '#f17cb0', '#b2912f', '#b276b2', '#decf3f']
    },
    'showLabels': false,
    'explodeSlices': false,
    'doughnut': false
}

export const barChartDefaultData = [
    {
        "name": "Germany",
        "value": 8940000,
        "occurrenceStartDate": new Date()
    },
    {
        "name": "USA",
        "value": 5000000,
        "occurrenceStartDate": new Date()
    },
    {
        "name": "France",
        "value": 7200000,
        "occurrenceStartDate": new Date()
    },
    {
        "name": "India",
        "value": 894000,
        "occurrenceStartDate": new Date()
    },
    {
        "name": "China",
        "value": 500000,
        "occurrenceStartDate": new Date()
    },
    {
        "name": "Australia",
        "value": 720000,
        "occurrenceStartDate": new Date()
    }
];

export const linechartDefaultData = [
    {
        'name': 'Germany',
        'series': [
            {
                'name': '2010',
                'value': 12
            },
            {
                'name': '2011',
                'value': 20
            },
            {
                'name': '2012',
                'value': 14
            },
            {
                'name': '2013',
                'value': 12
            },
            {
                'name': '2014',
                'value': 18
            }
        ]
    },

    {
        'name': 'USA',
        'series': [
            {
                'name': '2010',
                'value': 12
            },
            {
                'name': '2011',
                'value': 12
            },
            {
                'name': '2012',
                'value': 14
            },
            {
                'name': '2013',
                'value': 18
            },
            {
                'name': '2014',
                'value': 25
            }
        ]
    },

    {
        'name': 'France',
        'series': [
            {
                'name': '2010',
                'value': 25
            },
            {
                'name': '2011',
                'value': 34
            },
            {
                'name': '2012',
                'value': 14
            },
            {
                'name': '2013',
                'value': 20
            },
            {
                'name': '2014',
                'value': 15
            }
        ]
    }
];

export const bubblechartDefaultData = [ // make it empty, testing data
    {
        "name": "Germany",
        "series": [
            {
                "name": "2010",
                "x": "2009",
                "y": 80.3,
                "r": 80.4
            },
            {
                "name": "2000",
                "x": "1999",
                "y": 80.3,
                "r": 78
            },
            {
                "name": "1990",
                "x": "1989",
                "y": 75.4,
                "r": 79
            }
        ]
    },
    {
        "name": "United States",
        "series": [
            {
                "name": "2010",
                "x": "2009",
                "y": 78.8,
                "r": 310
            },
            {
                "name": "2000",
                "x": "1999",
                "y": 76.9,
                "r": 283
            },
            {
                "name": "1990",
                "x": "1989",
                "y": 75.4,
                "r": 253
            }
        ]
    },
    {
        "name": "France",
        "series": [
            {
                "name": "2010",
                "x": "2009",
                "y": 81.4,
                "r": 63
            },
            {
                "name": "2000",
                "x": "1999",
                "y": 79.1,
                "r": 59.4
            },
            {
                "name": "1990",
                "x": "1989",
                "y": 77.2,
                "r": 56.9
            }
        ]
    }
];

export const areafilledchartDefaultData = [
    {
        "name": "Germany",
        "series": [
            {
                "name": "2010",
                "value": 7300000
            },
            {
                "name": "2011",
                "value": 8940000
            }
        ]
    },

    {
        "name": "USA",
        "series": [
            {
                "name": "2010",
                "value": 7870000
            },
            {
                "name": "2011",
                "value": 8270000
            }
        ]
    },

    {
        "name": "India",
        "series": [
            {
                "name": "2010",
                "value": 5000002
            },
            {
                "name": "2011",
                "value": 5800000
            }
        ]
    }
];

export const ColorOptions = {
    'backgroundColor': 'white',
    'borderColor': '#d4d8de',
    'borderWidth': 1,
    'gradient': {
        'first': 'white',
        'second': 'white'
    }
}