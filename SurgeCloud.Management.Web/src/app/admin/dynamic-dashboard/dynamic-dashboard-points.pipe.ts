import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'PointsNamePipe'
})
export class PointsNamePipe implements PipeTransform {

  transform(items: any[], field: string, value: string): any[] {
    if (!value) return items;
    return items.filter(it => it[field].toLowerCase().includes(value.toLowerCase()));
  }

}
