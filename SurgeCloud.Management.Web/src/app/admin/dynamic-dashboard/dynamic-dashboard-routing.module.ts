import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {DynamicDashboardComponent} from "./dynamic-dashboard.component";
const routes: Routes = [
  {
    path: '',
    component: DynamicDashboardComponent
  }
]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DynamicDashboardRoutingModule {
}
