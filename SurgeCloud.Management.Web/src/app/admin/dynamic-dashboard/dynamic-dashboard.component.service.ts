import { Injectable } from '@angular/core';
import { Company } from '../../../app';
import { chartType } from './dynamic-dashboard-constant';
import * as _ from 'underscore';
import * as moment from 'moment';

@Injectable()
export class DynamicDashboardComponentService {
    public chartType = chartType;
    constructor() { }

    getCurrentBoxName(id) {
        let name = '';
        if (id === this.chartType.PIECHART) {
            name = 'Data Points';
        }
        return name;
    }

    setExcelDataForChart(excelData) {
        let seperateIndex;
        const object = {
            'chartInfo': {},
            'data': [],
            'header': []
        };
        _.each(excelData, function (data, index) {
            _.each(data, function (item, childIndex) {
                if (item === 'Data Points') {
                    seperateIndex = index;
                    return true;
                }
            });
        });
        _.each(excelData, function (data, index) {
            if (data.length > 0) {
                if (index < seperateIndex) {
                    if(data[1].toLowerCase() === 'true' || data[1].toLowerCase() === 'false') {
                        object.chartInfo[data[0]] = data[1].toLowerCase() == 'true' ? true : false;
                    } else {
                        object.chartInfo[data[0]] = data[1]
                    }
                } else if (index === seperateIndex) {
                    object.header.push(data);
                } else {
                    object.data.push(data);
                }
            }
        });
        return object;
    }

    updateChartValue(chart, excelData, chartData, selectedFileUploadIndex, id) {
        console.log(chart);
        console.log(excelData);
        const seperateName = this.getCurrentBoxName(id);
        const object = this.setExcelDataForChart(excelData);
        return object;
    }

    setPieChartData(data) {
        const excelData = [['showLegend', 'true'],
        ['showLabels', 'true'],
        ['explodeSlices', 'true'],
        [],
        ['Data Points', 'Display Name', 'Color']];
        _.each(data.data, function (item) {
            excelData.push([item.name]);
        });
        return excelData;
    }

    setBarChartData(data) {
        const excelData = [];
        _.each(data.barChartInfo, (value, key, obj) => {
            excelData.push([key, value]);
        });
        excelData.push([]);
        excelData.push(['Data Points', 'Display Name', 'Color']);
        _.each(data.data, function (item) {
            excelData.push([item.name]);
        });
        console.log(excelData);
        return excelData;
    }

    getExcelHeaderData(id, data) {
        console.log(data);
        if (id === 'dPieChart') {
            return this.setPieChartData(data);
        } else if (id === 'dBarChart') {
            return this.setBarChartData(data);
        } else if (id === 'dCurveChart') {
            return [['showXAxis', 'true'],
            ['showYAxis', 'true'],
            ['legend', 'true'],
            ['xAxisLabel', 'Country'],
            ['yAxisLabel', 'Population'],
            ['showXAxisLabel', 'true'],
            ['showYAxisLabel', 'true']
            ];
        } else if (id === 'dBubbleChart') {
            return [['width', 100],
            ['height', 300],
            ['showXAxis', 'true'],
            ['showYAxis', 'true'],
            ['showXAxisLabel', 'true'],
            ['xAxisLabel', 'X'],
            ['showYAxisLabel', 'true'],
            ['yAxisLabel', 'Y']
            ];
        } else if (id === 'dAreaFilledChart') {
            return [['width', 100],
            ['height', 300],
            ['showXAxis', 'true'],
            ['showYAxis', 'true'],
            ['showXAxisLabel', 'true'],
            ['xAxisLabel', 'X'],
            ['showYAxisLabel', 'true'],
            ['yAxisLabel', 'Y']
            ];
        } else {
            return null;
        }
    }

}