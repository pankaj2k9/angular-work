import { Component, OnInit, ViewChild, ElementRef, SimpleChange, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { DynamicDashboardService } from './dynamic-dashboard.service';
import { DynamicDashboardComponentService } from './dynamic-dashboard.component.service';
import { AppService } from './../../app.service';
import {
  chartType, items, graphics, ngGridConfig, linechartDefaultData,
  barChartDefaultData, PieChartConfig, pieChartDefaultData, bubblechartDefaultData,
  areafilledchartDefaultData, ColorOptions
} from './dynamic-dashboard-constant';
import {
  PieChartComponent, SingleChartData, MultiChartData, BarChartConfig, LineChartConfig,
  BubbleChartConfig, AreaFilledChartConfig
} from '../../core';
import { NgGrid, NgGridItem, NgGridConfig, NgGridItemConfig, NgGridItemEvent } from 'angular2-grid';
import * as jQuery from 'jquery';
import * as _ from 'underscore';
import * as moment from 'moment';
import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';

type AOA = Array<Array<any>>;
function s2ab(s: string): ArrayBuffer {
  const buf: ArrayBuffer = new ArrayBuffer(s.length);
  const view: Uint8Array = new Uint8Array(buf);
  for (let i = 0; i !== s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
  return buf;
}

interface Box {
  id: number;
  config: any;
}

@Component({
  selector: 'app-dynamic-dashboard',
  templateUrl: './dynamic-dashboard.component.html',
  styleUrls: ['./dynamic-dashboard.component.scss']
})
export class DynamicDashboardComponent implements OnInit {

  @ViewChild('previewDataContainer') previewDataContainer: ElementRef;
  public excelData: AOA = [[1, 2], [3, 4]];
  public wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'binary' };
  public activeCompanyInfoSubscription: any;
  public chartType = chartType;
  public currentIndex = 0;
  public copyCurrentIndex = 0;
  public assets = [];
  public dashboardMinHeight: any;
  public searchPointName: any = '';
  public items = items;
  public graphics = graphics;
  public droppedItems = [];
  public filterObject: any = null;
  public selectedItemIndex = -1;
  public selectedColumns = new Array();
  public selectedPoint: any;
  public selectedItem: {};
  public boxes: Array<Box> = [];
  public gridConfig: NgGridConfig = <NgGridConfig>ngGridConfig;
  public itemPositions: Array<any> = [];
  public validExts = new Array(".xlsx", ".xls");
  public selectedChart: any;
  public assetExpands = [];
  public librariesExpands = [];
  public headerExpands = [];
  public widgetList = [];
  public colorOptions = JSON.parse(JSON.stringify(ColorOptions));
  public assetExpandPoints = {
    'history': [],
    'realtime': [],
    'alarm': []
  }
  constructor(private route: Router,
    private appService: AppService,
    private dynamicDashboardComponentService: DynamicDashboardComponentService,
    private dynamicDashboardService: DynamicDashboardService) {
  }

  ngOnInit() {

    if (this.appService.getActiveCompany()) {
      this.getAsset();
    }

    this.dashboardMinHeight = (document.getElementsByTagName('body')[0].clientHeight -
      document.getElementById('appHeader').clientHeight -
      document.getElementById('appFooter').clientHeight) - 1 + 'px';

    this.activeCompanyInfoSubscription = this.appService.activeCompanyInfo.subscribe(
      (company) => {
        if (this.route.url === '/operation/dynamic-dashboard') {
          this.getAsset();
        }
      }
    );
  }


  ngOnDestroy() {
    this.activeCompanyInfoSubscription.unsubscribe();
  }

  editFunctionCall(data) {
    console.log(data);
    if (data.type === 'onSelect') {
      this.selectedItemIndex = data.data.index;
      this.selectedItem = data.data.item;
    } else if (data.type === 'downloadExcel') {
      this.downloadExcel(data.data.id);
    } else if (data.type === 'uploadExcel') {
      this.openFileBox(data.data.item);
    } else {
      this.selectedItemIndex = data.data.index;
    }
  }

  showPreviewModal() {
    document.getElementsByTagName('body')[0].style.overflow = 'auto';
    const previreModal = document.getElementById('preview-modal');
    previreModal.style.display = 'block';
    setTimeout(() => {
      previreModal.classList.add('show');
    }, 600);
  }

  hidePreviewModal() {
    document.getElementsByTagName('body')[0].style.overflow = '';
    const previreModal = document.getElementById('preview-modal');
    previreModal.classList.remove('show');
    this.previewDataContainer.nativeElement.innerHTML = '';
    setTimeout(() => {
      previreModal.style.display = '';
    }, 600);
  }

  loadPreviewData() {
    this.showPreviewModal();
    this.previewDataContainer.nativeElement.innerHTML = document.getElementById('data-canvas').innerHTML;
  }

  getAsset() {
    let companyId = this.appService.getActiveCompany().companyId;
    this.dynamicDashboardService.getAssetDesc(companyId).subscribe(data => {
      console.log(data);
      this.assets = data;
      this.getPointNameForAsset(data);
    }), (error) => {
      console.log(error);
    };
  }

  getPointNameForAsset(data) {
    data.forEach((element, index) => {
      this.dynamicDashboardService.getPointNameForAsset({
        companyId: this.appService.getActiveCompany().companyId,
        gwSno: element.gwSNO,
        assetId: element.assetId
      }).subscribe(response => {
        // console.log(response);
        if (response[0]) {
          this.assets[index].points = response[0].param;
          _.each(this.assets[index].points, function (point) {
            point.assetId = element.assetId
          });
        }
      }), (error) => {
        console.log(error);
      };
    });
  }

  toggleAsset(index) {
    this.searchPointName = '';
    let expand = this.assetExpands[index];
    this.assetExpands = [];
    this.assetExpands[index] = !expand;
  }

  toggleLibrary(index) {
    let expand = this.librariesExpands[index];
    this.librariesExpands = [];
    this.librariesExpands[index] = !expand;
  }

  togglePoints(prm, index) {
    let expand = this.assetExpandPoints[prm][index];
    // this.assetExpandPoints[prm] = [];
    this.assetExpandPoints[prm][index] = !expand;
  }

  onItemDropOnWidgetBox(event, index) {
    console.log(index);
  }

  onItemDropOnWidget(event, index) {
    if (!this.droppedItems[index].widgetList) {
      this.droppedItems[index].widgetList = [];
    }
    let clone = jQuery.extend(true, {}, event.dragData);
    let cloneConfig = jQuery.extend(true, {}, event.dragData.config);
    cloneConfig = this.setItemSize(clone, cloneConfig);
    clone.config = cloneConfig;
    clone.config.sizex = clone.sizex;
    clone.config.sizey = clone.sizey;
    this.droppedItems[index].widgetList.push(clone);
  }

  onItemDrop(event: any) {
    let targetElement = event.nativeEvent.target || event.nativeEvent.srcElement || event.nativeEvent.currentTarget;
    // Get the dropped data here
    if ((!event.dragData.config && event.dragData.id === 'table') || event.dragData.pointName) {
      this.showErrorMessage('Please Select Table First...');
    } else {
      let clone = jQuery.extend(true, {}, event.dragData);
      let cloneConfig = jQuery.extend(true, {}, event.dragData.config);
      let sizex = 0;
      this.droppedItems.forEach((element, index) => {
        if (index >= this.currentIndex) {
          sizex = sizex + element.config.sizex;
        }
      });
      let col = this.droppedItems[this.droppedItems.length - 1] ? sizex + 1 : 1;
      let index = parseInt(Number(col / 100).toString());
      if (this.copyCurrentIndex !== index && index !== 0) {
        this.copyCurrentIndex = index;
        this.currentIndex = this.droppedItems.length;
        col = 1;
      } else {
        if (index !== 0) {
          col = sizex - ((parseInt(Number(sizex / 100).toString()) * 100) + 1);
        }
      }
      cloneConfig = { 'dragHandle': '.handle', 'col': col, 'row': 1, sizex: clone.sizex, sizey: clone.sizey };
      cloneConfig = this.setItemSize(clone, cloneConfig);
      console.log(cloneConfig);
      clone.config = cloneConfig;
      this.droppedItems.push(clone);
    }
  }

  chartConfigOptions() {
    /*return {
      height: 300,
      width: 100,
      showXAxis: true,
      showYAxis: true,
      gradient: false,
      showXAxisLabel: true,
      xAxisLabel: 'X',
      showYAxisLabel: true,
      yAxisLabel: 'Y',
      showLegend: true,
      axisTickFormatting: false
    };*/
    const barChartColorScheme = {
      domain: ['#52ba9b', '#f48b37', '#ef4846', '#348ea9', '#f3af2f', '#cf4385', '#a6d9fd', '#935bcd', '#4da338'
        , '#aaaaaa', '#f17cb0', '#b2912f', '#b276b2', '#decf3f']
    };
    return new BarChartConfig(
      100,
      300,
      true,
      true,
      'date',
      'number',
      false,
      false,
      'Legend',
      false,
      'x',
      false,
      'y',
      barChartColorScheme,
      true
    );
  }

  setItemSize(clone, cloneConfig) {
    console.log(clone, this.chartType);
    let obj = {};
    if (clone.id === this.chartType.TABLE) {
    } else if (clone.id === this.chartType.LINECHART) {
      const colorScheme = { domain: ['#52ba9b', '#f48b37', '#ef4846', '#348ea9', '#f3af2f', '#cf4385', '#a6d9fd', '#935bcd', '#4da338'
          , '#aaaaaa', '#f17cb0', '#b2912f', '#b276b2', '#decf3f'] };
      obj['lineChartInfo'] = new LineChartConfig(
        100,
        255,
        true,
        true,
        'date',
        'number',
        'string',
        true,
        false,
        'x',
        false,
        'y',
        false,
        'Legend',
        colorScheme,
        false,
        true,
        true
      );
      //obj['lineChartInfo'] = { showLegend: true, height: 255, axisTickFormatting: false };
      obj['data'] = linechartDefaultData;
      cloneConfig.linechartData = obj;
    } else if (clone.id === this.chartType.CURVECHART) {
      //obj['lineChartInfo'] = { showLegend: true, height: 255, axisTickFormatting: false };
      const colorScheme = { domain: ['#52ba9b', '#f48b37', '#ef4846', '#348ea9', '#f3af2f', '#cf4385', '#a6d9fd', '#935bcd', '#4da338'
          , '#aaaaaa', '#f17cb0', '#b2912f', '#b276b2', '#decf3f'] };
      obj['lineChartInfo'] = new LineChartConfig(
        100,
        255,
        true,
        true,
        'date',
        'number',
        'string',
        true,
        false,
        'x',
        false,
        'y',
        false,
        'Legend',
        colorScheme,
        false,
        true,
        true
      );
      obj['data'] = linechartDefaultData;
      cloneConfig.linechartData = obj;
    } else if (clone.id === this.chartType.BARCHART) {
      obj['barChartInfo'] = this.chartConfigOptions();
      obj['data'] = barChartDefaultData;
      cloneConfig.barChartData = obj;
    } else if (clone.id === this.chartType.PIECHART) {
      obj['pieChartInfo'] = PieChartConfig;
      obj['data'] = pieChartDefaultData;
      cloneConfig.pieChartData = obj;
    } else if (clone.id === this.chartType.BUBBLECHART) {
      // obj['pieChartInfo'] = PieChartConfig;
      const colorScheme = {
        domain: ['#52ba9b', '#f48b37', '#ef4846', '#348ea9', '#f3af2f', '#cf4385', '#a6d9fd', '#935bcd', '#4da338'
          , '#aaaaaa', '#f17cb0', '#b2912f', '#b276b2', '#decf3f']
      };
      obj['bubbleChartInfo'] = new BubbleChartConfig(
        100,
        300,
        true,
        true,
        true,
        true,
        'x',
        true,
        'y',
        colorScheme,
        true
      );
      obj['data'] = bubblechartDefaultData;
      cloneConfig.bubblechartData = obj;
    } else if (clone.id === this.chartType.AREAFILLEDCHART) {
      obj['areaFilledChartInfo'] = new AreaFilledChartConfig(
        100,
        300,
        true,
        true,
        true,
        true,
        'x',
        true,
        'y',
        { domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA'] },
        true
      );
      obj['data'] = areafilledchartDefaultData;
      cloneConfig.areafilledchartData = obj;
    } else if (clone.id === this.chartType.THERMOMETER) {
    } else if (clone.id === this.chartType.GUAGE) {
    } else if (clone.id === this.chartType.ANGLEVECTORCHART) {
    } else if (clone.id === this.chartType.WIDGET) {
    } else {
      cloneConfig.sizex = 30;
      cloneConfig.sizey = 30;
    }
    return cloneConfig;
  }

  onItemDropOnContent(event: any) {
    let targetElement = event.nativeEvent.target || event.nativeEvent.srcElement || event.nativeEvent.currentTarget;
    let targetName = targetElement.getAttribute('id');
    if (targetName && !targetName.includes('tHead')) {
      const id = targetElement.getAttribute('id').split('-')[1];
      if (targetName.includes('pieChartLayer')) {
        this.checkDataToChart(id, event, this.droppedItems[id].config.pieChartData);
      } if (targetName.includes('barChartLayer')) {
        this.checkDataToChart(id, event, this.droppedItems[id].config.barChartData);
      }
    } else {
      if (targetElement.getAttribute('class') === 'header') {
        targetElement = targetElement.parentElement;
      } else if (targetElement.getAttribute('id') && event.nativeEvent.target.getAttribute('id').includes('table')) {
        targetElement = targetElement.parentElement;
      } else if (targetElement.nodeName === 'TH') {
        targetElement = targetElement.parentElement.parentElement.parentElement.parentElement.parentElement;
      } else if (targetElement.nodeName === 'TR') {
        targetElement = targetElement.parentElement.parentElement.parentElement.parentElement;
      } else if (targetElement.nodeName === 'I') {
        targetElement = targetElement.parentElement.parentElement.parentElement.parentElement;
      } else if (targetElement.getAttribute('class') && targetElement.getAttribute('class').includes('drag-drop-point-header')) {
        targetElement = targetElement.parentElement.parentElement;
      }
      this.checkIsTableExist(targetElement, event);
    }
  }

  checkDataToChart(id, event, chartData) {
    const filterObject = _.find(chartData.data, (item, index) => {
      return item.assetIndex === event.dragData.assetIndex && item.pointIndex === event.dragData.pointIndex;
    });
    if (filterObject) {
      this.showErrorMessage('Point Name Already Exits., Choose other one..');
    } else {
      this.setDataToChart(id, event.dragData, chartData);
    }
  }

  setDataToChart(id, data, chartData) {
    if (!this.droppedItems[id].isDataAvailable) {
      chartData.data = [];
      this.droppedItems[id].isDataAvailable = true;
    }
    let clone = jQuery.extend(true, [], chartData.data);
    chartData.data = [];
    clone.push({
      'name': data.pointName,
      'assetIndex': data.assetIndex,
      'pointIndex': data.pointIndex,
      'value': Math.floor(Math.random() * 100) + 1
    });
    chartData.data = clone;
    if (chartData.data.length > 2) {
      let cloneConfig = jQuery.extend(true, {}, this.droppedItems[id].config);
      this.droppedItems[id].config = {};
      cloneConfig.sizey = cloneConfig.sizey + 2;
      this.droppedItems[id].config = cloneConfig
    }
  }

  checkIsTableExist(targetElement, event) {
    if (targetElement.getElementsByClassName('table-data')[0] === undefined) {
      this.showErrorMessage('Please Select Table First...');
      return;
    }
    this.setColumnForTable(targetElement, event);
  }

  setColumnForTable(targetElement, event) {
    let currentIndex = Number(targetElement.getElementsByClassName('table-data')[0].getAttribute('id').replace('table', ''));
    if (!this.selectedColumns[currentIndex]) {
      this.selectedColumns[currentIndex] = new Array();
    }
    const name = _.find(this.selectedColumns[currentIndex], function (item) {
      return item.pointName === event.dragData.pointName && item.assetId === event.dragData.assetId && item.node === event.dragData.node;
    });
    if (name) {
      this.showErrorMessage('Point Name Already Exits...');
      return;
    }
    event.dragData.headerIndex = this.selectedColumns[currentIndex].length;
    let clone = jQuery.extend(true, {}, event.dragData);
    clone.tableIndex = currentIndex;
    this.selectedColumns[currentIndex].push(clone);
    this.selectedPoint = null;
  }

  showErrorMessage(msg) {
    this.appService.showErrorMessage(
      'Error',
      msg
    );
  }


  selectPointName(point, assetIndex, pointIndex) {
    this.selectedPoint = point;
    this.selectedPoint.assetIndex = assetIndex;
    this.selectedPoint.pointIndex = pointIndex;
    // this.selectedTableHeader = null;
  }

  removeTableHeader(point, index, childIndex) {
    // this.assets[point.assetIndex].points[point.pointIndex].isHidden = false;
    this.selectedColumns[index].splice(childIndex, 1);
    _.each(this.selectedColumns[index], (data, i) => {
      if (i >= childIndex) {
        data.headerIndex = data.headerIndex - 1;
      }
    });
  }

  uploadHeaderIconLogo(index) {
    document.getElementById('iconViewLogo').click();
  }


  updateItem(index: number, event: NgGridItemEvent): void {
    // Do something here
  }

  onDrag(index: number, event: NgGridItemEvent): void {
    // Do something here
  }

  onSelectItem(data) {
    this.selectedItem = data;
  }

  onDropComplateItem() {
    this.selectedItem = null;
  }

  onResize(index: number, event: NgGridItemEvent, id): void {
    if (id === this.chartType.PIECHART) {
      let cloneChartInfo = jQuery.extend(true, {}, this.droppedItems[this.selectedItemIndex].config.pieChartData.pieChartInfo);
      this.droppedItems[this.selectedItemIndex].config.pieChartData.pieChartInfo = {};
      cloneChartInfo.view = [event.width, event.height - 90 - event.sizey];
      this.droppedItems[this.selectedItemIndex].config.pieChartData.pieChartInfo = cloneChartInfo;
    } else if (id === this.chartType.BARCHART) {
      let cloneChartInfo = jQuery.extend(true, {}, this.droppedItems[this.selectedItemIndex].config.barChartData.barChartInfo);
      this.droppedItems[this.selectedItemIndex].config.barChartData.barChartInfo = {};
      this.droppedItems[this.selectedItemIndex].config.barChartData.barChartInfo = cloneChartInfo;
    }
  }


  downloadExcel(id) {
    console.log(id);
    let eData = [];
    if (id === this.chartType.PIECHART) {
      eData = this.droppedItems[this.selectedItemIndex].config.pieChartData;
    } else if (id === this.chartType.BARCHART) {
      eData = this.droppedItems[this.selectedItemIndex].config.barChartData;
    }
    this.excelData = this.dynamicDashboardComponentService.getExcelHeaderData(id, eData);
    /* generate worksheet */
    const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(this.excelData);
    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    /* save to file */
    const wbout: string = XLSX.write(wb, this.wopts);
    saveAs(new Blob([s2ab(wbout)]), id + '_' + moment().format('YYYYMMDDHHMM') + '.xlsx');
  }

  generateExportTable(headers) {
    const table = document.createElement('table');
    const thead = document.createElement('thead');
    const theadTR = document.createElement('tr');
    headers.forEach(element => {
      let td = document.createElement('td');
      td.textContent = element;
      theadTR.appendChild(td);
    });
    thead.appendChild(theadTR)
    table.appendChild(thead);
    return table;
  }

  openFileBox(box) {
    this.selectedChart = box;
    document.getElementById('excel-file').click();
  }

  uploadExcel(evt: any) {
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (this.checkfileType(evt.target)) {
      if (target.files.length !== 1) throw new Error('Cannot use multiple files');
      const reader: FileReader = new FileReader();
      reader.onload = (e: any) => {
        /* read workbook */
        const bstr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

        /* grab first sheet */
        const wsname: string = wb.SheetNames[0];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];

        /* save data */
        this.excelData = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
        this.updateChartValue(this.selectedChart);
      };
      reader.readAsBinaryString(target.files[0]);
    }
    (<HTMLInputElement>document.getElementById('excel-file')).value = '';
  }

  setExcelDataForChart(data, previousData, chartInfoType) {
    let cloneChartData = jQuery.extend(true, [], previousData[0]);
    let cloneChartInfo = jQuery.extend(true, {}, previousData[1]);
    let filterObject;
    let isExcelError = false;
    let filterObjectList = [];
    let colorScheme = [];
    _.each(data.data, (excelData, index) => {
      filterObject = _.find(previousData.data, (pieChart, index) => { return excelData[0] === pieChart.name; });
      if (!filterObject) {
        isExcelError = true;
        this.showErrorMessage('Invalid Data...' + '  ' + excelData[0]);
      } else {
        filterObjectList.push(excelData);
      }
    });
    if (isExcelError) {
      previousData.data = cloneChartData;
    } else {
      cloneChartData = [];
      _.each(previousData.data, (pieChart, index) => {
        const name = pieChart.name;
        pieChart.name = filterObjectList[index][1];
        colorScheme.push(filterObjectList[index][2]);
        pieChart.allies = name;
        cloneChartData.push(pieChart);
      });
      previousData.data = [];
      previousData.data = cloneChartData;
      previousData[chartInfoType] = {};
      cloneChartInfo.data = {};
      _.each(data.chartInfo, (value, key) => {
        cloneChartInfo.data[key] = value;
      });
      cloneChartInfo.colorScheme = {};
      cloneChartInfo.colorScheme.domain = colorScheme;
      previousData[chartInfoType] = cloneChartInfo;
      console.log(previousData);
      return previousData;
    }
  }

  updateChartValue(chart) {
    if (chart.id === this.chartType.PIECHART) {
      const data = this.dynamicDashboardComponentService.updateChartValue(this.selectedChart,
        this.excelData,
        this.droppedItems[this.selectedItemIndex].config.pieChartData,
        this.selectedItemIndex,
        chart.id);
      this.droppedItems[this.selectedItemIndex].config.pieChartData = this.setExcelDataForChart(data, this.droppedItems[this.selectedItemIndex].config.pieChartData, 'pieChartInfo');
    } else if (chart.id === this.chartType.BARCHART) {
      const data = this.dynamicDashboardComponentService.updateChartValue(this.selectedChart,
        this.excelData,
        this.droppedItems[this.selectedItemIndex].config.barChartData,
        this.selectedItemIndex,
        chart.id);
      const cloneData = jQuery.extend(true, [], this.droppedItems[this.selectedItemIndex].config.barChartData);
      this.droppedItems[this.selectedItemIndex].config.barChartData = this.setExcelDataForChart(jQuery.extend(true, [], data), jQuery.extend(true, [], cloneData), 'barChartInfo');
    } else if (chart.id === this.chartType.CURVECHART) {
      let clone = jQuery.extend(true, {}, this.droppedItems[this.selectedItemIndex].config.linechartData);
    } else if (chart.id === this.chartType.BUBBLECHART) {
      let clone = jQuery.extend(true, {}, this.droppedItems[this.selectedItemIndex].config.bubblechartData);
    } else if (chart.id === this.chartType.AREAFILLEDCHART) {
      let clone = jQuery.extend(true, {}, this.droppedItems[this.selectedItemIndex].config.areafilledchartData);
    }
  }

  checkfileType(sender) {
    var fileExt = sender.value;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (this.validExts.indexOf(fileExt) < 0) {
      this.showErrorMessage("Invalid file selected, valid files are of " +
        this.validExts.toString() + " types.");
      return false;
    }
    else return true;
  }

  updateColorOptions(event) {
    this.colorOptions = event;
  }

}

