import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { FormsModule } from '@angular/forms';
import { DynamicDashboardComponent } from './dynamic-dashboard.component';
import { DynamicDashboardService } from './dynamic-dashboard.service';
import { DynamicDashboardComponentService } from './dynamic-dashboard.component.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-bootstrap';
import { SharedModule } from '../../shared-module/shared-module.module';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { PointsNamePipe } from './dynamic-dashboard-points.pipe';
import { NgDragDropModule } from 'ng-drag-drop';
import { NgGridModule } from 'angular2-grid';
import { ColorPickerModule } from 'angular4-color-picker';
import { CoreModule } from '../../core';
import { ItemEditOptionsComponent } from './item-edit-options/item-edit-options.component';
import { ItemContentComponent } from './item-content/item-content.component';
import { ContentThemeComponent } from './content-theme/content-theme.component';
import { DynamicDashboardRoutingModule } from './dynamic-dashboard-routing.module';
import { TranslateModule, TranslateLoader, TranslateCompiler, TranslateParser, MissingTranslationHandler } from "@ngx-translate/core";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { HttpLoaderFactory } from "../../app.module";
declare var require: any;

@NgModule({
  imports: [
    CommonModule,
    SimpleNotificationsModule,
    FormsModule,
    NgbModule,
    SharedModule,
    AngularSvgIconModule,
    CoreModule,
    NgGridModule,
    ColorPickerModule,
    NgDragDropModule.forRoot(),
    ModalModule.forRoot(),
    DynamicDashboardRoutingModule,
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    DynamicDashboardComponent,
    PointsNamePipe,
    ItemEditOptionsComponent,
    ItemContentComponent,
    ContentThemeComponent
  ],
  exports: [
    DynamicDashboardComponent
  ],
  providers: [
    DynamicDashboardService,
    DynamicDashboardComponentService
  ]
})

export class DynamicDashboardModule { }
