import { Injectable } from '@angular/core';
import { HttpService } from './../../shared-module';
import { Observable } from 'rxjs/Observable';
import { Company } from '../../../app';
import * as moment from 'moment';
import {
  Http,
  ConnectionBackend,
  RequestOptions,
  RequestOptionsArgs,
  Response,
  Headers
} from '@angular/http';

@Injectable()
export class DynamicDashboardService {
  constructor(public httpService: HttpService, public http: Http) { }
  
    getAssetDesc(companyId: string): Observable<any> {
      return this.httpService.get(`device/api/iot/get/${companyId}`)
        .map(res => res.json())
        .catch(err => Observable.throw(err));
    }
  
    getPointNameForAsset(obj: { companyId: string, gwSno: string, assetId: string }): Observable<any> {
      return this.httpService.get(encodeURI(`device/api/iot/getdevice?companyId=${obj.companyId}&gwSno=${obj.gwSno}&assetId=${obj.assetId}`))
        .map(res => res.json())
        .catch(err => Observable.throw(err));
    }

}