import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { chartType, ngGridConfig, pieChartDefaultData } from '../dynamic-dashboard-constant';
import { NgGrid, NgGridItem, NgGridConfig, NgGridItemConfig, NgGridItemEvent } from 'angular2-grid';
import { AppService } from '../../../app.service';
import * as _ from 'underscore';
import * as jQuery from 'jquery';


@Component({
  selector: 'app-item-content',
  templateUrl: './item-content.component.html',
  styleUrls: ['./item-content.component.scss']
})
export class ItemContentComponent implements OnInit {

  @Input() ItemIndex;
  @Input() item;
  @Input() droppedItems = [];
  @Input() selectedColumns;
  @Output() editFunctionCall = new EventEmitter();
  @Output() onTableHeaderDropChild = new EventEmitter();

  public chartType = chartType;
  public uploadedImages = [];
  public gridConfig: NgGridConfig = <NgGridConfig>ngGridConfig;
  public selectedTableHeader: any;
  public editOptionExpand: Boolean = false;

  constructor(private appService: AppService) {
  }

  ngOnInit() {
    console.log(this.item);
    //console.log(this.item.config.barChartData.barChartInfo);
    document.getElementsByTagName('body')[0].addEventListener('click', () => {
      this.editOptionExpand = false;
    });
  }

  onFileChange(event) {
    if (event.target.files && event.target.files[0]) {
      this.uploadedImages[this.ItemIndex] = event.target.files[0];
      setTimeout(() => {
        this.previewImage(this.ItemIndex, event.target.files[0], 'thumbnail');
      }, 100);
    }
  }

  uploadLogo() {
    document.getElementById('uploadLogo' + this.ItemIndex).click();
  }

  uploadIconViewLogo(event) {
    if (event.target.files && event.target.files[0]) {
      this.droppedItems[this.ItemIndex].files = event.target.files[0];
      setTimeout(() => {
        this.previewImage(0, event.target.files[0], 'headerThumbnail');
      }, 100);
    }
  }

  previewImage(index, file, name) {
    const preview = <HTMLImageElement>document.getElementById(name + index);
    const reader = new FileReader();
    reader.onloadend = function () {
      preview.src = reader.result;
    }
    if (file) {
      reader.readAsDataURL(file);
    } else {
      preview.src = "";
    }
  }

  dragDropTableHeader(event: any, data) {
    event.stopPropagation();
    this.selectedTableHeader = data;
  }

  onTableHeaderDrop(event: any) {
    if (this.selectedColumns) {
      this.onTableHeaderDropChild.emit(event);
    } else {
      let targetElement = event.nativeEvent.target || event.nativeEvent.srcElement || event.nativeEvent.currentTarget;
      const div = targetElement.parentElement.parentElement.parentElement.parentElement.getAttribute('id');
      if (!div) {
        return;
      }
      let currentIndex = Number(div.replace('table', ''));
      const id = event.nativeEvent.target.getAttribute('id').split('-');
      if (Number(id[1]) !== this.selectedTableHeader.tableIndex) {
        this.showErrorMessage('Can not move to different table...');
      } else {
        const targetHeader = _.where(this.selectedColumns[currentIndex], { assetIndex: Number(id[2]), pointIndex: Number(id[3]) });
        const clone = jQuery.extend(true, {}, this.selectedTableHeader);
        const cloneTarget = jQuery.extend(true, {}, targetHeader[0]);
        cloneTarget.headerIndex = this.selectedTableHeader.headerIndex;
        cloneTarget.pointIndex = this.selectedTableHeader.pointIndex;
        cloneTarget.assetIndex = this.selectedTableHeader.assetIndex;
        clone.headerIndex = targetHeader[0].headerIndex;
        clone.pointIndex = targetHeader[0].pointIndex;
        clone.assetIndex = targetHeader[0].assetIndex;
        this.selectedColumns[currentIndex][targetHeader[0].headerIndex] = clone;
        this.selectedColumns[currentIndex][this.selectedTableHeader.headerIndex] = cloneTarget;
      }
    }
  }

  toggleEditOptionsExpands() {
    event.stopPropagation();
    this.editFunctionCall.emit({
      data : {
        index : this.ItemIndex
      }
    });
    this.editOptionExpand = !this.editOptionExpand;
  }

  removePointNameFromChart(index, id) {
    if (id === this.chartType.PIECHART) {
      let clone = jQuery.extend(true, [], this.item.config.pieChartData.data);
      this.item.config.pieChartData.data = [];
      clone.splice(index, 1);
      if (clone.length === 0) {
        this.item.isDataAvailable = false;
        this.item.config.pieChartData.data = pieChartDefaultData;
      } else {
        this.item.config.pieChartData.data = clone;
      }
    }
  }

  showErrorMessage(msg) {
    this.appService.showErrorMessage(
      'Error',
      msg
    );
  }
}
