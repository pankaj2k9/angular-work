import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemEditOptionsComponent } from './item-edit-options.component';

describe('ItemEditOptionsComponent', () => {
  let component: ItemEditOptionsComponent;
  let fixture: ComponentFixture<ItemEditOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemEditOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemEditOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
