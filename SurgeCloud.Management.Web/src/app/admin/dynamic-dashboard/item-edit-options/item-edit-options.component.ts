import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-item-edit-options',
  templateUrl: './item-edit-options.component.html',
  styleUrls: ['./item-edit-options.component.scss']
})
export class ItemEditOptionsComponent implements OnInit {

  @Input() ItemIndex;
  @Input() item;
  @Input() droppedItems = [];
  @Output() editFunctionCall = new EventEmitter();
  public editOptions = [];

  constructor() {
  }

  ngOnInit() {
    document.getElementById('dynamic-dashboard').addEventListener('click', () => {
      this.editOptions = [];
    });
  }

  toggleEditOptions(event) {
    event.stopPropagation();
    let expand = this.editOptions[this.ItemIndex];
    this.editOptions = [];
    this.editOptions[this.ItemIndex] = !expand;
    this.editFunctionCall.emit({
      type : 'onSelect',
      data : {
        index : this.ItemIndex,
        item : this.item
      }
    });
  }

  toggleIconView() {
    if (!this.droppedItems[this.ItemIndex].isIconView) {
      document.getElementById(this.item.id + 'handle' + this.ItemIndex).parentElement.style.height = '0px';
    } else {
      document.getElementById(this.item.id + 'handle' + this.ItemIndex).parentElement.style.height = 'auto';
    }
    this.droppedItems[this.ItemIndex].isIconView = !this.droppedItems[this.ItemIndex].isIconView;
  }

  openHeaderEditModal() {
    document.getElementById('editHeader').classList.add('in');
    document.getElementById('headerName').setAttribute('value', this.item.name);
  }

  closeHeaderEditModal() {
    document.getElementById('editHeader').classList.remove('in');
  }

  saveHeaderNameForBox() {
    this.droppedItems[this.ItemIndex].name = (<HTMLInputElement>document.getElementById('headerName')).value;
    this.closeHeaderEditModal();
  }

  downloadExcel() {
    this.editFunctionCall.emit({
      type : 'downloadExcel',
      data : {
        index : this.ItemIndex,
        id : this.item.id
      } 
    });
  }

  openFileBox() {
    this.editFunctionCall.emit({
      type : 'uploadExcel',
      data : {
        item : this.item
      } 
    });
  }

  removeWidget() {
    if (this.droppedItems[this.ItemIndex]) {
      this.droppedItems.splice(this.ItemIndex, 1);
      // this.selectedColumns.splice(index, 1);
    }
  }

}
