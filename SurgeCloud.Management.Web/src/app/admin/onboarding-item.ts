import { Type } from '@angular/core';
export class OnboardingItem {
    constructor(public component: Type<any>, public data: any) { }
};
