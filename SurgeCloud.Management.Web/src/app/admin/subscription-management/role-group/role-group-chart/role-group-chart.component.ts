import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'app-role-group-chart',
	templateUrl: './role-group-chart.component.html',
	styleUrls: ['./role-group-chart.component.scss']
})
export class RoleGroupChartComponent implements OnInit {

	@Input() roleGroupCount: { active: number, inActive: number };

	constructor() { }

	ngOnInit() {
	}

	onSelect(event) {
  }

}
