import { Component, OnInit, Input } from '@angular/core';
import { RoleGroup } from '../role-group';

@Component({
  selector: 'app-role-group-image',
  templateUrl: './role-group-image.component.html',
  styleUrls: ['./role-group-image.component.scss']
})
export class RoleGroupImageComponent implements OnInit {

  @Input() roleGroup: RoleGroup[];

  constructor() { }

  ngOnInit() {
  }

}
