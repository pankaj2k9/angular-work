import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleGroupTableComponent } from './role-group-table.component';

describe('RoleGroupTableComponent', () => {
  let component: RoleGroupTableComponent;
  let fixture: ComponentFixture<RoleGroupTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleGroupTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleGroupTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
