import { Component, OnInit, Input, EventEmitter, OnDestroy } from '@angular/core';
import { RoleGroup } from '../role-group';
import { SubscriptionManagementService } from '../../subscription-management.service';
import { CustomerOnboardingService } from '../../../customer-onboarding';
import { Router } from '@angular/router';
import { User } from '../../user/user';
import { AppService } from '../../../../../app';
import { RoleGroupService } from '../role-group.service';
import { Subscription } from "rxjs/Subscription";
import { ArrayFilterPipe } from '../../../../shared-module';
import * as jQuery from 'jquery';

@Component({
  selector: 'app-role-group-table',
  templateUrl: './role-group-table.component.html',
  styleUrls: ['./role-group-table.component.scss']
})
export class RoleGroupTableComponent implements OnInit, OnDestroy {

  @Input() roleGroup: RoleGroup[];
  roleGroupList: RoleGroup[];
  userView = false;

  user: User[] = [];

  groupId: string;
  roleLogo: string;
  public mainBodyHeight: any;
  private paginationOrgInfo = {
    roleGroupsortOrder: 'asc', roleGroupsortBy: 'name',
    numberRowPerPage: 5
  };
  private paginationUserInfo = {
    usersortOrder: 'asc', usersortBy: 'displayName',
    numberRowPerPage: 10
  };

  public statusClass: string[] = [
    'inactive',
    'moving'
  ];

  toggleFilterSearch: boolean = false;
  filterValues = {
    'name': '',
    'count': '',
    'status': ''
  };
  public filterWidthData = [{ 'width': 50, 'id': 'name', 'prm': true },
  { 'width': 25, 'id': 'count', 'prm': true },
  { 'width': 25, 'id': 'status', 'prm': true }];
  pieChartClickSubscription: Subscription;
  private arrayFilterPipe: ArrayFilterPipe = new ArrayFilterPipe();
  constructor(private subscriptionManagementService: SubscriptionManagementService,
    public customerOnboardingService: CustomerOnboardingService, private router: Router,
    private appService: AppService, private roleGroupService: RoleGroupService
  ) {
    this.paginationOrgInfo.numberRowPerPage = this.subscriptionManagementService.numberOfRecordPerPage;
    this.roleLogo = 'https://bsdevst01.blob.core.windows.net/images/default_role_group.png';
    this.pieChartClickSubscription = this.appService.pieChartClick.subscribe((data: object) => {
      //const status = data['name'] == 'Active' ? 1: 0;
      //console.log(this.roleGroupList);
      this.roleGroup = this.arrayFilterPipe.transform(this.roleGroupList, { name: data['name'] });
      //console.log(this.roleGroup);
    });
  }

  ngOnInit() {
    this.groupId = this.appService.getActiveCompany().groupId;
    this.appService.activeCompanyInfo.subscribe((company) => {
      if (company) {
        this.userView = false;
        this.groupId = company.groupId;
      }
    });
    this.roleGroupList = this.roleGroup;
    this.appService.setAllOptionPagination('tmRoleGroupOverviewTable');
    this.resetFilterData();
    this.mainBodyHeight = jQuery(document).height() - 392;
  }
  ngOnDestroy() {
    this.pieChartClickSubscription.unsubscribe();
  }
  editRole(role) { // redirect to role page
    this.customerOnboardingService.onboardEdit = { organizationId: null, userId: null, for: 3, roleGroup: role };
    this.appService.ManagementAddEdit = {
      'value': true,
      'id': 'role_group_overview'
    };
    this.router.navigate(['/admin/customer-onboarding']);
  }
  gotoUser(roleId: string) {
    if (roleId) {

      this.userView = true;
      this.roleGroupService.getUser(this.groupId, roleId).subscribe(
        (data) => {
          this.user = data;
        });
    } else {
      this.user = [];
    }
  }

  resetFilterData() {
    this.toggleFilterSearch = false;
    this.filterValues = {
      'name': '',
      'count': '',
      'status': ''
    };
  }

  updateData(event) {
    this.roleGroup = event;
    this.appService.appExportData.next(event);
    this.appService.setAllOptionPagination('tmRoleGroupOverviewTable');
  }
}
