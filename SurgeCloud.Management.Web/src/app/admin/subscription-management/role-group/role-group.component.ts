import { Component, OnInit, ViewChild, ComponentFactoryResolver, Input } from '@angular/core';
import { RoleGroupService } from './role-group.service';
import { RoleGroup } from './role-group';
import { AppService } from '../../../../app';
import { ArrayFilterPipe } from '../../../shared-module';
import { RoleGroupTableComponent } from './';
import { RoleGroupImageComponent } from './';
import { RoleGroupDirective } from './role-group.directive';
import { RoleGroupItem } from './role-group-item';
import { IWizardItem } from '../../../shared-module';
import {PieChartComponent, PieChatConfig, SingleChartData} from '../../../core';
import { CustomerOnboardingService } from '../../customer-onboarding';
import { Router } from '@angular/router';

@Component({
  selector: 'app-role-group',
  templateUrl: './role-group.component.html',
  styleUrls: ['./role-group.component.scss']
})

export class RoleGroupComponent implements OnInit {
  pieChartConfigOptions: PieChatConfig;
  roleGroup: RoleGroup[] = [];

  ArrayFilterPipe: ArrayFilterPipe = new ArrayFilterPipe();

  roleGroupItem: RoleGroupItem[] = [
    new RoleGroupItem(RoleGroupTableComponent, [])];

  currentIndex = 0;

  public wizardItems: IWizardItem[] = [
    {
      name: 'RoleGroup Add',
      icon: 'plus-icon',
      disabled: false,
      index: 1,
      success: false,
      error: false,
      active: false
    }
  ];
  roleChartData: SingleChartData[] = [];
  lastUpdated: number;
  public headers = [{
    'title': 'Name',
    'dataKey': 'name',
    'width': 300
  }, {
    'title': 'Count',
    'dataKey': 'count',
    'width': 200
  }, {
    'title': 'Status',
    'dataKey': 'status',
    'width': 100
  }];

  @ViewChild(RoleGroupDirective) roleGroupHost: RoleGroupDirective;

  constructor(private roleGroupService: RoleGroupService, public customerOnboardingService: CustomerOnboardingService,
    private componentFactoryResolver: ComponentFactoryResolver, private appService: AppService, private router: Router) {
    const colorScheme = { domain: ['#94d67e', '#ee7f32', '#ef4846', '#6f7588', '#eb3c33', '#5acbf9', '#cf4385',
        '#935bcd', '#f3af2f', '#a6d9fd', '#aaaaaa', '#f17cb0', '#b2912f', '#b276b2', '#decf3f' ] };
    this.pieChartConfigOptions = new PieChatConfig(
      [200, 200],
      true,
      colorScheme,
      false,
      false,
      false,
      false,
      true,
      true
    );
  }

  ngOnInit() {
    this.getRoleGroup(this.appService.getActiveCompany().companyId);
    this.appService.activeCompanyInfo.subscribe((company) => {
      if (company) {
        this.getRoleGroup(company.companyId);
      }
    })
  }

  getRoleGroup(companyId: string) {
    this.lastUpdated = Date.now();
    this.roleGroupService.getRoleGroup(companyId).subscribe(
      (data) => {

        this.roleGroup = data ? data : [];
        if (this.roleGroup.length > 0) {
          this.roleChartData = this.roleGroup.map(role => {
            let newObj = Object.assign({});
            newObj = { 'name': role.name, 'value': role.count };
            return newObj;
          });
        }
        this.loadComponent();
      }
    );
  }

  loadComponent() {
    const boardingItem = this.roleGroupItem[this.currentIndex];
    boardingItem.roleGroup = this.roleGroup;

    const componentFactory = this.componentFactoryResolver
      .resolveComponentFactory(boardingItem.component);

    const viewContainerRef = this.roleGroupHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);
    (<{ roleGroup: any }>componentRef.instance).roleGroup = boardingItem.roleGroup;

  }

  changeComponent(index) {
    if (index === 0) {
      this.wizardItems[this.currentIndex].active = false;
      this.currentIndex = index;
      this.wizardItems[this.currentIndex].active = true;
      this.loadComponent();
    } else if (index === 1) { // redirect add role page
      this.customerOnboardingService.onboardNew = { organizationId: null, userId: null, for: 3, roleGroup: null };
      this.appService.changeRoute = this.router.url;
      this.appService.ManagementAddEdit = {
        'value': true,
        'id': 'role_group_overview'
      };
      this.router.navigate(['/admin/customer-onboarding']);
    }
  }
  applyStatusFilter(filterData: object) {
    this.appService.emitEventOnPieChartClick(filterData);
  }
}
