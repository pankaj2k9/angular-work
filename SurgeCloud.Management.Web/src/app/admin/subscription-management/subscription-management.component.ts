import { Component, OnInit, ViewChild } from '@angular/core';
import { IWizardItem, TabsComponent } from '../../shared-module';
import { RoleGroupTableComponent } from './role-group';
import { CustomerOnboardingService } from '../customer-onboarding';
import { Router } from '@angular/router';
import { AppService } from '../../app.service';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-subscription-management',
  templateUrl: './subscription-management.component.html',
  styleUrls: ['./subscription-management.component.scss']
})
export class SubscriptionManagementComponent implements OnInit {
  public wizardItems: IWizardItem[] = [
    {
      name: 'User Overview',
      icon: 'assets/icons/CustomerOnboarding/icon_CustomerOnboarding_CreateUser.svg#Vector_Smart_Object',
      activeIcon: 'assets/icons/CustomerOnboarding/icon_CustomerOnboarding_CreateUserWhite.svg#Vector_Smart_Object',
      success: false,
      active: true,
      statusIcon: ''
    },
    {
      name: 'Role Group Overview',
      icon: 'assets/icons/CustomerOnboarding/icon_CustomerOnboarding_CreateRoleGroup.svg#Vector_Smart_Object',
      activeIcon: 'assets/icons/CustomerOnboarding/icon_CustomerOnboarding_CreateRoleGroupWhite.svg#Vector_Smart_Object',
      success: false,
      active: false,
      statusIcon: ''
    },
  ];
  currentIndex = 0;
  @ViewChild(RoleGroupTableComponent) roleGroupTableComponent: RoleGroupTableComponent;
  @ViewChild('tab') tab;

  constructor(
    public customerOnboardingService: CustomerOnboardingService,
    public appService: AppService,
    public router: Router,private translate: TranslateService) { }

  ngOnInit() {
    this.appService.changeRoute = this.router.url;
    this.customerOnboardingService.onboardEdit = { organizationId: null, userId: null, for: null, roleGroup: null };
    this.checkIsAddEditSubscription();
    // this.roleGroupTableComponent.moveToUser.subscribe(
    //   ()=>{
    //     this.tab.select('user_overview');
    //   }
    // )
    this.translate.stream("sm_user_overview").subscribe(res => {
      if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
        this.wizardItems[0].name = res;
      }
    });
    this.translate.stream("sm_role_overview").subscribe(res => {
      if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
        this.wizardItems[1].name = res;
      }
    });

  }

  checkIsAddEditSubscription() {
    if (this.appService.ManagementAddEdit.value) {
      this.appService.ManagementAddEdit.value = false;
      setTimeout(() => {
        if(document.getElementById(this.appService.ManagementAddEdit.id)) {
          document.getElementById(this.appService.ManagementAddEdit.id).click();
        }
      })
    }
  }

  beforeChange($event) {
    switch ($event.nextId) {
      case 'user_overview':
        this.wizardItems[this.currentIndex].active = false;
        this.wizardItems[0].active = true;
        this.currentIndex = 0;
        break;
      case 'role_group_overview':
        this.wizardItems[this.currentIndex].active = false;
        this.wizardItems[1].active = true;
        this.currentIndex = 1;
        break;

    }
  }

}
