import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { FormsModule } from '@angular/forms';
import { SubscriptionManagementComponent } from './subscription-management.component';
import { SubscriptionManagementService } from './subscription-management.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { UserModule, UserComponent } from './user';
import { RoleGroupModule, RoleGroupComponent } from './role-group';
import {AngularSvgIconModule} from "angular-svg-icon";
import {SubscriptionManagementRoutingModule} from "./subscription-management-routing.module";
import {TranslateModule,TranslateLoader,TranslateCompiler,TranslateParser,MissingTranslationHandler} from "@ngx-translate/core";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {HttpLoaderFactory} from "../../app.module";

@NgModule({
  imports: [
    SubscriptionManagementRoutingModule,
    CommonModule,
    SimpleNotificationsModule,
    FormsModule,
    NgbModule,
    UserModule,
    RoleGroupModule,
    AngularSvgIconModule,HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    SubscriptionManagementComponent,
  ],
  exports: [
    SubscriptionManagementComponent
  ],
  providers: [
    SubscriptionManagementService
  ],
  entryComponents : [
    UserComponent,
    RoleGroupComponent
  ]
})

export class SubscriptionManagementModule { }
