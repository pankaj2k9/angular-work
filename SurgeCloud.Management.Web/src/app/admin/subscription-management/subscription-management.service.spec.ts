import { TestBed, inject } from '@angular/core/testing';

import { SubscriptionManagementService } from './subscription-management.service';

describe('SubscriptionManagementService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SubscriptionManagementService]
    });
  });

  it('should ...', inject([SubscriptionManagementService], (service: SubscriptionManagementService) => {
    expect(service).toBeTruthy();
  }));
});
