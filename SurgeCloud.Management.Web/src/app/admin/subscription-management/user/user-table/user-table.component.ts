import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { User } from '../user';
import { AppService } from '../../../../app.service';
import { SubscriptionManagementService } from '../../subscription-management.service';
import { CustomerOnboardingService } from '../../../customer-onboarding';
import { Router } from '@angular/router';
import { ArrayFilterPipe } from '../../../../shared-module';
import { Subscription } from "rxjs/Subscription";
import * as jQuery from 'jquery';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.scss']
})
export class UserTableComponent implements OnInit, OnDestroy {

  @Input() data: { userList: User[], filterObject: any };
  user: User[];
  private arrayFilterPipe: ArrayFilterPipe = new ArrayFilterPipe();
  private paginationOrgInfo = {
    usersortOrder: 'asc', usersortBy: 'displayName',
    numberRowPerPage: 5
  };

  public statusClass: string[] = [
    'inactive',
    'moving'
  ];

  filterValues: {};
  toggleFilterSearch: boolean = false;
  public mainBodyHeight: any;
  public filterWidthData = [{ 'width': 30, 'id': 'displayName', 'prm': true },
  { 'width': 30, 'id': 'jobTitle', 'prm': true },
  { 'width': 30, 'id': 'emailId', 'prm': true },
  { 'width': 10, 'id': 'isActive', 'prm': true }];
  pieChartClickSubscription: Subscription;
  constructor(private subscriptionManagementService: SubscriptionManagementService, private appService: AppService,
    public customerOnboardingService: CustomerOnboardingService, private router: Router) {
    this.paginationOrgInfo.numberRowPerPage = this.subscriptionManagementService.numberOfRecordPerPage;
    this.pieChartClickSubscription = this.appService.pieChartClick.subscribe((data: object) => {
      const status = data['name'] == 'Active' ? 1 : 0;
      this.user = this.arrayFilterPipe.transform(this.data.userList, { status: status });
    });
  }

  ngOnInit() {

    if (this.data.filterObject && this.data.userList.length > 0) {
      let filter;
      if (typeof this.data.userList[0].isActive === 'number') {
        filter = this.data.filterObject;
      } else {
        filter = { isActive: this.data.filterObject.isActive === 0 ? false : true };
      }

      this.user = this.arrayFilterPipe.transform(this.data.userList, filter);
    } else {
      this.user = this.data.userList;
    }
    this.appService.setAllOptionPagination('smUserOverviewTable');
    this.resetFilterData();
    this.mainBodyHeight = jQuery(document).height() - 392;
  }
  ngOnDestroy() {
    this.pieChartClickSubscription.unsubscribe();
  }
  editUser(user: any) {
    this.customerOnboardingService.onboardEdit = {
      organizationId: this.appService.getActiveCompany().companyId
      , userId: user.userId, for: 2
    };

    this.customerOnboardingService.adminEditUserProfile = true;
    this.customerOnboardingService.userEditOwnProfile = false;

    this.router.navigate(['/admin/customer-onboarding']);
  }

  resetFilterData() {
    this.toggleFilterSearch = false;
    this.filterValues = {
      'displayName': '',
      'jobTitle': '',
      'emailId': '',
      'isActive': ''
    };
  }

  updateData(event) {
    this.user = event;
    this.appService.appExportData.next(event);
    this.appService.setAllOptionPagination('smUserOverviewTable');
  }
}
