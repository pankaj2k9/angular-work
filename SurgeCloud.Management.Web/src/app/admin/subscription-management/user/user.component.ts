import { Component, OnInit, ViewChild, ComponentFactoryResolver, Input } from '@angular/core';
import { UserService } from './user.service';
import { User } from './user';
import { AppService } from '../../../../app';
import { ArrayFilterPipe } from '../../../shared-module';
import { UserTableComponent } from './';
import { UserImageComponent } from './';
import { UserDirective } from './user.directive';
import { UserItem } from './user-item';
import { IWizardItem } from '../../../shared-module';
import { CustomerOnboardingService } from '../../customer-onboarding';
import { Router } from '@angular/router';
import * as _ from 'underscore';
import { PieChartComponent, PieChatConfig, SingleChartData } from '../../../core';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})

export class UserComponent implements OnInit {
  pieChartConfigOptions: PieChatConfig;
  user: User[] = [];

  ArrayFilterPipe = new ArrayFilterPipe();

  userCount: { active: number, inActive: number } = { active: 0, inActive: 0 };

  userItem: UserItem[] = [
    new UserItem(UserTableComponent, {}),
    new UserItem(UserImageComponent, {})];

  currentIndex = 0;
  public headers = [{
    'title': 'Display Name',
    'dataKey': 'displayName',
    'width': 159
  }, {
    'title': 'Job Title',
    'dataKey': 'jobTitle',
    'width': 150
  }, {
    'title': 'Email ID',
    'dataKey': 'emailId',
    'width': 200
  }, {
    'title': 'Status',
    'dataKey': 'status',
    'width': 100
  }];
  public wizardItems: IWizardItem[] = [
    {
      name: 'User Table',
      icon: 'glyphicon-th-list glyphicon gi-1-5x',
      disabled: false,
      index: 0,
      success: false,
      error: false,
      active: true
    },
    {
      name: 'User Image',
      icon: 'glyphicon-th glyphicon gi-1-5x',
      disabled: false,
      index: 1,
      success: false,
      error: false,
      active: false
    },
    {
      name: 'User Add',
      icon: 'plus-icon',
      disabled: false,
      index: 2,
      success: false,
      error: false,
      active: false
    }
  ];

  @ViewChild(UserDirective) userHost: UserDirective;
  filterObject: any = null;
  lastUpdated: number;
  public chartData: SingleChartData[] = [];
  public filterStatus: boolean = this.filterObject === null ? false : true;

  constructor(private userService: UserService, public customerOnboardingService: CustomerOnboardingService,
    private componentFactoryResolver: ComponentFactoryResolver, private appService: AppService,
    private router: Router,private translate: TranslateService) {
    const colorScheme = {
      domain: ['#94d67e', '#ee7f32', '#ef4846', '#6f7588', '#eb3c33', '#5acbf9', '#cf4385',
        '#935bcd', '#f3af2f', '#a6d9fd', '#aaaaaa', '#f17cb0', '#b2912f', '#b276b2', '#decf3f']
    };
    this.pieChartConfigOptions = new PieChatConfig(
      [200, 200],
      true,
      colorScheme,
      false,
      false,
      false,
      false,
      true,
      true
    );
  }

  ngOnInit() {
    if (this.appService.getActiveCompany()) {
      this.getUser(this.appService.getActiveCompany().groupId);
    }
    this.appService.activeCompanyInfo.subscribe((company) => {
      if (company) {
        this.getUser(company.groupId);
      }
    });
  }

  getUser(groupId: string) {
    this.lastUpdated = Date.now();
    this.user = [];

    this.userService.getUser(groupId).subscribe(
      (data) => {
        data.forEach(element => {
          element.status = element.isActive;
        });
        this.user = data;
        this.chartData = [];
        if (this.user.length > 0) {
          const activeCount = (Math.ceil((this.ArrayFilterPipe.transform(this.user, { isActive: 1 }).length / this.user.length) * 100) +
            (Math.ceil((this.ArrayFilterPipe.transform(this.user, { isActive: true }).length / this.user.length) * 100)));
          const inactiveCount = (Math.ceil((this.ArrayFilterPipe.transform(this.user, { isActive: 0 }).length / this.user.length) * 100)) +
            (Math.ceil((this.ArrayFilterPipe.transform(this.user, { isActive: false }).length / this.user.length) * 100));

          // this.userCount = {
          //   active: activeCount,
          //   inActive: inactiveCount
          // };
          this.chartData.push({ name: 'Active', value: activeCount },
            { name: 'Inactive', value: inactiveCount });

          this.translate.stream("generic_active").subscribe(res => {
            if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
              this.chartData[0].name = res;
            }
          });
          this.translate.stream("generic_inactive").subscribe(res => {
            if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
              this.chartData[1].name = res;
            }
          });
        }

        this.loadComponent();
      }
    );
  }

  loadComponent() {
    const boardingItem = this.userItem[this.currentIndex];
    boardingItem.data = { userList: this.user, filterObject: this.filterObject };

    const componentFactory = this.componentFactoryResolver
      .resolveComponentFactory(boardingItem.component);

    const viewContainerRef = this.userHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);
    (<{ data: any }>componentRef.instance).data = boardingItem.data;

  }

  changeComponent(index) {
    this.customerOnboardingService.selectedOrganization = null;
    if (index <= 1) {
      this.wizardItems[this.currentIndex].active = false;
      this.currentIndex = index;
      this.wizardItems[this.currentIndex].active = true;
      this.loadComponent();
    } else if (index === 2) { // redirect to create user page
      this.appService.changeRoute = this.router.url;
      this.appService.ManagementAddEdit = {
        'value': true,
        'id': 'user_overview'
      };
      this.customerOnboardingService.onboardNew = { organizationId: null, userId: null, for: 2, roleGroup: null };
      this.router.navigate(['/admin/customer-onboarding']);
    }

  }
  applyStatusFilter(filterData: object) {
    this.appService.emitEventOnPieChartClick(filterData);
  }

}
