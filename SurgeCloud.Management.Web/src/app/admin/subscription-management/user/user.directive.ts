import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appUser]'
})
export class UserDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
