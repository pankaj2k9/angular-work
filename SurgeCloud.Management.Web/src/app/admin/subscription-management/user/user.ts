export class User {
    public id: string;
    public username: string;
    public emailId: string;
    public password: string;
    public displayName: string;
    public givenName: string;
    public surname: string;
    public jobTitle: string;
    public companyId: string;
    public companyName: string;
    public facilityId: string;
    public facilityName: string;
    public drivingLicenseNumber: string;
    public driverId: string;
    public expirationDate: any;
    public landingPage: string;
    public logo: string;
    public isActive: Boolean | number;
    public roleGroup: string;
    public phoneNumber: string;
    constructor(public data: any = {}) {
        this.id = data.userId || null;
        this.username = data.username || '';
        this.emailId = data.emailId || '';
        this.password = data.password || '';
        this.displayName = data.displayName || '';
        this.givenName = data.givenName || '';
        this.surname = data.surname || '';
        this.jobTitle = data.jobTitle || '';
        this.companyId = data.companyId || null;
        this.companyName = data.companyName || '';
        this.facilityId = data.facilityId || null;
        this.facilityName = data.facilityName || '';
        this.drivingLicenseNumber = data.drivingLicenceNumber || '';
        this.driverId = data.driverId || null;
        this.expirationDate = data.expirationDate || '';
        this.landingPage = data.landingPage || '';
        this.logo = data.logo || '';
        this.isActive =  data.isActive || 0;
        this.roleGroup = data.roleGroup || '';
        this.phoneNumber = data.phoneNumber || '';
    }
}
