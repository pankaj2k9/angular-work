import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import { Asset } from '../asset';
import { TenantManagementService } from '../../tenant-management.service';
import { Router } from '@angular/router';
import { AssetOnboardingService } from '../../../asset-onboarding';
import { ArrayFilterPipe } from '../../../../shared-module';
import { AppService } from '../../../../app.service';
import {Subscription} from "rxjs/Subscription";
import * as jQuery from 'jquery';

@Component({
  selector: 'app-asset-table',
  templateUrl: './asset-table.component.html',
  styleUrls: ['./asset-table.component.scss']
})
export class AssetTableComponent implements OnInit, OnDestroy {

  @Input() data: { assetList: Asset[], filterObject: any };

  asset: Asset[];

  private paginationOrgInfo = {
    assetsortOrder: 'asc', assetsortBy: 'assetId',
    numberRowPerPage: 5
  }
  public statusClass: string[] = [
    'inactive',
    'moving'
  ];
  toggleFilterSearch: boolean = false;
  filterValues = {};
  public filterWidthData = [{ 'width': 25, 'id': 'assetId', 'prm': true },
  { 'width': 25, 'id': 'assetType', 'prm': true },
  { 'width': 20, 'id': 'gwSNO', 'prm': true },
  { 'width': 20, 'id': 'createdDate', 'prm': true },
  { 'width': 10, 'id': 'status', 'prm': true }];

  private arrayFilterPipe: ArrayFilterPipe = new ArrayFilterPipe();
  public mainBodyHeight:any;
  pieChartClickSubscription: Subscription;
  constructor(private tenantManagementService: TenantManagementService,
    private router: Router,
    private assetOnboardingService: AssetOnboardingService,
    private appService: AppService) {
    this.paginationOrgInfo.numberRowPerPage = this.tenantManagementService.numberOfRecordPerPage;
    this.appService.setLogInUserDetails().subscribe((res) => {
    });
    this.pieChartClickSubscription = this.appService.pieChartClick.subscribe((data: object) => {
      const status = data['name'] == 'Active' ? 1: 0;
      this.asset = this.arrayFilterPipe.transform(this.data.assetList, { status: status });
    });
  }

  ngOnInit() {
    if (this.data.filterObject && this.data.assetList.length > 0) {
      let filter;
      if (typeof this.data.assetList[0].status === "number") {
        filter = this.data.filterObject;
      } else {
        filter = { status: this.data.filterObject.status === 0 ? false : true };
      }
      this.asset = this.arrayFilterPipe.transform(this.data.assetList, filter);
    }
    else {
      this.asset = this.data.assetList;
    }
    this.asset.forEach(element => {
      element['tCreatedDate'] = element.createdDate;
    });
    this.appService.setAllOptionPagination('tmAssetTable');
    this.resetFilterData();
    this.mainBodyHeight = jQuery(document).height() - 398;
  }
  ngOnDestroy() {
    this.pieChartClickSubscription.unsubscribe();
  }

  resetFilterData() {
    this.toggleFilterSearch = false;
    this.filterValues = {
      'assetId': '',
      'assetType': '',
      'gwSNO': '',
      'createdDate': '',
      'status': ''
    };
  }

  updateData(event) {
    this.asset = event;
    this.appService.appExportData.next(event);
    this.appService.setAllOptionPagination('tmAssetTable');
  }

  editAsset(asset) {
    this.assetOnboardingService.onboardingDataForEdit = { index: 0, asset: asset };
    this.appService.ManagementAddEdit = {
      'value': true,
      'id': 'asset_overview'
    };
    this.router.navigate(['/admin/asset-onboarding']);
  }


}
