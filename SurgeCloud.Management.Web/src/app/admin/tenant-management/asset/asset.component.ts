import { Component, OnInit, ViewChild, ComponentFactoryResolver, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AssetService } from './asset.service';
import { Asset } from './asset';
import { AppService } from '../../../../app';
import { ArrayFilterPipe } from '../../../shared-module';
import { AssetTableComponent } from './';
import { AssetImageComponent } from './';
import { AssetDirective } from './asset.directive';
import { AssetItem } from './asset-item';
import { IWizardItem } from '../../../shared-module';
import { AssetOnboardingService } from '../../asset-onboarding';
import * as _ from 'underscore';
import {PieChartComponent, PieChatConfig, SingleChartData} from '../../../core';
import {TranslateService} from "@ngx-translate/core";


@Component({
  selector: 'app-asset',
  templateUrl: './asset.component.html',
  styleUrls: ['./asset.component.scss']
})

export class AssetComponent implements OnInit {
  pieChartConfigOptions: PieChatConfig;
  asset: Asset[] = [];

  ArrayFilterPipe = new ArrayFilterPipe();

  assetCount: { active: number, inActive: number } = { active: 0, inActive: 0 };

  public headers = [{
    'title': 'Asset ID',
    'dataKey': 'assetId',
    'width': 150
  }, {
    'title': 'Asset Type',
    'dataKey': 'assetType',
    'width': 100
  }, {
    'title': 'Device ID',
    'dataKey': 'gwSNO',
    'width': 150
  }, {
    'title': 'Asset Created Date',
    'dataKey': 'createdDate|date',
    'width': 150
  }, {
    'title': 'Status',
    'dataKey': 'status',
    'width': 50
  }];

  assetItem: AssetItem[] = [
    new AssetItem(AssetTableComponent, {}),
    new AssetItem(AssetImageComponent, {})];

  currentIndex = 0;

  public wizardItems: IWizardItem[] = [
    {
      name: 'Asset Table',
      icon: 'glyphicon gi-1-5x glyphicon-th-list',
      disabled: false,
      index: 0,
      success: false,
      error: false,
      active: true
    },
    {
      name: 'Asset Image',
      icon: 'glyphicon gi-1-5x glyphicon-th',
      disabled: false,
      index: 1,
      success: false,
      error: false,
      active: false
    },
    {
      name: 'Asset Add',
      icon: 'plus-icon',
      disabled: false,
      index: 2,
      success: false,
      error: false,
      active: false
    }
  ];

  @ViewChild(AssetDirective) assetHost: AssetDirective;
  filterObject: any = null;
  lastUpdated: number;
  public chartData: SingleChartData[] = [];
  public filterStatus: boolean = this.filterObject === null ? false : true;
  constructor(private assetService: AssetService, public assetOnboardingService: AssetOnboardingService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private appService: AppService, private router: Router,private translate: TranslateService) {
    const colorScheme = { domain: ['#94d67e', '#ee7f32', '#ef4846', '#6f7588', '#eb3c33', '#5acbf9', '#cf4385',
        '#935bcd', '#f3af2f', '#a6d9fd', '#aaaaaa', '#f17cb0', '#b2912f', '#b276b2', '#decf3f' ] };
    this.pieChartConfigOptions = new PieChatConfig(
      [200, 200],
      true,
      colorScheme,
      false,
      false,
      false,
      false,
      true,
      false
    );
  }

  ngOnInit() {
    this.getAsset(this.appService.getActiveCompany().companyId);
    this.appService.activeCompanyInfo.subscribe((company) => {
      if (company) {
        this.getAsset(company.companyId);
      }
    })
  }

  getAsset(companyId: string) {
    this.lastUpdated = Date.now();
    this.assetService.getAsset(companyId).subscribe(
      (data) => {
        this.asset = [];
        this.chartData = [];
        data.forEach(assetInfo => {
          this.asset.push(new Asset(assetInfo));
        });
        if (this.asset.length > 0) {
          const activeCount = (Math.ceil((this.ArrayFilterPipe.transform(this.asset, { status: 1 }).length / this.asset.length) * 100)) +
            (Math.ceil((this.ArrayFilterPipe.transform(this.asset, { status: true }).length / this.asset.length) * 100));
          const inactiveCount = (Math.floor((this.ArrayFilterPipe.transform(this.asset, { status: 0 }).length / this.asset.length) * 100)) +
            (Math.floor((this.ArrayFilterPipe.transform(this.asset, { status: false }).length / this.asset.length) * 100));
          // this.assetCount = {
          //   active: activeCount,
          //   inActive: inactiveCount
          // };
          this.chartData.push({ name: 'Active', value: activeCount },
            { name: 'Inactive', value: inactiveCount });
          this.translate.stream("generic_active").subscribe(res => {
            if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
              this.chartData[0].name = res;
            }
          });
          this.translate.stream("generic_inactive").subscribe(res => {
            if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
              this.chartData[1].name = res;
            }
          });
        }

        this.loadComponent();
      }
    );
  }

  loadComponent() {
    const boardingItem = this.assetItem[this.currentIndex];
    boardingItem.data = { assetList: this.asset, filterObject: this.filterObject };

    const componentFactory = this.componentFactoryResolver
      .resolveComponentFactory(boardingItem.component);

    const viewContainerRef = this.assetHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);
    (<{ data: any }>componentRef.instance).data = boardingItem.data;

  }

  changeComponent(index) {

    if (index <= 1) {
      this.wizardItems[this.currentIndex].active = false;
      this.currentIndex = index;
      this.wizardItems[this.currentIndex].active = true;
      this.loadComponent();
    } else if (index === 2) {
      this.assetOnboardingService.onboardingDataForEdit = { index: 0, asset: null };
      this.appService.changeRoute = this.router.url;
      this.appService.ManagementAddEdit = {
        'value': true,
        'id': 'asset_overview'
      };
      this.router.navigate(['/admin/asset-onboarding']);
    }

  }
  applyStatusFilter(filterData: object) {
    this.appService.emitEventOnPieChartClick(filterData);
  }
}
