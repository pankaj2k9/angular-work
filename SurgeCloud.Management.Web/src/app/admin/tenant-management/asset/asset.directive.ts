import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appAsset]'
})
export class AssetDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
