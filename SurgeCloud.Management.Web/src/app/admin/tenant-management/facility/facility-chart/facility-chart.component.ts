import { Component, OnInit, Input,EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-facility-chart',
  templateUrl: './facility-chart.component.html',
  styleUrls: ['./facility-chart.component.scss']
})
export class FacilityChartComponent implements OnInit {

  @Input() facilityCount: {active: number, inActive: number};
  @Output() clickedChartInfo : EventEmitter<number> = new EventEmitter<number>();
  activeStatus : {active:boolean, inActive:boolean}= {active : false, inActive : false};
  constructor() { }

  ngOnInit() {
  }
  filterData(status:number){
  	
    this.activeStatus = status ===1 ? {active : true, inActive : false} : {active : false, inActive : true};
  	this.clickedChartInfo.next(status);
  }

}
