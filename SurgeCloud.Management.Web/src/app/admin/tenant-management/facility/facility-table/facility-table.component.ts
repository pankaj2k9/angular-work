import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Facility } from '../facility';
import { TenantManagementService } from '../../tenant-management.service';
import { Router } from '@angular/router';
import { AssetOnboardingService, AssetOnboardingStatus } from '../../../asset-onboarding';
import { ArrayFilterPipe } from '../../../../shared-module';
import { AppService } from '../../../../app.service';
import { Subscription } from "rxjs/Subscription";
import * as jQuery from 'jquery';

@Component({
  selector: 'app-facility-table',
  templateUrl: './facility-table.component.html',
  styleUrls: ['./facility-table.component.scss']
})
export class FacilityTableComponent implements OnInit, OnDestroy {

  @Input() data: { facilityList: Facility[], filterObject: any };

  private paginationOrgInfo = {
    facilitysortOrder: 'asc', facilitysortBy: 'name',
    numberRowPerPage: 5
  }
  public statusClass: string[] = [
    'inactive',
    'moving'
  ];
  facility: Facility[];
  filterValues = {};
  toggleFilterSearch: boolean = false;
  private arrayFilterPipe: ArrayFilterPipe = new ArrayFilterPipe();
  public filterWidthData = [{ 'width': 35, 'id': 'name', 'prm': true },
  { 'width': 35, 'id': 'fullAddress', 'prm': true },
  { 'width': 20, 'id': 'facilityType', 'prm': true },
  { 'width': 10, 'id': 'status', 'prm': true }];
  pieChartClickSubscription: Subscription;
  public mainBodyHeight: any;
  constructor(private tenantManagementService: TenantManagementService,
    private router: Router,
    private onboardingService: AssetOnboardingService,
    private appService: AppService) {
    this.paginationOrgInfo.numberRowPerPage = this.tenantManagementService.numberOfRecordPerPage;
    this.pieChartClickSubscription = this.appService.pieChartClick.subscribe((data: object) => {
      const status = data['name'] == 'Active' ? 1 : 0;
      this.facility = this.arrayFilterPipe.transform(this.data.facilityList, { status: status });
    });
  }

  ngOnInit() {
    if (this.data.filterObject && this.data.facilityList.length > 0) {
      let filter;
      if (typeof this.data.facilityList[0].status === 'number') {
        filter = this.data.filterObject;
      } else {
        filter = { status: this.data.filterObject.status === 0 ? false : true };
      }
      this.facility = this.arrayFilterPipe.transform(this.data.facilityList, filter);
    } else {
      this.facility = this.data.facilityList;
    }
    this.appService.setAllOptionPagination('tmFacilityTable');
    this.resetFilterData();
    this.mainBodyHeight = jQuery(document).height() - 398;

  }
  ngOnDestroy() {
    this.pieChartClickSubscription.unsubscribe();
  }

  resetFilterData() {
    this.toggleFilterSearch = false;
    this.filterValues = {
      'name': '',
      'fullAddress': '',
      'facilityType': '',
      'status': ''
    };
  }

  editFacility(facility: Facility) {
    this.onboardingService.onboardingDataForEdit = { index: 1, facility: facility };
    this.appService.ManagementAddEdit = {
      'value': true,
      'id': 'facility_overview'
    };
    this.router.navigate(['admin/asset-onboarding']);
  }

  updateData(event) {
    this.facility = event;
    this.appService.appExportData.next(event);
    this.appService.setAllOptionPagination('tmFacilityTable');
  }
}
