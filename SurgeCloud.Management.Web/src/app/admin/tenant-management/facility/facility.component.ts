import { Component, OnInit, ViewChild, ComponentFactoryResolver, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FacilityService } from './facility.service';
import { Facility } from './facility';
import { AppService } from '../../../../app';
import { ArrayFilterPipe } from '../../../shared-module';
import { FacilityTableComponent } from './';
import { FacilityImageComponent } from './';
import { FacilityDirective } from './facility.directive';
import { FacilityItem } from './facility-item';
import { IWizardItem } from '../../../shared-module';
import { AssetOnboardingService } from '../../asset-onboarding';
import * as _ from 'underscore';
import {PieChartComponent, PieChatConfig, SingleChartData} from '../../../core';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-facility',
  templateUrl: './facility.component.html',
  styleUrls: ['./facility.component.scss']
})

export class FacilityComponent implements OnInit {
  pieChartConfigOptions: PieChatConfig;
  facility: Facility[] = [];

  ArrayFilterPipe: ArrayFilterPipe = new ArrayFilterPipe();

  facilityCount: { active: number, inActive: number } = { active: 0, inActive: 0 };

  facilityItem: FacilityItem[] = [
    new FacilityItem(FacilityTableComponent, {}),
    new FacilityItem(FacilityImageComponent, {})];

  currentIndex = 0;
  public headers = [{
    'title': 'Facility Name',
    'dataKey': 'name',
    'width': 150
  }, {
    'title': 'Location',
    'dataKey': 'fullAddress',
    'width': 300
  }, {
    'title': 'Facility Type',
    'dataKey': 'facilityType',
    'width': 100
  }, {
    'title': 'Status',
    'dataKey': 'status',
    'width': 50
  }];

  public wizardItems: IWizardItem[] = [
    {
      name: 'Facility Table',
      icon: 'glyphicon-th-list glyphicon gi-1-5x',
      disabled: false,
      index: 0,
      success: false,
      error: false,
      active: true
    },
    {
      name: 'Facility Image',
      icon: 'glyphicon-th glyphicon gi-1-5x',
      disabled: false,
      index: 1,
      success: false,
      error: false,
      active: false
    },
    {
      name: 'Facility Add',
      icon: 'plus-icon',
      disabled: false,
      index: 2,
      success: false,
      error: false,
      active: false
    }
  ];

  @ViewChild(FacilityDirective) facilityHost: FacilityDirective;
  filterObject: any = null;
  lastUpdated: number;
  public chartData: SingleChartData[] = [];
  filterStatus: boolean = this.filterObject === null ? false : true;
  constructor(private facilityService: FacilityService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private appService: AppService,
    private router: Router,
    private assetOnboardingService: AssetOnboardingService,private translate: TranslateService) {
    const colorScheme = { domain: ['#94d67e', '#ee7f32', '#ef4846', '#6f7588', '#eb3c33', '#5acbf9', '#cf4385',
        '#935bcd', '#f3af2f', '#a6d9fd', '#aaaaaa', '#f17cb0', '#b2912f', '#b276b2', '#decf3f' ] };
    this.pieChartConfigOptions = new PieChatConfig(
      [200, 200],
      true,
      colorScheme,
      false,
      false,
      false,
      false,
      true,
      false
    );
  }

  ngOnInit() {
    this.getFacility(this.appService.getActiveCompany().companyId);
    this.appService.activeCompanyInfo.subscribe((company) => {
      if (company) {
        this.getFacility(company.companyId);
      }
    })
  }

  getFacility(companyId: string): void {
    this.lastUpdated = Date.now();
    this.facilityService.getFacility(companyId).subscribe(
      (data) => {
        this.facility = [];
        data.forEach(fac => {
          fac.address = fac.address ? fac.address : {};
          this.facility.push(new Facility(fac));
        })
        this.chartData = [];
        if (this.facility.length > 0) {
          const activeCount = (Math.ceil((this.ArrayFilterPipe.transform(this.facility, { status: 1 }).length
            / this.facility.length) * 100)) + (Math.ceil((this.ArrayFilterPipe.transform(this.facility, { status: true }).length
              / this.facility.length) * 100));
          const inactiveCount = (Math.floor((this.ArrayFilterPipe.transform(this.facility, { status: 0 }).length
            / this.facility.length) * 100)) + (Math.floor((this.ArrayFilterPipe.transform(this.facility, { status: false }).length
              / this.facility.length) * 100));
          // this.facilityCount = {
          //   active: activeCount,
          //   inActive: inactiveCount
          // };

          this.chartData.push({ name: 'Active', value: activeCount },
            { name: 'Inactive', value: inactiveCount });
          this.translate.stream("generic_active").subscribe(res => {
            if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
              this.chartData[0].name = res;
            }
          });
          this.translate.stream("generic_inactive").subscribe(res => {
            if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
              this.chartData[1].name = res;
            }
          });

          this.facility.forEach(element => {
            element['fullAddress'] = element['address'] ? (element['address'].city ? element['address'].city + ' , ' : '') +
              (element['address'].stateCode ? element['address'].stateCode + ' , ' : '') +
              (element['address'].postcode ? element['address'].postcode + ' , ' : '') +
              element['address'].countryCode : '';
          });
        }
        this.loadComponent();
      }
    );
  }

  loadComponent() {
    const boardingItem = this.facilityItem[this.currentIndex];
    boardingItem.data = { facilityList: this.facility, filterObject: this.filterObject };

    const componentFactory = this.componentFactoryResolver
      .resolveComponentFactory(boardingItem.component);

    const viewContainerRef = this.facilityHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);
    (<{ data: any }>componentRef.instance).data = boardingItem.data;

  }

  changeComponent(index) {
    if (index <= 1) {
      this.wizardItems[this.currentIndex].active = false;
      this.currentIndex = index;
      this.wizardItems[this.currentIndex].active = true;
      this.loadComponent();
    } else if (index === 2) {
      // this.assetOnboardingService.set
      this.assetOnboardingService.onboardingDataForEdit = { index: 1, facility: null };
      this.appService.changeRoute = this.router.url;
      this.appService.ManagementAddEdit = {
        'value': true,
        'id': 'facility_overview'
      };
      this.router.navigate(['/admin/asset-onboarding']);
    }

  }
  applyStatusFilter(filterData: object) {
    this.appService.emitEventOnPieChartClick(filterData);
  }
}
