import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appFacility]'
})
export class FacilityDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
