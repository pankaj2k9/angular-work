import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Company } from '../../../../../app';
import { TenantManagementService } from '../../tenant-management.service';
import { Organization } from '../../../../app-common';
import { CustomerOnboardingService } from '../../../customer-onboarding';
import { AppService } from '../../../../app.service';
import { Router } from '@angular/router';
import { ArrayFilterPipe } from '../../../../shared-module';
import { Subscription } from "rxjs/Subscription";
import * as jQuery from 'jquery';

@Component({
  selector: 'app-organization-table',
  templateUrl: './organization-table.component.html',
  styleUrls: ['./organization-table.component.scss']
})
export class OrganizationTableComponent implements OnInit, OnDestroy {
  data: { organizationList: Company[], filterObject: any };
  private paginationOrgInfo = {
    organizationsortOrder: 'asc', organizationsortBy: 'name',
    numberRowPerPage: 5
  };
  private arrayFilterPipe: ArrayFilterPipe = new ArrayFilterPipe();
  private organization: Company[];

  public statusClass: string[] = [
    'inactive',
    'moving'
  ];

  toggleFilterSearch: boolean = false;
  filterValues = {};
  public filterWidthData = [{ 'width': 30, 'id': 'name', 'prm': true },
  { 'width': 40, 'id': 'fullAddress', 'prm': true },
  { 'width': 20, 'id': 'phone', 'prm': true },
  { 'width': 10, 'id': 'status', 'prm': true }];
  public mainBodyHeight: any;
  pieChartClickSubscription: Subscription;
  constructor(private tenantManagementService: TenantManagementService, private appService: AppService,
    private customerOnboardingService: CustomerOnboardingService, private router: Router) {
    this.paginationOrgInfo.numberRowPerPage = this.tenantManagementService.numberOfRecordPerPage;
    this.pieChartClickSubscription = this.appService.pieChartClick.subscribe((data: object) => {
      const status = data['name'] == 'Active' ? 1 : 0;
      this.organization = this.arrayFilterPipe.transform(this.data.organizationList, { status: status });
    });
  }

  ngOnInit() {
    //console.log(this.data);
    if (this.data.filterObject && this.data.organizationList.length > 0) {
      let filter;
      if (typeof this.data.organizationList[0].status === 'number') {
        filter = this.data.filterObject;
      } else {
        filter = { status: this.data.filterObject.status === 0 ? false : true };
      }
      //console.log(filter);
      this.organization = this.arrayFilterPipe.transform(this.data.organizationList, filter);
    } else {
      this.organization = this.data.organizationList;
    }
    this.appService.setAllOptionPagination('tmOrganizationTable');
    this.resetFilterData();
    this.mainBodyHeight = jQuery(document).height() - 398;

  }
  ngOnDestroy() {
    this.pieChartClickSubscription.unsubscribe();
  }
  editOrganization(organization: any) {
    this.customerOnboardingService.onboardEdit = { organizationId: organization.companyId, userId: null, for: 1 };
    this.appService.ManagementAddEdit = {
      'value': true,
      'id': 'organization_overview'
    };
    this.router.navigate(['/admin/customer-onboarding']);
  }

  resetFilterData() {
    this.toggleFilterSearch = false;
    this.filterValues = {
      'name': '',
      'fullAddress': '',
      'phone': '',
      'status': ''
    };
  }

  updateData(event) {
    this.organization = event;
    this.appService.appExportData.next(event);
    this.appService.setAllOptionPagination('tmOrganizationTable');
  }
}
