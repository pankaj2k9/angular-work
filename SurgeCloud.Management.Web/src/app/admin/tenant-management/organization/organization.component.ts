import { Component, OnInit, ViewChild, ComponentFactoryResolver, Input } from '@angular/core';
import { OrganizationService } from './organization.service';
import { Company } from '../../../../app';
import { ArrayFilterPipe } from '../../../shared-module';
import { OrganizationTableComponent } from './organization-table/organization-table.component';
import { OrganizationImageComponent } from './organization-image/organization-image.component';
import { OrganizationDirective } from './organization.directive';
import { OrganizationItem } from './organization-item';
import { IWizardItem } from '../../../shared-module';
import { AppService } from '../../../app.service';
import { Router } from '@angular/router';
import { CustomerOnboardingService } from '../../customer-onboarding';
import * as _ from 'underscore';
import {PieChartComponent, PieChatConfig, SingleChartData} from '../../../core';
import {Subject} from "rxjs/Subject";
import {TranslateService} from "@ngx-translate/core";
@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.scss']
})
export class OrganizationComponent implements OnInit {
  pieChartConfigOptions: PieChatConfig;
  organizationList: Company[] = [];

  ArrayFilterPipe: ArrayFilterPipe = new ArrayFilterPipe();

  userCount: { activeUser: number, inActiveUser: number } = { activeUser: 0, inActiveUser: 0 };

  organizationItem: OrganizationItem[] = [
    new OrganizationItem(OrganizationTableComponent, {}),
    new OrganizationItem(OrganizationImageComponent, {})];

  currentIndex = 0;
  public wizardItems: IWizardItem[] = [
    {
      name: 'Organization Table',
      icon: 'glyphicon-th-list glyphicon gi-1-5x',
      disabled: false,
      index: 0,
      success: false,
      error: false,
      active: true
    },
    {
      name: 'Organization Image',
      icon: 'glyphicon-th glyphicon gi-1-5x',
      disabled: false,
      index: 1,
      success: false,
      error: false,
      active: false
    },
    {
      name: 'Organization Add',
      icon: 'plus-icon',
      disabled: false,
      index: 2,
      success: false,
      error: false,
      active: false
    }
  ];
  filterObject = null;

  public headers = [{
    'title': 'Organization Name',
    'dataKey': 'name',
    'width': 150
  }, {
    'title': 'Location',
    'dataKey': 'fullAddress',
    'width': 300
  }, {
    'title': 'Contact Number',
    'dataKey': 'phone',
    'width': 100
  }, {
    'title': 'Status',
    'dataKey': 'status',
    'width': 50
  }];
  @ViewChild(OrganizationDirective) organizationHost: OrganizationDirective;
  chartData: SingleChartData[] = [];
  filterStatus: boolean = this.filterObject === null ? false : true;
  lastUpdated: number;
  pieChartInfo: any;

  constructor(private organizationService: OrganizationService,
    private componentFactoryResolver: ComponentFactoryResolver, public customerOnboardingService: CustomerOnboardingService,
    private appService: AppService,
    private router: Router,private translate: TranslateService) {
    this.customerOnboardingService.onboardEdit = { organizationId: null, userId: null, for: null };
    const colorScheme = { domain: ['#94d67e', '#ee7f32', '#ef4846', '#6f7588', '#eb3c33', '#5acbf9', '#cf4385',
        '#935bcd', '#f3af2f', '#a6d9fd', '#aaaaaa', '#f17cb0', '#b2912f', '#b276b2', '#decf3f' ] };
    this.pieChartConfigOptions = new PieChatConfig(
      [200, 200],
      true,
      colorScheme,
      false,
      false,
      false,
      false,
      true,
      true
    );
  }

  ngOnInit() {
    this.getOrganizationList();
    this.appService.activeCompanyInfo.subscribe(
      (company) => {
        this.getOrganizationList();
      }
    );



  }

  getOrganizationList() {
    this.lastUpdated = Date.now();
    this.organizationList = this.appService.companies;
    this.chartData = [];
    if (this.organizationList.length > 0) {
      const activeCount = (Math.ceil((this.ArrayFilterPipe.transform(this.organizationList, { status: 1 }).length /
        this.organizationList.length) * 100)) + (Math.ceil((this.ArrayFilterPipe.transform(this.organizationList, { status: true }).length /
          this.organizationList.length) * 100));
      const inactiveCount = (Math.floor((this.ArrayFilterPipe.transform(this.organizationList, { status: 0 }).length /
        this.organizationList.length) * 100)) + (Math.floor((this.ArrayFilterPipe.transform(this.organizationList,
          { status: false }).length / this.organizationList.length) * 100))

      this.chartData.push({ name: 'Active', value: activeCount },
        { name: 'Inactive', value: inactiveCount });

      this.translate.stream("generic_active").subscribe(res => {
        if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
          this.chartData[0].name = res;
        }
      });
      this.translate.stream("generic_inactive").subscribe(res => {
        if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
          this.chartData[1].name = res;
        }
      });



      this.organizationList.forEach(element => {
        element['phone'] = element['phones'][0];
        element['fullAddress'] = element['address'] ? (element['address'].city ? element['address'].city + ' , ' : '') +
          (element['address'].stateCode ? element['address'].stateCode + ' , ' : '') +
          (element['address'].postcode ? element['address'].postcode + ' , ' : '') +
          element['address'].countryCode : '';
      });
    }
    this.loadComponent();

  }
  loadComponent() {

    const boardingItem = this.organizationItem[this.currentIndex];
    boardingItem.data = { organizationList: this.organizationList, filterObject: this.filterObject };

    const componentFactory = this.componentFactoryResolver
      .resolveComponentFactory(boardingItem.component);

    const viewContainerRef = this.organizationHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);
    (<{ data: any }>componentRef.instance).data = boardingItem.data;
    // const componentRef = viewContainerRef.createComponent(componentFactory);
    // (<{ facility: any }>componentRef.instance).facility = boardingItem.facility;
  }
  changeComponent(index) {
    if (index <= 1) {
      this.wizardItems[this.currentIndex].active = false;
      this.currentIndex = index;
      this.wizardItems[this.currentIndex].active = true;
      this.loadComponent();
    } else if (index === 2) {
      this.customerOnboardingService.onboardEdit = { organizationId: null, userId: null, for: null, roleGroup: null };
      this.appService.changeRoute = this.router.url;
      this.appService.ManagementAddEdit = {
        'value': true,
        'id': 'organization_overview'
      };
      this.router.navigate(['/admin/customer-onboarding']);
    }
  }
  applyStatusFilter(filterData: object) {
    this.appService.emitEventOnPieChartClick(filterData);
  }
}
