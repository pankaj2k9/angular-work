import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appOrganization]'
})
export class OrganizationDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
