import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrganizationComponent } from './organization.component';
import { OrganizationTableComponent } from './organization-table/organization-table.component';
import { OrganizationImageComponent } from './organization-image/organization-image.component';
import { OrganizationChartComponent } from './organization-chart/organization-chart.component';
import { OrganizationDirective } from './organization.directive';
import { OrganizationService } from './organization.service';
import { DataTableModule } from 'angular2-datatable';
import { CoreModule} from '../../../core';
import { SharedModule } from '../../../shared-module/shared-module.module';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    SharedModule,
    CoreModule,
    SharedModule
  ],
  declarations: [OrganizationComponent,
   OrganizationTableComponent,
   OrganizationImageComponent,
   OrganizationChartComponent,
   OrganizationDirective,
   ],
   exports: [OrganizationComponent,
   OrganizationTableComponent,
   OrganizationImageComponent,
   OrganizationChartComponent
   ],
   providers: [
   OrganizationService
   ],
   entryComponents: [
   OrganizationTableComponent,
   OrganizationImageComponent
   ],
})
export class OrganizationModule { }
