import { Component, OnInit, ViewChild } from '@angular/core';
import { IWizardItem } from '../../shared-module';
import { AssetOnboardingService } from '../asset-onboarding';
import { Router } from '@angular/router';
import { AppService } from '../../app.service';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-tenant-management',
  templateUrl: './tenant-management.component.html',
  styleUrls: ['./tenant-management.component.scss']
})
export class TenantManagementComponent implements OnInit {
  public wizardItems: IWizardItem[] = [
    {
      name: 'Organization Overview',
      icon: 'assets/icons/CustomerOnboarding/icon_CustomerOnboarding_CreateOrg.svg#Vector_Smart_Object',
      activeIcon: 'assets/icons/CustomerOnboarding/icon_CustomerOnboarding_CreateOrgWhite.svg#Vector_Smart_Object',
      success: false,
      active: true,
      statusIcon: ''
    },
    {
      name: 'Facility Overview',
      icon: 'assets/icons/AssetOnboarding/icon_Assetonboarding_Facility.svg#Vector_Smart_Object',
      activeIcon: 'assets/icons/AssetOnboarding/icon_Assetonboarding_Facility_White.svg#Vector_Smart_Object',
      success: false,
      active: false,
      statusIcon: ''
    },
    {
      name: 'Asset Overview',
      icon: 'assets/icons/AssetOnboarding/icon_Assetonboarding_Asset.svg#Vector_Smart_Object',
      activeIcon: 'assets/icons/AssetOnboarding/icon_Assetonboarding_Asset_White.svg#Vector_Smart_Object',
      success: false,
      active: false,
      statusIcon: ''
    }
  ];
  currentIndex = 0;
  constructor(
    public assetOnboardingService: AssetOnboardingService,
    public router: Router,
    public appService: AppService,private translate: TranslateService) { }

  ngOnInit() {
    this.appService.changeRoute = this.router.url;
    this.assetOnboardingService.onboardingDataForEdit = { index: null, asset: null };
    this.checkIsAddEditTenant();
    this.translate.stream("tm_org_overview").subscribe(res => {
      if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
        this.wizardItems[0].name = res;
      }
    });
    this.translate.stream("tm_facility_overview").subscribe(res => {
      if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
        this.wizardItems[1].name = res;
      }
    });
    this.translate.stream("tm_asset_overview").subscribe(res => {
      if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
        this.wizardItems[2].name = res;
      }
    });
  }

  checkIsAddEditTenant() {
    if (this.appService.ManagementAddEdit.value) {
      this.appService.ManagementAddEdit.value = false;
      setTimeout(() => {
        document.getElementById(this.appService.ManagementAddEdit.id).click();
      })
    }
  }

  beforeChange($event) {
    switch ($event.nextId) {
      case 'organization_overview':
        this.wizardItems[this.currentIndex].active = false;
        this.wizardItems[0].active = true;
        this.currentIndex = 0;
        break;
      case 'facility_overview':
        this.wizardItems[this.currentIndex].active = false;
        this.wizardItems[1].active = true;
        this.currentIndex = 1;
        break;
      case 'asset_overview':
        this.wizardItems[this.currentIndex].active = false;
        this.wizardItems[2].active = true;
        this.currentIndex = 2;

        break;
    }
  }

}
