import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { FormsModule } from '@angular/forms';
import { TenantManagementComponent } from './tenant-management.component';
import { TenantManagementService } from './tenant-management.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { OrganizationModule, OrganizationComponent } from './organization';
import { FacilityModule, FacilityComponent } from './facility';
import { AssetModule, AssetComponent } from './asset';
import {AngularSvgIconModule} from "angular-svg-icon";
import {SharedModule} from "../../shared-module/shared-module.module";
import {TenantManagementRoutingModule} from "./tenant-management-routing.module";
import {TranslateModule,TranslateLoader,TranslateCompiler,TranslateParser,MissingTranslationHandler} from "@ngx-translate/core";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {HttpLoaderFactory} from "../../app.module";

@NgModule({
  imports: [
    CommonModule,
    SimpleNotificationsModule,
    FormsModule,
    NgbModule,
    OrganizationModule,
    FacilityModule,
    AssetModule,
    AngularSvgIconModule,
    SharedModule,
    TenantManagementRoutingModule,
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    TenantManagementComponent,
  ],
  exports: [
    TenantManagementComponent
  ],
  providers: [
    TenantManagementService
  ],
  entryComponents : [
    OrganizationComponent,
    FacilityComponent,
    AssetComponent
  ]
})

export class TenantManagementModule { }
