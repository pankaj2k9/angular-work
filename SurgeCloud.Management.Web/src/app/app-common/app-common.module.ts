import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { MaterialModule } from '@angular/material';
import { SharedModule } from '../shared-module';
import { CoreModule } from '../core/core.module';
import { AppCommonRoutingModule } from './app-common-routing.module';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderService } from './header/header.service';
import { CompanyResolverService } from './company-resolver.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent, LoginGuard } from './login';
import { OauthcallbackComponent, OauthGuard } from './oauthcallback';
import { AccessGuard } from './access.guard';
import { GmapComponent } from './gmap/gmap.component';
import {AngularSvgIconModule} from 'angular-svg-icon';
import {CreateRoleGroupService} from "../admin/customer-onboarding/create-role-group/create-role-group.service";
import { PushNotificationListComponent } from './push-notification-list/push-notification-list.component';
import {PushNotificationComponent} from "ng2-notifications/ng2-notifications";
import { EmailSummaryComponent } from './email-summary/email-summary.component';
import {DataTableModule} from "angular2-datatable";
import {TranslateModule} from "@ngx-translate/core";


@NgModule({
  imports: [
    CommonModule,
    AppCommonRoutingModule,
    // MaterialModule,
    SharedModule,
    CoreModule,
    NgbModule,
    AngularSvgIconModule,
    DataTableModule,
    TranslateModule
  ],
  providers: [
    HeaderService,
    CompanyResolverService,
    LoginGuard,
    OauthGuard,
    AccessGuard,
    CreateRoleGroupService
  ],
  declarations: [HomeComponent, HeaderComponent, FooterComponent, LoginComponent, OauthcallbackComponent, GmapComponent, PushNotificationListComponent, PushNotificationComponent, EmailSummaryComponent],
  exports: [HomeComponent, HeaderComponent, FooterComponent, GmapComponent,TranslateModule]
})
export class AppCommonModule { }
