import { environment } from '../../environments/environment';

export class Facility {
  constructor(
    public logo: string,
    public facilityName: string,
    public status: boolean,
    public address: any,
    public timezone: string,
    public facilityType: string,
    public facilityId?: string
  ) {
    this.logo = logo;
    this.facilityName = facilityName;
    this.status = status;
    this.timezone = timezone;
    this.facilityType = facilityType;
    this.address = new Address(address || {});
    this.facilityId = facilityId;
  }
}

export class Address {
  public name: string;
  public street1: string;
  public street2: string;
  public city: string;
  public stateCode?: string;
  public postcode: string;
  public countryCode: string;
  public formattedAddress: string;
  public latitude: number;
  public longitude: number;
  constructor(private address: any = {}) {
    this.name = address.name || '';
    this.street1 = address.street1 || '';
    this.street2 = address.street2 || '';
    this.city = address.city || '';
    this.stateCode = address.stateCode || '';
    this.postcode = address.postCode || '';
    this.countryCode = address.countryCode || '';
    this.formattedAddress = address.formattedAddress || '';
    this.latitude = address.latitude || 0;
    this.longitude = address.longitude || 0;
  }
}


export class Asset {
  constructor(
    public aId: string,
    public assetId: string,
    public companyId: string,
    public companyName: string,
    public assetType: string,
    public logo: string,
    public make: string,
    public model: string,
    public serialNumber: string,
    public unitNumber: string,
    public vin: string,
    public softwareId: string,
    public calibrationId: string,
    public customerReference: string,
    public customerFacilityGroup: string,
    public customerEquipmentGroup: string,
    public customerEquipmentId: string,
    public tags: string[],
    public status: string,
    public createdDate: string,
    public lastUpdatedDate: string
  ) {
    this.aId = aId;
    this.assetId = assetId;
    this.companyId = companyId;
    this.companyName = companyName;
    this.assetType = assetType;
    this.logo = logo || environment.defaultImage;
    this.make = make;
    this.model = model;
    this.serialNumber = serialNumber;
    this.unitNumber = unitNumber;
    this.vin = vin;
    this.softwareId = softwareId;
    this.calibrationId = calibrationId;
    this.customerReference = customerReference;
    this.customerFacilityGroup = customerFacilityGroup;
    this.customerEquipmentGroup = customerEquipmentGroup;
    this.customerEquipmentId = customerEquipmentId;
    this.tags = tags;
    this.status = status;
    this.lastUpdatedDate = new Date().toLocaleString();
    this.createdDate = new Date().toLocaleString();
  }
}


export class GeoFenceAsset {
  public expanded: Boolean = false;
  public icon = '';
  public zIndex = 90;
  public isPlaying = false;
  public isPaused = true;
  public isOpen = false;
  constructor(
    public aId: string,
    public altitude: number,
    public assetId: string,
    public assetLogo: string,
    public assetStatus: string,
    public assetType: string,
    public calibrationId: string,
    public companyId: string,
    public companyName: string,
    public controllerId: string,
    public customerEquipmentGroup: string,
    public customerEquipmentId: string,
    public facilityId: string,
    public facilityName: string,
    public iotHubName: string,
    public createdDate: string,
    public latestSyncDate: string,
    public latitude: number,
    public logo: string,
    public longitude: number,
    public make: string,
    public model: string,
    public serialNumber: string,
    public softwareId: string,
    public status: number,
    public unitNumber: string,
    public vin: string,
    public gwAppBuild: string,
    public gwDCAver: string,
    public gwDescription: string,
    public gwIMEI: string,
    public gwOSBuild: string,
    public gwRSSI: string,
    public gwSIMICCID: string,
    public gwSNO: string,
    public gwTimezone: string,
    public gwWANIP: string,
    public location: object,
    public pVer: string,
    public param: any[],
  ) {
    this.aId = aId;
    this.altitude = altitude;
    this.assetId = assetId;
    this.createdDate = createdDate;
    this.assetLogo = assetLogo || environment.defaultAssetImage;
    this.assetStatus = assetStatus;
    this.assetType = assetType;
    this.calibrationId = calibrationId;
    this.companyId = companyId;
    this.companyName = companyName;
    this.controllerId = controllerId;
    this.customerEquipmentGroup = customerEquipmentGroup;
    this.customerEquipmentId = customerEquipmentId;
    this.facilityId = facilityId;
    this.facilityName = facilityName;
    this.iotHubName = iotHubName;
    this.latestSyncDate = latestSyncDate;
    this.latitude = latitude || 0;
    this.logo = logo;
    this.longitude = longitude || 0;
    this.make = make;
    this.model = model;
    this.serialNumber = serialNumber;
    this.softwareId = softwareId;
    this.status = status;
    this.unitNumber = unitNumber;
    this.vin = vin;
    this.gwAppBuild = gwAppBuild;
    this.gwDCAver = gwDCAver;
    this.gwDescription = gwDescription;
    this.gwIMEI = gwIMEI;
    this.gwOSBuild = gwOSBuild;
    this.gwRSSI = gwRSSI;
    this.gwSIMICCID = gwSIMICCID;
    this.gwSNO = gwSNO;
    this.gwTimezone = gwTimezone;
    this.gwWANIP = gwWANIP;
    this.pVer = pVer;
    this.location = location;
    this.param = param;
  }
}

export class Generator {
  constructor(
    public aId: string,
    public assetId: string,
    public companyId: string,
    public companyName: string,
    public assetType: string,
    public logo: string,
    public make: string,
    public model: string,
    public serialNumber: string,
    public unitNumber: string,
    public vin: string,
    public softwareId: string,
    public calibrationId: string,
    public customerReference: string,
    public customerFacilityGroup: string,
    public customerEquipmentGroup: string,
    public customerEquipmentId: string,
    public tags: string[],
    public status: string,
    public createdDate: string,
    public lastUpdatedDate: string
  ) {
    this.aId = aId;
    this.assetId = assetId;
    this.companyId = companyId;
    this.companyName = companyName;
    this.assetType = assetType;
    this.logo = logo || environment.defaultImage;
    this.make = make;
    this.model = model;
    this.serialNumber = serialNumber;
    this.unitNumber = unitNumber;
    this.vin = vin;
    this.softwareId = softwareId;
    this.calibrationId = calibrationId;
    this.customerReference = customerReference;
    this.customerFacilityGroup = customerFacilityGroup;
    this.customerEquipmentGroup = customerEquipmentGroup;
    this.customerEquipmentId = customerEquipmentId;
    this.tags = tags;
    this.status = status;
    this.lastUpdatedDate = new Date().toLocaleString();
    this.createdDate = new Date().toLocaleString();
  }
}
// export class Address {
//     public name: string;
//     public street1: string;
//     public street2: string;
//     public city: string;
//     public stateCode?: string;
//     public postcode: string;
//     public countryCode: string;
//     public latitude: number;
//     public longitude: number;
//     constructor(private address: any = {}) {
//         this.name = address.name || '';
//         this.street1 = address.street1 || '';
//         this.street2 = address.street2 || '';
//         this.city = address.city || '';
//         this.stateCode = address.stateCode || '';
//         this.postcode = address.postCode || '';
//         this.countryCode = address.countryCode || '';
//         this.latitude = address.latitude || 0;
//         this.longitude = address.longitude || 0;
//     }
// }

export class Contact {
  public contactName: string;
  public contactPhoneNumber: string;
  public contactMobileNumber: string;
  public contactEmailId: string;
  public contactStatus: number;
  constructor(private contact: any = {}) {
    this.contactName = contact.contactName || '';
    this.contactPhoneNumber = contact.contactPhoneNumber || '';
    this.contactMobileNumber = contact.contactMobileNumber || '';
    this.contactEmailId = contact.contactEmailId || '';
    this.contactStatus = contact.stateCode || 0;
  }
}

export class Organization {
  public companyId: string;
  public name: string;
  public parentCompany: string;
  public address: IAddress;
  public contact: Contact;
  public emailId: string;
  public website: string;
  public phones: string[];
  public currencyCode: string;
  public createdBy: string;
  public updatedBy: string;
  public lastUpdatedDate: string;
  public status: Boolean;
  public logo: string;
  public tags: string[];
  public groupId: string;
  constructor(private organization: any = {}) {
    this.companyId = organization.companyId || null;
    this.name = organization.name || '';
    this.parentCompany = organization.parentCompany || '';
    this.address = new Address(organization.address);
    this.contact = new Contact(organization.contact);
    this.emailId = organization.emailId || '';
    this.website = organization.website || '';
    this.phones = organization.phones || [''];
    this.currencyCode = organization.currencyCode || '';
    this.createdBy = organization.createdBy || '';
    this.updatedBy = organization.updatedBy || '';
    this.lastUpdatedDate = organization.lastUpdatedDate || '';
    this.status = organization.status || true;
    this.logo = organization.logo || '';
    this.tags = organization.tags || [];
    this.groupId = organization.groupId || '';
  }
}

export interface IAddress {
  name: string;
  street1: string;
  street2?: string;
  city: string;
  stateCode?: string;
  countryCode: string;
  postcode: string;
  formattedAddress: string;
  latitude?: number;
  longitude?: number;
}

export class Currency {
  public code: string;
  public name: string;
  public symbol: string
  constructor(public data: any) {
    this.code = data.code;
    this.name = data.name;
    this.symbol = data.symbol;
  }
}

export class RoleGroup {
  public description: string;
  public displayName: string;
  public id: string;
  public status: number;
  constructor(
    description: string,
    displayName: string,
    id: string,
    status: number
  ) {
    this.description = description;
    this.displayName = displayName;
    this.id = id;
    this.status = status;
  }
}

export class Users {
  public Id: string;
  public emailId: string;
  public username: string;
  public password: string;
  public displayName: string;
  public givenName: string;
  public surname: string;
  public jobTitle: string;
  public groupId: string;
  public companyId: string;
  public companyName: string;
  public facilityId: string;
  public facilityName: string;
  public licenseDetailsAvailable: boolean;
  public modelDate: any;
  public drivingLicenseNumber: string;
  public driverId: string;
  public expirationDate: any;
  public landingPage: string;
  public logo: string;
  public status: Boolean = true;
  public roleGroup: string;
  public phoneNumber: string;
  public defaultLanguage: string;
  public measurementUnit: string;
  public timezone: string;
  public defaultCompanyId: string;
  public defaultCompanyName: string;
  constructor(public data: any = {}) {
    this.Id = data.userId || null;
    this.emailId = data.emailId || '';
    this.username = data.username || '';
    this.password = data.password || '';
    this.displayName = data.displayName || '';
    this.givenName = data.givenName || '';
    this.surname = data.surname || '';
    this.jobTitle = data.jobTitle || '';
    this.groupId = data.groupId || null;
    this.companyId = data.companyId || null;
    this.companyName = data.companyName || '';
    this.facilityId = data.facilityId || null;
    this.facilityName = data.facilityName || '';
    this.licenseDetailsAvailable = data.licenseDetailsAvailable || false;
    this.modelDate = data.modelDate || '';
    this.drivingLicenseNumber = data.drivingLicenseNumber || '';
    this.driverId = data.driverId || null;
    this.expirationDate = data.expirationDate || '';
    this.landingPage = data.landingPage || '';
    this.logo = data.logo || '';
    this.status = data.isActive === 0 ? false : true;
    this.roleGroup = data.roleGroup || '';
    this.phoneNumber = data.phoneNumber || '';
    this.defaultLanguage = data.defaultLanguage || '';
    this.measurementUnit = data.measurementUnit || '';
    this.timezone = data.timezone || '';
    this.defaultCompanyId = data.defaultCompanyId || '';
    this.defaultCompanyName = data.defaultCompanyName || '';
  }
}


export interface GeneratorLocationMap {
  controllerId: string,
  companyId: string,
  companyName: string,
  facilityId: string,
  facilityName: string,
  aId: string,
  assetId: string,
  iotHubName: string,
  msgType: string,
  pVer: string,
  gwSNO: string,
  gwDescription: string,
  gwOSBuild: string,
  gwAppBuild: string,
  gwDCAver: string,
  gwSIMICCID: string,
  gwIMEI: string,
  gwRSSI: string,
  gwTimezone: string,
  gwWANIP: string,
  latitude: number,
  longitude: number,
  altitude: string,
  tags: string[],
  status: number,
  param: string[]
}

export interface GenAssetTable {
  gwSno: string,
  assetId: string,
  occurrenceDate: Date,
  param: GenIotRealTimeData[]
}

export interface GenIotRealTimeData {
  ts: Date,
  name: string,
  value: string,
  status: string
}

export class GeneratorAlarmStatus {
  constructor(
    public assetId: string,
    public occurrenceDate: string,
    public status: string,
    public gwSno: string
  ) {
    this.assetId = assetId;
    this.occurrenceDate = occurrenceDate;
    this.status = status;
  }
}

export class GenAssetTableShrinkView {
  selRTtableView: boolean;
  selAlarmView: boolean;
  rtClass: string;
  rtInnerClass: string;
  alarmClass: string;
  alarmInnerClass: string;
  RTtableMaxView: boolean;
  AlarmMaxView: boolean;
}

export class GenRealtimeStatus {
  constructor(
    public name: string,
    public value: Number,
    public unit: string,
    public node: Number,
    public lastUpdate: string,
    public status: string,
    public pointDataType: string,
    public notInSync: string,
    public displayName:string
  ) {
    this.name = name;
    this.value = value;
    this.unit = unit;
    this.node = node;
    this.status = status;
    this.lastUpdate = lastUpdate;
    this.pointDataType = pointDataType;
    this.notInSync = notInSync;
    this.displayName = displayName;
  }
}
/*export enum Images {
    ORGANIZATION = 'assets/images/org_default.svg',
    USER = 'assets/images/logo.png'
}
*/
