import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../shared-module';
import { AppService } from '../app.service';

@Injectable()
export class CompanyResolverService {

  constructor(private httpsService: HttpService, public appService: AppService) { }
  resolve() {
    return this.httpsService.get(`users/api/users/getuser/${this.appService.getUserId()}`)
      .map((res) => {
        this.appService.user = res.json();
        return res.json();
      })
      .flatMap(
      (data) => {
        return Observable.forkJoin(
          this.httpsService.get(`organization/api/organization/get/${this.appService.user.companyName}`)
            .map(response => response.json())
        )
      });
  }
}
