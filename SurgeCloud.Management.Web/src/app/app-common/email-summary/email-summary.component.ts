import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, OnDestroy, Output, HostListener, ViewEncapsulation } from '@angular/core';
import { AppService } from "../../app.service";
import { HeaderService } from "../header/header.service";

import * as _ from 'underscore';
declare var moment: any;

@Component({
  selector: 'app-email-summary',
  templateUrl: './email-summary.component.html',
  styleUrls: ['./email-summary.component.scss'],
  providers: [HeaderService]
})
export class EmailSummaryComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {
  @Input() showEmailSummary: any;
  public isRefreshData: boolean = false;
  emailSummaryBottomPosition: string;
  emailSummary: Array<object>;
  loaderLoad = false;
  tableHeight: any;
  @Output() emailSummaryCount: EventEmitter<number> = new EventEmitter<number>();
  toggleFilterSearch = false;
  filterWidthData = [
    { 'width': 22, 'id': 'subject', 'prm': true },
    { 'width': 22, 'id': 'sentOn', 'prm': true }
  ];
  filterValues = {};
  emailSummaryInterval: any;
  constructor(private appService: AppService,
    private headerService: HeaderService) {
    setTimeout(() => {
      if (document.getElementById('appFooter')) {
        const pageFooterHeight = document.getElementById('appFooter').offsetHeight;
        //console.log(pageFooterHeight);
        this.emailSummaryBottomPosition = pageFooterHeight + 'px';
      }
    }, 0);
    this.emailSummary = [];
    this.getEmailSummary(true);
    this.emailSummaryInterval = setInterval(() => {
      this.getEmailSummary(false);
    }, 600000);
  }

  @HostListener('window:resize') onWindowResize() {
    this.setResizeEvent();
  }

  ngOnInit() {
    this.setResizeEvent();
  }

  ngOnDestroy() {
    clearInterval(this.emailSummaryInterval);
  }

  setResizeEvent() {
    const windowHeight = window.innerHeight;
    const headerHeight = document.getElementById('appHeader').offsetHeight;
    const footerHeight = document.getElementById('appFooter').offsetHeight;
    this.tableHeight = windowHeight - (headerHeight + footerHeight + 100);
  }

  ngOnChanges() {
    //console.log(this.showEmailSummary);
    if (this.showEmailSummary === true) {
      this.toggleFilterSearch = false;
      this.appService.setAllOptionPagination('email-summary-table');
    } else if (this.showEmailSummary === 'update') {
      this.getEmailSummary(false);
    }
  }

  ngAfterViewInit() {
    //this.appService.setAllOptionPagination('email-summary-table');
  }

  getEmailSummary(isFirst) {
    this.loaderLoad = true;
    let date = isFirst ? '' : moment.utc(this.appService.userProfileDetails && this.appService.userProfileDetails['emailViewed']).format('MM/DD/YYYY, hh:mm:ss.SSS A');
    this.appService.getEmailSummary(date)
      .finally(() => {
        this.isRefreshData = false;
      })
      .subscribe((response) => {
        if (isFirst) {
          response.forEach(element => {
            element.subject = element.subject.replace(element.assetId, `<strong>${element.assetId}</strong>`);
          });
          this.emailSummary = response;
          this.getEmailSummary(false);
        } else {
          if (response.length > 0 && this.emailSummary.length === 0) {
            response.forEach(element => {
              element.subject = element.subject.replace(element.assetId, `<strong>${element.assetId}</strong>`);
            });
            this.emailSummary = response;
          }
          this.emailSummaryCount.emit(response.length);
          this.checkAndUpdateEmailNotificationList(response.reverse());
        }
        this.loaderLoad = false;
      });
  }

  checkAndUpdateEmailNotificationList(data) {
    for (let i = 0; i < data.length; i++) {
      const item = data[i];
      if ((moment(item['sentOn']).valueOf() > moment(this.emailSummary[0]['sentOn']).valueOf())) {
        item.subject = item.subject.replace(item.assetId, `<strong>${item.assetId}</strong>`);
        this.emailSummary.unshift(item);
      }
    }
    this.emailSummary = _.sortBy(JSON.parse(JSON.stringify(this.emailSummary)), (item) => {
      return -moment(item['sentOn']).valueOf();
    });
    if (this.emailSummary.length > 0 && this.appService.userProfileDetails)
      this.appService.userProfileDetails['latestEmailDate'] = this.emailSummary[0]['sentOn'];
  }

  groupByWithAssetIdAndSubscriptionId(array, f) {
    let groups = {};
    array.forEach(function (o) {
      var group = JSON.stringify(f(o));
      groups[group] = groups[group] || [];
      groups[group].push(o);
    });
    return Object.keys(groups).map(function (group) {
      return groups[group];
    });
  }

  loadEmailSummary() {
    this.isRefreshData = true;
    this.emailSummaryCount.emit(0);
    this.updateEmailViewedDate();
  }

  updateEmailViewedDate() {
    let date = this.getUserTimeZoneDateFormat(moment());
    let data = {
      'userId': this.appService.userProfileDetails['userId'],
      'dateViewed': this.getUserTimeZoneDateFormat(moment()).format(),
      'type': 'email'
    }
    this.headerService.updateViewedDate(data)
      .subscribe((obj) => {
        this.appService.userProfileDetails['emailViewed'] = data['dateViewed'];
        this.getEmailSummary(false);
      }, (err) => {
        console.log(err);
      });
  }

  getUserTimeZoneDateFormat(date) {
    let tempDate = moment();
    if (this.appService.userTimezone != null) {
      tempDate = moment.tz(date, this.appService.userTimezone);
      tempDate = moment(tempDate).utcOffset(-tempDate.utcOffset());
    } else {
      tempDate = moment(date).utc();
    }
    return tempDate;
  }

  updateData(event) {
    this.emailSummary = event;
    this.appService.setAllOptionPagination('email-summary-table');
  }
}
