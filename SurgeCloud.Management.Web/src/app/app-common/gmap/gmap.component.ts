import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MapsAPILoader, GoogleMapsAPIWrapper, CircleManager } from '@agm/core';

@Component({
  selector: 'app-gmap',
  templateUrl: './gmap.component.html',
  styleUrls: ['./gmap.component.scss']
})
export class GmapComponent implements OnInit {

  @Output() mapReady: EventEmitter<any> = new EventEmitter();

  constructor(private mapWrapper: GoogleMapsAPIWrapper) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.mapWrapper.getNativeMap().then((m) => {
      this.mapReady.next(m);
    }, err => {
      console.log('error', err);
    })
  }

}
