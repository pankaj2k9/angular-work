import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, OnChanges, SimpleChanges } from '@angular/core';
import {Router, Event ,ActivatedRoute} from '@angular/router';
import { HeaderService } from './header.service';
import { Company, ICompany } from '../../app';
import { AppService } from '../../app.service';
import { AdalService } from '../../shared-module';
import * as _ from 'underscore';
import { CustomerOnboardingService } from "../../admin/customer-onboarding";
import { Subscription } from "rxjs/Subscription";
import {TranslateService, LangChangeEvent} from "@ngx-translate/core";
declare var moment: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy, OnChanges {
  public userInfo: any={};
  public userInfoo: any={};
  public userId: any;
  public userLang:any='en';
  public companies: ICompany[] = [];
  public currentCompany: ICompany;
  public user: any;
  public userData: any;
  public navBarToggle = false;
  private routerParam: Subscription;
  public Icon: any = {
    alarm: {
      normal: 'assets/icons/TopBar/Alarm01.svg#Vector_Smart_Object',
      hover: 'assets/icons/TopBar/Alarm02.svg#Vector_Smart_Object'
    },
    message: {
      normal: 'assets/icons/TopBar/Message01.svg#Vector_Smart_Object',
      hover: 'assets/icons/TopBar/Message02.svg#Vector_Smart_Object'
    },
    search: {
      normal: 'assets/icons/TopBar/Search02.svg#Search_Icon',
      hover: 'assets/icons/TopBar/Search01.svg#Search_Icon'
    },
    help: {
      normal: 'assets/icons/TopBar/Help01.svg#Vector_Smart_Object',
      hover: 'assets/icons/TopBar/Help02.svg#Vector_Smart_Object'
    }
  };
  isAdmin: boolean;
  showNotificationList = false;
  @Output() openNotificationDrawer: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() set isAdminModule(isAdmin) {
    this.isAdmin = isAdmin;
  }
  @Input() pushNotification = {
    'count': 0,
    'loading': true
  };
  @Input() emailSummaryCount = 0;
  showEmailSummaryList = false;
  @Output() openEmailSummaryDrawer: EventEmitter<any> = new EventEmitter<any>();
  closeDrawerSubscription: Subscription;
  constructor(public translate: TranslateService,private route: ActivatedRoute, private router: Router, private headerService: HeaderService, private appService: AppService, public adalService: AdalService, private customerOnboardingService: CustomerOnboardingService) {
    this.appService.activeCompanyInfo.subscribe((res) => {
      //console.log(res);
      this.currentCompany = res;
    });
    this.closeDrawerSubscription = this.appService.closeDrawer.subscribe((clicked: boolean) => {
      //console.log(clicked);
      if (clicked && this.showEmailSummaryList) {
        this.showEmailSummary();
      }
      if (clicked && this.showNotificationList) {
        this.showPushNotification();
      }
    });
  }

  ngOnInit() {
    if(this.adalService.userInfo){
      this.userInfo=this.adalService.userInfo;
      if(this.adalService.userInfo.profile){
        this.userId = this.adalService.userInfo.profile.oid;
      }else{
      }

    } else{
      this.userInfoo={};
      this.userId=null;
    }

    this.appService.getUserDetailsById(this.userId).subscribe(data => {

      if(data.defaultLanguage == 'English'){
        this.userLang='es';
        this.translate.setDefaultLang(this.userLang);
        this.translate.use(this.userLang);
      } else{
        this.translate.setDefaultLang(this.userLang);
        this.translate.use(this.userLang);
      }

    });




    //console.log(this.pushNotificationCount);
    this.companies = [];
    this.appService.companies.forEach(company => {
      if (company.status === 1 || company.status === true) {
        this.companies.push(company);
      }
    });
    this.currentCompany = this.companies[0];
    this.user = this.adalService.userInfo;
    this.appService.companyList.subscribe(companies => {
      this.companies = [];
      companies.forEach(company => {
        if (company.status === 1 || company.status === true) {
          this.companies.push(company);
        }
      });
      this.userData = this.appService.user;
    });

    this.appService.setCompanyOnNotificationClick.subscribe(data => {
      for (let index = 0; index < this.companies.length; index++) {
        const company = this.companies[index];
        if (company['companyId'] === data['companyId']) {
          this.setActiveCompany(company);
          break;
        }
      }
      this.showPushNotification();
    });

    document.getElementsByTagName('body')[0].addEventListener('click', () => {
      this.navBarToggle = false;
      if (this.showEmailSummaryList) {
        this.showEmailSummaryList = false;
        this.openEmailSummaryDrawer.emit(this.showEmailSummaryList);
      }
      if (this.showNotificationList) {
        this.showNotificationList = false;
        this.openNotificationDrawer.emit(this.showNotificationList);
      }
    });



  }

  ngOnChanges(changes: SimpleChanges) {
  }

  ngOnDestroy() {
    this.closeDrawerSubscription.unsubscribe();
  }
  /*getcompanies() {
    this.headerService.getcompanies().subscribe(
      (companies) => {
        companies.forEach(company => {
          this.companies.push(new Company(company));
        });
        this.currentCompany = this.companies[0];
      }
    );
  }*/
  updateProfile() {
    this.customerOnboardingService.userEditMode = true;
    this.customerOnboardingService.userEditOwnProfile = true;
    this.customerOnboardingService.adminEditUserProfile = false;
    this.router.navigate(['/user/update-profile']);
  }

  setActiveCompany(company: Company) {
    this.currentCompany = company;
    this.appService.activeCompany = company;
    if (this.router.url.includes('/operation/alarm')) {
      this.router.navigate(['/operation/alarm']);
    }
  }

  logout() {
    this.adalService.logout();
  }
  showPushNotification($event = null) {
    if ($event)
      $event.stopPropagation();
    if (this.showNotificationList) {
      this.showNotificationList = false;
    } else {
      this.showNotificationList = true;
      this.pushNotification.count = 0;
      this.updateNotificationViewedDate('push');
    }
    if (this.showEmailSummaryList) {
      this.showEmailSummaryList = false;
      this.openEmailSummaryDrawer.emit(this.showEmailSummaryList);
    }
    this.openNotificationDrawer.emit(this.showNotificationList);
  }

  showEmailSummary($event = null) {
    if ($event)
      $event.stopPropagation();
    if (this.showEmailSummaryList) {
      this.showEmailSummaryList = false;
    } else {
      this.showEmailSummaryList = true;
      this.emailSummaryCount = 0;
      this.updateNotificationViewedDate('email');
    }
    if (this.showNotificationList) {
      this.showNotificationList = false;
      this.openNotificationDrawer.emit(this.showNotificationList);
    }
    this.openEmailSummaryDrawer.emit(this.showEmailSummaryList);
  }

  updateNotificationViewedDate(type) {
    let date = moment();
    if (type === 'email' && this.appService.userProfileDetails['latestEmailDate']) {
      date = this.appService.userProfileDetails['latestEmailDate'];
    } else if (type === 'push' && this.appService.userProfileDetails['latestPushDate']) {
      date = this.appService.userProfileDetails['latestPushDate'];
    } else {
      date = this.getUserTimeZoneDateFormat(moment());
    }
    let data = {
      'userId': this.appService.userProfileDetails['userId'],
      'dateViewed': date,
      'type': type
    }
    this.headerService.updateViewedDate(data)
      .subscribe((obj) => {
        if (type === 'push')
          this.appService.userProfileDetails['pushViewed'] = data['dateViewed'];
        else if (type === 'email') {
          this.appService.userProfileDetails['emailViewed'] = data['dateViewed'];
          this.openEmailSummaryDrawer.emit('update');
        }
      }, (err) => {
        console.log(err);
      });
  }

  getUserTimeZoneDateFormat(value) {
    let convertedTimezone = '';
    if (this.appService.userTimezone != null) {
      convertedTimezone = moment.tz(value, this.appService.userTimezone).utc().format();
    } else {
      convertedTimezone = moment.utc(value).local().format();
    }
    return convertedTimezone;
  }
}
