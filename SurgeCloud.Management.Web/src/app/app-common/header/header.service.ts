import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../../shared-module';


@Injectable()
export class HeaderService {

  constructor(private httpsService: HttpService) { }

  getcompanies(): Observable<any> {
    return this.httpsService.get('organization/api/organization/get')
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  updateViewedDate(data): Observable<any> {
    return this.httpsService.post('users/api/users/UpdateViewedDate', data)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

}
