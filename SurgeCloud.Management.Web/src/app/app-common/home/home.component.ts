import { AfterViewInit, ChangeDetectorRef, ViewEncapsulation, Component, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { AppStartService } from '../../';
import * as _ from 'underscore';
import { Company, INavigation } from '../../app';
import { AppService } from '../../app.service';
import { ActivatedRoute } from '@angular/router';
import { AdalService } from "../../shared-module";
import { Subscription } from "rxjs/Subscription";
import * as jQuery from 'jquery';
import {TranslateService, LangChangeEvent} from "@ngx-translate/core";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit, AfterViewInit, OnDestroy {
  public isOpened = false;
  private sidebar: INavigation[] = [];
  public isLoading = false;
  private iconAvailable: string[] = [
    "icon_CustomerOnboarding.svg#CustomerOnboarding",
    "icon_AssetDeviceOnboarding.svg#AssetDeviceOnboarding",
    "icon_SubscriptionManagement.svg#SubscriptionManagement",
    "icon_TenantManagement.svg#TenantManagement",
    "icon_DeviceCloudManagement.svg#DeviceCloudManagement",
    "icon_CreateGeoFence.svg#CreateGeoFence"
  ];
  isAdminModule: boolean;
  showNotification: boolean;
  pushNotification = {
    'count': 0,
    'loading': true
  };
  showESummary: boolean;
  eSummaryCount = 0;
  loaderSubscription: Subscription;
  constructor(public appStartService: AppStartService, private appService: AppService, private adalService: AdalService,
    private route: ActivatedRoute, private renderer: Renderer2, private changeDetectorRef: ChangeDetectorRef,private translate: TranslateService) {
    /*this.appService.userDataSetUp.subscribe((userData) => {
      console.log('userDataSetUp', userData);
    });*/
    //console.log(this.adalService.userInfo);
    //console.log(this.appService.userTimezone);
    /*if(!this.appService.userTimezone) {
      this.appService.setTimezone();
    }
    console.log(appService.userProfileDetails);
    console.log(this.appService.userProfileDetails['userId']);*/
    //console.log(appService.userProfileDetails);
    if (!appService.userProfileDetails) {
      this.appService.setLogInUserDetails();
    }

    this.loaderSubscription = this.appService.loader.subscribe((data) => {
      setTimeout(() => {
        this.isLoading = data;
      }, 0);
    });
    //console.log('home component', this.adalService.userInfo);

    if (this.appService.availableFeatures.length == 0) {
      this.appService.getPermissions();
    }
    this.appService.getCompaniesData().subscribe(data => {
      //console.log(data);
      this.appService.companies = data[0];
      //console.log(this.appService.userProfileDetails);
      this.appService.setLogInUserDetails().subscribe((res) => {
        //console.log(res);
        let defaultCompany: Company;
        if (res.defaultCompanyId && res.defaultCompanyId != '') {
          this.appService.companies.forEach((company, key) => {
            if (company.companyId == res.defaultCompanyId) {
              defaultCompany = this.appService.companies[key];
              return false;
            }
          });
        } else {
          defaultCompany = this.appService.companies[0];
        }
        this.appService.activeCompany = defaultCompany;
        this.appService.loader.next(false);
        //this.appService.activeCompany = this.appService.companies[0];
      });

    });

    this.route.params.subscribe((param) => {
      if (param['type'] === 'admin') {
        this.isAdminModule = true;
        this.renderer.addClass(document.body, 'admin');
      } else {
        this.isAdminModule = false;
        this.renderer.removeClass(document.body, 'admin');
      }
    });
    this.appService.loader.next(true);
    // this.appService.companies = this.route.snapshot.data['company'][0];
    // this.appService.activeCompany = this.appService.companies[0];
  }

  ngOnInit() {


    document.getElementsByTagName('body')[0].addEventListener('click', function () {
      let exportDiv = document.getElementById('exportAsOptions');
      if (exportDiv) {
        exportDiv.classList.remove('export-as-options-show');
      }
    });
  }
  ngAfterViewInit() {
    //console.log('After view init');
    this.appService.feature.subscribe(
      () => {
        this.settingUpSidebar();
      }
    );
    //console.log(this.appService.availableFeatures);
    if (this.appService.availableFeatures.length > 0) {
      this.settingUpSidebar();
    }
    this.appService.userDataSetUp.subscribe((userData) => {
      //console.log(userData);
    });
    this.changeDetectorRef.detectChanges();

    jQuery(document).on('click', () => {
      this.appService.clickOutsideDrawer();
    });
    jQuery(document).on('click', '.email-summary, .e-summary-li, .push-notification-container, .push-notification-li', function (e) {
      //console.log('Click');
      e.stopPropagation();
    });
  }
  ngOnDestroy() {
    this.loaderSubscription.unsubscribe();
  }
  settingUpSidebar() {
    let availableSidebar = _.sortBy(this.appStartService.sidebar, 'sortOrder');
    availableSidebar.forEach((side, key) => {
      side.imageIcon = `${side.imageIcon}${this.iconAvailable[key]}`;
    });
    let sidebar = this.appService.getAccessibleSideNavigation(availableSidebar);
    this.translate.onLangChange.subscribe((event:LangChangeEvent) => {
    sidebar.forEach(data => {
      let name=this.sentenceToKey(data.name);
      this.translate.stream(name).subscribe(res => {
        console.log(res);
        this.sidebar.push({ controlLevel: data.controlLevel,
          featureId: data.featureId,
          name: res,
          sortOrder: data.sortOrder,
          status: data.status,
          imageIcon:data.imageIcon,
          imageHoverIcon: data.imageHoverIcon,
          color: data.color,
          url: data.url,
          enabled: data.enabled});

      });

    });
    });
    //this.changeDetectorRef.detectChanges();
  }
  toggleMenu() {
    this.isOpened = !this.isOpened;
  }
  showPushNotification(showNotification: boolean) {
    //console.log(showNotification);
    //this.showESummary = false;
    this.showNotification = showNotification;
  }

  setPushNotificationCount(data) {
    this.pushNotification = data;
  }
  emailSummaryCount(count: number) {
    this.eSummaryCount = count;
  }
  showEmailSummary(open: boolean) {
    //this.showNotification = false;
    //console.log(open);
    this.showESummary = open;
  }

  changeRoute(data) {
    this.isOpened = false;
    this.appService.changeRoute = data.url;
  }

  sentenceToKey(str){
    let strr=str.toLowerCase().trim().split(/\s+/).join('_');
    return strr;

  }

}
