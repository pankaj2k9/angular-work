import { Component, OnInit } from '@angular/core';
import { AdalService } from '../../shared-module';
import {ActivatedRoute, Router} from '@angular/router';
import {AppService} from "../../app.service";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  isLoggedIn = false;
  constructor(public adalService: AdalService, private router: Router, private appService: AppService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    // this.adalService.handleCallback();

    if (!this.adalService.userInfo) {
      this.router.navigate(['login']);
    } else {
      this.isLoggedIn = true;
      this.adalService.applyRedirectionAtLogin = true;
      this.router.navigate(['/admin/feature']);
    }
  }

  signIn() {
    this.adalService.login();
  }
}
