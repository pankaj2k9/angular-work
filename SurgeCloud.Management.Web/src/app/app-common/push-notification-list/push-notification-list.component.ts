import { Component, ViewEncapsulation, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, NavigationError, NavigationCancel, RoutesRecognized } from '@angular/router';
import { AppService } from "../../app.service";
import { Socket } from "ng-socket-io";
declare var moment: any;
import { CreateRoleGroupService } from "../../admin/customer-onboarding/create-role-group/create-role-group.service";
import { Subscription } from "rxjs/Subscription";
import * as _ from 'underscore';

@Component({
  selector: 'app-push-notification-list',
  templateUrl: './push-notification-list.component.html',
  styleUrls: ['./push-notification-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PushNotificationListComponent implements OnInit, OnChanges, OnDestroy {
  @Input() openNotificationDrawer = false;
  pushData = [];
  pushSubscription: Array<string>;
  socketConnectPrm: any;
  subscribedTopics: Array<string>;
  pushNotifications: Array<object>;
  body: any;
  private userCompanyRoleData = {};
  public pushNotificationInterval: any;
  @ViewChild('notification') notification;
  notificationContainerBottomPosition: string;
  @Output() setPushNotificationCount: EventEmitter<any> = new EventEmitter<any>();
  public pushNotificationsCount = 0;
  private assetIdArray = [];
  constructor(private appService: AppService,
    private pushSocket: Socket,
    private createRoleGroupService: CreateRoleGroupService,
    private router: Router) {
    setTimeout(() => {
      const pageFooterHeight = document.getElementById('appFooter').offsetHeight;
      this.notificationContainerBottomPosition = pageFooterHeight + 'px';
    }, 0);
  }

  ngOnInit() {
    this.appService.userProfileDetailsSubscription.subscribe((data) => {
      this.loadPushSummary(false, true);
    });
    this.appService.removeAllListeners.subscribe((data) => {
      // console.log(data);
    });
  }

  loadPushSummary(updateProfile = false, isFirst) {
    let date = (isFirst || (this.appService.userProfileDetails && this.appService.userProfileDetails['pushViewed'] === null)) ? '' : moment.utc(this.appService.userProfileDetails['pushViewed']).format('MM/DD/YYYY, hh:mm:ss.SSS A');
    date = date.replace(',', '');
    this.appService.getPushSummary(date)
      .finally(() => {
        if (!isFirst) {
          this.setIntervalForPushNotification();
        }
      })
      .subscribe((pushSummary) => {
        if (isFirst) {
          this.pushNotifications = JSON.parse(JSON.stringify(pushSummary));
          this.loadPushSummary(false, false);
        } else {
          this.setPushNotificationCount.emit({
            'count': pushSummary.length,
            'loading': false
          });
          if (pushSummary.length > 0 && this.pushNotifications && this.pushNotifications.length === 0) {
            this.pushNotifications = JSON.parse(JSON.stringify(pushSummary));
          }
          if (this.pushNotifications)
            this.checkAndUpdatePushNotificationList(pushSummary.reverse());
        }
        this.userCompanyRoleData['companyId'] = this.appService.userProfileDetails.companyId;
      }, (err) => {
        console.log(err);
      });
  }

  checkAndUpdatePushNotificationList(data) {
    let result = this.groupByWithAssetIdAndSubscriptionId(this.pushNotifications, (item) => {
      return [item['assetId'], item['subscriptionId']];
    });
    let array = [];
    for (let i = 0; i < data.length; i++) {
      const item = data[i];
      if ((moment(item['sentOn']).valueOf() > moment(this.pushNotifications[0]['sentOn']).valueOf())) {
        this.pushNotifications.unshift(item);
        array.push(item);
      }
    }
    this.pushNotifications = _.sortBy(JSON.parse(JSON.stringify(this.pushNotifications)), (item) => {
      return -moment(item['sentOn']).valueOf();
    });
    if (array.length > 0)
      this.updateNotifications(JSON.parse(JSON.stringify(array[0])));
    if (this.pushNotifications.length > 0)
      this.appService.userProfileDetails['latestPushDate'] = this.pushNotifications[0]['sentOn'];
  }

  groupByWithAssetIdAndSubscriptionId(array, f) {
    let groups = {};
    array.forEach(function (o) {
      var group = JSON.stringify(f(o));
      groups[group] = groups[group] || [];
      groups[group].push(o);
    });
    return Object.keys(groups).map(function (group) {
      return groups[group];
    });
  }

  setIntervalForPushNotification() {
    this.pushNotificationInterval = setTimeout(() => {
      this.loadPushSummary(false, false);
      clearTimeout(this.pushNotificationInterval);
    }, 5000);
  }

  subscribeForAlarm() {
    this.pushSocket.connect();
    this.pushSocket.emit('subscribe', this.pushSubscription);
    this.pushSocket.on('data', (data) => {
      data.event_param = JSON.parse(data.event_param);
      if (this.subscribedTopics.includes(data.event_param[0].name) && this.assetIdArray.includes(data.assetID)) {
        if (this.pushNotifications.length > 0) {
          let item = this.pushNotifications[0];
          let date = item['updatedOn'] ? item['updatedOn'] : item['createdOn'];
          if ((moment(date) < moment(data.occurrence_date_time))) {
            this.updateNotifications(JSON.parse(JSON.stringify(data)));
          }
        } else {
          this.updateNotifications(JSON.parse(JSON.stringify(data)));
        }
      }
    });
  }

  updateNotifications(data) {
    this.showNotification(data['topic'], data['body']);
  }

  showNotification(tag, body) {
    let ask = Notification.requestPermission();
    ask.then((permission) => {
      if (permission === 'granted') {
        var div = document.createElement("div");
        div.innerHTML = body;
        var text = div.textContent || div.innerText || "";
        let msg = new Notification('', {
          body: text,
          icon: './assets/icons/notification.png',
          tag: tag
        });
        setTimeout(msg.close.bind(msg), 3000);
      }
    })
    //this.body = body;
    //this.notification.show();
  }

  removeNotification(key: number) {
    this.pushNotifications.splice(key, 1);
  }

  getTopicDataByTopicName(topicName: string) {
    let topicData = {};
    this.pushData.forEach((item) => {
      if (item.topic == topicName) {
        topicData = JSON.parse(JSON.stringify(item));
        return false;
      }
    });
    return topicData;
  }

  pushNotificationMessageBodyFilter(body: string, replaceableContent: object) {
    let ATS = this.getDateFormat(replaceableContent['event_param'][0]['ATS']);
    replaceableContent['assetID'] = replaceableContent['assetID'];
    body = body.replace('$display_name', this.appService.userProfileDetails['displayName']);
    body = body.replace('$assetId', replaceableContent['assetID']);
    body = body.replace('$gw_Sno', replaceableContent['gw_sno']);
    body = body.replace('$priority', replaceableContent['event_param'][0]['pri']);
    body = body.replace('$alarm_name', replaceableContent['event_param'][0]['name']);
    body = body.replace('$value', replaceableContent['event_param'][0]['val']);
    body = body.replace('$status', replaceableContent['event_param'][0]['sts']);
    body = body.replace('$alarm_description', replaceableContent['event_param'][0]['des'] == null ? '' : replaceableContent['event_param'][0]['des']);
    body = body.replace('$active_timestamp', ATS == null ? '' : ATS);
    body = body.replace('$normal_timestamp', replaceableContent['event_param'][0]['NTS'] == null ? '' : replaceableContent['event_param'][0]['NTS']);
    body = body.replace('$reference', replaceableContent['event_param'][0]['_ref'] == null ? '' : replaceableContent['event_param'][0]['_ref']);
    //body = body.replace('$latitude', replaceableContent['event_param'][0]['_ref'] == null ? '' : replaceableContent['event_param'][0]['_ref']);
    return body;
  }

  getDateFormat(value) {
    if (value && !value.includes('Z')) value = value + 'Z';
    let convertedTimezone = '';
    if (this.appService.userTimezone != null) {
      convertedTimezone = moment.tz(value, this.appService.userTimezone).format('MM/DD/YYYY, hh:mm A');
    } else {
      convertedTimezone = moment.utc(value).local().format('MM/DD/YYYY, hh:mm A');
    }
    return convertedTimezone;
  }

  ngOnChanges() {
  }

  ngOnDestroy() {
    clearInterval(this.pushNotificationInterval);
  }

  destroySocketConnection() {
    if (this.socketConnectPrm && this.socketConnectPrm != null) {
      this.socketConnectPrm.removeAllListeners();
      this.socketConnectPrm.disconnect();
    }
    this.pushSocket.removeAllListeners();
    this.pushSocket.disconnect();
  }

  goToOperationAlarm(data) {
    this.openNotificationDrawer = false;
    this.appService.setCompanyOnNotificationClick.next(data);
    this.router.navigate(['/operation/alarm/' + data.assetId]);
  }
}



 /*this.pushData = [
              {
                "subscriptionId": "6ca9b111-586f-46d0-880e-fed2b8ab90e7",
                "topicId": null,
                "topic": "Unplug Event",
                "companyId": "4eabb792-03f6-4d8f-b0c6-25f52bbc2c68",
                "companyName": "BlueSurge Technologies",
                "groupId": null,
                "roleId": "e022a0a5-e12c-426e-8944-aeb8b650e1f1",
                "userId": "3c5630bc-ec8a-4623-aec1-889b83136a69",
                "assetId": "101",
                "gwSNO": "R115LLA0055",
                "body": "<p>Dear&nbsp;$display_name</p>\n\n<p>&nbsp;</p>\n\n<p>There is an issue with an asset.&nbsp;</p>\n",
                "createdBy": "Admin",
                "createdOn": '2017-12-06T05:56:36.000Z',
                "updatedBy": "Admin",
                "updatedOn": "2018-01-14T07:23:50.731",
                "status": 1
              },
              {
                "subscriptionId": "9890a55d-1c59-453f-8a5f-47f6194cbeb7",
                "topicId": null,
                "topic": "Engine Speed",
                "companyId": "4eabb792-03f6-4d8f-b0c6-25f52bbc2c68",
                "companyName": "BlueSurge Technologies",
                "groupId": null,
                "roleId": "e022a0a5-e12c-426e-8944-aeb8b650e1f1",
                "userId": "3c5630bc-ec8a-4623-aec1-889b83136a69",
                "assetId": "Abhiv2",
                "gwSNO": "R115LLA0055",
                "body": "<p>Dear&nbsp;$display_name</p>\n\n<p>&nbsp;</p>\n\n<p>There is an issue with an asset.&nbsp;</p>\n",
                "createdBy": "Admin",
                "createdOn": '2017-12-06T05:56:36.000Z',
                "updatedBy": "Admin",
                "updatedOn": "2018-01-14T07:23:50.731",
                "status": 1
              },
              {
                "subscriptionId": "e81e8f34-06df-4033-b473-cf01b23cab99",
                "topicId": null,
                "topic": "Engine Speed",
                "companyId": "4eabb792-03f6-4d8f-b0c6-25f52bbc2c68",
                "companyName": "BlueSurge Technologies",
                "groupId": null,
                "roleId": "e022a0a5-e12c-426e-8944-aeb8b650e1f1",
                "userId": "3c5630bc-ec8a-4623-aec1-889b83136a69",
                "assetId": "Abhiv2",
                "gwSNO": "R115LLA0055",
                "body": "<p>Dear&nbsp;$display_name</p>\n\n<p>&nbsp;</p>\n\n<p>There is an issue with an asset.&nbsp;</p>\n",
                "createdBy": "Admin",
                "createdOn": '2017-12-06T05:56:36.000Z',
                "updatedBy": "Admin",
                "updatedOn": "2018-01-14T07:23:50.731",
                "status": 1
              }
            ];*/



                  // this.appService.getUserRoleData().subscribe((roleData) => {
      //   this.userCompanyRoleData['roleId'] = roleData.roleId;
      //   this.createRoleGroupService.getTopicPushSubscription(this.userCompanyRoleData['companyId'], this.userCompanyRoleData['roleId'], true).subscribe((pushDataResponse) => {
      //     this.pushData = pushDataResponse;
      //     this.pushData.forEach((pushItem) => {
      //       if (pushItem.status === 1 && pushItem.userId == this.appService.userProfileDetails['userId']) {
      //         const subscriptionString = pushItem.assetId + '.' + pushItem.gwSNO + '.' + 'alarm';
      //         if (this.pushSubscription.indexOf(subscriptionString) == -1) {
      //           this.pushSubscription.push(subscriptionString);
      //           this.assetIdArray.push(pushItem.assetId);
      //         }
      //         this.subscribedTopics.push(pushItem.topic);
      //       }
      //     });
      //     this.destroySocketConnection();
      //     // this.subscribeForAlarm();
      //   });
      // });



  // updateNotifications(data) {
  //   const topicData = this.getTopicDataByTopicName(data.event_param[0].name);
  //   const filteredBody = this.pushNotificationMessageBodyFilter(topicData['body'], JSON.parse(JSON.stringify(data)));
  //   topicData['body'] = filteredBody;
  //   topicData['updatedOn'] = data.occurrence_date_time;
  //   topicData['assetId'] = data.assetID;
  //   this.pushNotifications.unshift(topicData);
  //   this.showNotification(data.event_param[0].name, filteredBody);
  //   this.setPushNotificationCount.emit({
  //     'count': this.pushNotifications.length,
  //     'loading': false
  //   });
  // }
