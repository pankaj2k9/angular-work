import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { HomeComponent } from './app-common';
import { AuthenticationGuard } from "./authentication.guard";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'user',
    component: HomeComponent,
    canActivate: [AuthenticationGuard],
    children: [
      {
        path: '',
        loadChildren: './admin/customer-onboarding/customer-onboarding.module#CustomerOnboardingModule'
      }
    ]
  },
  {
    path: ':type',
    component: HomeComponent,
    // resolve: {
    //   company: CompanyResolverService
    // },
    canActivate: [AuthenticationGuard],

    children: [
      {
        path: '',
        loadChildren: './admin/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'tenant-management',
        loadChildren: './admin/tenant-management/tenant-management.module#TenantManagementModule'
      },
      {
        path: 'subscription-management',
        loadChildren: './admin/subscription-management/subscription-management.module#SubscriptionManagementModule'
      },
      {
        path: 'device-management',
        loadChildren: './admin/device-management/device-management.module#DeviceManagementModule'
      },
      {
        path: 'geo-fencing',
        loadChildren: './operation/geo-fencing/geo-fencing.module#GeoFencingModule'
      },
      {
        path: 'asset-dashboard/:assetId',
        loadChildren: './operation/asset-dashboard/asset-dashboard.module#AssetDashboardModule'
      },
      {
        path: 'asset-dashboard',
        loadChildren: './operation/asset-dashboard/asset-dashboard.module#AssetDashboardModule'
      },
      {
        path: 'alarm',
        loadChildren: './operation/alarm-console/alarm-console.module#AlarmConsoleModule'
      },
      {
        path: 'rule-engine',
        loadChildren: './operation/rule-engine/rule-engine.module#RuleEngineModule'
      },
      {
        path: 'alarm/:assetId',
        loadChildren: './operation/alarm-console/alarm-console.module#AlarmConsoleModule'
      },
      {
        path: 'alarm/:assetId/:gwSNO',
        loadChildren: './operation/alarm-console/alarm-console.module#AlarmConsoleModule'
      },
      {
        path: 'generator-monitoring',
        loadChildren: './operation/generator-monitoring/generator-monitoring.module#GeneratorMonitoringModule'
      },
      {
        path: 'routing',
        loadChildren: './operation/create-route/create-route.module#CreateRouteModule'
      },
      {
        path: 'geo-fence-create',
        loadChildren: './operation/geo-fence-create/geo-fence-create.module#GeoFenceCreateModule'
      },
      {

        path: 'payload-data',
        data: { reuse: true },
        loadChildren: './operation/payload-data/payload-data.module#PayLoadDataModule'
      },
      {
        path: 'person-counter',
        loadChildren: './operation/person-counter/person-counter.module#PersonCounterModule'
      },
      {
        path: 'routing-dispatch',
        loadChildren: './operation/routing-dispatch/routing-dispatch.module#RoutingDispatchModule'

      },
      {
        path: 'industrial-building',
        loadChildren:'./operation/industrial-building/industrial-building.module#IndustrialBuildingModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
