import {Component, OnInit, DoCheck, KeyValueDiffers} from '@angular/core';
import { AppStartService } from './app-start.service';
// import { AdalService } from 'ng2-adal/services/adal.service';
// import { OAuthData } from 'ng2-adal/services/oauthdata.model';
import { environment } from '../environments/environment';
import {Router, ActivatedRoute} from "@angular/router";
import { AdalService } from './shared-module';
import { AppService } from './app.service';
import {TranslateService} from "@ngx-translate/core";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit  {

  public options = {
    position: ["bottom", "right"],
    timeOut: 5000,
    lastOnBottom: true
  };

  public routerParam: any;

  constructor(private differs: KeyValueDiffers,public appStartService: AppStartService, private adalService: AdalService, public appService: AppService, public translate: TranslateService, private route: ActivatedRoute) {





  }

  ngOnInit(){













  }

  ngOnDestroy() {
    this.routerParam.unsubscribe();
  }




}
