import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import 'hammerjs';
import { CoreModule } from './core/core.module';
import { AppCommonModule } from './app-common/app-common.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { environment } from '../environments/environment';
import { AppStartService, startupServiceFactory } from './app-start.service';
import { AppService } from './app.service';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NgProgressModule } from 'ng2-progressbar';
import { DataTableModule } from 'angular2-datatable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { AuthenticationGuard } from './authentication.guard';
import { LoginResolver } from './login-resolver.service';
import { CustomerOnboardingService } from './admin/customer-onboarding';
import { AssetOnboardingService } from './admin/asset-onboarding';
import { OperationModule, OperationService } from './operation';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { AssetDashboardService } from './operation/asset-dashboard';
import { AdalService } from './shared-module/services/adal/adal.service';
import { LocalStorageModule } from 'angular-2-local-storage';
import { UnitConversionFormulaService } from './unit-conversion-formula.service';
import { FlexLayoutModule } from '@angular/flex-layout';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

const config: SocketIoConfig = { url: environment.socket.alarm, options: {} };
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    NgProgressModule,
    CoreModule,
    AppCommonModule,
    FlexLayoutModule,
    //AdminModule,
    //OperationModule,
    AgmCoreModule.forRoot({
      apiKey: environment.googleApiKey,
      libraries: ['places', 'drawing']
    }),
    LocalStorageModule.withConfig({
      prefix: 'surgeCloud',
      storageType: 'localStorage'
    }),
    SimpleNotificationsModule.forRoot(),
    AppRoutingModule,
    DataTableModule,
    NgbModule.forRoot(),
    SocketIoModule.forRoot(config),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    AppStartService,
    {
      provide: APP_INITIALIZER,
      useFactory: startupServiceFactory,
      deps: [AppStartService],
      multi: true
    },
    AdalService,
    AppService,
    UnitConversionFormulaService,
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    AuthenticationGuard,
    GoogleMapsAPIWrapper,
    LoginResolver,
    CustomerOnboardingService,
    AssetOnboardingService,
    OperationService,
    AssetDashboardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
