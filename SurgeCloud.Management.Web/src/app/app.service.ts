import { Injectable } from '@angular/core';
import { Company, Features } from './app';
import { NotificationsService } from 'angular2-notifications';
import { environment } from '../environments/environment';
import { Subject } from 'rxjs/Subject';
import * as _ from 'underscore';
import { AdalService, HttpService } from './shared-module';
import { Observable, } from 'rxjs/Observable';
import { AppStartService } from './app-start.service';
import * as jQuery from 'jquery';
import { CreateRoleGroupService } from "./admin/customer-onboarding/create-role-group/create-role-group.service";
import { Users } from "./app-common";
import { Http, Headers } from '@angular/http';
declare var moment: any;
import { LocalStorageService } from 'angular-2-local-storage';

import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';
declare var jsPDF: any; // Important

type AOA = Array<Array<any>>;
function s2ab(s: string): ArrayBuffer {
  const buf: ArrayBuffer = new ArrayBuffer(s.length);
  const view: Uint8Array = new Uint8Array(buf);
  for (let i = 0; i !== s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
  return buf;
}

@Injectable()
export class AppService {

  private _companies: Company[] = [];
  private _activeCompany: Company;
  public activeCompanyInfo = new Subject<Company>();
  public loader = new Subject<boolean>();
  public availableFeatures: Features[] = [];
  public feature: Subject<any> = new Subject();
  public _user: any;
  public previousRoute: String = 'admin/feature';
  public companyList = new Subject<Company[]>();
  public defaultLat = 37.090200;
  public defaultLng = -95.712900;
  public ManagementOperation: any = {
    'value': false,
    'id': ''
  };
  userProfileDetails: Users;
  userProfileDetailsSubscription: Subject<object> = new Subject<object>();
  timezone: Array<object>;
  userTimezone: string;
  userDataSetUp: Subject<object> = new Subject<object>();
  pieChartClick: Subject<object> = new Subject<object>();
  closeDrawer: Subject<boolean> = new Subject<boolean>();
  setCompanyOnNotificationClick: Subject<any> = new Subject<any>();
  removeAllListeners: Subject<boolean> = new Subject<boolean>();
  getPushNotification: Subject<boolean> = new Subject<boolean>();
  public appExportData = new Subject();
  public wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'binary' };

  constructor(private _notificationsService: NotificationsService,
    private adalService: AdalService, public httpService: HttpService,
    public appStartService: AppStartService, private createRoleGroupService: CreateRoleGroupService,
    public localStorageService: LocalStorageService, private http: Http
  ) {
    //console.log(adalService);
  }

  getGeoCodeLocation(latitude: number, longitude: number) {
    let url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&output=json&sensor=false&key=${environment.googleApiKey}`;
    let headers = new Headers();
    return this.http.get(url, { headers: headers });
  }

  setTimezone() {
    this.getTimezone().subscribe(
      (data) => {
        this.timezone = data;
        this.userTimezone = this.getUserTimezone(this.userProfileDetails.timezone);
      }
    );
  }

  set companies(companies: Company[]) {
    this._companies = [];
    companies.forEach(company => {
      this._companies.push(new Company(company));
    });
    this.companyList.next(this._companies);
  }

  get companies(): Company[] {
    return this._companies;
  }
  getFeatureDashboardSvgIcons() {
    return [
      // 'icon_AdvancedAanalytics.svg#Vector_Smart_Object_copy',
      'LandingPageIcons/BasicAnalytics',
      'LandingPageIcons/GeofenceTracking',
      'LandingPageIcons/Events&Alarms',
      'LandingPageIcons/AssetDashboard',
      'LandingPageIcons/RoutingDispatch',
      'LandingPageIcons/Reports',
      'LandingPageIcons/ConnectedDiognostics',
      'LandingPageIcons/ConnectedCalibration',
      'LandingPageIcons/SafetyCompliance',
      'LandingPageIcons/ServiceManagement',
      'LandingPageIcons/Geofencing',
      'LandingPageIcons/DynamicDashboard',
      'LandingPageIcons/GeneratorMonitoring',
      'LandingPageIcons/RuleEngine',
      'LandingPageIcons/PersonCounter'
    ];
  }
  getFeatureDashboardCardColors() {
    return [
      'light-green',
      'light-sea-blue',
      'violet',
      'light-blue',
      'grey',
      'light-red',
      'blue',
      'light-yellow',
      'light-blue',
      'green',
      'purple',
      'orange-red',
      'dark-red',
      'purple-blue',
      'brown'
    ];
  }
  getCompaniesData() {
    return this.httpService.get(`users/api/users/getuser/${this.getUserId()}`)
      .map((res) => {
        this.user = res.json();
        return res.json();
      })
      .flatMap(
        (data) => {
          return Observable.forkJoin(
            this.httpService.get(`organization/api/organization/get/${this.user.companyName}`)
              .map(response => response.json())
          )
        });
  }
  getOrganizationList(companyName: string) {
    return this.httpService.get(`organization/api/organization/get/${companyName}`).map((res) => {
      return res.json();
    });
  }
  getLandingPages() {
    return this.httpService.get('users/api/roles/getfeatures').map((res) => {
      return res.json();
    });
  }
  setLogInUserDetails() {
    return this.httpService.get(`users/api/users/getuser/${this.getUserId()}`).map((res) => {
      this.userProfileDetails = res.json();
      this.setTimezone();
      this.userProfileDetails.landingPage = this.userProfileDetails.landingPage.includes('admin/dashboard') ? 'admin/feature' : this.userProfileDetails.landingPage;
      this.localStorageService.set('userDetails', JSON.stringify(this.userProfileDetails));
      this.userProfileDetailsSubscription.next(this.userProfileDetails);
      return res.json();
    });
  }
  getUserDetailsById(userId: string) {
    return this.httpService.get(`users/api/users/getuser/${userId}`).map((res) => {
      return res.json();
    });
  }
  getLanguages() {
    return ['English'];
  }
  getUnits() {
    return ['Metric', 'Imperial'];
  }
  getTimezone(): Observable<any> {
    return this.httpService.get('common/api/timezone')
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }
  getUserTimezone(timezoneId: string) {
    let timezoneValue = '';
    this.timezone.forEach((item) => {
      if (item['id'] == timezoneId) {
        timezoneValue = item['javaTimezone'];
        // console.log(item);
        return false;
      }
    });
    return timezoneValue;
  }
  getPushSummary(sentOn) {
    return this.httpService.get(`users/api/users/getpushsummary/${this.userProfileDetails.companyId}?userid=${this.getUserId()}&sentOn=${sentOn}`).map((res) => {
      return res.json();
    })
  }

  getEmailSummary(sentOn) {
    const userDetails = JSON.parse(this.localStorageService.get('userDetails'));
    //console.log(userDetails, this.getUserId());
    return this.httpService.get(`users/api/users/getemailsummary/${userDetails.companyId}?userid=${userDetails.userId}&sentOn=${sentOn}`).map((res) => {
      return res.json();

    })
  }

  getLoadType() {
    return this.httpService.get('common/api/generic/getloadtype')
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }
  getActiveCompany(): Company {
    return this._activeCompany;
  }

  updateCompany(data: Company, companyId: string) {
    let index = _.indexOf(this._companies, _.findWhere(this._companies, { companyId: companyId }));
    this._companies[index] = data;
    this.companyList.next(this._companies);
  }

  addCompany(data: Company) {
    this._companies.push(data);
    this.companyList.next(this._companies);
  }

  set changeRoute(data) {
    this.previousRoute = data;
  }

  get changeRoute() {
    return this.previousRoute;
  }

  get ManagementAddEdit() {
    return this.ManagementOperation;
  }

  set ManagementAddEdit(data) {
    this.ManagementOperation = data;
  }

  showErrorMessage(title: string, message: string) {
    this._notificationsService.error(
      title,
      message,
      {
        timeOut: 5000,
        showProgressBar: true,
        pauseOnHover: true,
        clickToClose: false,
        maxLength: 100
      }
    );
  }

  showSuccessMessage(title: string, message: string) {
    this._notificationsService.success(
      title,
      message,
      {
        timeOut: 5000,
        showProgressBar: true,
        pauseOnHover: true,
        clickToClose: false,
        maxLength: 100
      }
    );
  }

  set activeCompany(activeCompany: Company) {
    this._activeCompany = activeCompany;
    this.activeCompanyInfo.next(activeCompany);
  }

  getUserId(): string {
    return this.adalService.userInfo.profile.oid;
  }

  getPermissions() {
    this.httpService.get(`users/api/roles/GetUserRoleData/${this.getUserId()}`)
      .map((res) => res.json())
      .subscribe(
        (data) => {
          if (data && data.features) {
            data.features.forEach((feature) => {
              const model = new Features(
                feature.canAccess,
                feature.canAdd,
                feature.canDelete,
                feature.canUpdate,
                feature.featureId,
                feature.roleId
              )
              this.availableFeatures.push(model);
            });
            this.feature.next(data.features);
          }
        }
      );
  }

  getAccessibleSideNavigation(availableSidebar: any[]): any[] {
    let features = [];
    availableSidebar.forEach((sidebar, key) => {
      let feature = _.findWhere(this.availableFeatures, { featureId: sidebar.featureId })
      if (feature && feature.canAccess) {
        features.push(sidebar);
      }
    });
    return features;
  }

  getAccessibleFeatures(availableFeatures: any[]): any[] {
    let features = [];
    availableFeatures.forEach((sidebar, key) => {
      let feature = _.findWhere(this.availableFeatures, { featureId: sidebar.featureId })
      if (feature && feature.canAccess) {
        features.push(sidebar);
      }
    });
    return features;
  }

  getLandingPage(): string {
    return 'admin/feature';
  }
  getUserRoleData() {
    //console.log('getUserRoleData');
    return this.httpService.get(`users/api/roles/GetUserRoleData/${this.getUserId()}`).map((res) => {
      return res.json();
    });
  }
  isAccessible(url: string): boolean {
    let available = this.appStartService.features;
    let feature = null;
    if (url.startsWith('/')) {
      url = url.replace('/', '');
    }
    feature = _.findWhere(available, { Url: url });
    return this.checkFeatureAccessible(feature && feature.FeatureId ? feature.FeatureId : null);
  }

  checkFeatureAccessible(featureId: string = null) {
    let accessible = _.pluck(this.availableFeatures, 'featureId');
    if (accessible.indexOf(featureId) > -1) {
      return true;
    } else {
      return false;
    }
  }

  set user(data: any) {
    this._user = data;
  }

  get user() {
    return this._user;
  }

  getCompanyNameByUser() {
    return this._user && this._user.CompanyName ? this._user.CompanyName : null;
  }

  setAllOptionPagination(id) {
    setTimeout(function () {
      jQuery('#' + id + ' .pagination.pull-right.float-sm-right li').children('.page-link').children('span').remove();
      let div = jQuery('#' + id + ' .pagination.pull-right.float-sm-right li:first-child').children('.page-link');
      div.append('<span class="all-option-pagination"> All</span>');
      jQuery('#' + id + ' .pagination.pull-right.float-sm-right li:first-child').remove('active');
    });
  }

  typeCastingForChart(value: any, toType: string) {
    let result: any;
    switch (toType) {
      case 'string': {
        result = String(value);
        break;
      }
      case 'number': {
        result = Number(value);
        break;
      }
      case 'date': {
        const date = new Date(value);
        // result = moment(date).format('DD/MM/YYYY hh:mm tt');
        let convertedTimezone = '';
        if (this.userTimezone != null) {
          convertedTimezone = moment.tz(value, this.userTimezone).format('MM/DD/YYYY, hh:mm A');
        } else {
          convertedTimezone = moment.utc(value).local().format('MM/DD/YYYY, hh:mm A');
        }
        result = convertedTimezone;
        // console.log(convertedTimezone);
        break;
      }
      default: {
        result = value;
        break;
      }
    }
    return result;
  }
  emitEventOnPieChartClick(data: object) {
    this.pieChartClick.next(data);
  }
  clickOutsideDrawer() {
    this.closeDrawer.next(true);
  }

  getUserTimeZoneDateFormat(date) {
    // if(date.includes(':') && !date.includes('Z')){
    //   date = date + 'Z';
    //   date = moment(date).utc().format();
    // }
    let tempDate = moment();
    if (this.userTimezone != null) {
      tempDate = moment.tz(date, this.userTimezone);
      tempDate = moment(tempDate).utcOffset(-tempDate.utcOffset());
    } else {
      tempDate = moment(date).utc();
    }
    return tempDate;
  }

  downloadAsExcel(headers, data, dates) {
    let excelData = [];
    let excelHeader = [];
    let downloadableHeader = [];
    headers.forEach(header => {
      downloadableHeader.push(header.dataKey);
    });
    headers.forEach(header => {
      excelHeader.push(header.title);
    });
    excelData.push(excelHeader);
    data.forEach((element, index) => {
      excelData[index + 1] = [];
      _.each(element, (value, key, obj) => {
        let cellValue = value === null || value === '' || value === undefined ? '' : value.toString();
        cellValue = (dates.includes(key) && moment(cellValue).isValid()) ? this.getUserTimeZoneDateFormat(cellValue).format('MM/DD/YYYY, hh:mm A') : cellValue;
        let cellIndex = Number(downloadableHeader.indexOf(key));
        excelData[index + 1][cellIndex] = cellValue;
      });
    });
    /* generate worksheet */
    const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(excelData);
    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    /* save to file */
    const wbout: string = XLSX.write(wb, this.wopts);
    saveAs(new Blob([s2ab(wbout)]), 'historical-data' + '_' + this.getActiveCompany().name + '_' + moment().format('YYYYMMDDHHMMSS') + '.xlsx');
  }
}


function _window(): any {
  // return the global native browser window object
  return window;
}
@Injectable()
export class WindowRef {
  get nativeWindow(): any {
    return _window();
  }
}
