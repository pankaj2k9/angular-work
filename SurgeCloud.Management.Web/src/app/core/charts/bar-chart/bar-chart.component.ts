import {Component, OnInit, Input, SimpleChange, SimpleChanges, Injector} from '@angular/core';
import {SingleChartData, MultiChartData, SingleChartDataa, BarChartConfig} from '../ichart';
import { DatePipe } from '@angular/common';
import {AppService} from "../../../app.service";
declare var moment: any;
@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss'],
  providers: [DatePipe]
})
export class BarChartComponent implements OnInit {


  @Input() data: any[] = [];
  public getAnalyticInfoo: any[] = [];
  public results: any[] = [];
  axisTickFormatting: any;

  barChartInfo: BarChartConfig;
  @Input() chartInfo: BarChartConfig;

  private appService: AppService;
  constructor(private datePipe: DatePipe, injector:Injector) {
    this.appService = injector.get(AppService);
    // console.log(this.appService.userTimezone);
  }

  ngOnInit() {
    //console.log(this.chartInfo);
    //this.chartInfo = this.chartInfo === undefined ? {} : this.chartInfo;
    //this.barChartInfo = new BarChart(this.chartInfo);
    this.barChartInfo = this.chartInfo;
    //console.log(this.barChartInfo);
    if (this.barChartInfo && this.barChartInfo.axisTickFormatting === false) {
      this.axisTickFormatting = null;
    }else{
      this.axisTickFormatting = ((data) => {
        return this.datePipe.transform(data, 'M/d');
      });
    }

  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.data && changes.data.currentValue) {
      //console.log(changes.data.currentValue);
      this.getAnalyticInfoo = changes.data.currentValue;
      this.getAnalyticInfo();
    }

    if (changes['chartInfo'] && changes['chartInfo'].currentValue) {
      let data = changes['chartInfo'].currentValue;
      console.log(data);
      if(changes['chartInfo'].currentValue.data) {
        data = changes['chartInfo'].currentValue.data;
        data.colorScheme = changes['chartInfo'].currentValue.colorScheme;
      }
      //console.log('Innnn');
      //console.log(data);
      this.barChartInfo = new BarChartConfig(
        data.width,
        data.height,
        data.showXAxis,
        data.showYAxis,
        data.xAxisValueDataType,
        data.yAxisValueDataType,
        data.gradient,
        data.showLegend,
        data.legendTitle,
        data.showXAxisLabel,
        data.xAxisLabel,
        data.showYAxisLabel,
        data.yAxisLabel,
        data.colorScheme,
        data.axisTickFormatting
      );
    }
  }

  getAnalyticInfo(){
    this.results = [];
    //console.log(this.getAnalyticInfoo);
    //console.log(this.barChartInfo);
    if(this.barChartInfo == undefined) {
      const barChartColorScheme = {
        domain: ['#52ba9b', '#f48b37', '#ef4846', '#348ea9', '#f3af2f', '#cf4385', '#a6d9fd', '#935bcd', '#4da338'
          , '#aaaaaa', '#f17cb0', '#b2912f', '#b276b2', '#decf3f']
      };
      this.barChartInfo = new BarChartConfig(
        100,
        300,
        true,
        true,
        'date',
        'number',
        false,
        false,
        'for last 6 months',
        false,
        'x',
        false,
        'y',
        barChartColorScheme,
        true
      );
    }
    if (this.getAnalyticInfoo) {
      this.getAnalyticInfoo.forEach(data => {
        this.results.push(new SingleChartDataa(this.appService.typeCastingForChart(data.value, this.barChartInfo.yAxisValueDataType), this.appService.typeCastingForChart(data.name, this.barChartInfo.xAxisValueDataType)));
      });
    }
    //console.log(this.results);
  }


  onSelect(event) {

  }
}
