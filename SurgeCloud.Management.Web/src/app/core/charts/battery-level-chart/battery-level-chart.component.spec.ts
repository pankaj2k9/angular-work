import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BatteryLevelChartComponent } from './battery-level-chart.component';

describe('BatteryLevelChartComponent', () => {
  let component: BatteryLevelChartComponent;
  let fixture: ComponentFixture<BatteryLevelChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BatteryLevelChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BatteryLevelChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
