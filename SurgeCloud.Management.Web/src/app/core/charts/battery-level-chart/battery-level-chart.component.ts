import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';

@Component({
  selector: 'app-battery-level-chart',
  templateUrl: './battery-level-chart.component.html',
  styleUrls: ['./battery-level-chart.component.scss']
})
export class BatteryLevelChartComponent implements OnInit {

  @Input() public data: any;
  public className:string = 'red';
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.data = changes.data.currentValue;
    if(this.data['value'] > 12.6) {
      this.className = 'green';
    } else if(this.data['value'] > 12) {
      this.className = 'orange';
    } else {
      this.className = 'red';
    }
  }

}
