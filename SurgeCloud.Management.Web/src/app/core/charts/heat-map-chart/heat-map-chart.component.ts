import { Component, OnInit, Input } from '@angular/core';
import { HeatMapChartConfig, MultiChartData } from '../ichart';
@Component({
  selector: 'app-heat-map-chart',
  templateUrl: './heat-map-chart.component.html',
  styleUrls: ['./heat-map-chart.component.scss']
})
export class HeatMapChartComponent implements OnInit {

  @Input() data: MultiChartData[] = this.data ? this.data : null;
  @Input() chartInfo: any;

  heatMapChartInfo: HeatMapChartConfig;


  constructor() { }

  ngOnInit() {
    //this.chartInfo = this.chartInfo === undefined ? {} : this.chartInfo;
    this.heatMapChartInfo = this.chartInfo;
  }

  onSelect(event) {
  }

}
