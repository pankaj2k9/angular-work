import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import * as jQuery from 'jquery';
declare var d3: any;

@Component({
  selector: 'app-radial-gauge',
  templateUrl: './radial-gauge.component.html',
  styleUrls: ['./radial-gauge.component.scss']
})
export class RadialGaugeComponent implements OnInit {

  @Input() public data = [];
  private powerGauge;
  private kPaRange = [0, 600];
  private psiRange = [0, 150];
  private selectedRange = this.kPaRange;
  constructor() { }
  ngOnInit() {
    this.setBasicConfigGauge();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.data = changes.data.currentValue;
    if (this.data) {
      if (this.data['unit'] === 'kPa') {
        this.selectedRange = this.kPaRange;
      } else {
        this.selectedRange = this.psiRange;
      }
    }
    if (this.powerGauge) {
      this.powerGauge['update'](this.data && this.data['value'] ? this.data['value'] : 0, { minValue: this.selectedRange[0], maxValue: this.selectedRange[1] });
    }
  }

  setBasicConfigGauge() {
    this.powerGauge = this.getGaugeObject('#power-gauge', {
      size: 150,
      clipWidth: 150,
      clipHeight: 100,
      ringWidth: 30,
      transitionMs: 4000
    });
    this.powerGauge['render']();
  }

  getGaugeObject(container, configuration) {
    let that = {};
    let config = {
      size: 100,
      clipWidth: 100,
      clipHeight: 55,
      ringInset: 10,
      ringWidth: 10,

      pointerWidth: 5,
      pointerTailLength: 5,
      pointerHeadLengthPercent: 0.9,

      minValue: this.selectedRange[0],
      maxValue: this.selectedRange[1],

      minAngle: -90,
      maxAngle: 90,

      transitionMs: 750,

      majorTicks: 3,
      labelFormat: d3.format(',g'),
      labelInset: 10,
      arcColorFn: d3.scale.quantize()
        .domain([0, 0.5, 1])
        .range(['#3fa5db', '#f08d3c', '#ed3833'])
    };
    let range = undefined;
    let r = undefined;
    let pointerHeadLength = undefined;
    let value = 0;

    let svg = undefined;
    let arc = undefined;
    let scale = undefined;
    let ticks = undefined;
    let tickData = undefined;
    let pointer = undefined;

    let donut = d3.layout.pie();

    function deg2rad(deg) {
      return deg * Math.PI / 180;
    }

    function newAngle(d) {
      let ratio = scale(d);
      let newAngle = config.minAngle + (ratio * range);
      return newAngle;
    }

    function configure(configuration) {
      let prop = undefined;
      for (prop in configuration) {
        config[prop] = configuration[prop];
      }
      range = config.maxAngle - config.minAngle;
      r = config.size / 2;
      pointerHeadLength = Math.round(r * config.pointerHeadLengthPercent);

      // a linear scale that maps domain values to a percent from 0..1
      scale = d3.scale.linear()
        .range([0, 1])
        .domain([config.minValue, config.maxValue]);

      ticks = scale.ticks(config.majorTicks);
      tickData = d3.range(config.majorTicks).map(function () { return 1 / config.majorTicks; });

      arc = d3.svg.arc()
        .innerRadius(r - config.ringWidth - config.ringInset)
        .outerRadius(r - config.ringInset)
        .startAngle(function (d, i) {
          var ratio = d * i;
          return deg2rad(config.minAngle + (ratio * range));
        })
        .endAngle(function (d, i) {
          var ratio = d * (i + 1);
          return deg2rad(config.minAngle + (ratio * range));
        });
    }
    that['configure'] = configure;

    function centerTranslation() {
      return 'translate(' + r + ',' + r + ')';
    }

    function isRendered() {
      return (svg !== undefined);
    }
    that['isRendered'] = isRendered;

    function render(newValue) {
      svg = d3.select(container)
        .append('svg:svg')
        .attr('class', 'gauge')
        .attr('width', config.clipWidth)
        .attr('height', config.clipHeight);

      let centerTx = centerTranslation();

      let arcs = svg.append('g')
        .attr('class', 'arc')
        .attr('transform', centerTx);

      arcs.selectAll('path')
        .data(tickData)
        .enter().append('path')
        .attr('fill', function (d, i) {
          return config.arcColorFn(d * i);
        })
        .attr('d', arc);

      let lg = svg.append('g')
        .attr('class', 'label')
        .attr('transform', centerTx);
      lg.selectAll('text')
        .data(ticks)
        .enter().append('text')
        .attr('transform', function (d) {
          let ratio = scale(d);
          let newAngle = config.minAngle + (ratio * range);
          return 'rotate(' + newAngle + ') translate(0,' + (config.labelInset - r) + ')';
        })
        .text(config.labelFormat);

      let lineData = [[config.pointerWidth / 2, 0],
      [0, -pointerHeadLength],
      [-(config.pointerWidth / 2), 0],
      [0, config.pointerTailLength],
      [config.pointerWidth / 2, 0]];
      let pointerLine = d3.svg.line().interpolate('monotone');
      let pg = svg.append('g').data([lineData])
        .attr('class', 'pointer')
        .attr('transform', centerTx);

      pointer = pg.append('path')
        .attr('d', pointerLine/*function(d) { return pointerLine(d) +'Z';}*/)
        .attr('transform', 'rotate(' + config.minAngle + ')');

      update(newValue === undefined ? 0 : newValue);
    }
    that['render'] = render;

    function update(newValue, newConfiguration = undefined) {
      if (newConfiguration !== undefined) {
        configure(newConfiguration);
      }
      let ratio = scale(newValue);
      let newAngle = config.minAngle + (ratio * range);
      pointer.transition()
        .duration(config.transitionMs)
        .ease('elastic')
        .attr('transform', 'rotate(' + newAngle + ')');
    }
    that['update'] = update;
    configure(configuration);
    return that;
  }
}
