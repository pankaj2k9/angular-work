import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpiderRadarChartComponent } from './spider-radar-chart.component';

describe('SpiderRadarChartComponent', () => {
  let component: SpiderRadarChartComponent;
  let fixture: ComponentFixture<SpiderRadarChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpiderRadarChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpiderRadarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
