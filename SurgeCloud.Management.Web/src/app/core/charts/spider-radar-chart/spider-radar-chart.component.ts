import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import * as shape from 'd3-shape';
import { spiderradarChartConfig } from '../ichart'


@Component({
  selector: 'app-spider-radar-chart',
  templateUrl: './spider-radar-chart.component.html',
  styleUrls: ['./spider-radar-chart.component.scss']
})
export class SpiderRadarChartComponent implements OnInit {


/**
   * emit click portion of object 
   */
  @Output() selectSpidarEvent = new EventEmitter();
  /**
   * Get dynamic data 
   */
  @Input() dataMap = [];

  @Input() spidarData = spiderradarChartConfig

  datas:spiderradarChartConfig


    // options
    showXAxis = true;
    showYAxis = true;
    gradient = false;
    showLegend = true;
    legendTitle = 'Legend';
    showXAxisLabel = true;
    tooltipDisabled = false;
    xAxisLabel = 'Country';
    showYAxisLabel = true;
    yAxisLabel = 'GDP Per Capita';
    showGridLines = true;
    innerPadding = '10%';
    barPadding = 8;
    groupPadding = 16;
    roundDomains = false;
    maxRadius = 10;
    minRadius = 3;
    showSeriesOnHover = true;
    roundEdges: boolean = true;
    animations: boolean = true;
    xScaleMin: any;
    xScaleMax: any;
    yScaleMin: number;
    yScaleMax: number;
    schemeType: string = 'linear';


  curves = {
    Basis: shape.curveBasis,
    'Basis Closed': shape.curveBasisClosed,
    Bundle: shape.curveBundle.beta(1),
    Cardinal: shape.curveCardinal,
    'Cardinal Closed': shape.curveCardinalClosed,
    'Catmull Rom': shape.curveCatmullRom,
    'Catmull Rom Closed': shape.curveCatmullRomClosed,
    Linear: shape.curveLinear,
    'Linear Closed': shape.curveLinearClosed,
    'Monotone X': shape.curveMonotoneX,
    'Monotone Y': shape.curveMonotoneY,
    Natural: shape.curveNatural,
    Step: shape.curveStep,
    'Step After': shape.curveStepAfter,
    'Step Before': shape.curveStepBefore,
    default: shape.curveLinear
  };


  curveType:any = this.curves["Linear Closed"];

  constructor() { }
 
  ngOnInit() {


   // this.datas = this.spidarData;


  }

}
