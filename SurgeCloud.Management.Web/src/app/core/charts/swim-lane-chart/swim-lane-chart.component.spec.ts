import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwimLaneChartComponent } from './swim-lane-chart.component';

describe('SwimLaneChartComponent', () => {
  let component: SwimLaneChartComponent;
  let fixture: ComponentFixture<SwimLaneChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwimLaneChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwimLaneChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
