import { Component, OnInit,ViewEncapsulation,Input } from '@angular/core';
import * as d3 from "d3";
import { swimeLineMap,SwimeLaneData } from '../ichart'

declare var randomData:any; 
declare var swimeLineCode:any;

@Component({
  selector: 'app-swim-lane-chart',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './swim-lane-chart.component.html',
  styleUrls: ['./swim-lane-chart.component.css']
})
export class SwimLaneChartComponent implements OnInit {


  @Input() swimeLineMap = swimeLineMap;
  @Input() SwimeLinedata = SwimeLaneData;

  constructor() { }

  ngOnInit() {
    //let pie = D3.layout.pie();
    this.getDataFromMap()
    swimeLineCode(this.swimeLineMap,this.SwimeLinedata)
  }


   getDataFromMap()
  {
 

  }

}
