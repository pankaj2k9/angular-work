import { Component, OnInit,Output,EventEmitter,Input } from '@angular/core';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import { treeMapChartConfig } from '../ichart'

@Component({
  selector: 'app-tree-map-chart',
  templateUrl: './tree-map-chart.component.html',
  styleUrls: ['./tree-map-chart.component.css']
})
export class TreeMapChartComponent implements OnInit {
  single: any[];
  multi: any[];
  /**
   * emit click portion of object 
   */
  @Output() selectTreeEvent = new EventEmitter();
  /**
   * Get dynamic data 
   */
  @Input() data = [];
  @Input() treeConfigurations = treeMapChartConfig;
  view: any[] = [700, 400];
  dataMap = [dataKey]

  constructor() { 
    //Object.assign(this, {single}) 
  }

  ngOnInit() {
    this.dataMap = this.data;
  }

  /**
   * 
   * @param data emit the map click event data 
   */
  onSelect(data): void {
    this.selectTreeEvent.emit(data);
  }

}

export class dataKey {
  name:string
  value:string
}

