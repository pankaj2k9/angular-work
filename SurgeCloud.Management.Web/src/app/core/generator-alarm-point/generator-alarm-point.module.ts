import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeneratorAlarmPointComponent } from './generator-alarm-point.component';
import { DataTableModule } from 'angular2-datatable';
import { SharedModule } from '../../shared-module/shared-module.module';
import {TranslateModule} from "@ngx-translate/core";
@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    SharedModule,
    TranslateModule
  ],
  declarations: [GeneratorAlarmPointComponent],
  exports: [GeneratorAlarmPointComponent, TranslateModule]
})
export class GeneratorAlarmPointModule { }
