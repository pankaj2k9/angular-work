import { Component, OnInit, Input, SimpleChanges, Output, EventEmitter, OnDestroy, ViewChild } from '@angular/core';
import { Generator } from '../../app-common';
import { AppService } from '../../app.service';
import { ModalDirective } from 'ngx-bootstrap';
import { Router } from '@angular/router';
import { GeneratorDescService } from './generator-desc.service';
import * as jQuery from 'jquery';
import * as _ from 'underscore';

@Component({
  selector: 'app-generator-desc',
  templateUrl: './generator-desc.component.html',
  styleUrls: ['./generator-desc.component.scss']
})
export class GeneratorDescComponent implements OnInit, OnDestroy {
  @ViewChild('imagesSlider') public imagesSlider: ModalDirective;

  @Output() assetChanged: EventEmitter<any> = new EventEmitter();
  public assetDescInfo: any;
  defaultImg: string;
  public movablePx = 15;
  public selectedIndex = 0;
  @Input() assetsAvailable: Generator[] = [];
  public assetId: any = null;
  public devices = [];
  private activeCompanyInfoSubscription: any;
  public images = [];
  public selectedImageNumber: number = 0;
  constructor(private appService: AppService,
    private router: Router,
    private generatorDescService: GeneratorDescService) {
    this.defaultImg = 'assets/images/default_asset.svg#def_asset';
  }

  ngOnInit() {
    this.activeCompanyInfoSubscription = this.appService.activeCompanyInfo.subscribe(
      (company) => {
        jQuery('app-generator-location').css('display', 'none');
        this.selectedIndex = 0;
        this.assetId = undefined;
      }
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    this.getGenAvailable();
  }

  ngOnDestroy() {
    this.activeCompanyInfoSubscription.unsubscribe();
  }

  getGenAvailable() {
    if (!this.assetId) {
      this.selectedIndex = 0;
      if (this.assetsAvailable[0]) {
        this.assetId = this.assetsAvailable[0].assetId;
      }
    } else {
      this.selectedIndex = _.indexOf(this.assetsAvailable, _.findWhere(this.assetsAvailable, { 'assetId': this.assetId }));
    }
    this.devices = this.assetsAvailable;
    if (this.devices && this.devices.length > 0) {
      this.getAssetSelected();
    }
    setTimeout(() => {
      this.setDynamicPaddingToDiv();
    });
  }

  ngAfterViewInit() {
    const that = this;
    document.getElementById('genAssetsSelect').addEventListener('change', function () {
      that.getGenAvailable();
    });
  }

  setDynamicPaddingToDiv() {
    let length = jQuery('#genAssetDescApp .panel-desc-all-p').children().length;
    if (length !== 0 && length !== undefined) {
      jQuery('.system-health-btn-type').addClass(`padding-${length}`);
      jQuery('.run-status .card-body').addClass(`padding-${length}`);
      jQuery('.command-btn-type').addClass(`padding-${length}`);
      jQuery('.mode-btn-type').addClass(`padding-${length}`);
    }
  }

  getAssetSelected() {
    jQuery('app-generator-location').css('display', 'none');
    this.setUnitToAssetsAvailable();
    this.assetDescInfo = this.assetsAvailable ? this.assetsAvailable[this.selectedIndex] : null;
    if (this.assetDescInfo) {
      this.isAssetDescInfoAvailable();
    }
    this.assetChanged.emit(this.assetsAvailable[this.selectedIndex]);
    setTimeout(() => {
      this.setDynamicPaddingToDiv();
    });
  }

  isAssetDescInfoAvailable() {
    if (this.assetDescInfo && !this.assetDescInfo['assetType'] && !this.assetDescInfo['vin'] && !this.assetDescInfo['serialNumber'] && !this.assetDescInfo['unitNumber'] && !this.assetDescInfo['calibrationId']) {
      this.assetDescInfo.isRequiredFiledAvailable = false;
    } else {
      this.assetDescInfo.isRequiredFiledAvailable = true;
    }
  }


  setUnitToAssetsAvailable() {
    this.assetsAvailable.forEach(asset => {
      this.devices.forEach(device => {
        if (device.assetId === asset.assetId) {
          asset['param'] = device.param;
        }
      });
    });
  }

  showArrow(id) {
    if (jQuery('#' + id + ' .arrows').hasClass('active-arrow')) {
      jQuery('#' + id + ' .arrows').removeClass('active-arrow');
    } else {
      jQuery('.arrows').removeClass('active-arrow');
      jQuery('#' + id + ' .arrows').addClass('active-arrow');
    }
  }

  leftClick(event, id) {
    event.stopPropagation();
    let movableDiv = document.getElementById(id);
    let left = parseInt(movableDiv.style.left, 10);
    if (!left) {
      movableDiv.style.left = this.movablePx + 'px';
    } else {
      movableDiv.style.left = left + this.movablePx + 'px';
    }
  }

  rightClick(event, id) {
    event.stopPropagation();
    let movableDiv = document.getElementById(id);
    let left = parseInt(movableDiv.style.left, 10);
    if (!left) {
      movableDiv.style.left = -this.movablePx + 'px';
    } else {
      movableDiv.style.left = left - this.movablePx + 'px';
    }
  }

  showImagesSlider() {
    this.selectedImageNumber = 0;
    this.imagesSlider.show();
    let gwSNO = '';
    this.assetsAvailable.forEach((item) => {
      if (item.assetId === this.assetId) {
        gwSNO = item['gwSNO'];
      }
    })
    this.generatorDescService.getImages(gwSNO).subscribe((data) => {
      this.images = data;
    }, (err) => {
      console.log(err);
    });
  }

  goToAssetDashboardPage() {
    this.router.navigate(['/operation/asset-dashboard']);
  }

}
