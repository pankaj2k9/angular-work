import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeneratorDescComponent } from './generator-desc.component';
import { GeneratorDescService } from './generator-desc.service';
import { DataTableModule } from 'angular2-datatable';
import { FormsModule } from '@angular/forms';
import { CoreModule } from "../core.module";
import { ModalModule } from 'ngx-bootstrap';
import {TranslateModule} from "@ngx-translate/core";
@NgModule({
    imports: [
        CommonModule,
        CoreModule,
        DataTableModule,
        FormsModule,
        ModalModule.forRoot(),
        TranslateModule
    ],
    declarations: [GeneratorDescComponent],
    exports: [GeneratorDescComponent, TranslateModule],
    providers: [GeneratorDescService]
})
export class generatorDescModule { }
