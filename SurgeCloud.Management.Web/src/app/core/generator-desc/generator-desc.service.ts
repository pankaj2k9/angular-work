import { Injectable } from '@angular/core';
import { HttpService } from './../../shared-module';
import { Observable } from 'rxjs/Observable';
import * as moment from 'moment';
import {
    Http,
    ConnectionBackend,
    RequestOptions,
    RequestOptionsArgs,
    Response,
    Headers
} from '@angular/http';
@Injectable()
export class GeneratorDescService {

    constructor(public httpService: HttpService, public http: Http) { }

    getImages(id): Observable<any> {
        return this.httpService.get(`common/api/image/${id}`)
            .map(res => res.json())
            .catch(err => Observable.throw(err));
    }
}
