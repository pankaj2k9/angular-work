import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {GeneratorLocationComponent} from "./generator-location.component";
import { environment } from '../../../environments/environment';
import { AppCommonModule } from '../../app-common';
import { SharedModule } from "./../../shared-module";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  imports: [
    CommonModule,
    AppCommonModule,
    SharedModule,
    TranslateModule
  ],
  declarations: [
    GeneratorLocationComponent
  ],
  exports: [
    GeneratorLocationComponent,
    TranslateModule
  ]


})
export class GeneratorLocationModule { }
