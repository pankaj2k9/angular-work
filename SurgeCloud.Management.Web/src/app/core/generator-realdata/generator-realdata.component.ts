import * as jQuery from 'jquery';

import { Component, OnInit, ViewEncapsulation, AfterViewInit, Input, Output, EventEmitter, SimpleChanges, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { GenAssetTable, GenAssetTableShrinkView, GenRealtimeStatus } from '../../app-common';
import { UnitConversionFormulaService } from './../../unit-conversion-formula.service';
import { GeneratorRealdataService } from "./generator-realdata.service";
import { ModalDirective } from 'ngx-bootstrap';
import { AppService } from '../../app.service';
import { Socket } from 'ng-socket-io';
import * as _ from 'underscore';
import * as moment from 'moment';
import * as jsPDF from 'jspdf';

declare let d3: any;
declare let iopctrl: any;

@Component({
  selector: 'app-generator-realdata',
  templateUrl: './generator-realdata.component.html',
  styleUrls: ['./generator-realdata.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GeneratorRealdataComponent implements OnInit, AfterViewInit {
  @ViewChild('dataPointsModal') public dataPointsModal: ModalDirective;
  @Input() assetTableInfo: GenAssetTable[] = [];
  private paginationInfo = {
    sortOrder: 'asc', sortBy: 'Name',
    numberRowPerPage: 100
  };
  @Input() getGenUnitForRealTimeData: any[] = [];
  @Input() genAssetTableInfo: any[] = [];
  public genresponse: any[];
  public genunitresponse: any[];
  @Output() private selectedRTData: EventEmitter<string[]> = new EventEmitter();
  private selectedData: string[] = [];
  @Output() private RTtableView: EventEmitter<boolean> = new EventEmitter();
  private selectedView = true;  // shrink
  @Input() tableviewChange: GenAssetTableShrinkView;
  private maxmizeView = false; // max
  @Output() RTmaxmizeView: EventEmitter<boolean> = new EventEmitter();
  public widthArr = [300, 50, 50, 50, 90, 50];
  @Output() remoteCommand: EventEmitter<any> = new EventEmitter();
  @Output() dataUpdate: EventEmitter<any> = new EventEmitter();
  public realtimeData = [];
  public tempLiveData = [];
  public unitData = [];
  public exportDiv: any;
  public minInnerHeight = 0;
  public tableHeight;
  public tableThirdHeight;
  public tableHeightt = 0;
  public isFirstTimePagination = true;
  public headers = ['DATAPOINT NAME', 'VALUE', 'UNIT', 'NODE', 'LAST UPDATE', 'STATUS'];
  public analyticsType = ['Min', 'Max', 'Sum', 'Samplecount', 'Mean', 'Sigma', 'Distribution'];
  public colorsListForGraph = ['DodgerBlue', 'DarkSeaGreen', 'Fuchsia', 'Grey', 'MediumSeaGreen', 'SlateBlue', 'DarkOrange', 'DarkTurquoise', 'GoldenRod', 'IndianRed'];
  @Input() selectedAsset: any;
  public AlarmStatus: boolean = true;
  public genCmd: boolean;
  public toggleAddPointsList: boolean = false;
  public engineParameters = [];
  public acMeasurements = [];
  public loadMeasurements = [];
  public timers = [];
  public commands = {};
  public runStatusData = {};
  public modeData = {};
  public loadStatusData = {};
  public systemHealth = {};
  public oilPressureData = {};
  public dynamicHeight: number = 65;
  public coolantTemperatureData = {};
  public batteryVoltageData = {};
  public fuelLevelData = {};
  public selectedDataPoint: any = -1;
  public finalData = [];
  public timeSpan: any;
  public dataPointList = [];
  public activeCompanyInfoSubscription: any;
  public selectedDataOnGraph = [];
  public genViewAnalyzerData = [];
  public genViewObj: any;
  public toggleEngineParameters = {
    'value': true,
    'data': []
  };
  public toggleACMeasurements = {
    'value': true,
    'data': []
  };;
  public toggleLoadMeasurements = {
    'value': true,
    'data': []
  };

  public axisWidth = 40;
  public graphMargin = { top: 30, right: 50, bottom: 30, left: 50 };
  public widthTick = 0;
  public leftaline = 0;
  public genViewAnalyzerLoader: boolean = false;

  constructor(private appService: AppService,
    public unitService: UnitConversionFormulaService,
    private generatorRealdataService: GeneratorRealdataService) {
    this.RTtableView.emit(this.selectedView);
  }

  ngOnInit() {
    this.activeCompanyInfoSubscription = this.appService.activeCompanyInfo.subscribe(
      (company) => {
        this.dataPointList = [];
        this.selectedDataPoint = -1;
        this.realtimeData = [];
      }
    );
  }

  setHeightOfDiv() {
    const tableFirstHeight = document.getElementById('power-data').offsetHeight;
    setTimeout(() => {
      this.tableHeight = tableFirstHeight;
      this.tableThirdHeight = (tableFirstHeight - 50) / 3;
    })

    const headerHeight = document.getElementById('appHeader').offsetHeight;
    const footerHeigth = document.getElementById('appFooter').offsetHeight;
    this.minInnerHeight = window.innerHeight - headerHeight - footerHeigth;
  }

  public fuleMeterWidth: any;
  ngAfterViewInit() {
    this.fuleMeterWidth = jQuery('.fuel-meter').width();
    // this.setHeightOfDiv();
    document.getElementById('dataPointsModal').addEventListener('click', () => {
      this.toggleAddPointsList = false;
    })
  }

  ngOnDestroy() {
    this.activeCompanyInfoSubscription.unsubscribe();
  }

  onChangeSelectedDataPoint(index) {
    this.realtimeData[index]['id'] = index;
    if (this.dataPointList.length === 0) {
      this.addDataToPointList(index);
    } else {
      let isAvailable = false;
      for (let i = 0; i < this.dataPointList.length; i++) {
        if (this.dataPointList[i].id === this.realtimeData[index]['id']) {
          isAvailable = true;
        }
      }
      if (!isAvailable) {
        this.addDataToPointList(index);
      }
    }
    this.dataPointsModal.show();
  }

  addDataToPointList(index) {
    this.dataPointList.unshift(this.realtimeData[index]);
    this.dataPointList[0].analytics = [true];
    this.dataPointList[0].selectedAnalytics = ['Min'];
    setTimeout(() => {
      let checked = !jQuery(`#pointCheckbox-${0}`).prop('checked');
      this.dataPointList[0].checked = checked;
    });
  }

  onPointAnalyticTypeChecked(pi, index, analytic) {
    this.dataPointList[pi].analytics[index] = !this.dataPointList[pi].analytics[index];
    if (this.dataPointList[pi].analytics[index]) {
      this.dataPointList[pi].selectedAnalytics.push(analytic);
    } else {
      this.dataPointList[pi].selectedAnalytics = _.without(this.dataPointList[pi].selectedAnalytics, analytic)
    }
  }

  onPointTypeChecked(index, type) {
    if (type === 'sanpshot') {
      this.dataPointList[index].isSnapshot = !this.dataPointList[index].isSnapshot;
      this.dataPointList[index].isAnalytics = false;
    } else {
      this.dataPointList[index].isAnalytics = !this.dataPointList[index].isAnalytics;
      this.dataPointList[index].isSnapshot = false;
    }
  }

  realTimeDataChange($event) {
    let x = $event.target || $event.srcElement;
    if (x.checked) {
      this.selectedData.push(x.value);
    } else {
      this.selectedData = _.without(this.selectedData, x.value);
    }
    this.selectedRTData.emit(this.selectedData);
  }

  onselectedDataOnGraphClick(index) {
    if (this.selectedDataOnGraph.length === 1) {
      if (document.getElementById('oneDataPointError'))
        document.getElementById('oneDataPointError').style.display = 'block';
    } else {
      this.selectedDataOnGraph[index].checked = !this.selectedDataOnGraph[index].checked;
      let data = [];
      let genViewObj = JSON.parse(JSON.stringify(this.genViewObj));
      this.selectedDataOnGraph.forEach((element, i) => {
        if (element.checked) {
          this.genViewAnalyzerData.forEach(item => {
            if (item.pointName.includes(element.name)) {
              data.push(item);
            }
          });
        } else {
          genViewObj['dataPoints'][i] = undefined;
        }
      });
      genViewObj['dataPoints'] = genViewObj['dataPoints'].filter((name) => { return name !== undefined });
      if (data.length === 0) {
        if (document.getElementById('oneDataPointError'))
          document.getElementById('oneDataPointError').style.display = 'block';
      } else {
        this.setDataForGenViewAnalyzer(data, genViewObj, true);
        if (document.getElementById('oneDataPointError'))
          document.getElementById('oneDataPointError').style.display = '';
      }
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.getGenUnitForRealTimeData && changes.getGenUnitForRealTimeData.currentValue) {
      this.genunitresponse = this.getGenUnitForRealTimeData;
      if (this.genunitresponse && this.genunitresponse.length > 0) {
        this.unitData = this.genunitresponse[0].param;
        if (this.genresponse) {
          this.getAssetTableInfo();
        }
      }
      if (this.genresponse) {
        this.getAssetTableInfo();
      }
    }

    if (changes.genAssetTableInfo && changes.genAssetTableInfo.currentValue) {
      this.genresponse = this.genAssetTableInfo;
      if (this.unitData && this.unitData.length > 0) {
        this.getAssetTableInfo();
      }
    }

    if (changes.assetTableInfo && changes.assetTableInfo.currentValue) {
      let data = changes.assetTableInfo.currentValue;
      data.event_param = typeof data.event_param === 'string' ? JSON.parse(data.event_param) : data.event_param;
      let changed = this.checkValueChagesForSocket(data);
      if (changed || this.realtimeData.length === 0) {
        this.updateRealTimeData(data.event_param);
      }
      this.isFirstTimePagination = true;
    }
    if (changes.selectedAsset && changes.selectedAsset.currentValue) {
      this.getAssetTableInfo();
    }
  }

  checkValueChagesForSocket(data) {
    let isChangeValue = false;
    for (let index = 0; index < this.realtimeData.length; index++) {
      let element = this.realtimeData[index];
      let item = data.event_param[index];
      if (element && item && item['TS'] !== element.lastUpdate) {
        isChangeValue = true;
        break;
      }
    }
    return isChangeValue;
  }

  removeDataPointsFromList(index) {
    this.dataPointList.splice(index, 1)
  }

  getUserTimeZoneDate(value) {
    let convertedTimezone;
    if (this.appService.userTimezone != null) {
      convertedTimezone = moment.tz(value, this.appService.userTimezone);
    } else {
      convertedTimezone = moment.utc(value).local();
    }
    return convertedTimezone;
  }

  getAssetTableInfo() {
    this.realtimeData = [];
    this.tempLiveData = [];
    if (this.genresponse[0]) {
    } else {
      document.getElementsByTagName('body')[0].style.pointerEvents = '';
    }
  }

  toggleTableRowsData(index, prm) {
    if (prm === 'engine') {
      let data = this.engineParameters.splice(index, 1);
      if (data.length > 0) {
        this.toggleEngineParameters.data.push(data[0].displayName ? data[0].displayName : data[0].name);
      }
    } else if (prm === 'ac') {
      let data = this.acMeasurements.splice(index, 1);
      if (data.length > 0) {
        this.toggleACMeasurements.data.push(data[0].displayName ? data[0].displayName : data[0].name);
      }
    } else if (prm === 'load') {
      let data = this.loadMeasurements.splice(index, 1);
      if (data.length > 0) {
        this.toggleLoadMeasurements.data.push(data[0].displayName ? data[0].displayName : data[0].name);
      }
    }
  }

  resetUpdateData() {
    this.engineParameters = [];
    this.acMeasurements = [];
    this.loadMeasurements = [];
    this.timers = [];
    this.commands = {};
    this.runStatusData = {};
    this.modeData = {};
    this.loadStatusData = {};
    this.systemHealth = {};
    this.oilPressureData = {};
    this.dynamicHeight = 65;
    this.coolantTemperatureData = {};
    this.batteryVoltageData = {};
    this.fuelLevelData = {};
  }

  updateRealTimeData(data) {
    this.resetUpdateData();
    data.forEach((evt, index) => {
      evt._val = typeof evt._val === 'string' && evt._val.toLowerCase() === 'nan' ? '' : evt._val;
      let isNumber = !isNaN(evt._val);
      if (isNumber) {
        evt._val = this.unitService.convertUnit(evt.unit, this.appService.userProfileDetails.measurementUnit, evt._val);
        if (Number(evt._val) <= Number.MAX_SAFE_INTEGER) {
          if (evt._val < 0.001 && evt._val > 0) {
            evt._val = Number(evt._val).toExponential(2);
          } else {
            evt._val = parseFloat(Number(evt._val).toFixed(3));
          }
        } else {
          evt._val = Number(evt._val).toFixed(3).replace(/\.0+$/, '');
        }
      }
      evt.unit = this.unitService.convertUnitText(evt.unit, this.appService.userProfileDetails.measurementUnit);
      this.tempLiveData[index] = {
        'name': evt.name,
        'value': evt._val,
        'unit': evt.unit,
        'node': evt.node,
        'ts': evt.TS,
        'status': evt.sts
      };
      if (!evt.datatype) evt.datatype = 'String - Read-Only';
      this.realtimeData[index] = new GenRealtimeStatus(evt.name, evt._val, evt.unit, evt.node, evt.TS, evt.sts, evt.datatype, evt.notInSync, evt.displayName);
      this.realtimeData[index]['tLastUpdate'] = evt.TS;
      this.setEngineAcLoadData(this.realtimeData[index]);
    });
    this.dataUpdate.next({
      'timers': this.timers,
      'command': this.commands,
      'modeData': this.modeData,
      'runStatusData': this.runStatusData,
      'loadStatusData': this.loadStatusData,
      'systemHealth': this.systemHealth
    });
    if (this.dataPointList.length === 0) {
      let isUpdate = true;
      this.selectedDataOnGraph.forEach(element => {
        if (element.checked === false) {
          isUpdate = false;
        }
      });
      if (this.selectedDataOnGraph.length === 0) {
        this.getGenviewAnalyzerData('default');
      }
    }
  }

  setEngineAcLoadData(data) {
    let name = data.displayName ? data.displayName : data.name;
    if (data.node == 4 && data.name) {
      if (!this.toggleEngineParameters.data.includes(name)) {
        this.engineParameters.push(JSON.parse(JSON.stringify(data)));
      }
    }
    if (data.node == 5) {
      if (!this.toggleACMeasurements.data.includes(name)) {
        this.acMeasurements.push(JSON.parse(JSON.stringify(data)));
      }
    } else if (data.node == 6 && data.name.toLowerCase() !== 'engine oil pressure') {
      if (!this.toggleLoadMeasurements.data.includes(name)) {
        this.loadMeasurements.push(JSON.parse(JSON.stringify(data)));
      }
    } else if (data.node == 7 && data.name.toLowerCase() !== 'engine oil pressure') {
      this.timers.push(data);
    } else if (data.node == 0 && data.name.toLowerCase() === 'remote command') {
      this.commands = JSON.parse(JSON.stringify(data));
    } else if (data.node == 0 && data.name.toLowerCase() === 'genset mode') {
      this.modeData = JSON.parse(JSON.stringify(data));
    } else if (data.node == 1 && data.name.toLowerCase() === 'genset run status') {
      this.runStatusData = JSON.parse(JSON.stringify(data));
    } else if (data.node == 2 && data.name.toLowerCase() === 'genset alarm status') {
      this.systemHealth['alarm'] = JSON.parse(JSON.stringify(data));
    } else if (data.node == 2 && data.name.toLowerCase() === 'genset warning status') {
      this.systemHealth['warning'] = JSON.parse(JSON.stringify(data));
    } else if (data.node == 3 && data.name.toLowerCase() === 'system power') {
      this.loadStatusData = JSON.parse(JSON.stringify(data));
    } else if (data.node == 4 && data.name.toLowerCase() === 'engine oil pressure') {
      data.value = this.unitService.convertUnit(data.unit, this.appService.userProfileDetails.measurementUnit, data.value);
      if (Number(data.value) <= Number.MAX_SAFE_INTEGER) {
        if (data.value < 0.001 && data.value > 0) {
          data.value = Number(data.value).toExponential(2);
        } else {
          data.value = parseFloat(Number(data.value).toFixed(3));
        }
      }
      data.unit = this.unitService.convertUnitText(data.unit, this.appService.userProfileDetails.measurementUnit);
      this.oilPressureData = JSON.parse(JSON.stringify(data));
    } else if (data.node == 4 && data.name.toLowerCase() === 'engine coolant temperature') {
      this.coolantTemperatureData = JSON.parse(JSON.stringify(data));
      if (this.coolantTemperatureData['unit'] == '&degF') {
        this.dynamicHeight = 65 - ((this.coolantTemperatureData['value'] * 65) / 212);
      } else {
        this.dynamicHeight = 65 - ((this.coolantTemperatureData['value'] * 65) / 100);
      }
    } else if (data.node == 4 && data.name.toLowerCase() === 'engine battery voltage') {
      this.batteryVoltageData = JSON.parse(JSON.stringify(data));
    } else if (data.node == 4 && data.name.toLowerCase() === 'engine fuel level') {
      this.fuelLevelData = JSON.parse(JSON.stringify(data));
    }
  }

  getGenViewAnalyticsObject(data) {
    let obj = {};
    obj['assets'] = [{ 'assetId': this.assetTableInfo['assetID'], 'gwSNO': this.assetTableInfo['gw_sno'] }];
    obj['dataPoints'] = [];
    data.forEach(element => {
      obj['dataPoints'].push(`${element.name}:n${element.node}`);
    });
    // obj['endDate'] = this.getUserTimeZoneDate(moment()).format('MM/DD/YYYY, hh:mm A');
    obj['endDate'] = this.getUserTimeZoneDate(moment()).subtract(5, 'days').format('MM/DD/YYYY, hh:mm A')
    obj['startDate'] = this.getUserTimeZoneDate(moment()).subtract(12, 'days').format('MM/DD/YYYY, hh:mm A');
    return obj;
  }

  getGenviewAnalyzerData(prm, isdefaultOpt = false) {
    let data = [];
    if (prm === 'default') {
      this.selectedDataOnGraph = _.filter(JSON.parse(JSON.stringify(this.realtimeData)), (obj) => {
        return obj.name.toLowerCase() === 'engine oil pressure' || obj.name.toLowerCase() === 'engine coolant temperature' || obj.name.toLowerCase() === 'generator true total power';
      });
    } else if (prm === 'new') {
      this.dataPointsModal.hide();
      if (isdefaultOpt) {
        data = _.filter(JSON.parse(JSON.stringify(this.selectedDataOnGraph)), (obj) => {
          return obj.checked;
        });
      }
      else {
        data = JSON.parse(JSON.stringify(this.dataPointList));
        this.selectedDataOnGraph = _.filter(data, (obj) => {
          return obj.isAnalytics;
        });
      }
    }
    if (isdefaultOpt ? data.length > 0 : this.selectedDataOnGraph.length > 0) {
      this.genViewObj = this.getGenViewAnalyticsObject(isdefaultOpt ? JSON.parse(JSON.stringify(data)) : JSON.parse(JSON.stringify(this.selectedDataOnGraph)));
      this.genViewAnalyzerLoader = true;
      this.generatorRealdataService.getGenviewAnalyzerData(this.genViewObj)
        .subscribe((response) => {
          console.log(response);
          this.genViewAnalyzerData = response;
          if (prm === 'default') {
            this.selectedDataOnGraph.forEach((element, i) => {
              element.id = i;
              element.selectedAnalytics = i == 0 ? ['Samplecount'] : ['Max'];
              element.checked = true;
              element.isAnalytics = true;
            });
          }
          if (response.length > 0) {
            this.setDataForGenViewAnalyzer(response, this.genViewObj);
          } else {
            this.genViewAnalyzerLoader = false;
          }
        }, (err) => {
          this.genViewAnalyzerLoader = false;
        });
    }
  }

  setDataForGenViewAnalyzer(mydata, obj, isFromGraph = false) {
    jQuery('#genviewAnalyserGraph').empty();
    // let endDate = parseInt((this.getUserTimeZoneDate(moment()).valueOf() / 1000).toString());
    let endDate = parseInt((this.getUserTimeZoneDate(moment()).subtract(5, 'days').valueOf() / 1000).toString());
    let startDate = parseInt((this.getUserTimeZoneDate(moment()).subtract(12, 'days').valueOf() / 1000).toString());
    this.timeSpan = 3600 * 12;
    let nextDate = startDate + this.timeSpan;
    mydata = _.sortBy(mydata, (q) => { return q.occurrenceStartDate });
    let loopData = [];

    obj['dataPoints'].forEach((element, index) => {
      loopData[index] = [];
      loopData[index] = _.filter(mydata, (q) => { return q.pointName === element });
    });

    this.finalData = [];
    let tempSelectedData = JSON.parse(JSON.stringify(this.selectedDataOnGraph));
    if (isFromGraph) {
      tempSelectedData = _.filter(tempSelectedData, (q) => { return q.checked });
    }

    let indexCount = 0;
    loopData.forEach((element, index) => {
      let selectedAnalytics = JSON.parse(JSON.stringify(tempSelectedData[index]['selectedAnalytics']));
      selectedAnalytics.forEach((item, i) => {
        this.finalData[indexCount] = [];
        tempSelectedData[index].selectedAnalytics = item;
        this.averageData(startDate, endDate, element, nextDate, indexCount, tempSelectedData[index]);
        indexCount++;
      });
    });

    let dateArray = [{ date: new Date(startDate * 1000) }, { date: new Date(endDate * 1000) }];
    let svgWidth = jQuery('.genview-analyser .card-body').width();
    let height = 260 - this.graphMargin.top - this.graphMargin.bottom;

    if (this.finalData.length != 1 && this.finalData.length != 2) {
      if (this.finalData.length % 2 == 0) {
        this.widthTick = ((this.finalData.length - 2)) * this.axisWidth;
        this.leftaline = this.widthTick / 2;
      } else {
        this.widthTick = ((this.finalData.length - 2)) * this.axisWidth;
        this.leftaline = (this.widthTick / 2) + (this.axisWidth / 2);
      }
    }
    let pointsName = [];
    if (isFromGraph) {
      this.selectedDataOnGraph.forEach(element => {
        pointsName.push(`${element.name}:n${element.node}`);
      });
    }

    let svg = this.setSvgToGenViewAnalyzer(svgWidth, height);
    let allxAxis = svg.append("g").attr("class", "allxAxis");
    let allyAxis = svg.append("g").attr("class", "allyAxis");
    let allGraph = svg.append("g").attr("class", "allGraph");
    let allGraphPointCircle = svg.append("g").attr("class", "allGraphPointCircle");

    let x = d3.time.scale().range([0, svgWidth - this.widthTick]);
    x.domain(d3.extent(dateArray, (d) => { return d.date; }));

    let xAxis = d3.svg.axis().scale(x)
      .orient("bottom").ticks(7);

    allxAxis.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(" + this.leftaline + "," + height + ")")
      .call(xAxis);

    this.finalData.forEach((element, index) => {
      if (!loopData[index]) {
        loopData[index] = loopData[index - 1];
      }
    });

    this.finalData.forEach((data, index) => {
      let color = '';
      let tempIndex = 0;
      if (isFromGraph) {
        let i = pointsName.indexOf(loopData[index][0].pointName);
        color = this.colorsListForGraph[i % 10];
      } else {
        color = this.colorsListForGraph[index % 10];
        if (this.selectedDataOnGraph[index])
          this.selectedDataOnGraph[index].color = color;
      }

      let y = d3.scale.linear().range([height, 0]);
      y.domain([0, d3.max(data, function (d) {
        return Math.max(d.value);
      })]);

      let yAxis = d3.svg.axis().scale(y)
        .orient("left").ticks(5);

      this.setYaxisUnitText(index, svgWidth, allyAxis, color, yAxis, loopData);

      let valueline = d3.svg.line()
        .x(function (d) { return x(d.date); })
        .y(function (d) { return y(d.value); });

      let valueline2 = d3.svg.area()
        .x(function (d) { return x(d.date); })
        .y0(height)
        .y1(function (d) { return y(d.value); });

      let path = allGraph.append("g")
        .attr("class", "path path" + index);

      this.gradientForPath(path, index, color);

      let pathForLine = path.append("path")
        .attr("class", "line")
        .style("stroke", color)
        .attr("transform", "translate(" + this.leftaline + ",0)")
        .attr("d", valueline(data));

      let pathForArea = path.append("path")
        .attr("id", "gradient" + index)
        .attr("class", "gradient")
        .attr("fill", "url(#grad" + index + ")")
        .attr("transform", "translate(" + this.leftaline + ",0)")
        .attr("d", valueline2(data));

      this.animationOnGraph(pathForLine);

      let lineAndDots = allGraphPointCircle.append("g")
        .attr("class", "dots dots" + index)
        .attr("transform", "translate(" + this.leftaline + ",0)")

      this.lineAndDotsSelectAll(lineAndDots, index, color, x, y, data);
    });
    this.genViewAnalyzerLoader = false;
  }

  setSvgToGenViewAnalyzer(svgWidth, height) {

    return d3.select("#genviewAnalyserGraph")
      .append("svg")
      .attr("width", '100%')
      .attr("enable-background", 'new 0 0 ' + (svgWidth + this.graphMargin.left + this.graphMargin.right) + ' ' + (height + this.graphMargin.top + this.graphMargin.bottom))
      .attr("viewBox", '0 0 ' + (svgWidth + this.graphMargin.left + this.graphMargin.right) + ' ' + (height + this.graphMargin.top + this.graphMargin.bottom))
      .attr("version", '1.0"')
      .append("g")
      .attr("transform",
        "translate(" + (this.graphMargin.left + 10) + "," + this.graphMargin.top + ")");
  }

  lineAndDotsSelectAll(lineAndDots, index, color, x, y, data) {
    let div = d3.select("body").append("div")
      .attr("class", "gen-tooltip")
      .style("opacity", 0);

    lineAndDots.selectAll("line-circle").data(data)
      .enter().append("g").attr("data-index", index)
      .on("mouseover", handleMouseOver)
      .on("mouseout", handleMouseOut)
      .append("circle")
      .attr("class", "data-circle")
      .attr("data", "grad" + index)
      .attr("r", 5)
      .style("fill", color)
      .style("opacity", 1)
      .attr("cx", (d) => { return x(d.date); })
      .attr("cy", (d) => { return y(d.value); });

    const that = this;
    function handleMouseOver(d) {
      let index = d3.select(this).attr("data-index");
      let nonGradient = d3.selectAll(".path").classed("notActive", true);
      let nonDots = d3.selectAll(".dots").classed("notActive", true);
      let nonyAxis = d3.selectAll(".yAxis").classed("notActive", true);
      let dots = d3.selectAll(".dots" + index).classed("notActive", false).classed("active", true);
      let gradient = d3.selectAll(".path" + index).classed("notActive", false).classed("active", true).selectAll(".gradient").style("fill", "url(#grad" + index + ")");
      let yAxis = d3.selectAll(".yAxis" + index).classed("notActive", true).classed("active", true);
      div.transition()
        .style("transform", "translate(0px,0px)")
        .style("opacity", .9);
      div.html('<div class="info"> <p>Date: <span>' + moment(d.date).format('MM/DD/YYYY, hh:mm A') + '</span> </p><p>Type: <span>' + d.pointName + '</span> </p><p>Value: <span>' + d.value + '</span> </p></div>')
        .style("left", (d3.event.pageX + 12) + "px")
        .style("top", (d3.event.pageY - 28) + "px")
        .style("z-index", 1);
    }

    function handleMouseOut(d) {
      d3.selectAll(".path").classed("notActive", false).classed("active", false);
      d3.selectAll(".dots").classed("notActive", false).classed("active", false);
      d3.selectAll(".yAxis").classed("notActive", false).classed("active", false);
      let gradient = d3.selectAll(".gradient").style("fill", "none");
      div.transition()
        .style("transform", "translate(10px,0px)")
        .style("opacity", 0).style("z-index", -1);
    }
  }

  animationOnGraph(path) {
    let totalLength = path.node().getTotalLength();
    path.attr("stroke-dasharray", totalLength + " " + totalLength)
      .attr("stroke-dashoffset", totalLength)
      .transition().delay(0)
      .duration(1500)
      .ease("linear")
      .attr("stroke-dashoffset", 0);
  }

  gradientForPath(path, index, color) {
    let linearGradient = path.append('defs')
      .append('linearGradient')
      .attr("id", "grad" + index)
      .attr("x1", "0%")
      .attr("y1", "0%")
      .attr("x2", "100%")
      .attr("y2", "100%");
    linearGradient.append('stop')
      .attr("offset", "0%")
      .style("stop-color", color).style("stop-opacity", 1);
    linearGradient.append('stop')
      .attr("offset", "100%")
      .style("stop-color", color).style("stop-opacity", 0);
  }

  setYaxisUnitText(index, width, allyAxis, color, yAxis, loopData) {
    let yAxisUnitText = function (node, value) {
      node.append("g")
        .attr("transform", "translate(0,-7)")
        .attr("class", "yAxisUnit")
        .append("text")
        .attr("y", 0)
        .attr("x", 0)
        .style("text-anchor", "middle")
        .text(value);
    }
    let yAxisUnit;
    let translateValue;
    if ((index + 1) % 2 == 0) {
      translateValue = width - ((index / 2) - 1) * this.axisWidth;
    } else {
      translateValue = ((index + 1) / 2 - 1) * this.axisWidth;
    }
    yAxisUnit = allyAxis.append("g")
      .attr("class", "axis yAxis yAxis" + index)
      .style("fill", color)
      .attr("transform", "translate(" + translateValue + ",0)")
      .call(yAxis);
    yAxisUnitText(yAxisUnit, loopData[index][0].unit);
  }

  averageData(startDate, endDate, data, nextDate, index, selectedData = undefined) {
    let oneHourData = _.filter(data, (q) => {
      let date = new Date(q.occurrenceStartDate).getTime() / 1000;
      return startDate < date && date <= nextDate;
    });
    if (oneHourData.length > 0) {
      if (selectedData && selectedData.selectedAnalytics === 'Max' && JSON.parse(oneHourData[0].value)[0].Max !== undefined) {
        this.setMaxDataPoint(oneHourData, index);
      } else if (selectedData && selectedData.selectedAnalytics === 'Min' && JSON.parse(oneHourData[0].value)[0].Min !== undefined) {
        this.setMinDataPoint(oneHourData, index);
      } else if (selectedData && selectedData.selectedAnalytics === 'Sum' && JSON.parse(oneHourData[0].value)[0].Sum !== undefined) {
        this.setSumDataPoint(oneHourData, index);
      } else if (selectedData && selectedData.selectedAnalytics === 'Mean' && JSON.parse(oneHourData[0].value)[0].Mean !== undefined) {
        this.setSampleCountDataPoint(oneHourData, index);
      } else {
        this.setSampleCountDataPoint(oneHourData, index);
      }
    }
    if (endDate > nextDate) {
      this.averageData(nextDate, endDate, data, nextDate + this.timeSpan, index, selectedData);
    }
  }

  setMinDataPoint(oneHourData, index) {
    let selectedIndex = 0;
    let value = Number(JSON.parse(oneHourData[selectedIndex].value)[0].Min);
    let pointName = '';
    oneHourData.forEach((data, index) => {
      let min = Number(JSON.parse(data.value)[0].Min);
      if (value >= min) {
        value = min;
        selectedIndex = index;
      }
      pointName = data.pointName;
    });
    this.finalData[index].push({
      date: new Date(oneHourData[selectedIndex].occurrenceStartDate).getTime(),
      value: value,
      pointName: pointName
    });
  }

  setMaxDataPoint(oneHourData, index) {
    let selectedIndex = 0;
    let value = Number(JSON.parse(oneHourData[selectedIndex].value)[0].Max);
    let pointName = '';
    oneHourData.forEach((data, index) => {
      let max = Number(JSON.parse(data.value)[0].Max);
      if (value <= max) {
        value = max;
        selectedIndex = index;
      }
      pointName = data.pointName;
    });
    this.finalData[index].push({
      date: new Date(oneHourData[selectedIndex].occurrenceStartDate).getTime(),
      value: value,
      pointName: pointName
    });
  }

  setSumDataPoint(oneHourData, index) {
    let time = 0;
    let sum = 0;
    let pointName = '';
    oneHourData.forEach((data) => {
      time += new Date(data.occurrenceStartDate).getTime() / 1000;
      sum += Number(JSON.parse(data.value)[0].Samplecount);
      pointName = data.pointName;
    });
    time = time / oneHourData.length;
    sum = sum;
    this.finalData[index].push({
      date: new Date(time * 1000),
      value: sum,
      pointName: pointName
    });
  }

  setSampleCountDataPoint(oneHourData, index) {
    let time = 0;
    let Samplecount = 0;
    let pointName = '';
    oneHourData.forEach((data) => {
      time += new Date(data.occurrenceStartDate).getTime() / 1000;
      Samplecount += Number(JSON.parse(data.value)[0].Samplecount);
      pointName = data.pointName;
    });
    time = time / oneHourData.length;
    Samplecount = Samplecount / oneHourData.length;
    this.finalData[index].push({
      date: new Date(time * 1000),
      value: Samplecount,
      pointName: pointName
    });
  }

  getRandomColor() {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
}