import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeneratorRealdataComponent } from './generator-realdata.component';
import { DataTableModule } from 'angular2-datatable';
import { FormsModule } from '@angular/forms';
import { CoreModule } from "../core.module";
import { GeneratorRealdataService } from "./generator-realdata.service";
import { ModalModule } from 'ngx-bootstrap';
import {TranslateModule} from "@ngx-translate/core";
@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    DataTableModule,
    FormsModule,
    TranslateModule,
    ModalModule.forRoot()
  ],
  declarations: [GeneratorRealdataComponent],
  exports: [GeneratorRealdataComponent, TranslateModule],
  providers: [GeneratorRealdataService]
})
export class generatorRealDataModule { }
