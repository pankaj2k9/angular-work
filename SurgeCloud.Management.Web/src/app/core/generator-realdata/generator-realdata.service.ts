import { Injectable } from '@angular/core';
import { HttpService } from './../../shared-module';
import { Observable } from 'rxjs/Observable';
import * as moment from 'moment';
import {
  Http,
  ConnectionBackend,
  RequestOptions,
  RequestOptionsArgs,
  Response,
  Headers
} from '@angular/http';
@Injectable()
export class GeneratorRealdataService {

  constructor(public httpService: HttpService, public http: Http) { }

  getGenviewAnalyzerData(data): Observable<any> {
    return this.httpService.post(`analytics/api/analytics/GetPointAnalyticData`, data)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }
}
