import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {AppService} from "./app.service";
import {Observer} from "rxjs/Observer";
@Injectable()
export class LoginResolver implements Resolve<object>{

  constructor(private appService: AppService) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<object> | Promise<object> | object {
    if(this.appService.userProfileDetails) {
      return new Promise((resolve) => {
          resolve(this.appService.userProfileDetails);
        }
      );
    } else {
      return this.appService.setLogInUserDetails();
    }
  }
}
