import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {AlarmConsoleComponent} from "./alarm-console.component";
const routes: Routes = [
  {
    path: '',
    component: AlarmConsoleComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlarmConsoleRoutingModule {

}
