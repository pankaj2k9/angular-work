import * as jQuery from 'jquery';

import { Component, EventEmitter, OnInit, AfterViewInit, Output, ViewChild, ElementRef, Renderer, OnDestroy } from '@angular/core';
import { AlarmConsoleService } from './alarm-console.service';
import { AppService, WindowRef } from '../../app.service';
import { Facility, IValues, Asset, AlarmNotes } from './alarm-console';
import { AssetDashboardTable } from '../asset-dashboard/asset-dashboard';
import { OperationStripComponent } from '../';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import * as _ from 'underscore';
import * as moment from 'moment';
import { AdalService } from '../../shared-module';
import { Socket } from 'ng-socket-io';
import * as jsPDF from 'jspdf';


@Component({
  selector: 'app-alarm-console',
  templateUrl: './alarm-console.component.html',
  styleUrls: ['./alarm-console.component.scss'],
  providers: [WindowRef]
})

export class AlarmConsoleComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('childModal') public childModal: ModalDirective;
  @ViewChild('fileInput') fileInput;
  public facility: Facility[] = [];
  public companyId: string;
  public errorType: IValues[];
  public alarmConsole: IValues[];
  public gwSno: string;
  public assetAlarmInfo: any[] = [];
  public copyAssetAlarmInfo: any[] = [];
  public assetAlarmAvailable: AssetDashboardTable[] = [];
  public asset: Asset[] = [];
  public assetId: string = null;
  private logoFile: any;
  public minInnerHeight = 0;
  public isNoteSave: Boolean = false;
  public isToggle: Boolean = true;
  public exportDiv: any;
  public assetAlarmInfoMessage = 'Loading...';
  private svgIcons: string[] = [
    'assets/icons/AlarmConsole/Alarms.svg#Alarms',
    'assets/icons/AlarmConsole/Acknowledged.svg#Acknowledged',
    'assets/icons/AlarmConsole/Normal.svg#Normal'
  ];
  facilitySearch: any;
  private paginationInfo = {
    sortOrder: 'asc',
    sortBy: 'dataoccured',
    numberRowPerPage: 10
  };
  public filter: any = {
    facility: {
      enabled: false,
      keyword: '',
      selected: [],
      filtered: []
    },
    error: {
      enabled: false,
      selected: [],
      filtered: []
    },
    asset: {
      enabled: false,
      selected: [],
      filtered: []
    },
    alarm: {
      enabled: false,
      selected: [],
      filtered: [],
      statusValue: []
    }
  };
  public toggleFilterSearch: Boolean = false;
  public alarmNotes: AlarmNotes = new AlarmNotes('', 'Alarm');
  private keysForSubscription = [];
  public mainBodyHeight: any;
  selectedPopupAlarm: any = {};
  alarmModal: any;
  notes: string = null;
  alarmstatus: any;
  alarmstatusCheckbox: any;
  stripOuter: any;
  user: any = {};
  assetIdParam: any;
  socketConnectPrm = null;
  public isFacility: Boolean = true;
  public isError: Boolean = true;
  public isAsset: Boolean = true;
  public isAlarm: Boolean = true;
  public selectedAlarmIndex: any;
  public activeCompanyInfoSubscription: any;
  public keyUp = new Subject<string>();
  public filterValues = {};
  public headers = [{
    'title': 'Facility',
    'dataKey': 'facilityName',
    'width': 50
  }, {
    'title': 'Asset ID',
    'dataKey': 'assetId',
    'width': 50
  }, {
    'title': 'Asset Type',
    'dataKey': 'assetType',
    'width': 50
  }, {
    'title': 'Topic',
    'dataKey': 'name',
    'width': 50
  }, {
    'title': 'Value',
    'dataKey': 'value',
    'width': 50
  }, {
    'title': 'Alarm Description',
    'dataKey': 'description',
    'width': 170
  }, {
    'title': 'Priority',
    'dataKey': 'priority',
    'width': 40
  }, {
    'title': 'Status',
    'dataKey': 'status',
    'width': 40
  }, {
    'title': 'Trigger Time',
    'dataKey': 'activeTS|date',
    'width': 50
  }, {
    'title': 'Normalized Time',
    'dataKey': 'normalTS|date',
    'width': 50
  },];

  public filterWidthData = [{ 'width': 2, 'id': 'selectCheckbox', 'prm': false },
  { 'width': 10, 'id': 'facilityName', 'prm': true },
  { 'width': 6, 'id': 'assetId', 'prm': true },
  { 'width': 7, 'id': 'assetType', 'prm': true },
  { 'width': 9, 'id': 'name', 'prm': true },
  { 'width': 10, 'id': 'value', 'prm': true },
  { 'width': 17, 'id': 'description', 'prm': true },
  { 'width': 5, 'id': 'priority', 'prm': true },
  { 'width': 4, 'id': 'status', 'prm': true },
  { 'width': 13, 'id': 'activeTS', 'prm': true },
  { 'width': 13, 'id': 'normalTS', 'prm': true },
  { 'width': 4, 'id': 'search', 'prm': false }];

  constructor(private alarmConsoleService: AlarmConsoleService, private socket: Socket,
    private winRef: WindowRef,
    private elRef: ElementRef,
    private renderer: Renderer,
    private appService: AppService, public router: Router, private route: ActivatedRoute, private adalService: AdalService) {
    this.errorType = this.alarmConsoleService.errorType;
    this.alarmConsole = this.alarmConsoleService.alarmConsole;
    this.alarmstatus = this.alarmConsole[0].value;

    this.renderer.listen(this.elRef.nativeElement, 'click', (event) => {
      if (event.target.classList.value === 'page-link') {
        this.setStripOuterPosition(true);
      }
    });
    this.resetFilterData();
    // this.setSearchFilter();
  }

  resetFilterData() {
    this.toggleFilterSearch = false;
    this.filterValues = {
      'facilityName': '',
      'assetId': '',
      'assetType': '',
      'name': '',
      'value': '',
      'description': '',
      'priority': '',
      'status': '',
      'activeTS': '',
      'normalTS': ''
    };
  }

  updateData(event) {
    this.assetAlarmInfo = event;
    this.assetAlarmInfoMessage = 'No Alarm exists';
    this.appService.setAllOptionPagination('alarmConsoleTable');
  }

  setHeightOfDiv() {
    const headerHeight = document.getElementById('appHeader').offsetHeight;
    const footerHeigth = document.getElementById('appFooter').offsetHeight;
    this.minInnerHeight = window.innerHeight - headerHeight - footerHeigth - 25;
  }

  ngOnInit() {
    this.user = this.adalService.userInfo;
    this.route.paramMap.subscribe(
      (mapped: any) => {
        if (mapped.params) {
          this.gwSno = mapped.params['gwSNO'] ? mapped.params['gwSNO'] : this.gwSno;
          this.assetId = mapped.params['assetId'] ? mapped.params['assetId'] : this.assetId;
        }
        if (this.appService.getActiveCompany()) {
          this.getAssetAlaramInfo();
        }
        this.selectedPopupAlarm = this.assetAlarmInfo[0];
      }
    );

    this.assetIdParam = this.route.snapshot.params['assetId'];
    this.activeCompanyInfoSubscription = this.appService.activeCompanyInfo.subscribe(
      (company) => {
        this.companyChanged();
        this.resetFilterData();
        this.appService.setAllOptionPagination('alarmConsoleTable');
      }
    );
    this.getAssetInfo();
    // this.companyChanged();
    this.setHeightOfDiv();
    const that = this;
    setTimeout(function () {
      jQuery('#app-header-drop-down-menu .app-header-companies').click(function (e) {
        if (that.socketConnectPrm) {
          that.destroySocketConnection();
        }
        that.setStripOuterPosition(1000);
      });
    });
  }

  ngOnDestroy() {
    this.destroySocketConnection();
    this.activeCompanyInfoSubscription.unsubscribe();
  }

  destroySocketConnection() {
    if (this.socketConnectPrm && this.socketConnectPrm != null) {
      this.socketConnectPrm.removeAllListeners();
      this.socketConnectPrm.disconnect();
      this.socket.removeAllListeners();
      this.socket.disconnect();
      this.socketConnectPrm = null
    }
  }

  ngAfterViewInit() {
    this.stripOuter = document.getElementById('stripOuter');
    this.stripOuter.classList.add('active');
    const that = this;
    jQuery(window).scroll(function () {
      let scrollHeight = parseInt((jQuery(window).scrollTop() + jQuery(window).height()).toFixed(0));
      if (scrollHeight === jQuery(document).height()) {
        that.stripOuter.classList.remove('fixed');
        that.stripOuter.classList.add('fixedwith-footer');
      } else {
        that.stripOuter.classList.remove('fixedwith-footer');
        that.stripOuter.classList.add('fixed');
      }
    });

    jQuery('.toggle-icon').on('click', function () {
      that.setStripOuterPosition(1000);
    });
  }

  setFixedWIdthFooter(data) {
    if (data.length == 0) {
      this.stripOuter.classList.remove('fixed');
      this.stripOuter.classList.add('fixedwith-footer');
    } else {
      this.stripOuter.classList.remove('fixedwith-footer');
      this.stripOuter.classList.add('fixed');
    }
  }

  showExportAsOptions(event) {
    event.stopPropagation();
    this.exportDiv = document.getElementById('exportAsOptions');
    if (this.exportDiv) {
      this.exportDiv.classList.add('export-as-options-show');
    }
  }

  setCheckBoxForAssetAlarm(prm) {
    this.assetAlarmInfo.forEach(element => {
      element.checked = prm;
    });
  }

  detectCheckboxChanges(event, $page) {
    let el: any = jQuery('.' + $page);
    el.find('.sb-item-head').find('a')[0].click();

    let isChecked = event.target.checked;
    if ($page === 'facility') {
      this.setFacilityFilter(isChecked);
    } else if ($page === 'error') {
      this.setErrorFilter(isChecked);
    } else if ($page === 'asset') {
      this.setAssetFilter(isChecked);
    } else if ($page === 'alarm') {
      this.setAlarmFilter(isChecked);
    } else {

    }
    this.filterAssetAlarmInfo();
    this.setStripOuterPosition(1000);
  }

  setFacilityFilter(isChecked) {
    if (isChecked) {
      _.map(this.facility, function (obj) {
        obj.checked = true;
      });
      this.filter.facility.selected = _.pluck(_.where(this.facility, true), 'value');
    } else {
      _.map(this.facility, function (obj) {
        obj.checked = false;
      });
      this.filter.facility.selected = [];
    }
  }

  setErrorFilter(isChecked) {
    if (isChecked) {
      _.map(this.errorType, function (obj) {
        obj.checked = true;
      });
      this.filter.error.selected = _.pluck(_.where(this.errorType, true), 'value');
    } else {
      _.map(this.errorType, function (obj) {
        obj.checked = false;
      });
      this.filter.error.selected = [];
    }
  }

  setAssetFilter(isChecked) {
    if (isChecked) {
      _.map(this.asset, function (obj) {
        obj.checked = true;
      });
      this.filter.asset.selected = _.pluck(_.where(this.asset, true), 'value');
    } else {
      _.map(this.asset, function (obj) {
        obj.checked = false;
      });
      this.filter.asset.selected = [];
    }
  }

  setAlarmFilter(isChecked) {
    if (isChecked) {
      _.map(this.alarmConsole, function (obj) {
        obj.checked = true;
      });
      this.filter.alarm.selected = _.pluck(_.where(this.alarmConsole, true), 'value');
      this.filter.alarm.statusValue = [0, 1, 2];
    } else {
      _.map(this.alarmConsole, function (obj) {
        obj.checked = false;
      });
      this.filter.alarm.selected = [];
      this.filter.alarm.statusValue = [];
    }
  }

  getStatusValue(name) {
    if (name === 'Normal') {
      return 0;
    } else if (name === 'Acknowledged') {
      return 2;
    } else {
      return 1;
    }
  }

  setStripOuterPosition(time) {
    const that = this;
    setTimeout(function () {
      if (document.body.scrollHeight > document.body.clientHeight) {
        that.stripOuter.classList.remove('active');
        that.stripOuter.classList.add('fixed');
      } else {
        that.stripOuter.classList.add('active');
        that.stripOuter.classList.remove('fixed');
      }
    }, time);
  }

  getFacility() {
    this.alarmConsoleService.getFacility(this.appService.getActiveCompany().companyId).subscribe(
      (data) => {
        data.forEach((fac) => {
          fac.address = fac.address ? fac.address : {};
          this.facility.push(new Facility(fac));
        });
      });
  }

  getPriorityName(value) {
    if (value === 3) {
      return 'Maintenance';
    } else if (value === 1) {
      return 'Critical';
    } else {
      return 'Low';
    }
  }

  setMainBodyDataHeight(isSearch) {
    setTimeout(() => {
      this.mainBodyHeight = Number(window.innerHeight - isSearch);
    }, true);
  }

  getAssetAlaramInfo() {
    const that = this;
    this.assetIdParam = this.route.snapshot.params['assetId'];
    this.alarmConsoleService.getAssetAlaramInfo({
      companyId: this.appService.getActiveCompany().companyId,
      gwSno: this.gwSno,
      assetId: this.assetId
    }).subscribe(response => {
      this.assetAlarmInfoMessage = 'No Alarm exists';
      this.assetAlarmAvailable = response;
      this.assetAlarmInfo = response;
      this.copyAssetAlarmInfo = response;
      this.setMainBodyDataHeight(300);
      this.assetAlarmInfo.forEach((element) => {
        element.occurrenceDate = moment.utc(element.occurrenceDate);
        element.occurrenceDate = element.occurrenceDate.local();
        element.occurrenceDate = element.occurrenceDate.format('DD-MM-YYYY HH:mm:ss');
        element.value = typeof element.param[0].value === 'string' && element.param[0].value.toLowerCase() === 'nan' ? '' : element.param[0].value;
        element.name = element.param[0].name;
        element.description = element.param[0].description;
        element.priority = that.getPriorityName(element.param[0].priority);
        element.status = element.param[0].status ? element.param[0].status : 'Alarm';
        element.activeTS = moment(element.param[0].activeTS).isValid() ? element.param[0].activeTS : '';
        element.tActiveTS = moment(element.param[0].activeTS).isValid() ? moment(element.param[0].activeTS) : '';
        element.normalTS = moment(element.param[0].normalTS).isValid() ? element.param[0].normalTS : '';
        element.tNormalTS = moment(element.param[0].normalTS).isValid() ? moment(element.param[0].normalTS) : '';
        element.value = isNaN(element.value) ? element.value : parseFloat(Number(element.value).toFixed(3));
        element.isNormalStringAsStatus = true;
      });
      if (this.assetIdParam) {
        let obj = Object.assign([], this.assetAlarmInfo);
        this.assetAlarmInfo = [];
        this.assetAlarmInfo = obj.filter(li => li.assetId === this.assetIdParam);
      }
      // _.map(this.assetAlarmInfo, function (obj) {
      //   obj.isHidden = false;
      // });
      this.getAsset();
      this.setCheckBoxForAssetAlarm(false);
      this.appService.appExportData.next(this.assetAlarmInfo);
      this.appService.setAllOptionPagination('alarmConsoleTable');
    });
  }

  getAsset() {
    this.keysForSubscription = [];
    // this.destroySocketConnection();
    this.alarmConsoleService.getAsset(this.appService.getActiveCompany().companyId)
      .subscribe(response => {
        response.forEach(
          (value) => {
            // console.log(value);
            this.keysForSubscription.push(`${value.assetId}.${value.gwSNO}.alarm`);
          });
        this.socketConnectPrm = this.socket.connect();
        this.subscribeForAlarm();
        this.appService.loader.next(false);
      });
  }

  getAssetInfo() {
    this.alarmConsoleService.getAssetInfo()
      .subscribe(response => {
        this.asset = [];
        response.forEach(asset => {
          this.asset.push(new Asset(asset))
        })
      })
  }

  companyChanged() {
    this.getFacility();
    this.getAssetAlaramInfo();
    this.appService.loader.next(true);
  }

  applyExtraFilterOnAlarm(event, value) {
    let status = this.getStatusValue(value);
    if (event.target.checked) {
      if (this.filter.alarm.statusValue.includes(status)) {
      } else {
        this.filter.alarm.statusValue.push(status);
      }
    } else {
      if (this.filter.alarm.statusValue.includes(status)) {
        this.filter.alarm.statusValue = _(this.filter.alarm.statusValue).filter(function (item) {
          return item !== status;
        });
      }
    }
  }

  applyFilter(event) {
    this.filter.facility.selected = _.pluck(_.where(this.facility,
      { checked: true }), 'name');
    this.filter.error.selected = _.pluck(_.where(this.errorType,
      { checked: true }), 'value');
    this.filter.asset.selected = _.pluck(_.where(this.asset,
      { checked: true }), 'Name');
    this.filter.alarm.selected = _.pluck(_.where(this.alarmConsole,
      { checked: true }), 'value');

    if (this.filter.facility.enabled) {
      this.filter.facility.filtered = _.filter(this.assetAlarmInfo, (asset) => this.filter.facility.selected.indexOf(asset.name) > -1);
    }

    this.filterAssetAlarmInfo();
    this.setStripOuterPosition(true);
  }

  filterAssetAlarmInfo() {
    this.assetAlarmInfo = [];
    this.copyAssetAlarmInfo.forEach((element, index) => {
      this.isFacility = this.checkFilterCriteria(this.filter.facility.selected, element.facilityName, this.facility);
      this.isError = this.checkFilterCriteria(this.filter.error.selected, element.alarmStatus, this.errorType);
      this.isAsset = this.checkFilterCriteria(this.filter.asset.selected, element.assetType, this.asset);
      // this.isAlarm = this.checkFilterCriteria(this.filter.alarm.selected, element.Param[0].Status, this.alarmConsole);
      let isAlarmStatus;
      if (this.filter.alarm.statusValue.length === 3 || this.filter.alarm.statusValue.length === 0) {
        isAlarmStatus = true;
      } else {
        if (element.param[0].status === '' && this.filter.alarm.statusValue.includes(1)) {
          isAlarmStatus = true;
        } else if (!parseInt(element.param[0].status) && parseInt(element.param[0].status) !== 0 && this.filter.alarm.statusValue.includes(1)) {
          isAlarmStatus = true;
        } else {
          isAlarmStatus = this.filter.alarm.statusValue.includes(parseInt(element.param[0].status));
        }
      }
      if (this.isFacility && this.isAsset && isAlarmStatus) {
        this.assetAlarmInfo[index] = element;
      }
    });
    this.assetAlarmInfo = this.assetAlarmInfo.filter(function (element) {
      return element !== undefined;
    });
  }

  checkFilterCriteria(data, element, filterType) {
    if (filterType.length === data.length || data.length === 0 || data.includes(element)) {
      return true;
    } else {
      return false;
    }
  }

  subscribeForAlarm() {
    this.socket.connect();
    this.socket.emit('subscribe', this.keysForSubscription);
    this.socket.on('data', (data) => {
      let alarms = [];
      let param = typeof data.event_param === 'string' ? JSON.parse(data.event_param)[0] : data.event_param[0];
      alarms = _.filter(this.copyAssetAlarmInfo, (o) => { return o.assetId === data.assetID; });
      alarms = _.sortBy(alarms, (o) => { return -o.activeTS; });
      if (alarms[0] && !alarms[0].activeTS.includes('Z'))
        alarms[0].activeTS = alarms[0].activeTS + 'Z';
      if (alarms.length > 0 && (moment(alarms[0].activeTS).valueOf() < moment(param.ATS).valueOf())) {
        if ((this.assetIdParam && (data.assetID == this.assetIdParam)) || !this.assetIdParam) {
          this.addAsset(data, param, alarms[0]);
        }
      }
    });

    this.socket.on('disconnect', () => {
      if (this.router.url === '/operation/alarm') {
        console.log('Got disconnect!');
        this.destroySocketConnection();
        this.subscribeForAlarm();
      }
    });
  }

  addAsset(data, param, alarm) {
    let element = {};
    element['assetId'] = data.assetID;
    element['facilityName'] = alarm.facilityName;
    element['assetType'] = alarm.assetType;
    element['occurrenceDate'] = moment.utc(data.occurrence_date_time);
    element['occurrenceDate'] = element['occurrenceDate'].local();
    element['occurrenceDate'] = element['occurrenceDate'].format('DD-MM-YYYY HH:mm:ss');
    element['value'] = typeof param._val === 'string' && param._val.toLowerCase() === 'nan' ? '' : param._val;
    element['name'] = param.name;
    element['description'] = param.des;
    element['priority'] = this.getPriorityName(param.pri);
    element['param'] = typeof data.event_param === 'string' ? JSON.parse(data.event_param) : data.event_param;
    element['param'][0].ts = param.TS;
    element['status'] = param.sts ? param.sts : 'Alarm';
    element['activeTS'] = moment(param.ATS).isValid() ? moment(param.ATS) : '';
    element['tActiveTS'] = moment(param.ATS).isValid() ? moment(param.ATS) : '';
    element['normalTS'] = moment(param.NTS).isValid() ? moment(param.NTS) : '';
    element['tNormalTS'] = moment(param.NTS).isValid() ? moment(param.NTS) : '';
    if (!isNaN(element['value'])) {
      if (Number(JSON.stringify(element['value'])) <= Number.MAX_SAFE_INTEGER) {
        element['value'] = parseFloat(Number(element['value']).toFixed(3));
      }
      else {
        element['value'] = element['value'] ? element['value'].replace(/\.0+$/, '') : element['value'];
      }
    }
    this.assetAlarmInfo.unshift(element);
    this.assetAlarmInfo = _.sortBy(JSON.parse(JSON.stringify(this.assetAlarmInfo)), (item) => { return - moment(item['activeTS']) });
    this.copyAssetAlarmInfo = this.assetAlarmInfo;
    setTimeout(() => {
      this.appService.setAllOptionPagination('alarmConsoleTable');
    });
    this.appService.appExportData.next(this.assetAlarmInfo);
  }

  uploadLogo(alarm, fileName) {
    this.alarmConsoleService.uploadLogo(this.logoFile, fileName)
      .subscribe(
        (data) => {
          alarm.fileReference = data._body;
          this.saveAlarmNotes(alarm)
        },
        (error) => {
          console.log(error);
        },
        () => {
          this.isNoteSave = false;
        }
      );
  }

  createAlarmNotes(alarm) {
    this.isNoteSave = true;
    let alarmStatus = this.alarmstatusCheckbox ? 2 : this.assetAlarmInfo[this.selectedAlarmIndex].param[0].status;
    const data = {
      companyId: this.appService.getActiveCompany().companyId,
      companyName: this.appService.getActiveCompany().name,
      gwSno: alarm.gwSno,
      assetId: alarm.assetId,
      occurrenceDate: alarm.UTCOccurenceDate,
      updatedBy: this.user.profile.name,
      notes: this.notes,
      alarmStatus: alarmStatus
    }

    const fi = this.fileInput.nativeElement;
    if (fi.files && fi.files[0]) {
      this.logoFile = fi.files[0];
      const randomNumber: any = Math.random().toFixed(3);
      const fileName = `notes_${alarm.assetId}_${alarm.gwSno}_${(randomNumber * 1000).toString()}.${this.logoFile.name.split('.').pop()}`;
      this.uploadLogo(data, fileName);
    } else {
      this.saveAlarmNotes(data);
    }
    // FileReference = fi.files[0].name;
  }

  saveAlarmNotes(alarm) {
    this.alarmConsoleService.saveNote(Object.assign({}, alarm))
      .finally(() => this.isNoteSave = false)
      .subscribe(res => {
        if (res.fileReference) {
          let fileName = res.fileReference.split('/');
          res.fileName = fileName[fileName.length - 1];
        }
        this.selectedPopupAlarm['notes'].push(res);
        this.notes = null;
        this.selectedPopupAlarm.occurrenceDate = alarm.occurrenceDate;
        this.selectedPopupAlarm.status = alarm.alarmStatus.toString();
        this.alarmstatusCheckbox = false;
        this.assetAlarmInfo[this.selectedAlarmIndex].param[0].status = res.alarmStatus;
      });
  }

  openAlarmNoteForm(alarm, index) {
    this.selectedAlarmIndex = index;
    this.selectedPopupAlarm = alarm;
    this.selectedPopupAlarm.UTCOccurenceDate = alarm.param[0].ts;
    if (alarm.param[0].status === '2') {
      this.alarmstatusCheckbox = true;
    } else {
      this.alarmstatusCheckbox = false;
    }
    this.alarmConsoleService.getAlarmNoteInfo({
      companyId: this.appService.getActiveCompany().companyId,
      gwSno: alarm.gwSno,
      assetId: alarm.assetId,
      startDate: alarm.param[0].ts,
      endDate: alarm.param[0].ts,
    }).subscribe(response => {
      alarm['notes'] = response ? response : [];
      alarm['notes'] = this.setFileNameForNote(alarm['notes']);
      this.childModal.show();
    });
  }

  setFileNameForNote(data) {
    let fileName = [];
    data.forEach(element => {
      if (element.fileReference) {
        fileName = element.fileReference.split('/');
        element.fileName = fileName[fileName.length - 1];
      }
    });
    return data;
  }

  hideAlarmNoteForm() {
    // this.selectedAlarmInfo = null;
    this.childModal.hide();
  }

  toggleFilter() {
    let div = document.getElementById('sideOptions');
    if (div.style.display === 'none') {
      div.style.display = 'block';
      setTimeout(() => {
        this.isToggle = !this.isToggle;
      }, 100);
    } else {
      this.isToggle = !this.isToggle;
      setTimeout(() => {
        div.style.display = 'none';
      }, 600);
    }
    this.setStripOuterPosition(1000);
  }

  onFileChange(file) {
  }


  getPdfObjetc() {
    let pdf = new jsPDF('p', 'pt', 'letter');
    pdf.cellInitialize();
    pdf.setFontSize(6.5);
    return pdf;
  }

  getStatusNameFromValue(value) {
    if (value === 0) {
      return 'Normal';
    } else if (value === 2) {
      return 'Acknowledged';
    } else {
      return 'Alarm';
    }
  }

  getPriorityNameFromValue(value) {
    if (value === 1) {
      return 'Critical';
    } else if (value === 3) {
      return 'Maintenance';
    } else {
      return 'Low';
    }
  }
}
