import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'angular2-datatable';
import { AlarmConsoleComponent } from './alarm-console.component';
import { AlarmConsoleService } from './alarm-console.service';
import { OperationStripModule } from '../operation-strip/operation-strip.module';
import { OperationStripComponent } from '../operation-strip/operation-strip.component';
import { FormsModule } from '@angular/forms';
import { CoreModule } from '../../core';
import { DropDownFilter } from './dropdownfilter.pipe';
import { AssetFilter } from './assetfilter.pipe';
import { ValueFilter } from './valuefilter.pipe';
import { ModalModule } from 'ngx-bootstrap';
import { CategoryPipe } from './category.pipe';
import { SqueezeBoxModule } from 'squeezebox';
import { SharedModule } from "./../../shared-module";
import {AlarmConsoleRoutingModule} from "./alarm-console-routing.module";

@NgModule({
  imports: [
    AlarmConsoleRoutingModule,
    CommonModule,
    DataTableModule,
    FormsModule,
    CoreModule,
    SqueezeBoxModule,
    ModalModule.forRoot(),
    OperationStripModule,
    SharedModule
  ],
  declarations: [AlarmConsoleComponent, DropDownFilter, AssetFilter, ValueFilter, CategoryPipe],
  exports: [AlarmConsoleComponent],
  providers: [AlarmConsoleService]
})
export class AlarmConsoleModule { }
