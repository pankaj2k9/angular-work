import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http'
import { HttpService } from '../../shared-module';
import { Observable } from 'rxjs/Observable';
import { AppService } from '../../app.service';
import { IValues } from './alarm-console';
import * as moment from 'moment';
import { Router } from '@angular/router';
@Injectable()
export class AlarmConsoleService {

  errorType: IValues[] = [
    { key: 'Accident', value: 'Accident', checked: false },
    { key: 'Over speeding', value: 'Over speeding', checked: false },
    { key: 'Geofence crossing', value: 'Geofence crossing', checked: false }
  ];

  alarmConsole: IValues[] = [
    { key: 'Alarm', value: 'Alarm', checked: false },
    { key: 'Acknowledged', value: 'Acknowledged', checked: false },
    { key: 'Normal', value: 'Normal', checked: false }
  ];

  constructor(private httpService: HttpService, private appService: AppService, private router: Router) {

  }

  getFacility(data?: string): Observable<any[]> {
    return this.httpService.get(`facility/api/facility/get/${data}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getAssetAlaramInfo(obj: { companyId: string, gwSno: string, assetId: string }): Observable<any> {
    let url = '';
    if (obj.gwSno && obj.assetId) {
      url = `device/api/iot/getalarmdata?companyId=${obj.companyId}${'&gwSno=' + obj.gwSno}&assetId=${obj.assetId}`;
    } else {
      url = `device/api/iot/getalarmdata?companyId=${obj.companyId}`;
    }
    return this.httpService.get(encodeURI(url))
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getAlarmNoteInfo(obj: { companyId: string, gwSno: string, assetId: string, startDate: string, endDate: string }): Observable<any> {
    let url = '';
    if (obj.gwSno && obj.assetId) {
      url = `device/api/iot/getalarmnotes?companyId=${obj.companyId}${'&gwSno=' + obj.gwSno}&assetId=${obj.assetId}&startDate=${obj.startDate}&endDate=${obj.endDate}`;
    } else {
      url = `device/api/iot/getalarmdata?companyId=${obj.companyId}`;
    }
    return this.httpService.get(encodeURI(url))
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getAssetInfo(): Observable<any> {
    return this.httpService.get(encodeURI(`device/api/iot/getassettype`))
      .map(res => res.json())
      .catch(err => Observable.throw(err));

  }

  getAlarmNotes(gwSno: string, assetId: string) {
    const companyId = this.appService.getActiveCompany().companyId;
    const url = `device/api/iot/getalarmnotes?companyId=${companyId}${'&gwSno=' + gwSno}&assetId=${assetId}`;
    return this.httpService.get(encodeURI(url))
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  uploadLogo(logo: File, fileName: string): Observable<any> {
    const formData = new FormData();
    formData.append('', logo);
    formData.append('fname', fileName);
    return this.httpService.saveFile('common/api/image', formData)
      .catch(err => Observable.throw(err));
  }

  saveNote(param) {

    return this.httpService.post('device/api/iot/addalarmnotes', param)
      .map(res => res.json())
      .catch(err => Observable.throw(err));

  }

  getAsset(data?: string): Observable<any> {
    return this.httpService.get(`device/api/iot/get/${data}`).map(res => res.json()).catch(err => Observable.throw(err));
  }

}
