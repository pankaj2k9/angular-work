/**
 * Created by manzurulhaque on 28/09/2017.
 */
import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'assetFilter'
})
@Injectable()
export class AssetFilter implements PipeTransform {
  transform(list: any[], args: String): any {
    if (!args) {
      return list;
    }
    return list.filter(li => li.Name.toLowerCase().indexOf(args.toLowerCase()) !== -1);
  }
}
