/**
 * Created by manzurulhaque on 28/09/2017.
 */
import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'valueFilter'
})
@Injectable()
export class ValueFilter implements PipeTransform {
  transform(list: any[], args: String): any {
    if (!args) return list;
    return list.filter(li => li.value.toLowerCase().indexOf(args.toLowerCase()) !== -1);
  }
}
