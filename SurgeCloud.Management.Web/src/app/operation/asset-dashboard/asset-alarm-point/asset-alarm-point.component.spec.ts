import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetAlarmPointComponent } from './asset-alarm-point.component';

describe('AssetAlarmPointComponent', () => {
  let component: AssetAlarmPointComponent;
  let fixture: ComponentFixture<AssetAlarmPointComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetAlarmPointComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetAlarmPointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
