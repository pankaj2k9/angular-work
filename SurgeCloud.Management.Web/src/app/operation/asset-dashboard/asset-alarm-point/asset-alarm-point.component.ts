import { DatePipe } from '@angular/common';
import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { OperationService } from '../../operation.service';
import { AlarmStatus, AssetTableShrinkView } from './../asset-dashboard';
import { AssetDashboardService } from '../asset-dashboard.service';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { AppService } from '../../../app.service';
import { Socket } from 'ng-socket-io';
import { Router } from '@angular/router';
import * as _ from 'underscore';
import * as jQuery from 'jquery';
import * as moment from 'moment';

@Component({
  selector: 'app-asset-alarm-point',
  templateUrl: './asset-alarm-point.component.html',
  styleUrls: ['./asset-alarm-point.component.scss']
})
export class AssetAlarmPointComponent implements OnInit, OnDestroy {

  @Input() assetAlarmInfo: any;

  @Input() selectedRealTimeData: string[];

  private selectedView = true;
  public isFirstTimePagination = true;
  public toggleFilterSearch = false;
  public keyUp = new Subject<string>();
  public filterValues = {};
  public mainBodyHeight: any;
  @Output() private ACtableView: EventEmitter<boolean> = new EventEmitter();

  private maxmizeView = false;
  @Output() ACmaxmizeView: EventEmitter<boolean> = new EventEmitter();

  private paginationInfo = {
    sortOrder: 'asc', sortBy: 'dataoccured',
    numberRowPerPage: 25
  };

  public filterWidthData = [{ 'width': 40, 'id': 'name', 'prm': true },
  { 'width': 45, 'id': 'occurrence_date_time', 'prm': true },
  { 'width': 15, 'id': 'status', 'prm': true }];

  @Input() tableviewChange: AssetTableShrinkView;
  @Input() selectedAsset: any;

  public liveData: any;
  public minInnerHeight = 0;
  public alarmData: AlarmStatus[] = [];
  public copyAlarmData: AlarmStatus[] = [];
  public activeCompanyInfoSubscription: any;
  constructor(private router: Router, private operationService: OperationService, public assetDashboardService: AssetDashboardService,
    public appService: AppService, private socket: Socket,
  ) {
    this.paginationInfo.numberRowPerPage = this.operationService.numberOfRecordPerPage;
  }

  ngOnInit() {
    this.activeCompanyInfoSubscription = this.appService.activeCompanyInfo.subscribe(
      (company) => {
        this.alarmData = [];
        this.resetFilterData();
      }
    );
    // this.setSearchFilter();
    this.setHeightOfDiv();

  }

  setHeightOfDiv() {
    const headerHeight = document.getElementById('appHeader').offsetHeight;
    const footerHeigth = document.getElementById('appFooter').offsetHeight;
    this.minInnerHeight = window.innerHeight - headerHeight - footerHeigth;
  }

  ngOnDestroy() {
    this.activeCompanyInfoSubscription.unsubscribe();
  }

  resetFilterData() {
    this.toggleFilterSearch = false;
    this.filterValues = {
      'name': '',
      'occurrence_date_time': '',
      'status': ''
    };
  }

  updateData(event) {
    this.alarmData = event;
    this.appService.setAllOptionPagination('AdAlarmConsoleTable');
  }

  changeTableView() {
    this.selectedView = this.selectedView ? false : true;
    if (!this.selectedView && !this.tableviewChange.selRTtableView) {
      this.selectedView = true;
    }
    this.ACtableView.emit(this.selectedView);
  }
  changeTableMaxView() {
    this.maxmizeView = this.maxmizeView ? false : true;
    this.ACmaxmizeView.emit(this.maxmizeView);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.mainBodyHeight = (jQuery('.panel.panel-app.asset-table').height() - 125);
    if (changes.selectedAsset && changes.selectedAsset.currentValue) {
      this.alarmData = [];
      this.liveData = [];
      this.copyAlarmData = [];
      this.getAssetAlaramInfo();
    }
    if (changes.assetAlarmInfo && changes.assetAlarmInfo.currentValue) {
      if (changes.assetAlarmInfo.previousValue && (!this.filterValues['name'] && !this.filterValues['occurrence_date_time'] && !this.filterValues['status'])) {
        this.setAssetAlarmInfo(changes);
        this.appService.setAllOptionPagination('AdAlarmConsoleTable');
      }
    }
  }

  getUserTimeZoneDateFormat(value) {
    let convertedTimezone: any;
    if (this.appService.userTimezone != null) {
      convertedTimezone = moment.tz(value, this.appService.userTimezone).utc();
    } else {
      convertedTimezone = moment.utc(value).local();
    }
    return convertedTimezone;
  }

  setAssetAlarmInfo(changes) {
    let previousValue = changes.assetAlarmInfo.previousValue;
    let currentValue = changes.assetAlarmInfo.currentValue;
    currentValue.event_param = typeof currentValue.event_param === 'string' ? JSON.parse(currentValue.event_param) : currentValue.event_param;
    currentValue.event_param[0]._val = typeof currentValue.event_param[0]._val === 'string' && currentValue.event_param[0]._val.toLowerCase() === 'nan' ? '' : currentValue.event_param[0]._val;
    currentValue.name = currentValue.event_param[0].des ? currentValue.event_param[0].des : currentValue.event_param[0].name
    currentValue.tOccurrence_date_time = currentValue.occurrence_date_time;
    if (this.copyAlarmData.length === 0) {
      this.liveData.unshift(currentValue);
      this.copyAlarmData = JSON.parse(JSON.stringify(this.liveData));
      return;
    }
    if (this.liveData[0] && !this.liveData[0].occurrence_date_time.includes('Z'))
      this.liveData[0].occurrence_date_time = this.liveData[0].occurrence_date_time + 'Z';
    if (moment(this.liveData[0].occurrence_date_time).valueOf() < moment(currentValue.occurrence_date_time).valueOf()) {
      currentValue.tOccurrence_date_time = currentValue.occurrence_date_time;
      if (this.liveData.length > 24) {
        this.liveData.pop();
      }
      currentValue.name = currentValue.event_param[0].des;
      this.liveData.unshift(currentValue);
    }
    this.alarmData = this.liveData;
    this.copyAlarmData = this.liveData;
  }

  getAssetAlaramInfo() {
    this.assetDashboardService.getAssetAlaramInfo({
      companyId: this.appService.getActiveCompany().companyId,
      gwSno: this.selectedAsset.gwSNO,
      assetId: this.selectedAsset.assetId,
      rows: 10
    }).subscribe(response => {
      this.alarmData = [];
      this.copyAlarmData = [];
      if (response) {
        this.alarmData = [];
        this.copyAlarmData = [];
        response.forEach(data => {
          data.assetID = data.assetId;
          data.occurrence_date_time = data.occurrenceDate;
          data.event_param = data.param;
          data.event_param[0].des = data.param[0].description;
          data.event_param[0].sts = data.param[0].status;
          data.status = data.param[0].status;
          data.name = data.param[0].description ? data.param[0].description : data.event_param[0].name;
          data.tOccurrence_date_time = data.occurrence_date_time;
          this.alarmData.push(data);
        });
        this.liveData = this.alarmData;
        this.copyAlarmData = JSON.parse(JSON.stringify(this.alarmData));
      }
      this.appService.setAllOptionPagination('AdAlarmConsoleTable');
    });
  }

  goToOperationAlarm(id) {
    this.router.navigate(['/operation/alarm/' + id]);
  }
}
