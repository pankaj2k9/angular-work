import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssetAlarmPointComponent } from './asset-alarm-point.component';
import { DataTableModule } from 'angular2-datatable';
import { SharedModule } from "../../../shared-module";
import {TranslateModule} from "@ngx-translate/core";
@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    SharedModule,
    TranslateModule
  ],
  declarations: [AssetAlarmPointComponent],
  exports: [AssetAlarmPointComponent,TranslateModule]
})
export class AssetAlarmPointModule { }
