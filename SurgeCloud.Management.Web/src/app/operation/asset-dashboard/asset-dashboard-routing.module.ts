import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {AssetDashboardComponent} from "./asset-dashboard.component";
const routes: Routes = [
  {
    path: '',
    component: AssetDashboardComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssetDashboardRoutingModule {

}
