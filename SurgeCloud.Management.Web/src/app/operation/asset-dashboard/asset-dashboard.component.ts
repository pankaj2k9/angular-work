import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { AppService } from './../../app.service';
import { Subscription } from 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { AssetDashboardService } from './asset-dashboard.service';
import { AssetDashboardDesc, AssetDashboardMap, AssetDashboardTable, AssetTableShrinkView } from './asset-dashboard';
import { SingleChartData, MultiChartData } from '../../core';
import * as jQuery from 'jquery';
import { Socket } from 'ng-socket-io';
import * as _ from 'underscore';

@Component({
  selector: 'app-asset-dashboard',
  templateUrl: './asset-dashboard.component.html',
  styleUrls: ['./asset-dashboard.component.scss']
})
export class AssetDashboardComponent implements OnInit, OnDestroy {

  private routerParam: Subscription;
  companyId: string;
  assetId: string;
  assetDescInfo: AssetDashboardDesc; // asset available
  assetMapInfo: AssetDashboardMap;
  assetTableInfo: AssetDashboardTable[] = null;
  public assetAlarmInfo: any = {};

  linechartData: MultiChartData[] = [
    {
      'name': 'Germany',
      'series': [
        {
          'name': '2010',
          'value': 7300000
        },
        {
          'name': '2011',
          'value': 8940000
        }
      ]
    },

    {
      'name': 'USA',
      'series': [
        {
          'name': '2010',
          'value': 7870000
        },
        {
          'name': '2011',
          'value': 8270000
        }
      ]
    },

    {
      'name': 'France',
      'series': [
        {
          'name': '2010',
          'value': 5000002
        },
        {
          'name': '2011',
          'value': 5800000
        }
      ]
    }
  ];
  selectedRealTimeData: string[] = [];
  dateInfo: string[] = [];
  timeInfo: number[] = [];
  lineChartInfo: any = { showLegend: false, height: 255 };
  private tableviewChange: AssetTableShrinkView = {
    selRTtableView: true, selAlarmView: true,
    rtClass: 'col-lg-6 col-md-6', rtInnerClass: '',
    alarmClass: 'col-lg-3 col-md-3 asset-right-align', alarmInnerClass: '',
    RTtableMaxView: false, AlarmMaxView: false
  };

  public selectedIndex = 0;
  public selectedAsset: any = null;
  private keysForSubscription = [];
  public socketConnectPrm: any = '';
  activeCompanyInfoSubscription: any;
  private appService: AppService;
  constructor(injector: Injector,
    private route: ActivatedRoute,
    private router: Router,
    private socket: Socket,
    private assetDashboardService: AssetDashboardService) {
    // this.companyId = '84ca6adb-67c1-49b5-a89e-bd644c9f0aa7';
    // this.companyId = this.appService.getActiveCompany().companyId;
    this.appService = injector.get(AppService);

  }

  ngOnInit() {
    this.removeAllSocketListeners();
    this.routerParam = this.route.params.subscribe(params => {
      this.assetId = params['assetId']; // get the project id from parent class URl
      // this.getAssetDesc();
    });
    // this.getAssetDesc();

    this.activeCompanyInfoSubscription = this.appService.activeCompanyInfo.subscribe(
      (company) => {
        this.companyId = this.appService.getActiveCompany().companyId;
        this.removeAllSocketListeners();
        // this.getAssetDesc();
      }
    );
  }

  changeRTtableView(tableView: boolean) {

    this.tableviewChange.selRTtableView = tableView;
    this.tableViewClass();
  }
  changeACtableView(tableView: boolean) {
    this.tableviewChange.selAlarmView = tableView;
    this.tableViewClass();
  }

  changeRTMaxView(view: boolean) {
    this.tableviewChange.RTtableMaxView = view;

    if (this.tableviewChange.RTtableMaxView) {
      this.tableviewChange.rtClass = 'col-lg-9 col-md-9';
      this.tableviewChange.rtInnerClass = '';
    } else {
      this.tableviewChange.selRTtableView = true;
      this.tableviewChange.selAlarmView = true;
      this.tableViewClass();
    }
  }
  changeACMaxView(view: boolean) {
    this.tableviewChange.AlarmMaxView = view;
    if (this.tableviewChange.AlarmMaxView) {
      this.tableviewChange.alarmClass = 'col-lg-9 col-md-9';
      this.tableviewChange.alarmInnerClass = '';
    } else {
      this.tableviewChange.selRTtableView = true;
      this.tableviewChange.selAlarmView = true;
      this.tableViewClass();
    }
  }

  tableViewClass() {
    if (!this.tableviewChange.selRTtableView && !this.tableviewChange.selAlarmView) {
      this.tableviewChange.selRTtableView = true;
      this.tableviewChange.selAlarmView = false;
    }

    if (this.tableviewChange.selRTtableView && this.tableviewChange.selAlarmView) {
      this.tableviewChange.rtClass = 'col-lg-6 col-md-6';
      this.tableviewChange.rtInnerClass = '';
      this.tableviewChange.alarmClass = 'col-lg-3 col-md-3 asset-right-align';
      this.tableviewChange.alarmInnerClass = '';
    } else if (!this.tableviewChange.selRTtableView && this.tableviewChange.selAlarmView) {
      this.tableviewChange.rtClass = 'col-lg-1 col-md-1 width-0-4';
      this.tableviewChange.rtInnerClass = '';
      this.tableviewChange.alarmClass = 'col-lg-8 col-md-8 pl-10';
      this.tableviewChange.alarmInnerClass = 'row mr-8';
    } else if (this.tableviewChange.selRTtableView && !this.tableviewChange.selAlarmView) {
      this.tableviewChange.rtClass = 'col-lg-8 col-md-8 pl-10';
      this.tableviewChange.rtInnerClass = 'mr-7';
      this.tableviewChange.alarmClass = 'col-lg-1 col-md-1 width-0-4 alarm-mp';
      this.tableviewChange.alarmInnerClass = '';
    }
  }

  changeDate(data: string[]) {
    this.dateInfo = data;
  }
  changeTime(data: number[]) {
    this.timeInfo = data;

  }
  changeRTData(data: string[]) {
    this.selectedRealTimeData = data;
    this.getHistoricalData();

  }

  getHistoricalData() {
    if (this.selectedRealTimeData.length > 0) {
      this.changeRTMaxView(false);
      let startdate = `${this.dateInfo[0]} ${this.timeInfo[0]}:00:00`;
      let enddate = `${this.dateInfo[1]} ${this.timeInfo[1]}:00:00`;
      this.assetDashboardService.getHistoricalData({
        companyId: this.companyId,
        gwSno: this.assetMapInfo.gwSNO, assetId: this.assetId,
        datapointname: this.selectedRealTimeData[0],
        startDate: startdate,
        endDate: enddate
      }).subscribe(response => {

      });
    }
  }

  ngOnDestroy() {
    this.routerParam.unsubscribe();
    this.removeAllSocketListeners();
    this.activeCompanyInfoSubscription.unsubscribe();
    // this.socket.removeAllListeners();
  }

  removeAllSocketListeners() {
    this.keysForSubscription = [];
    if (this.socketConnectPrm && this.socketConnectPrm != null) {
      this.socketConnectPrm.removeAllListeners();
      this.socketConnectPrm.disconnect();
    }
    this.socket.disconnect();
  }

  getAssetSelected(selectedIndex) {
    this.selectedAsset = this.assetDescInfo[selectedIndex];
    this.subscribeForAsset();
  }

  subscribeForAsset() {
    this.removeAllSocketListeners();
    this.socketConnectPrm = this.socket.connect();
    let company = this.appService.getActiveCompany();
    const that = this;
    this.keysForSubscription = [];
    this.keysForSubscription.push(`${this.selectedAsset.assetId}.${this.selectedAsset.gwSNO}.realtime`);
    this.keysForSubscription.push(`${this.selectedAsset.assetId}.${this.selectedAsset.gwSNO}.geo`);
    this.keysForSubscription.push(`${this.selectedAsset.assetId}.${this.selectedAsset.gwSNO}.alarm`);
    this.socket.emit('subscribe', this.keysForSubscription);
    document.getElementsByTagName('body')[0].style.pointerEvents = 'none';
    let socketData;
    this.socket.on('data', function (data) {
      socketData = data;
      document.getElementsByTagName('body')[0].style.pointerEvents = '';
      if (data.status === 'Geo') {
        that.assetMapInfo = data;
      } else if (data.status === 'Realtime') {
        that.assetTableInfo = data;
      } else {
        that.assetAlarmInfo = data;
      }
    });
    this.socket.on('disconnect', () => {
      console.log('Got disconnect!');
      this.subscribeForAsset();
    });
    this.socketErrorConectHandler();
    this.socketErrorHandler();
   
    setTimeout(() => {
      if (!socketData) {
        document.getElementsByTagName('body')[0].style.pointerEvents = '';
      }
    }, 5000);
  }

  socketErrorConectHandler() {
    this.socket.on('connect_error', () => {
      console.log('connect_error');
      this.subscribeForAsset();
    });
  }

  socketErrorHandler() {
    this.socket.on('error', (err) => {
      console.log('Error connecting to server', err);
      this.subscribeForAsset();
    });
  }

  assetChanged(asset) {
    this.appService.loader.next(true);
    this.removeAllSocketListeners();
    setTimeout(() => {
      if (asset) {
        this.selectedAsset = asset;
        this.subscribeForAsset();
      }
      this.appService.loader.next(false);
    }, 2000);
    this.assetDashboardService.assetChanged.next(asset);
  }

  dataFromSocket(data) {
    if (data.status === 'Geo') {
      this.assetMapInfo = data;
    } else if (data.status === 'Realtime') {
      this.assetTableInfo = data;
    } else {
      this.assetAlarmInfo = data;
    }
  }

}
