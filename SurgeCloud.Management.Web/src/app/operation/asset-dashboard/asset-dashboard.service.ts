import { Injectable } from '@angular/core';
import { HttpService } from './../../shared-module';
import { Observable } from 'rxjs/Observable';
import { Company } from '../../../app';
import { Subject } from 'rxjs/Subject';
import * as moment from 'moment';
import {
  Http,
  ConnectionBackend,
  RequestOptions,
  RequestOptionsArgs,
  Response,
  Headers
} from '@angular/http';
@Injectable()
export class AssetDashboardService {

  public assetChanged: Subject<any> = new Subject();
  constructor(public httpService: HttpService, public http: Http) { }
  getAssetDesc(companyId: string): Observable<any> {

    return this.httpService.get(`device/api/iot/get/${companyId}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));

  }

  getDevice(companyId): Observable<any> {
    return this.httpService.get(`device/api/iot/getdevicebycompany/${companyId}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getAssetMapInfo(companyId: string): Observable<any> {

    return this.httpService.get(`device/api/iot/getdevicebycompany/${companyId}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));

  }

  getUnitForRealTimeData(obj: { companyId: string, gwSno: string, assetId: string }): Observable<any> {

    return this.httpService.get(encodeURI(`device/api/iot/getdevice?companyId=${obj.companyId}&gwSno=${obj.gwSno}&assetId=${obj.assetId}`))
      .map(res => res.json())
      .catch(err => Observable.throw(err));

  }

  getAssetTableInfo(obj: { companyId: string, gwSno: string, assetId: string }): Observable<any> {

    return this.httpService.get(encodeURI(`device/api/iot/getrealtimedata?companyId=${obj.companyId}&gwSno=${obj.gwSno}&assetId=${obj.assetId}`))
      .map(res => res.json())
      .catch(err => Observable.throw(err));

  }

  getAssetAlaramInfo(obj: { companyId: string, gwSno: string, assetId: string, rows: Number }): Observable<any> {

    return this.httpService.get(encodeURI(`device/api/iot/getalarmdata?companyId=${obj.companyId}&gwSno=${obj.gwSno}&assetId=${obj.assetId}&rows=${obj.rows}`))
      .map(res => res.json())
      .catch(err => Observable.throw(err));

  }
  getHistoricalData(obj: { companyId: string, gwSno: string, assetId: string, datapointname: string, startDate: string, endDate: string }): Observable<any> {
    return this.httpService.get(encodeURI(`device/api/iot/GetHistoryData?companyId=${obj.companyId}&gwSno=${obj.gwSno}&assetId=${obj.assetId}&datapointname=${obj.datapointname}&startDate=${obj.startDate}&endDate=${obj.endDate}`))
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  downloadHistoricalData(data): Observable<any> {
    return this.httpService.post(`analytics/api/Analytics/GetPointAnalyticData`, data)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  savePointDataTypeModal(data): Observable<any> {
    return this.httpService.post('device/api/Iot/SendCommand', data)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

}
