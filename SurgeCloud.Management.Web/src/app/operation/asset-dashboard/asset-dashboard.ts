export interface AssetDashboardDesc {
  aId: string,
  assetId: string,
  companyId: string,
  companyName: string,
  assetType: string,
  logo: string,
  make: string,
  model: string,
  serialNumber: string,
  unitNumber: string,
  vin: string,
  softwareId: string,
  calibrationId: string,
  customerReference: string,
  customerFacilityGroup: string,
  customerEquipmentGroup: string,
  customerEquipmentId: string,
  tags: string[],
  status: number,
  createdBy: string,
  updatedBy: string,
  lastUpdatedDate: Date
}

export interface AssetDashboardMap {
  controllerId: string,
  companyId: string,
  companyName: string,
  facilityId: string,
  facilityName: string,
  aId: string,
  assetId: string,
  iotHubName: string,
  msgType: string,
  pVer: string,
  gwSNO: string,
  gwDescription: string,
  gwOSBuild: string,
  gwAppBuild: string,
  gwDCAver: string,
  gwSIMICCID: string,
  gwIMEI: string,
  gwRSSI: string,
  gwTimezone: string,
  gwWANIP: string,
  latitude: number,
  longitude: number,
  altitude: string,
  tags: string[],
  status: number,
  param: string[]
}

export interface iotRealTimeData {
  ts: Date,
  name: string,
  value: string,
  status: string
}

export interface AssetDashboardTable {
  gwSno: string,
  assetId: string,
  occurrenceDate: Date,
  param: iotRealTimeData[]
}

export class AssetTableShrinkView {
  selRTtableView: boolean;
  selAlarmView: boolean;
  rtClass: string;
  rtInnerClass: string;
  alarmClass: string;
  alarmInnerClass: string;
  RTtableMaxView: boolean;
  AlarmMaxView: boolean;
}


export class AlarmStatus {
  constructor(
    public assetId: string,
    public occurrenceDate: string,
    public status: string,
    public gwSno: string
  ) {
    this.assetId = assetId;
    this.occurrenceDate = occurrenceDate;
    this.status = status;
  }
}


export class RealtimeStatus {
  constructor(
    public name: string,
    public value: Number,
    public unit: string,
    public node: Number,
    public lastUpdate: string,
    public status: string,
    public pointDataType: string,
    public notInSync: string,
    public displayName: string,
    public originalUnit: string
  ) {
    this.name = name;
    this.value = value;
    this.unit = unit;
    this.node = node;
    this.status = status;
    this.lastUpdate = lastUpdate;
    this.pointDataType = pointDataType;
    this.notInSync = notInSync;
    this.displayName = displayName;
    this.originalUnit = originalUnit;
  }
}

export class Param {
  constructor(
    public name: string,
    public status: string,
    public ts: string,
    public type: number,
    public unit: string,
    public value: string
  ) {
    this.name = name;
    this.status = status;
    this.ts = ts;
    this.type = type;
    this.unit = unit;
    this.value = value;
  }
}

