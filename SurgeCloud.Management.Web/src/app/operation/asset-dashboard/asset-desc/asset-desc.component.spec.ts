import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetDescComponent } from './asset-desc.component';

describe('AssetDescComponent', () => {
  let component: AssetDescComponent;
  let fixture: ComponentFixture<AssetDescComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetDescComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetDescComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
