import * as jQuery from 'jquery';

import { Component, OnInit, OnDestroy, AfterViewInit, Input, NgZone, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { AssetDashboardDesc } from './../asset-dashboard';
import { AppService } from './../../../app.service';
import { AssetDashboardService } from '../asset-dashboard.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as _ from 'underscore';


@Component({
  selector: 'app-asset-desc',
  templateUrl: './asset-desc.component.html',
  styleUrls: ['./asset-desc.component.scss']
})
export class AssetDescComponent implements OnInit, OnDestroy, AfterViewInit {

  public assetDescInfo: any;

  @Output() assetChanged: EventEmitter<any> = new EventEmitter();


  defaultImg: string;
  public movablePx = 15;
  public selectedIndex = 0;
  public assetsAvailable: any[] = [];
  public selectedAsset: any;
  public assetId: any = null;
  public devices = [];
  public activeCompanyInfoSubscription: any;
  public isNoDataFound: boolean = true;
  constructor(private appService: AppService, public assetDashboardService: AssetDashboardService, private route: ActivatedRoute,
    private router: Router, private _zone: NgZone) {
    this.defaultImg = 'assets/images/default_asset.svg#def_asset';
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.assetId = params['assetId']; // get the project id from parent class URl
      if (this.appService.getActiveCompany()) {
        this.isNoDataFound = true;
        this.getAssetAvailable();
      }
    });
    this.activeCompanyInfoSubscription = this.appService.activeCompanyInfo.subscribe(
      (company) => {
        this.assetId = undefined;
        this.isNoDataFound = true;
        this.getAssetAvailable();
      }
    );
  }

  ngOnDestroy() {
    this.activeCompanyInfoSubscription.unsubscribe();
  }

  ngAfterViewInit() {
    const that = this;
    document.getElementById('assetsSelect').addEventListener('change', function () {
      that.getAssetAvailable();
    });
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  getAssetAvailable() {
    let companyId = this.appService.getActiveCompany().companyId;
    this.assetDashboardService.getAssetDesc(companyId)
      .subscribe(response => {
        this.isNoDataFound = response.length === 0 ? true : false;
        if (response) {
          response = response ? response : [];
          this.assetsAvailable = _.filter(response, function (data) {
            return data.status === 1;
          });
          if (!this.assetId) {
            this.selectedIndex = 0;
            if (this.assetsAvailable[0]) {
              this.assetId = this.assetsAvailable[0].assetId;
            }
          } else {
            this.selectedIndex = _.indexOf(this.assetsAvailable, _.findWhere(this.assetsAvailable, { 'assetId': this.assetId }));
          }
          this.devices = response;
          this.getAssetSelected();
          this.isNoDataFound = this.assetsAvailable.length === 0 ? true : false;
        }
      });
  }

  getAssetSelected() {
    this.setUnitToAssetsAvailable();
    this.assetDescInfo = this.assetsAvailable ? this.assetsAvailable[this.selectedIndex] : null;
    if (this.assetDescInfo) {
      this.isAssetDescInfoAvailable();
    }
    this.assetChanged.emit(this.assetsAvailable[this.selectedIndex]);
  }

  isAssetDescInfoAvailable() {
    if (this.assetDescInfo && !this.assetDescInfo['assetType'] && !this.assetDescInfo['vin'] && !this.assetDescInfo['serialNumber'] &&
      !this.assetDescInfo['unitNumber'] && !this.assetDescInfo['calibrationId']) {
      this.assetDescInfo.isRequiredFiledAvailable = false;
    } else {
      this.assetDescInfo.isRequiredFiledAvailable = true;
    }
  }

  setUnitToAssetsAvailable() {
    this.assetsAvailable.forEach(asset => {
      this.devices.forEach(device => {
        if (device.assetId === asset.assetId) {
          asset['param'] = device.param;
        }
      });
    });
  }

  showArrow(pId, id) {
    let rect1 = document.getElementById(pId).getBoundingClientRect();
    let rect2 = document.getElementById(id).getBoundingClientRect();
    if (rect1.width < rect2.width) {
      if (jQuery('#' + pId + ' .arrows').hasClass('active-arrow')) {
        jQuery('#' + pId + ' .arrows').removeClass('active-arrow');
      } else {
        jQuery('.arrows').removeClass('active-arrow');
        jQuery('#' + pId + ' .arrows').addClass('active-arrow');
      }
      return true
    }
    return false;
  }

  leftClick(event, id) {
    event.stopPropagation();
    let movableDiv = document.getElementById(id);
    let left = parseInt(movableDiv.style.left, 10);
    if (!left) {
      movableDiv.style.left = this.movablePx + 'px';
    } else {
      movableDiv.style.left = left + this.movablePx + 'px';
    }
  }

  rightClick(event, id) {
    event.stopPropagation();
    let movableDiv = document.getElementById(id);
    let left = parseInt(movableDiv.style.left, 10);
    if (!left) {
      movableDiv.style.left = -this.movablePx + 'px';
    } else {
      movableDiv.style.left = left - this.movablePx + 'px';
    }
  }
}
