import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssetDescComponent } from './asset-desc.component';
import { FormsModule } from '@angular/forms';
import {TranslateModule} from "@ngx-translate/core";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule
  ],
  declarations: [AssetDescComponent],
  exports: [AssetDescComponent,TranslateModule
  ]
})
export class AssetDescModule { }
