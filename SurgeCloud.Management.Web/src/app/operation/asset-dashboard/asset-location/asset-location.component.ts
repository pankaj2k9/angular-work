import { Component, OnInit, OnDestroy, NgZone, Input, SimpleChanges, ElementRef } from '@angular/core';
import { AssetDashboardDesc } from './../asset-dashboard';
import { AssetDashboardService } from '../asset-dashboard.service';
import { AppService } from '../../../app.service';
import { Socket } from 'ng-socket-io';
import * as jQuery from 'jquery';
import * as _ from 'underscore';
declare let google: any;

@Component({
  selector: 'app-asset-location',
  templateUrl: './asset-location.component.html',
  styleUrls: ['./asset-location.component.scss']
})
export class AssetLocationComponent implements OnInit, OnDestroy {

  @Input() assetMapInfo: any;
  @Input() selectedAsset: any;

  public icon: any;
  public zIndex: any;
  public assetLocationHeight = 0;
  public geoMapValues: any = [];
  public map: any;
  public mapZoom = 15;
  private asset: AssetDashboardDesc;
  private logoType: string;
  public isNoDataFound: boolean = true;
  public assetLocationAddress = {
    'address0': null,
    'address1': null,
    'address2': null,
    'address3': null
  };
  public latitude = this.appService.defaultLat;
  public longitude = this.appService.defaultLng;
  public activeCompanyInfoSubscription: any;
  public labelOptions = '';
  constructor(private appService: AppService, public assetDashboardService: AssetDashboardService,
    private elementRef: ElementRef, private _zone: NgZone) {
  }

  ngOnInit() {

    this.activeCompanyInfoSubscription = this.appService.activeCompanyInfo.subscribe(
      (company) => {
        this.isNoDataFound = true;
        this.logoType = null;
        this._zone.run(() => {
          this.latitude = this.appService.defaultLat;
          this.longitude = this.appService.defaultLng;
          this.resetAssetLocationAddress();
          setTimeout(() => {
            this.icon = null;
            this.mapZoom = 2;
            if (this.map) {
              const bounds = new google.maps.LatLngBounds();
              bounds.extend({ lat: this.appService.defaultLat, lng: this.appService.defaultLng });
              this.map.setCenter({ lat: this.appService.defaultLat, lng: this.appService.defaultLng });
            }
          });
        });
      }
    );
  }

  ngOnDestroy() {
    if (this.activeCompanyInfoSubscription)
      this.activeCompanyInfoSubscription.unsubscribe();
  }

  getAssetAvailable(val) {
    let companyId = this.appService.getActiveCompany().companyId;
    this.assetDashboardService.getAssetDesc(companyId)
      .subscribe(response => {
        this.isNoDataFound = response.length === 0 ? true : false;
        if (response) {
          response = response ? response : [];
          this.asset = _.filter(response, function (data) {
            return data.assetId === val;
          });
          if (this.asset) {
            this.logoType = this.asset[0] && this.asset[0].assetType;
          }
        }
        this.icon = this.getIcon();
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.assetMapInfo && changes.assetMapInfo.currentValue) {
      this.setMinHeightAssetLocation();
      if (changes.assetMapInfo.previousValue) {
        this.setGoogleMapMarker(changes);
      }
    } else {
      this.icon = null;
      this.mapZoom = 2;
      if (this.map) {
        const bounds = new google.maps.LatLngBounds();
        bounds.extend({ lat: this.appService.defaultLat, lng: this.appService.defaultLng });
        this.map.setCenter({ lat: this.appService.defaultLat, lng: this.appService.defaultLng });
      }
    }
    if ((changes.selectedAsset && changes.selectedAsset.currentValue)) {
      this.logoType = null;
      this.getAssetAvailable(changes.selectedAsset.currentValue.assetId);
      this.assetLocationHeight = 0;
      this.setMinHeightAssetLocation();
      this.labelOptions = null;
      this.resetAssetLocationAddress();
      this.latitude = this.appService.defaultLat;
      this.longitude = this.appService.defaultLng;
    }
  }

  resetAssetLocationAddress() {
    this.assetLocationAddress = {
      'address0': null,
      'address1': null,
      'address2': null,
      'address3': null
    };
  }

  setMapInfo(asset) {
    this.zIndex = Number(asset.event_param[0].status) === 1 ? 94 : 93;
    this.assetMapInfo = asset;
    this.icon = this.getIcon();
  }
  centerChange(latlng) {
    const position = new google.maps.LatLng(latlng.lat.toFixed(6), latlng.lng.toFixed(6));
    this.map.panTo(position);
    const mapObject = this.map;
  }

  mapRendered($event) {
    this.map = $event;
    const mapObject = this.map;
    setTimeout(function () {
      google.maps.event.trigger(mapObject, 'resize');
    }, true);

  }

  setMinHeightAssetLocation() {
    setTimeout(() => {
      if (document.getElementById('assetDashboardRight')) {
        this.assetLocationHeight = document.getElementById('assetDashboardRight').clientHeight - document.getElementById('assetDescApp')
          .clientHeight - 4;
      }
      const mapObject = this.map;
      setTimeout(function () {
        google.maps.event.trigger(mapObject, 'resize');
      }, true);
    }, 1000);
  }

  checkStatusType(statusType) {
    if (statusType[1] === 1) {
      if (parseInt(statusType[1]) === 1) {
        this.zIndex = 94;
      } else {
        this.zIndex = 91;
      }
    } else {
      if (statusType[0] === 'Moving') {
        this.zIndex = 93;
      } else if (statusType[0] === 'Idle') {
        this.zIndex = 92;
      } else if (statusType[0] === 'Inactive') {
        this.zIndex = 95;
      } else {
        this.zIndex = 91;
      }
    }
  }


  setGoogleMapMarker(changes) {
    this.assetMapInfo = changes.assetMapInfo.currentValue;
    this.latitude = parseFloat(this.assetMapInfo.latitude);
    this.longitude = parseFloat(this.assetMapInfo.longitude);
    this.labelOptions = this.assetMapInfo.assetID;
    if (this.assetMapInfo.latitude) {
      let statusType;
      if (this.assetMapInfo.event_param[0] === '[') {
        let event_param = JSON.parse(this.assetMapInfo.event_param)[0];
        event_param._val = typeof event_param._val === 'string' && event_param._val.toLowerCase() === 'nan' ? '' : event_param._val;
        this.geoMapValues = event_param._val.split(',');
        statusType = JSON.parse(this.assetMapInfo.event_param)[0].sts.split(',');
      } else {
        this.assetMapInfo.event_param[0]._val.split(',')
        statusType = this.assetMapInfo.event_param[0].sts.split(',');
      }
      this.checkStatusType(statusType);
      this.icon = this.getIcon(statusType);
      this.getLocationInfo(this.assetMapInfo.latitude, this.assetMapInfo.longitude);
      this.mapZoom = 15;
    } else {
      this.latitude = this.appService.defaultLat;
      this.longitude = this.appService.defaultLng;
      this.mapZoom = 2;
      this.icon = null;
    }
    const bounds = new google.maps.LatLngBounds();
    bounds.extend({ lat: this.latitude, lng: this.longitude });
    this.map.setCenter(bounds.getCenter());
  }

  getLocationInfo(latitude, longitude) {
    let geocoder = new google.maps.Geocoder;
    let latlng = { lat: parseFloat(latitude), lng: parseFloat(longitude) };
    const that = this;
    geocoder.geocode({ 'location': latlng }, function (results, status) {
      if (status == 'OK') {
        let assetLocationAddress = results[0].formatted_address.split(',');
        if (assetLocationAddress && assetLocationAddress[0]) {
          that.assetLocationAddress.address0 = assetLocationAddress[0].trim();
          that.assetLocationAddress.address1 = assetLocationAddress[1].trim();
          that.assetLocationAddress.address2 = assetLocationAddress[2].trim().split(' ')[0];
          that.assetLocationAddress.address3 = assetLocationAddress[3] && assetLocationAddress[3].trim();
        }
      }
    });
  }

  getIcon(statusType = []) {
    let imageName = 'assets/icons/assettype/semitrailer.svg';
    if (this.logoType) {
      imageName = 'assets/icons/assettype/' + this.logoType.toLocaleLowerCase().replace(new RegExp(' ', 'g'), '')
        .toLocaleLowerCase();
      if (statusType && statusType.length > 0 && statusType[1] == 1) {
        imageName = imageName + '-alarm';
      }
    }
    return new google.maps.MarkerImage(imageName + '.svg',
      new google.maps.Size(75, 75),
      new google.maps.Point(0, 0),
      new google.maps.Point(17, 34),
      new google.maps.Size(36, 36));
  }
}
