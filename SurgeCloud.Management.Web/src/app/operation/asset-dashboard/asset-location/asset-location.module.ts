import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssetLocationComponent } from './asset-location.component';
import { environment } from '../../../../environments/environment';
import { AppCommonModule } from '../../../app-common';
import { SharedModule } from "../../../shared-module";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
	imports: [
		CommonModule,
		AppCommonModule,
		SharedModule,
    TranslateModule
	],
	declarations: [
		AssetLocationComponent
	],
	exports: [
		AssetLocationComponent,
    TranslateModule
	]


})
export class AssetLocationModule { }
