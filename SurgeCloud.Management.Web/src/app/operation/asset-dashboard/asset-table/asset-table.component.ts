import * as jQuery from 'jquery';

import { Component, OnInit, AfterViewInit, Input, OnDestroy, Output, ElementRef, EventEmitter, SimpleChanges, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ModalDirective } from 'ngx-bootstrap';
import { AssetDashboardTable, AssetTableShrinkView, RealtimeStatus } from '../asset-dashboard';
import { OperationService } from '../../operation.service';
import { AssetDashboardService } from './../asset-dashboard.service';
import { AppService } from '../../../app.service';
import { UnitConversionFormulaService } from './../../../unit-conversion-formula.service';
import { AppStartService } from '../../../app-start.service';
import { AdalService } from '../../../shared-module';
import { Socket } from 'ng-socket-io';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import * as _ from 'underscore';
import * as moment from 'moment';
// import { setTimeout } from 'timers';

@Component({
  selector: 'app-asset-table',
  templateUrl: './asset-table.component.html',
  styleUrls: ['./asset-table.component.scss']
})
export class AssetTableComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() assetTableInfo: AssetDashboardTable[] = [];
  private paginationInfo = {
    sortOrder: 'asc', sortBy: 'Name',
    numberRowPerPage: 100
  };
  @Output() private selectedRTData: EventEmitter<string[]> = new EventEmitter();
  private selectedData: string[] = [];
  @Output() private RTtableView: EventEmitter<boolean> = new EventEmitter();
  private selectedView = true;  // shrink
  @Input() tableviewChange: AssetTableShrinkView;
  private maxmizeView = false; // max
  @Output() RTmaxmizeView: EventEmitter<boolean> = new EventEmitter();
  public realtimeData = [];
  public tempLiveData = [];
  public unitData = [];
  public minInnerHeight = 0;
  public isFirstTimePagination = true;
  public keyUp = new Subject<string>();
  public toggleFilterSearch: Boolean = false;
  public filterValues: {};
  public totalCheckBoxClicked = [];
  public allEqualsInArray = true;
  public frequency = ['Hourly', 'Daily', 'Weekly', 'Monthly'];
  public selectedFrequency = 'Hourly';
  public startDate = moment();
  public endDate = moment();
  public isInValidDate = false;
  public user: any;
  public measurementUnit = '';
  public pointDataValue: any;
  public units = [];
  public isLoadingHistoricalData = false;
  public startOfWeekDate;
  public tempStartOfWeekDate;
  public historicalDataList = {
    'isNoData': false,
    'data': []
  };
  public headers = [{
    'title': 'DATAPOINT NAME',
    'dataKey': 'name',
    'width': 150
  }, {
    'title': 'VALUE',
    'dataKey': 'value',
    'width': 100
  }, {
    'title': 'UNIT',
    'dataKey': 'unit',
    'width': 100
  }, {
    'title': 'NODE',
    'dataKey': 'node',
    'width': 100
  }, {
    'title': 'LAST UPDATE',
    'dataKey': 'lastUpdate|date',
    'width': 100
  }, {
    'title': 'STATUS',
    'dataKey': 'status',
    'width': 50
  }];

  public historicalDataHeader = [{
    'title': 'Asset Id',
    'dataKey': 'assetId'
  }, {
    'title': 'GWSno',
    'dataKey': 'gwSno'
  }, {
    'title': 'Datapoint Name',
    'dataKey': 'pointName'
  }, {
    'title': 'Date Time',
    'dataKey': 'occurrenceStartDate'
  }, {
    'title': 'Max',
    'dataKey': 'max'
  }, {
    'title': 'Min',
    'dataKey': 'min'
  }, {
    'title': 'Sum',
    'dataKey': 'sum',
  }, {
    'title': 'Mean',
    'dataKey': 'mean'
  }];

  public expandedAssets = [];
  public selectedExpandedAssetsIndex: number;
  public activeCompanyInfoSubscription: any;
  public minusHeightForTable = 93;
  public mainBodyHeight: any;
  @Input() selectedAsset: any;
  public filterWidthData = [{ 'width': 36, 'id': 'name', 'prm': true },
  { 'width': 14, 'id': 'value', 'prm': true },
  { 'width': 10, 'id': 'unit', 'prm': true },
  { 'width': 8, 'id': 'node', 'prm': true },
  { 'width': 24, 'id': 'lastUpdate', 'prm': true },
  { 'width': 8, 'id': 'status', 'prm': true },];
  public selectedAssetPointName = {};
  public isAccessForPointName = false;
  public assetChangedSubscriber: any;
  constructor(private operationService: OperationService, public assetDashboardService: AssetDashboardService,
    public appService: AppService, public unitService: UnitConversionFormulaService, private socket: Socket,
    private appStartService: AppStartService, private adalService: AdalService) {
    // this.paginationInfo.numberRowPerPage = this.operationService.numberOfRecordPerPage;
    this.RTtableView.emit(this.selectedView);
    this.selectedAssetPointName['pointDataType'] = '';
    let units = this.appService.getUnits();
    this.units = [];
    units.forEach((item, index) => {
      this.units[index] = {};
      this.units[index]['name'] = item;
    });
  }

  ngOnInit() {
    this.setHeightOfDiv();
    this.resetFilterData();
    this.activeCompanyInfoSubscription = this.appService.activeCompanyInfo.subscribe(
      (company) => {
        this.toggleFilterSearch = false;
        this.realtimeData = [];
        this.resetFilterData();
        this.resetHistoricalData();
        this.cancelPointDataTypeModal();
      }
    );
    this.appStartService.getFeatures().subscribe((res) => {
      this.checkAvailableAccessForPointName(res);
    });
    this.user = this.adalService.userInfo;
    setTimeout(() => {
      let minusHeight = parseInt(jQuery('.asset-table').css('min-height'), 10) - jQuery('.panel-full-page-height .org-table thead').height() - (this.minusHeightForTable);
      this.mainBodyHeight = minusHeight;
    });

    this.assetChangedSubscriber = this.assetDashboardService.assetChanged.subscribe((data) => {
      this.realtimeData = [];
    });
  }

  ngOnDestroy() {
    if (this.assetChangedSubscriber)
      this.assetChangedSubscriber.unsubscribe();
  }

  setHeightOfDiv() {
    const headerHeight = document.getElementById('appHeader').offsetHeight;
    const footerHeigth = document.getElementById('appFooter').offsetHeight;
    this.minInnerHeight = window.innerHeight - headerHeight - footerHeigth;
    return this.minInnerHeight;
  }

  checkAvailableAccessForPointName(features) {
    features.forEach(feature => {
      this.appService.availableFeatures.forEach(availablefearute => {
        if (feature.name === 'Device Writeback' && availablefearute.featureId === feature.featureId && availablefearute.canAccess === 1) {
          this.isAccessForPointName = true;
        }
      });
    });
  }

  ngAfterViewInit() {

  }

  resetFilterData() {
    this.toggleFilterSearch = false;
    this.filterValues = {
      'name': '',
      'value': '',
      'unit': '',
      'node': '',
      'lastUpdate': '',
      'status': ''
    };
  }

  updateData(event) {
    this.realtimeData = event;
    this.appService.setAllOptionPagination('realTimeDataTable');
  }

  onCheckEventOfData($event, index) {
    this.historicalDataList.isNoData = false;
    let data = $event.target || $event.srcElement;
    if (data.checked) {
      this.totalCheckBoxClicked[index] = {
        'name': data.value,
        'node': this.realtimeData[index].node
      };
    } else {
      delete this.totalCheckBoxClicked[index];
    }
    const filter = [];
    for (let i = 0; i < this.totalCheckBoxClicked.length; i++) {
      if (!this.totalCheckBoxClicked[i]) {
        filter.push('');
      }
    }
    jQuery('body').css('overflow', 'hidden');
    this.allEqualsInArray = filter.length === this.totalCheckBoxClicked.length ? true : false;
    setTimeout(() => {
      let minusHeight = parseInt(jQuery('.asset-table').css('min-height'), 10) - jQuery('.panel-full-page-height .org-table thead').height() - this.minusHeightForTable;
      minusHeight = !this.allEqualsInArray ? minusHeight - 130 : minusHeight;
      this.mainBodyHeight = minusHeight;
      jQuery('body').css('overflow', '');
    });
  }

  realTimeDataChange($event) {
    let x = $event.target || $event.srcElement;
    if (x.checked) {
      this.selectedData.push(x.value);
    } else {
      this.selectedData = _.without(this.selectedData, x.value);
    }
    this.selectedRTData.emit(this.selectedData);
  }

  changeTableView() {
    this.selectedView = this.selectedView ? false : true;

    if (!this.selectedView && !this.tableviewChange.selAlarmView) {
      this.selectedView = true;
    }
    this.RTtableView.emit(this.selectedView);
  }

  changeTableMaxView() {
    this.maxmizeView = this.maxmizeView ? false : true;
    this.RTmaxmizeView.emit(this.maxmizeView);
  }

  checkValueChagesForSocket(data) {
    let isChangeValue = false;
    for (let index = 0; index < this.realtimeData.length; index++) {
      let element = this.realtimeData[index];
      let item = data.event_param[index];
      if (element && item && item['TS'] !== element.lastUpdate) {
        isChangeValue = true;
        break;
      }
    }
    return isChangeValue;
  }

  ngOnChanges(changes: SimpleChanges) {
    let isSearch = this.filterValues && !this.filterValues['name'] && !this.filterValues['value'] && !this.filterValues['unit'] && !this.filterValues['node'] && !this.filterValues['lastUpdate'] && !this.filterValues['status'];
    if (changes.assetTableInfo && changes.assetTableInfo.currentValue && isSearch) {
      let data = changes.assetTableInfo.currentValue;
      data.event_param = typeof data.event_param === 'string' ? JSON.parse(data.event_param) : data.event_param;
      data.event_param = _.filter(data.event_param, (item) => { return item.notInSync != 1 });
      let changed = this.checkValueChagesForSocket(data);
      if (changed || this.realtimeData.length === 0) {
        this.updateRealTimeData(data.event_param);
      }
      this.isFirstTimePagination = true;
    }

    if (changes.selectedAsset && changes.selectedAsset.currentValue) {
      this.toggleFilterSearch = false;
      // this.getAssetTableInfo();
      this.resetHistoricalData();
      this.cancelPointDataTypeModal();
    }
  }

  isFloat(n) {
    return Number(n) === n && n % 1 !== 0;
  }

  updateRealTimeData(data) {
    data.forEach((evt, index) => {
      evt._val = typeof evt._val === 'string' && evt._val.toLowerCase() === 'nan' ? '' : evt._val;
      let isNumber = !isNaN(evt._val);
      if (isNumber) {
        if (evt.unit !== '') {
          evt._val = this.unitService.convertUnit(evt.unit, this.appService.userProfileDetails.measurementUnit, evt._val);
        }
        if (Number(evt._val) <= Number.MAX_SAFE_INTEGER) {
          if (evt._val < 0.001 && evt._val > 0) {
            evt._val = Number(evt._val).toExponential(2);
          } else {
            evt._val = parseFloat(Number(evt._val).toFixed(3));
          }
        } else {
          evt._val = evt._val ? evt._val.replace(/\.0+$/, '') : evt._val;
        }
      }
      let unit = this.unitService.convertUnitText(evt.unit, this.appService.userProfileDetails.measurementUnit);
      this.tempLiveData[index] = {
        'name': evt.name,
        'value': evt._val,
        'unit': unit,
        'node': evt.node,
        'ts': evt.TS,
        'status': evt.sts,
        'originalUnit': evt.unit
      };
      if (!evt.datatype) evt.datatype = 'String - Read-Only';
      if (evt.notInSync != 1) {
        this.realtimeData[index] = new RealtimeStatus(evt.name, evt._val, unit, evt.node, evt.TS, evt.sts, evt.datatype, evt.notInSync, evt.displayName, evt.unit);
        this.realtimeData[index]['tLastUpdate'] = evt.TS;
      }
    });
    this.appService.setAllOptionPagination('realTimeDataTable');
  }

  showSearchFilter() {
    // this.minusHeightForTable = this.toggleFilterSearch ? 140 : 100;
    this.toggleFilterSearch = !this.toggleFilterSearch;
  }

  getAssetTableInfo() {
    this.assetDashboardService.getAssetTableInfo({
      companyId: this.appService.getActiveCompany().companyId,
      gwSno: this.selectedAsset.gwSNO,
      assetId: this.selectedAsset.assetId
    }).subscribe(response => {
      this.realtimeData = [];
      this.tempLiveData = [];
      if (response[0]) {
        this.getUnitForRealTimeData(response);
      } else {
        document.getElementsByTagName('body')[0].style.pointerEvents = '';
      }
    });
  }

  getUnitForRealTimeData(realtimeData) {
    this.assetDashboardService.getUnitForRealTimeData({
      companyId: this.appService.getActiveCompany().companyId,
      gwSno: this.selectedAsset.gwSNO,
      assetId: this.selectedAsset.assetId
    }).subscribe(response => {
      if (response[0]) {
        this.unitData = response[0].param;
        this.setUnitForRealTimeData(realtimeData[0].param);
        this.appService.appExportData.next(this.realtimeData);
      }
    });
  }

  setUnitForRealTimeData(realtimeData) {
    let count = 0;
    realtimeData.forEach((data, index) => {
      setTimeout((data) => {
        for (let i = 0; i < this.unitData.length; i++) {
          const element = this.unitData[i];
          element.node = element.node ? element.node : '';
          if (data.name === element.pointName && data.node === element.node) {
            data.unit = element.pointUnit;
            data.value = typeof data.value === 'string' && data.value.toLowerCase() === 'nan' ? '' : data.value;
            let isNumber = !isNaN(data.value);
            if (isNumber) {
              data.value = this.unitService.convertUnit(data.unit, this.appService.userProfileDetails.measurementUnit, data.value);
              if (Number(data.value) <= Number.MAX_SAFE_INTEGER) {
                if (data.value < 0.001 && data.value > 0) {
                  data.value = Number(data.value).toExponential(2);
                } else {
                  data.value = parseFloat(Number(data.value).toFixed(3));
                }
              }
            }
            let unit = this.unitService.convertUnitText(data.unit, this.appService.userProfileDetails.measurementUnit);
            if (count !== this.selectedExpandedAssetsIndex) {
              this.realtimeData[count] = new RealtimeStatus(data.name, data.value, unit, data.node, data.ts, data.status, element.pointDataType, element.notInSync, element.displayName, data.unit);
              this.realtimeData[count]['tLastUpdate'] = data.ts;
            }
            count++;
            break;
          }
        }
        if (this.realtimeData.length !== this.tempLiveData.length) {
          this.realtimeData = _.filter(this.realtimeData, (a) => {
            return _.find(this.tempLiveData, (b) => {
              return b.name === a.name && b.value === a.value && b.node === a.node;
            });
          });
        }
      }, true, data);
    });
    this.appService.setAllOptionPagination('realTimeDataTable');
  }

  resetHistoricalData() {
    this.selectedFrequency = 'Hourly';
    this.isInValidDate = false;
    this.allEqualsInArray = true;
    this.startDate = moment();
    this.endDate = moment();
    this.totalCheckBoxClicked = [];
  }

  downloadHistoricalData() {
    this.historicalDataList.isNoData = false;
    let diff = moment(this.endDate).diff(moment(this.startDate), 'days');
    if (diff < 0) {
      this.isInValidDate = true;
    } else {
      this.isInValidDate = false;
      this.isLoadingHistoricalData = true;
      this.totalCheckBoxClicked = this.totalCheckBoxClicked.filter((data) => {
        return data;
      });
      let dataPoints = [];
      this.totalCheckBoxClicked.forEach((point) => {
        if (point.node) {
          dataPoints.push(`${point.name}:n${point.node}`);
        } else {
          dataPoints.push(point.name);
        }
      });
      let data = {
        'assets': [{
          'assetId': this.selectedAsset.assetId,
          'gwSNO': this.selectedAsset.gwSNO
        }],
        'dataPoints': dataPoints,
        'startDate': this.appService.getUserTimeZoneDateFormat(this.startDate).format('MM/DD/YYYY HH:mm:00'),
        'endDate': this.appService.getUserTimeZoneDateFormat(this.endDate).format('MM/DD/YYYY HH:mm:00')
      }
      this.assetDashboardService.downloadHistoricalData(data)
        .finally(() => { this.isLoadingHistoricalData = false })
        .subscribe(response => {
          this.historicalDataList = {
            'isNoData': response.length === 0 ? true : false,
            'data': []
          };
          this.totalCheckBoxClicked.forEach((point) => {
            let array = _.filter(response, (item) => { return point.name === item['pointName'] });
            if (array.length > 0) {
              this.historicalDataList.data.push(_.sortBy(array, (list) => { return moment(list['occurrenceStartDate']) }));
            }
          });
          this.setDataBasedOnFrequency(this.historicalDataList.data);
        });
    }
  }

  setDataBasedOnFrequency(data) {
    if (this.selectedFrequency === this.frequency[0]) {
      this.setPointDataBasedOnHour(data);
    } else if (this.selectedFrequency === this.frequency[1]) {
      this.setPointDataBasedOnDay(data);
    } else if (this.selectedFrequency === this.frequency[2]) {
      this.setPointDataBasedOnWeek(data);
    } else {
      this.setPointDataBasedOnMonth(data);
    }
  }

  setPointDataBasedOnHour(data) {
    let count = 0;
    let array = [];
    data.forEach((item, parentIndex) => {
      array[parentIndex] = [];
      let count = -1;
      item.forEach((element, index) => {
        if (index < item.length - 1) {
          if (item[index + 1]) {
            if (moment(item[index + 1]['occurrenceStartDate']).hours() - moment(element['occurrenceStartDate']).hours() === 0) {
              if (!array[parentIndex][count]) {
                count++;
                array[parentIndex][count] = [];
              }
              array[parentIndex][count].push(element);
            } else {
              count++;
              array[parentIndex][count] = [];
              array[parentIndex][count].push(element);
            }
          }
        } else {
          if (!array[parentIndex][count]) {
            array[parentIndex][count] = [];
          }
          array[parentIndex][count].push(element);
        }
      });
    });
    this.getDownloadablePointName(array);
  }

  setPointDataBasedOnDay(data) {
    let count = 0;
    let array = [];
    data.forEach((item, parentIndex) => {
      array[parentIndex] = [];
      let count = 0;
      item.forEach((element, index) => {
        if (index < item.length - 1) {
          if (item[index + 1]) {
            if (moment(item[index + 1]['occurrenceStartDate']).date() - moment(element['occurrenceStartDate']).date() === 0) {
              if (!array[parentIndex][count]) {
                array[parentIndex][count] = [];
              }
              array[parentIndex][count].push(element);
            } else {
              array[parentIndex][count].push(element);
              count++;
              array[parentIndex][count] = [];
            }
          }
        } else {
          if (!array[parentIndex][count]) {
            array[parentIndex][count] = [];
          }
          array[parentIndex][count].push(element);
        }
      });
    });
    this.getDownloadablePointName(array);
  }

  setPointDataBasedOnWeek(data) {
    let count = 0;
    let array = [];
    data.forEach((item, parentIndex) => {
      array[parentIndex] = [];
      let count = -1;
      item.forEach((element, index) => {
        this.startOfWeekDate = this.getUserTimeZoneDateFormat(element['occurrenceStartDate']).startOf('week').dates();
        if (this.startOfWeekDate !== this.tempStartOfWeekDate) {
          count++;
          this.tempStartOfWeekDate = this.startOfWeekDate;
          if (!array[parentIndex][count]) {
            array[parentIndex][count] = [];
          }
          array[parentIndex][count].push(element);
        } else {
          if (!array[parentIndex][count]) {
            array[parentIndex][count] = [];
          }
          array[parentIndex][count].push(element);
        }
      });
    });
    this.getDownloadablePointName(array);
  }

  setPointDataBasedOnMonth(data) {
    let count = 0;
    let array = [];
    data.forEach((item, parentIndex) => {
      array[parentIndex] = [];
      let count = 0;
      item.forEach((element, index) => {
        if (index < item.length - 1) {
          if (item[index + 1]) {
            if (Number(moment(item[index + 1]['occurrenceStartDate']).format('M')) !== Number(moment(element['occurrenceStartDate']).format('M'))) {
              if (!array[parentIndex][count]) {
                array[parentIndex][count] = [];
              }
              array[parentIndex][count].push(element);
              count++;
            } else {
              if (!array[parentIndex][count]) {
                array[parentIndex][count] = [];
              }
              array[parentIndex][count].push(element);
            }
          }
        } else {
          if (!array[parentIndex][count]) array[parentIndex][count] = [];
          array[parentIndex][count].push(element);
        }
      });
    });
    this.getDownloadablePointName(array);
  }

  getDownloadablePointName(array) {
    let downloadArray = [];
    array.forEach(item => {
      item.forEach(element => {
        let min = JSON.parse(element[0].value)[0].Min, max = JSON.parse(element[0].value)[0].Max, sum = 0, mean;
        element.forEach(info => {
          let value = JSON.parse(info.value)[0];
          if (min > Number(value['Min'])) min = Number(value['Min']);
          if (max < Number(value['Max'])) max = Number(value['Max']);
          sum = Number(sum) + Number(value['Sum']);
        });
        downloadArray.push({
          'assetId': element[0].assetId,
          'gwSno': element[0].gwSno,
          'pointName': element[0].pointName,
          'occurrenceStartDate': element[0].occurrenceStartDate,
          'min': min,
          'max': max,
          'sum': sum,
          'mean': sum / element.length
        });
      });
    });
    this.appService.downloadAsExcel(this.historicalDataHeader, downloadArray, ['occurrenceStartDate'])
  }

  getUserTimeZoneDateFormat(date) {
    let tempDate = moment();
    if (this.appService.userTimezone != null) {
      tempDate = moment.tz(date, this.appService.userTimezone);
      tempDate = moment(tempDate).utcOffset(-tempDate.utcOffset());
    } else {
      tempDate = moment(date).utc();
    }
    return tempDate;
  }

  openpointDataTypeModal(data, index) {
    if (data['pointDataType'].toLowerCase().includes('write') || data['pointDataType'].toLowerCase().includes('writable')) {
      let i = this.expandedAssets[index];
      this.selectedExpandedAssetsIndex = index;
      this.expandedAssets = [];
      this.expandedAssets[index] = !i;
      this.pointDataValue = '';
      this.measurementUnit = '';
      this.selectedAssetPointName = data;
      this.setMeasurementsUnitForDataTypeModal(data);
    }
  }

  setMeasurementsUnitForDataTypeModal(data) {
    this.units[0]['value'] = data.unit;
    this.units[1]['value'] = data.originalUnit;
    if (data.unit == data.originalUnit) {
      if (this.appService.userProfileDetails.measurementUnit === 'Metric') {
        this.units[0]['value'] = this.unitService.convertUnitText(data.originalUnit, 'Imperial');
      } else if (this.appService.userProfileDetails.measurementUnit === 'Imperial') {
        this.units[0]['value'] = this.unitService.convertUnitText(data.originalUnit, 'Metric');
      }
    }
  }

  cancelPointDataTypeModal() {
    this.expandedAssets = [];
    this.selectedExpandedAssetsIndex = undefined;
  }

  savePointDataTypeModal() {
    let obj = {
      'companyId': this.appService.getActiveCompany().companyId,
      'companyName': this.appService.getActiveCompany().name,
      'gwSNO': this.selectedAsset.gwSNO,
      'assetId': this.selectedAsset.assetId,
      'createdBy': this.user.profile.name,
      'param': [{
        name: this.selectedAssetPointName['name'],
        val: this.unitService.convertUnit(this.selectedAssetPointName['unit'], this.measurementUnit, this.pointDataValue)
      }]
    }
    this.appService.loader.next(true);
    this.assetDashboardService.savePointDataTypeModal(obj)
      .finally(() => {
        this.appService.loader.next(false);
      })
      .subscribe(response => {
        this.appService.showSuccessMessage(
          'Success',
          'Data updated successfully'
        );
        this.cancelPointDataTypeModal();
      });
  }

}
