import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssetTableComponent } from './asset-table.component';
import { DataTableModule } from 'angular2-datatable';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap';
import { SharedModule } from "../../../shared-module";
import {TranslateModule} from "@ngx-translate/core";
@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    FormsModule,
    SharedModule,
    ModalModule.forRoot(),
    TranslateModule
  ],
  declarations: [AssetTableComponent],
  exports: [AssetTableComponent,TranslateModule]
})
export class AssetTableModule { }
