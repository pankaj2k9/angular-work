import {Component, OnInit, ElementRef} from '@angular/core';
import {  } from '@types/googlemaps';
import { MapsAPILoader, GoogleMapsAPIWrapper } from '@agm/core';
import {Loc} from "../../core/charts/ichart";
declare let google: any;



@Component({
  selector: 'app-create-route',
  templateUrl: './create-route.component.html',
  styleUrls: ['./create-route.component.scss']
})
export class CreateRouteComponent implements OnInit {

  public totalDistance;
  public  directionsDisplay;
  public  directionsService;
  public  map;
  public wayPoint=[];
  public originalWayPoint: any[] = [];
  public  origin = "Mellon Labs, MG Ramachandran Rd, Anna Colony, Besant Nagar, Chennai, Tamil Nadu";
  public  points = ["Guru Nanak College, Velachery, Chennai, Tamil Nadu","Gandhi Nagar Club, Gandhi Nagar 4th Main Road, Gandhi Nagar, Adyar, Chennai, Tamil Nadu","TICEL Biotech Park, Tharamani, Chennai, Tamil Nadu","The B Bar, MRC Nagar, Raja Annamalai Puram, Chennai, Tamil Nadu","Advantix Technologies, Chamiers Road, Nandanam, Chennai, Tamil Nadu","Chemplast Cricket Ground, Chennai, Tamil Nadu"];


  constructor( private mapsAPILoader: MapsAPILoader, private elementRef: ElementRef) {}

  ngOnInit() {




  }

  ngAfterViewInit(){
    this.mapsAPILoader.load().then(() => {
      this.initMap();
    });
  }

  initMap() {
   let map = new google.maps.Map(document.getElementById('map'), {
       zoom: 4,
       center: new google.maps.LatLng(-24.345, 134.46)
    });
    let drawingManager = new google.maps.drawing.DrawingManager({
      drawingMode: google.maps.drawing.OverlayType.MARKER,
      drawingControl: true,
      drawingControlOptions: {
        position: google.maps.ControlPosition.TOP_CENTER,
        drawingModes: ['marker', 'circle', 'polygon', 'polyline', 'rectangle']
      },
      markerOptions: {
        animation: google.maps.Animation.DROP,
        icon: new google.maps.MarkerImage('https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'),
        draggable: true,
        flat: true,
        raiseOnDrag: true
      },

      circleOptions: {
        fillColor: '#ffff00',
        fillOpacity: 1,
        strokeWeight: 5,
        clickable: false,
        editable: true,
        zIndex: 1
      }
    });
    drawingManager.setMap(map);
    let wayPoint = [];

    google.maps.event.addDomListener(drawingManager, 'markercomplete',
     (marker) => {
        let temlat=marker.position.lat();
        let temlng=marker.position.lng();
        this.getLocationInfo(temlat,temlng,(locationn)=>{
         this.wayPoint.push(locationn);
          console.log(locationn);
       });
       this.displayRoute(this.wayPoint[0], this.wayPoint[1], directionsService,
         directionsDisplay);
     });




    let directionsService = new google.maps.DirectionsService;
    let directionsDisplay = new google.maps.DirectionsRenderer({
    draggable: true,
    map: map,
    panel: document.getElementById('right-panel')
  });

    directionsDisplay.addListener('directions_changed', (e) => {
      this.computeTotalDistance(directionsDisplay.getDirections());
      console.log(directionsDisplay.getDirections());
    });


}



  displayRoute(origin, destination, service, display) {
    let originalWayPointt=this.wayPoint;
    originalWayPointt.forEach(data => {
      this.originalWayPoint.push({location: data});
    });

     service.route({
       origin: origin,
       destination: destination,
       waypoints: this.originalWayPoint,
       travelMode: 'DRIVING',
       optimizeWaypoints: true,
       avoidTolls: true
  }, function(response, status) {
    if (status === 'OK') {
      display.setDirections(response);
    } else {
      alert('Could not display directions due to: ' + status);

    }
  });
}

  computeTotalDistance(result) {
    var total = 0;
    var myroute = result.routes[0];
    for (var i = 0; i < myroute.legs.length; i++) {
      total += myroute.legs[i].distance.value;
   }
   total = total / 1000;
   document.getElementById('total').innerHTML = total + ' km';
}

  getLocationInfo(latitude, longitude, callback) {
    let geocoder = new google.maps.Geocoder;
    let latlng = { lat: parseFloat(latitude), lng: parseFloat(longitude) };
    const that = this;
    let location;
    geocoder.geocode({ 'location': latlng }, function (results, status) {
      if (status == 'OK') {
        let assetLocationAddress = results[0].formatted_address.split(',');
        if (assetLocationAddress) {
          location = assetLocationAddress[1] + ',' + assetLocationAddress[2];
          callback(location);
        }
      }
    });

  }




}
