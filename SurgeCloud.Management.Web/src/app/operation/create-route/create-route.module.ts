import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from '../../../environments/environment';
import { AppCommonModule } from '../../app-common';

import {CreateRouteComponent} from "./create-route.component";
import {CreateRouteRoutingModule} from "./create-route-routing.module";
import { AgmCoreModule } from '@agm/core';
import {TranslateModule,TranslateLoader,TranslateCompiler,TranslateParser,MissingTranslationHandler} from "@ngx-translate/core";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {HttpLoaderFactory} from "../../app.module";



@NgModule({
  imports: [
    CommonModule,
    AppCommonModule,
    CreateRouteRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: environment.googleApiKey,
      libraries: ['places', 'drawing']
    }),
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    CreateRouteComponent

  ],
  exports: [
    CreateRouteComponent
  ]


})
export class CreateRouteModule { }
