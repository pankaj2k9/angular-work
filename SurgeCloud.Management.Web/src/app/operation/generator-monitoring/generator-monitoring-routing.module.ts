import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {GeneratorMonitoringComponent} from "./generator-monitoring.component";
const routes: Routes = [
  {
    path: '',
    component: GeneratorMonitoringComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GeneratorMonitoringRoutingModule {

}
