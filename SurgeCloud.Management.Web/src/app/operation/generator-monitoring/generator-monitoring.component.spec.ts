import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneratorMonitoringComponent } from './generator-monitoring.component';

describe('GeneratorMonitoringComponent', () => {
  let component: GeneratorMonitoringComponent;
  let fixture: ComponentFixture<GeneratorMonitoringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneratorMonitoringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneratorMonitoringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
