import {
  Component, OnInit, OnDestroy, AfterViewInit, ViewChild, Input, SimpleChanges, Output, EventEmitter,
  AfterContentInit
} from '@angular/core';
import { Generator, GeneratorLocationMap, GenAssetTable, GeneratorAlarmStatus, GenAssetTableShrinkView } from '../../app-common';
import { AppService } from './../../app.service';
import { GeneratorMonitoringService } from './generator-monitoring.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Socket } from 'ng-socket-io';
import { ModalDirective } from 'ngx-bootstrap';
import { Subscription } from 'rxjs/Rx';
import { OperationService } from '../operation.service';
import { AdalService } from '../../shared-module';
import { SingleChartDataa, MultiChartDataa } from '../../core/charts/ichart';
import { BarChartConfig, LineChartConfig } from "../../core";
import * as moment from 'moment';

@Component({
  selector: 'app-generator-monitoring',
  templateUrl: './generator-monitoring.component.html',
  styleUrls: ['./generator-monitoring.component.scss']
})
export class GeneratorMonitoringComponent implements OnInit, AfterContentInit {
  @ViewChild('commandWarningModal') public commandWarningModal: ModalDirective;
  public commandWarningMessage: string = '';
  public lineChartConfig: LineChartConfig;
  public barChartConfig: BarChartConfig;
  public assetId: any = null;
  public activeCompanyInfoSubscription: any;
  public activeAlarmInfoSubscription: any;
  public assetsAvailable: Generator[] = [];
  assetDescInfo: Generator;
  assetMapInfo: GeneratorLocationMap;
  public selectedAsset: any = null;
  private routerParam: Subscription;
  public socketConnectPrm: any = '';
  private keysForSubscriptionn = [];
  assetTableInfo: GenAssetTable[] = null;
  public assetAlarmInfo: any = {};
  public numberRowPerPage: number;
  public getGenAssetAlaramInfo: any[];
  public getGenUnitForRealTimeData: any[] = [];
  public selectedIndex = 0;
  public activeGenDataInfoSubscription: any;
  public user: any;
  dateInfo: string[] = [];
  timeInfo: number[] = [];
  public gAnalyticData: any[] = [];
  public cAnalyticData: any[] = [];
  public oAnalyticData: any[] = [];
  public gDataPointName = 'Engine Total Runtime';
  public cDataPointName = 'Engine Coolant Temperature';
  public oDataPointName = 'Engine Oil Pressure';
  public isNoGeneratorsAvailable: boolean = false;
  public rhtInnerHeight = 0;
  public cooloil: MultiChartDataa[] = [];
  public cooloill: MultiChartDataa[] = [];
  public toggleAlarms = {
    'value': true,
    'data': []
  };
  public toggleTimers = {
    'value': true,
    'data': []
  };

  private tableviewChange: GenAssetTableShrinkView = {
    selRTtableView: true, selAlarmView: true,
    rtClass: 'col-lg-6 col-md-6', rtInnerClass: '',
    alarmClass: 'col-lg-3 col-md-3 asset-right-align', alarmInnerClass: '',
    RTtableMaxView: false, AlarmMaxView: false
  };
  selectedRealTimeData: string[] = [];
  companyId: string;
  public timers = [];
  public loadStatusDataValue = '';
  public modeDataValue = 'Off';
  public runStatusDataValue = '';
  public systemHealth = {
    'alarm': {},
    'warning': {}
  }
  public genAssetTableInfo: any[] = [];
  constructor(private appService: AppService, private operationService: OperationService, private socket: Socket,
    public generatorMonitoringService: GeneratorMonitoringService, private adalService: AdalService, private route: ActivatedRoute,
    private router: Router) {
    this.numberRowPerPage = this.operationService.numberOfRecordPerPage;
  }

  ngOnInit() {

    if (document.documentElement.clientWidth < 1000) {
      if (document.getElementById('genAssetDescApp')) {
        this.rhtInnerHeight = window.innerHeight - document.getElementById('genAssetDescApp').clientHeight;
      }
    } else {
      let body = document.body;
      let html = document.documentElement;

      this.rhtInnerHeight = Math.max(body.scrollHeight, body.offsetHeight,
        html.clientHeight, html.scrollHeight, html.offsetHeight);

    }
    this.companyId = this.appService.getActiveCompany() ? this.appService.getActiveCompany().companyId : '';
    this.routerParam = this.route.params.subscribe(params => {
      this.assetId = params['assetId'];
      if (this.appService.getActiveCompany()) {
        this.getAssetAvailable();
      }
    });
    this.user = this.adalService.userInfo;
    this.activeCompanyInfoSubscription = this.appService.activeCompanyInfo.subscribe(
      (company) => {
        this.companyId = this.appService.getActiveCompany().companyId;
        this.selectedAsset = null;
        this.getAssetAvailable();
      }
    );

  }

  ngOnDestroy() {
    if (this.selectedAsset !== null) {
      this.routerParam.unsubscribe();
      this.removeAllSocketListeners();
      this.activeCompanyInfoSubscription.unsubscribe();
      this.activeAlarmInfoSubscription.unsubscribe();
      this.activeGenDataInfoSubscription.unsubscribe();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  ngAfterContentInit() {

  }

  alarmGenInfo() {
    let companyId = this.appService.getActiveCompany().companyId;
    this.activeAlarmInfoSubscription = this.generatorMonitoringService.getGenAssetAlaramInfo({
      companyId: companyId,
      gwSno: this.selectedAsset.gwSNO,
      assetId: this.selectedAsset.assetId,
      rows: 10
    }).subscribe(response => {
      if (response) {

        this.getGenAssetAlaramInfo = response;
      }
    });
  }

  changeRTtableView(tableView: boolean) {

    this.tableviewChange.selRTtableView = tableView;
    this.tableViewClass();
  }

  changeACtableView(tableView: boolean) {
    this.tableviewChange.selAlarmView = tableView;
    this.tableViewClass();
  }

  changeRTMaxView(view: boolean) {
    this.tableviewChange.RTtableMaxView = view;

    if (this.tableviewChange.RTtableMaxView) {
      this.tableviewChange.rtClass = 'col-lg-9 col-md-9';
      this.tableviewChange.rtInnerClass = '';
    } else {
      this.tableviewChange.selRTtableView = true;
      this.tableviewChange.selAlarmView = true;
      this.tableViewClass();
    }
  }

  changeACMaxView(view: boolean) {
    this.tableviewChange.AlarmMaxView = view;
    if (this.tableviewChange.AlarmMaxView) {
      this.tableviewChange.alarmClass = 'col-lg-9 col-md-9';
      this.tableviewChange.alarmInnerClass = '';
    } else {
      this.tableviewChange.selRTtableView = true;
      this.tableviewChange.selAlarmView = true;
      this.tableViewClass();
    }
  }

  tableViewClass() {
    if (!this.tableviewChange.selRTtableView && !this.tableviewChange.selAlarmView) {
      this.tableviewChange.selRTtableView = true;
      this.tableviewChange.selAlarmView = false;
    }

    if (this.tableviewChange.selRTtableView && this.tableviewChange.selAlarmView) {
      this.tableviewChange.rtClass = 'col-lg-6 col-md-6';
      this.tableviewChange.rtInnerClass = '';
      this.tableviewChange.alarmClass = 'col-lg-3 col-md-3 asset-right-align';
      this.tableviewChange.alarmInnerClass = '';
    } else if (!this.tableviewChange.selRTtableView && this.tableviewChange.selAlarmView) {
      this.tableviewChange.rtClass = 'col-lg-1 col-md-1 width-0-4';
      this.tableviewChange.rtInnerClass = '';
      this.tableviewChange.alarmClass = 'col-lg-8 col-md-8 pl-10';
      this.tableviewChange.alarmInnerClass = 'row mr-8';
    } else if (this.tableviewChange.selRTtableView && !this.tableviewChange.selAlarmView) {
      this.tableviewChange.rtClass = 'col-lg-8 col-md-8 pl-10';
      this.tableviewChange.rtInnerClass = 'mr-7';
      this.tableviewChange.alarmClass = 'col-lg-1 col-md-1 width-0-4 alarm-mp';
      this.tableviewChange.alarmInnerClass = '';
    }
  }

  changeRTData(data: string[]) {
    this.selectedRealTimeData = data;
    this.getHistoricalData();
  }

  getHistoricalData() {
    if (this.selectedRealTimeData.length > 0) {
      this.changeRTMaxView(false);
      let startdate = `${this.dateInfo[0]} ${this.timeInfo[0]}:00:00`;
      let enddate = `${this.dateInfo[1]} ${this.timeInfo[1]}:00:00`;
      this.generatorMonitoringService.getGenHistoricalData({
        companyId: this.companyId,
        gwSno: this.assetMapInfo.gwSNO, assetId: this.assetId,
        datapointname: this.selectedRealTimeData[0],
        startDate: startdate,
        endDate: enddate
      }).subscribe(response => {

      });
    }
  }

  getAnalyticData(gDataPointName, startDate, endDate) {
    let start = moment().subtract(77, 'd').valueOf();

    let now = moment.utc(start).format();
    let mstart = moment().subtract(6, 'months').valueOf();

    let mnow = moment.utc(mstart).format();

    const DataGraph = {

      Assets: [{
        AssetId: this.selectedAsset.assetId,
        gwSNO: this.selectedAsset.gwSNO,
        //AssetId:"118425104",
        //gwSNO: "R115LLA0473",
      }],
      DataPoints: [gDataPointName],
      StartDate: startDate,
      EndDate: endDate

    };

    this.generatorMonitoringService.getGenAnalyticData(DataGraph).subscribe(
      (res) => {
        if (res) {
          if (gDataPointName === this.cDataPointName) {

            let lastSevenDayValue = res;
            let specificdayValue = [];
            lastSevenDayValue.forEach(data => {
              let singleData = data.value;
              let singleDataJson = JSON.parse(singleData);
              let singleDataMax = singleDataJson[0].Max;
              let day = this.getReadableDay(data.occurrenceStartDate);
              specificdayValue.push({ value: singleDataMax, name: day, occurrenceStartDate: data.occurrenceStartDate });

            });
            let results = [];
            results.push(this.getDayByValue("Sat", specificdayValue));
            results.push(this.getDayByValue("Sun", specificdayValue));
            results.push(this.getDayByValue("Mon", specificdayValue));
            results.push(this.getDayByValue("Tue", specificdayValue));
            results.push(this.getDayByValue("Wed", specificdayValue));
            results.push(this.getDayByValue("Thu", specificdayValue));
            results.push(this.getDayByValue("Fri", specificdayValue));
            this.cooloil.push(new MultiChartDataa(this.cDataPointName, results));
          } else if (gDataPointName === this.gDataPointName) {
            let lastSixMonthValue = res;
            let specificmonthValue = [];
            lastSixMonthValue.forEach(data => {
              let singleData = data.value;
              let singleDataJson = JSON.parse(singleData);
              let singleDataMax = singleDataJson[0].Max;
              let month = this.getReadableMonth(data.occurrenceStartDate);
              specificmonthValue.push({ value: singleDataMax, name: month });

            });
            let dateEnd = moment();
            let dateStart = moment().subtract(5, 'months');
            let timeValues = [];

            while (dateEnd > dateStart || dateStart.format('M') === dateEnd.format('M')) {
              timeValues.push(dateStart.format('MMM'));
              dateStart.add(1, 'month');
            }
            let resultsBar = [];
            timeValues.forEach(data => {
              resultsBar.push(this.getMonthByValue(data, specificmonthValue));
            });
            this.gAnalyticData = resultsBar;
          } else if (gDataPointName === this.oDataPointName) {
            let lastSevenDayValue = res;
            let specificdayValue = [];
            lastSevenDayValue.forEach(data => {
              let singleData = data.value;
              let singleDataJson = JSON.parse(singleData);
              let singleDataMax = singleDataJson[0].Max;
              let day = this.getReadableDay(data.occurrenceStartDate);
              specificdayValue.push({ value: singleDataMax, name: day, occurrenceStartDate: data.occurrenceStartDate });

            });
            let results = [];
            results.push(this.getDayByValue("Sat", specificdayValue));
            results.push(this.getDayByValue("Sun", specificdayValue));
            results.push(this.getDayByValue("Mon", specificdayValue));
            results.push(this.getDayByValue("Tue", specificdayValue));
            results.push(this.getDayByValue("Wed", specificdayValue));
            results.push(this.getDayByValue("Thu", specificdayValue));
            results.push(this.getDayByValue("Fri", specificdayValue));
            this.cooloil.push(new MultiChartDataa(this.oDataPointName, results));
            this.cooloill = this.cooloil;
          }
        }
      },
      (error) => {
      }
    );
  }

  getAssetAvailable() {
    if (this.socketConnectPrm) {
      this.removeAllSocketListeners();
    }
    let companyId = this.appService.getActiveCompany().companyId;
    this.generatorMonitoringService.getGenAssetDesc(companyId)
      .subscribe(response => {
        let dateEnd = moment().format("YYYY-MM-DD HH:mm:ss");
        let dateStartDay = moment().subtract(77, 'd').format("YYYY-MM-DD HH:mm:ss");
        let dateStartMonth = moment().subtract(6, 'months').format("YYYY-MM-DD HH:mm:ss");
        if (response && response.length > 0) {
          this.assetsAvailable = response ? response : [];
          this.selectedAsset = this.assetsAvailable[0];
          this.alarmGenInfo();
          this.getAssetTableInfo();
          this.getGenUnitRealData();
          this.getAnalyticData(this.gDataPointName, dateStartMonth, dateEnd);
          this.getAnalyticData(this.cDataPointName, dateStartDay, dateEnd);
          this.getAnalyticData(this.oDataPointName, dateStartDay, dateEnd);
          this.isNoGeneratorsAvailable = false;
        } else {
          if (this.router.url === '/operation/generator-monitoring') {
            this.isNoGeneratorsAvailable = true;
            if (confirm('Sorry, there is no Generators available. Please have a Generator setup and access this page')) {
              this.router.navigate(['/admin/feature']);
            } else {
              this.assetsAvailable = [];
              this.selectedAsset = null;
              // this.getGenUnitForRealTimeData=null;
              this.genAssetTableInfo = [];
              this.getGenAssetAlaramInfo = [];
              this.gAnalyticData = [];
            }
          }
        }
      });
  }

  removeAllSocketListeners() {
    if (this.socketConnectPrm && this.socketConnectPrm != null) {
      this.socketConnectPrm.removeAllListeners();
      this.socketConnectPrm.disconnect();
    }
    this.socket.disconnect();
  }

  getAssetSelected(selectedIndex) {
    this.selectedAsset = this.assetDescInfo[selectedIndex];
    this.subscribeForAsset();
  }

  subscribeForAsset() {
    if (this.socketConnectPrm) {
      this.removeAllSocketListeners();
    }
    this.socketConnectPrm = this.socket.connect();
    const company = this.appService.getActiveCompany();
    this.keysForSubscriptionn = [];
    this.keysForSubscriptionn.push(`${this.selectedAsset.assetId}.${this.selectedAsset.gwSNO}.realtime`);
    this.keysForSubscriptionn.push(`${this.selectedAsset.assetId}.${this.selectedAsset.gwSNO}.geo`);
    this.keysForSubscriptionn.push(`${this.selectedAsset.assetId}.${this.selectedAsset.gwSNO}.alarm`);
    this.socket.emit('subscribe', this.keysForSubscriptionn);
    document.getElementsByTagName('body')[0].style.pointerEvents = 'none';
    let socketData;
    this.socket.on('data', (data) => {
      socketData = data;
      document.getElementsByTagName('body')[0].style.pointerEvents = '';
      if (data.status === 'Geo') {
        this.assetMapInfo = data;
      } else if (data.status === 'Realtime') {
        this.assetTableInfo = data;
      } else {
        this.assetAlarmInfo = data;
      }
    });
    setTimeout(() => {
      if (!socketData) {
        document.getElementsByTagName('body')[0].style.pointerEvents = '';
      }
    }, 5000);
    this.socketDisconnectHandler();
  }

  assetChanged(asset) {
    if (this.socketConnectPrm) {
      this.removeAllSocketListeners();
    }
    if (asset) {
      this.selectedAsset = asset;
      this.getAssetTableInfo();
      this.getGenUnitRealData();
      this.alarmGenInfo();
    }
  }

  socketDisconnectHandler() {
    this.socket.on('disconnect', () => {
      this.subscribeForAsset();
    });
    this.socketErrorConectHandler();
    this.socketErrorHandler();
  }

  socketErrorConectHandler() {
    this.socket.on('connect_error', () => {
      console.log('connect_error');
      this.subscribeForAsset();
    });
  }

  socketErrorHandler() {
    this.socket.on('error', (err) => {
      console.log('Error connecting to server', err);
      this.subscribeForAsset();
    });
  }

  dataUpdate(data) {
    setTimeout(() => {
      this.updateTimersData(data.timers);
      this.loadStatusDataValue = data.loadStatusData.value;
      this.modeDataValue = data.modeData.value ? data.modeData.value : 'Off';
      this.runStatusDataValue = data.runStatusData.value;
      this.systemHealth['alarm'] = data.systemHealth.alarm ? data.systemHealth.alarm : {};
      this.systemHealth['warning'] = data.systemHealth.warning ? data.systemHealth.warning : {};
    });
  }

  updateTimersData(data) {
    if (this.toggleTimers.data.length > 0) {
      let array = [];
      data.forEach(element => {
        let name = element.displayName ? element.displayName : element.name;
        if (!this.toggleTimers.data.includes(name)) {
          array.push(JSON.parse(JSON.stringify(element)));
        }
      });
      this.timers = array;
    } else {
      this.timers = data;
    }
  }

  rmtCmd(cmd) {
    this.commandWarningModal.show();
    if (cmd) {
      this.commandWarningMessage = 'stop';
    } else {
      this.commandWarningMessage = 'start';
    }
  }

  goToAssetDashboardPage() {
    this.router.navigate(['/operation/asset-dashboard']);
  }

  rmtCmdCallBack(data: string) {
    const Data = {
      companyId: this.appService.getActiveCompany().companyId,
      assetId: this.selectedAsset.assetId,
      SentBy: this.user.profile.name,
      gwSno: this.selectedAsset.gwSNO,
      Param: [{
        Name: "Remote Command",
        Val: data
      }]
    };

    this.generatorMonitoringService.postCommand(Data).subscribe(
      (res) => {
        this.appService.showSuccessMessage(
          'Success',
          `Asset is Updated`
        );
      },
      (error) => {
        this.appService.showErrorMessage('Error', (error.json() && error.json().message || error.json().ErrorMessage));
      }
    );
  }

  dataFromSocket(data) {
    if (data.status === 'Geo') {
      this.assetMapInfo = data;
    } else if (data.status === 'Realtime') {
      this.assetTableInfo = data;
    } else {
      this.assetAlarmInfo = data;
    }
  }

  getAssetTableInfo() {
    this.activeGenDataInfoSubscription = this.generatorMonitoringService.getGenAssetTableInfo({
      companyId: this.appService.getActiveCompany().companyId,
      gwSno: this.selectedAsset.gwSNO,
      assetId: this.selectedAsset.assetId
    }).subscribe(response => {
      this.genAssetTableInfo = response;
    });
  }

  getGenUnitRealData() {
    this.generatorMonitoringService.getGenUnitForRealTimeData({
      companyId: this.appService.getActiveCompany().companyId,
      gwSno: this.selectedAsset.gwSNO,
      assetId: this.selectedAsset.assetId
    }).subscribe(response => {
      this.getGenUnitForRealTimeData = response;
      this.subscribeForAsset();
    });
  }

  getReadableDay(element) {
    element = moment.utc(element);
    element = element.local();
    element = element.format('ddd');
    return element;
  }
  getReadableMonth(element) {
    element = moment.utc(element);
    element = element.local();
    element = element.format('MMM');
    return element;
  }

  getReadableMilisec(element) {
    element = moment.utc(element);
    element = element.local();
    element = element.valueOf();
    return element;
  }

  getDayByValue(day, specificdayValue) {

    let satDaymuiltivalue = [];
    let satDayValue = specificdayValue.filter((obj) => {
      return obj.name === day;
    }).map(function (obj) {
      satDaymuiltivalue.push(obj.value)
      return obj;
    });
    let singleSatData: any = {};
    if (!(satDayValue.length > 0)) {
      singleSatData.value = 0;
      singleSatData.name = day;

    } else {
      singleSatData.value = Math.max(...satDaymuiltivalue);
      singleSatData.name = day;
    }
    let results = [];
    results.push(new SingleChartDataa(singleSatData.value, singleSatData.name));
    return results[0];
  }

  getMonthByValue(month, specificmonthValue) {

    let satMonthmuiltivalue = [];
    let satMonthValue = specificmonthValue.filter((obj) => {
      return obj.name === month;
    }).map(function (obj) {
      satMonthmuiltivalue.push(parseFloat(obj.value))
      return obj;
    });
    let singleSatData: any = {};
    if (!(satMonthValue.length > 0)) {
      singleSatData.value = 0;
      singleSatData.name = month;

    } else {
      let sum = satMonthmuiltivalue.reduce((a, b) => a + b, 0);
      singleSatData.value = sum;
      singleSatData.name = month;
    }
    let results = [];
    results.push(new SingleChartDataa(singleSatData.value, singleSatData.name));
    return results[0];
  }

  toggleTableRowsData(index, prm) {
    if (prm === 'timers') {
      let data = this.timers.splice(index, 1);
      if (data.length > 0) {
        this.toggleTimers.data.push(data[0].displayName ? data[0].displayName : data[0].name);
      }
    } else if (prm === 'alarms') {

    }
  }
}
