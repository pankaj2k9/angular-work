import { CommonModule } from "@angular/common";
import { CoreModule } from "../../core/core.module";
import { FormsModule } from "@angular/forms";
import { GeneratorMonitoringComponent } from "./generator-monitoring.component";
import { AssetDashboardService } from "../asset-dashboard/asset-dashboard.service";
import { NgModule } from "@angular/core";
import { GeneratorMonitoringService } from "./generator-monitoring.service";
import { GeneratorLocationModule } from "../../core/generator-location/generator-location.module";
import { GeneratorAlarmPointModule } from "../../core/generator-alarm-point/generator-alarm-point.module";
import { generatorRealDataModule } from "../../core/generator-realdata/generator-realdata.module";
import { generatorDescModule } from "../../core/generator-desc/generator-desc.module";
import { GeneratorMonitoringRoutingModule } from "./generator-monitoring-routing.module";
import { SharedModule } from "../../shared-module";
import { ModalModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    GeneratorMonitoringRoutingModule,
    CommonModule,
    CoreModule,
    FormsModule,
    GeneratorLocationModule,
    GeneratorAlarmPointModule,
    generatorRealDataModule,
    generatorDescModule,
    SharedModule,
    ModalModule.forRoot()
  ],
  declarations: [
    GeneratorMonitoringComponent

  ],
  exports: [
    GeneratorMonitoringComponent
  ],
  providers: [
    GeneratorMonitoringService
  ]
})
export class GeneratorMonitoringModule { }
