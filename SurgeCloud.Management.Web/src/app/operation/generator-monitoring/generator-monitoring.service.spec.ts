import { TestBed, inject } from '@angular/core/testing';

import { GeneratorMonitoringService } from './generator-monitoring.service';

describe('GeneratorMonitoringService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GeneratorMonitoringService]
    });
  });

  it('should ...', inject([GeneratorMonitoringService], (service: GeneratorMonitoringService) => {
    expect(service).toBeTruthy();
  }));
});
