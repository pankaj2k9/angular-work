import { Injectable } from '@angular/core';
import { HttpService } from './../../shared-module';
import { Observable } from 'rxjs/Observable';
import { Company } from '../../../app';
import * as moment from 'moment';
import {
  Http,
  ConnectionBackend,
  RequestOptions,
  RequestOptionsArgs,
  Response,
  Headers
} from '@angular/http';
@Injectable()
export class GeneratorMonitoringService {

  constructor(public httpService: HttpService, public http: Http) { }

  getGenAssetDesc(companyId: string): Observable<any> {

    return this.httpService.get(`device/api/iot/get/${companyId}?assetType=Generator`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));

  }

  getGenDevice(companyId): Observable<any> {
    return this.httpService.get(`device/api/iot/getdevicebycompany/${companyId}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getGenAssetMapInfo(companyId: string): Observable<any> {

    return this.httpService.get(`device/api/iot/getdevicebycompany/${companyId}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));

  }

  getGenUnitForRealTimeData(obj: { companyId: string, gwSno: string, assetId: string }): Observable<any> {

    return this.httpService.get(encodeURI(`device/api/iot/getdevice?companyId=${obj.companyId}&gwSno=${obj.gwSno}&assetId=${obj.assetId}`))
      .map(res => res.json())
      .catch(err => Observable.throw(err));

  }

  getGenAssetTableInfo(obj: { companyId: string, gwSno: string, assetId: string }): Observable<any> {

    return this.httpService.get(encodeURI(`device/api/iot/getrealtimedata?companyId=${obj.companyId}&gwSno=${obj.gwSno}&assetId=${obj.assetId}`))
      .map(res => res.json())
      .catch(err => Observable.throw(err));

  }

  getGenAssetAlaramInfo(obj: { companyId: string, gwSno: string, assetId: string, rows: Number }): Observable<any> {

    return this.httpService.get(encodeURI(`device/api/iot/getalarmdata?companyId=${obj.companyId}&gwSno=${obj.gwSno}&assetId=${obj.assetId}&rows=${obj.rows}`))
      .map(res => res.json())
      .catch(err => Observable.throw(err));

  }

  getGenHistoricalData(obj: { companyId: string, gwSno: string, assetId: string, datapointname: string, startDate: string, endDate: string }): Observable<any> {
    return this.httpService.get(encodeURI(`device/api/iot/GetHistoryData?companyId=${obj.companyId}&gwSno=${obj.gwSno}&assetId=${obj.assetId}&datapointname=${obj.datapointname}&startDate=${obj.startDate}&endDate=${obj.endDate}`))
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getGenAnalyticData(data): Observable<any> {
    return this.httpService.post(`analytics/api/Analytics/GetPointAnalyticData`, data)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  postCommand(data): Observable<any> {
    return this.httpService.post(`device/api/iot/SendCommand`, data)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

}
