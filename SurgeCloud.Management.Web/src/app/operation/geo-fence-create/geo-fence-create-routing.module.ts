import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {GeoFenceCreateComponent} from "./geo-fence-create.component";
const routes: Routes = [
  {
    path: '',
    component: GeoFenceCreateComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GeoFenceCreateRoutingModule {

}
