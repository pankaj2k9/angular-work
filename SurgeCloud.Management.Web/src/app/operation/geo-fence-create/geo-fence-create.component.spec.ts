import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeoFenceCreateComponent } from './geo-fence-create.component';

describe('GeoFenceCreateComponent', () => {
  let component: GeoFenceCreateComponent;
  let fixture: ComponentFixture<GeoFenceCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeoFenceCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeoFenceCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
