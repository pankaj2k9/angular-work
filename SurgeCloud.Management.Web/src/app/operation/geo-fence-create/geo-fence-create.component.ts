import { AfterViewChecked, Component, HostListener, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { GeoFenceAsset } from '../../app-common';
import { GeoFenceCreateService } from './geo-fence-create.service';
import { GeofenceMapViewComponent } from './geofence-map-view';
import { newShape } from './geonfence-create';
import { shadeRGBColor } from '@swimlane/ngx-charts/release/utils';

@Component({
  selector: 'app-geo-fence-create',
  templateUrl: './geo-fence-create.component.html',
  styleUrls: ['./geo-fence-create.component.scss']
})
export class GeoFenceCreateComponent implements OnInit {
  firstViewCheckedExecuted = false;
  public windowResizeData: object;
  @HostListener('window:resize') onWindowResize() {
    this.setResizedDataOnDocumentLoad(true);
  }

  public filter: any = {};
  public geoDataAvailable: any[];
  public assetFiltered: GeoFenceAsset[] = [];
  public fullscreen: any = {
    map: false,
    list: false,
  };
  public minimize: any = {
    map: false,
    list: false
  };

  public selectedShape: newShape;



  constructor(private geoFenceCreateService: GeoFenceCreateService) { }

  ngOnInit() {
    this.geoFenceCreateService.minMaxApplied.subscribe(
      (res) => {
        this.fullscreen = res.fullscreen;
      }
    )
    this.setResizedDataOnDocumentLoad();
  }

  setResizedDataOnDocumentLoad(resize: boolean = false) {
    const windowInnerWidth = window.innerWidth;
    const windowInnerHeight = window.innerHeight;
    const headerHeight = document.getElementById('appHeader').offsetHeight;
    const footerHeight = document.getElementById('appFooter').offsetHeight;
    const gmapHeaderHeight = document.getElementById('gmap-header').offsetHeight;
    this.windowResizeData = {
      headerHeight: headerHeight,
      footerHeight: footerHeight,
      windowInnerWidth: windowInnerWidth,
      windowInnerHeight: windowInnerHeight,
      gmapHeaderHeight: gmapHeaderHeight,
      resize: resize
    };
  }

  drawingmodeChanged(shape: newShape) {
    this.selectedShape = shape;
  }


}
