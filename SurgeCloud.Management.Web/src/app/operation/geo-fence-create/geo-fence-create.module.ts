import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeofenceMapViewComponent } from './geofence-map-view/geofence-map-view.component';
import { GeoFenceCreateComponent } from './geo-fence-create.component';
import { GeoFenceCreateService } from './geo-fence-create.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '../../core';
import { GeofenceTableViewComponent } from './geofence-table-view/geofence-table-view.component';
import { DataTableModule } from 'angular2-datatable';
import { AppCommonModule } from '../../app-common';
import { ColorPickerModule } from 'angular4-color-picker';
import { ModalModule } from 'ngx-bootstrap';
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { SharedModule } from '../../shared-module';
import { GeoFenceCreateRoutingModule } from "./geo-fence-create-routing.module";
import {TranslateModule,TranslateLoader,TranslateCompiler,TranslateParser,MissingTranslationHandler} from "@ngx-translate/core";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {HttpLoaderFactory} from "../../app.module";


@NgModule({
  imports: [
    GeoFenceCreateRoutingModule,
    CommonModule,
    CoreModule,
    DataTableModule,
    AppCommonModule,
    FormsModule,
    ReactiveFormsModule,
    ColorPickerModule,
    SharedModule,
    AngularMultiSelectModule,
    ModalModule.forRoot(),
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    GeofenceMapViewComponent,
    GeoFenceCreateComponent, GeofenceTableViewComponent
  ],
  providers: [
    GeoFenceCreateService
  ]
})
export class GeoFenceCreateModule { }
