import { TestBed, inject } from '@angular/core/testing';

import { GeoFenceCreateService } from './geo-fence-create.service';

describe('GeoFenceCreateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GeoFenceCreateService]
    });
  });

  it('should ...', inject([GeoFenceCreateService], (service: GeoFenceCreateService) => {
    expect(service).toBeTruthy();
  }));
});
