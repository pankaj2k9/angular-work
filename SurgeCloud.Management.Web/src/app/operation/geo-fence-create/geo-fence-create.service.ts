import { Injectable } from '@angular/core';
import { HttpService } from '../../shared-module';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { GeoFenceAsset } from '../../app-common';

@Injectable()
export class GeoFenceCreateService {
  public filterApplied: Subject<Array<GeoFenceAsset>> = new Subject();
  public minMaxApplied: Subject<{ fullscreen, minimize }> = new Subject();
  public drawShape: Subject<any> = new Subject();
  public selectedShap: Subject<any> = new Subject();
  public circleRadius: Subject<any> = new Subject();
  public drawOptionsPointerEvent: Subject<any> = new Subject();
  public polygonBoundChange: Subject<any> = new Subject();
  public onChangeColorEvent: Subject<any> = new Subject();
  public renderShapeOnMap: Subject<any> = new Subject();
  public clearShapeOnMap: Subject<any> = new Subject();
  public geoFenceAvailable: Subject<any> = new Subject();
  public geoFilter: Subject<any> = new Subject();

  constructor(private httpService: HttpService) { }

  getAsset(companyId: string): Observable<any> {
    return this.httpService.get(`device/api/iot/Get/${companyId}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getGeofence(companyId) {
    return this.httpService.get(`device/api/iot/GetGeofences/${companyId}`)
      .map(res => {
        return res.json();
      })
      .catch(err => Observable.throw(err));
  }

  createGeofence(data) {
    return this.httpService.post(`device/api/Iot/CreateGeofence`, data)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  upateGeofence(data) {
    return this.httpService.post(`device/api/Iot/UpdateGeofence`, data)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  deleteGeofence(data) {
    return this.httpService.post(`device/api/Iot/DeleteGeofence`, data)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getAssetType(): Observable<any> {
    return this.httpService.get('device/api/iot/getassettype')
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  setMapData(data: GeoFenceAsset[]) {
    this.filterApplied.next(data);
  }

  getAlarmData(companyId: string, gwSno: string, assetId: string, startDate: string, endDate: string) {
    return this.httpService.get(`device/api/iot/GetAlarmData?companyId=${companyId}&gwSno=${gwSno}&assetId=${assetId}&startDate=${startDate}&endDate=${endDate}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  applyFullscreen(data) {
    this.minMaxApplied.next(data);
  }

  drawOrRemoveShape(data) {
    this.drawShape.next(data);
  }

  setSelectedShap(data) {
    this.selectedShap.next(data);
  }

  setCircleRadius(data) {
    this.circleRadius.next(data);
  }


  setDrawOptionsPointerEvent(data) {
    this.drawOptionsPointerEvent.next(data);
  }

  onChangeColor(data) {
    this.onChangeColorEvent.next(data);
  }
}
