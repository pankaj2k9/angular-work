import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeofenceMapViewComponent } from './geofence-map-view.component';

describe('GeofenceMapViewComponent', () => {
  let component: GeofenceMapViewComponent;
  let fixture: ComponentFixture<GeofenceMapViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeofenceMapViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeofenceMapViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
