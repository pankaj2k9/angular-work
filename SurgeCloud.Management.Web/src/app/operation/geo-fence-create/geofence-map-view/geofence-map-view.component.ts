import { Component, OnInit, AfterViewInit, OnDestroy, Input, Output, EventEmitter, ElementRef, NgZone } from '@angular/core';
import { Shape, newShape } from '../geonfence-create';
import { } from '../ass';
import { AppService } from '../../../app.service';
import { GeoFenceCreateService } from '../geo-fence-create.service';
import { MapsAPILoader, GoogleMapsAPIWrapper, CircleManager, AgmMarker, AgmMap, AgmCircle } from '@agm/core';
import * as jQuery from 'jquery';
import {TranslateService} from "@ngx-translate/core";
declare const google: any;

@Component({
  selector: 'app-geofence-map-view',
  templateUrl: './geofence-map-view.component.html',
  styleUrls: ['./geofence-map-view.component.scss']
})
export class GeofenceMapViewComponent implements OnInit, OnDestroy {

  public selectedShape: any;
  public fitBounds: [{ lat: number, lng: number }] = null;
  public zoom = 19;
  public geoFenceData = [];
  public mapTypeControlOptions: any;
  public zoomControlOptions: any;
  public drawingManager: any;
  public map: any;
  public newShape: any;
  public totalCircle = [];
  public totalPolygon = [];
  public mapData = [];
  public totalGeoCoordinates = [];
  public placeholder: string;
  public renderShapeOnMapSubScription;
  public svgIcons: string[] = [
    'assets/icons/common/clear.svg#clear',
    'assets/icons/common/save.svg#save'
  ];
  public categoryList = [];
  public colorList = [];
  public shapeList = [];
  public statusList = [];
  public geoFilter = {
    'category': '',
    'color': '',
    'shape': '',
    'status': ''
  }

  public minimize: any = {
    map: false,
    list: false
  };
  public fullscreen: any = {
    map: false,
    list: false,
  };
  @Output() drawingmodeChanged = new EventEmitter<newShape>();
  public mainContainerHeight: number;

  constructor(public geoFenceCreateService: GeoFenceCreateService,
    private mapsAPILoader: MapsAPILoader,
    private _zone: NgZone,
    private elementRef: ElementRef,
    private appService: AppService,
    private translate: TranslateService
  ) {
    this.geoFenceCreateService.geoFenceAvailable.subscribe(data => {
      this.geoFenceData = data;
      this.setFilterData();
    });
    this.mapsAPILoader.load().then(() => {
      this.loadDataAfterGooleApiLoaded();
    });
  }

  ngOnInit() {
    this.circleRadiusSubscriber();
    this.setAutoCompleteLocation();
    this.geoFenceCreateService.clearShapeOnMap.subscribe(data => {
      this.totalCircle.forEach(circle => {
        circle.setMap(null);
      });
      this.totalPolygon.forEach(polygon => {
        polygon.setMap(null);
      });
      this.totalCircle = [];
      this.totalPolygon = [];
    });
    jQuery('agm-map').height(document.getElementsByTagName('body')[0].clientHeight - jQuery('#appHeader').height() - jQuery('footer').height() - 28);
    this.translate.stream("geo_location").subscribe(res => {
      if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
        this.placeholder = res;

      }
    });
  }

  ngOnDestroy() {
    google.maps.event.clearListeners(this.map, 'bounds_changed');
    google.maps.event.clearListeners(this.map, 'overlaycomplete');
    google.maps.event.clearListeners(this.map, 'drawingmode_changed');
    google.maps.event.clearListeners(this.map, 'click');
    this.renderShapeOnMapSubScription.unsubscribe();
  }

  loadDataAfterGooleApiLoaded() {
    this.geoFenceCreateService.drawShape.subscribe(
      (data) => {
        this.editableItemOnMap = data;
        if (this.selectedShape) {
          this.selectedShape.setMap(null);
        }
        if (data.type === 'circle') {
          this.selectedShape = this.setSelectedShapeAsCircle(data);
          this.selectedShape.type = 'circle';
          this.setCircleToMapCenter(data['path'][0].latitude, data['path'][0].longitude);
          google.maps.event.clearListeners(this.selectedShape, 'dragend');
          google.maps.event.addListener(this.selectedShape, 'dragend', () => {
            this.setCircleToMapCenter(this.selectedShape.getCenter().lat(), this.selectedShape.getCenter().lng());
          });
        } else if (data.type === 'polygon') {
          this.selectedShape = this.setSelectedShapeAsPolygon(data);
          this.selectedShape.type = 'polygon';
          this.selectedShape.setMap(this.map);
          data['path'].forEach(cor => {
            this.totalGeoCoordinates.push({
              'latitude': cor.latitude,
              'longitude': cor.longitude
            });
          });
          google.maps.event.addListener(this.selectedShape.getPath(), 'set_at', (index, obj) => {
            this.polygonBoundsChangeHandler(this.selectedShape);
          });

          google.maps.event.addListener(this.selectedShape.getPath(), 'insert_at', (index, obj) => {
            this.polygonBoundsChangeHandler(this.selectedShape);
          });
          this.setBoundsOnMap();
        }
        this.totalGeoCoordinates = [];
        this.geoFenceCreateService.clearShapeOnMap.next();
        this.selectedShape.setEditable(true);
        this.selectedShape.setOptions({ 'draggable': true });
        google.maps.event.clearListeners(this.selectedShape, 'click');
        google.maps.event.addListener(this.selectedShape, 'click', () => {
          this.selectedShape.setEditable(true);
        });
        google.maps.event.clearListeners(this.selectedShape, 'bounds_changed');
        google.maps.event.addListener(this.selectedShape, 'bounds_changed', () => {
          this.saveSelectedShape();
        });
      }
    );
    this.renderShapeOnMapSubScription = this.geoFenceCreateService.renderShapeOnMap.subscribe(data => {
      this.geoFenceData = data;
      this.renderShapeOnMap(this.geoFenceData);
      this.changeFilter();
    });

    this.onChangeColorEventSubscriber();
  }

  triggerResize() {
    if (google) {
      google.maps.event.trigger(this.map, 'resize');
    }
  }

  setCircleToMapCenter(latitude, longitude) {
    this.selectedShape.setMap(this.map);
    this.selectedShape.setEditable(true);
    this.selectedShape.setOptions({ 'draggable': true });
    const marker = new google.maps.Marker({
      map: this.map,
      position: new google.maps.LatLng(latitude, longitude)
    });
    this.selectedShape.bindTo('center', marker, 'position');
    this.map.fitBounds(this.selectedShape.getBounds());
    marker.setMap(null);
  }

  setFilterData() {
    this.categoryList = [];
    this.colorList = [];
    this.shapeList = [];
    this.statusList = [];
    this.geoFenceData.forEach((data) => {
      !this.categoryList.includes(data['category']) ? this.categoryList.push(data['category']) : null;
      !this.colorList.includes(data['color']) ? this.colorList.push(data['color']) : null;
      !this.shapeList.includes(data['shape']) ? this.shapeList.push(data['shape']) : null;
      !this.statusList.includes(data['status'] === 0 ? 'Inactive' : 'Active') ? this.statusList.push(data['status'] === 0 ? 'Inactive' : 'Active') : null;
    });
  }

  changeFilter() {
    this.geoFenceCreateService.clearShapeOnMap.next();
    let data = this.geoFenceData.filter((element) => {
      let status = this.geoFilter.status ? this.geoFilter.status === 'Active' ? 1 : 0 : element.status;
      return ((this.geoFilter.category === '' || this.geoFilter.category === element.category) &&
        (this.geoFilter.color === '' || this.geoFilter.color === element.color) &&
        (this.geoFilter.shape === '' || this.geoFilter.shape === element.shape) &&
        (this.geoFilter.status === '' || status === element.status))
    });
    if (data.length > 0) {
      this.renderShapeOnMap(data);
    } else {
      const position = new google.maps.LatLng(this.appService.defaultLat, this.appService.defaultLng);
      this.map.setZoom(2);
      this.map.panTo(position);
    }
    this.geoFenceCreateService.geoFilter.next(this.geoFilter);
  }

  changeMinMax() {
    this.geoFenceCreateService.applyFullscreen({ fullscreen: this.fullscreen, minimize: this.minimize });
    setTimeout(
      () => {
        window.dispatchEvent(new Event('resize'));
        if (this.mapData.length) {
          const bounds = new google.maps.LatLngBounds();
          this.mapData.forEach((ast) => {
            if (this.isFloat(ast.latitude) && this.isFloat(ast.longitude)) {
              bounds.extend({ lat: ast.latitude, lng: ast.longitude });
            }
          });
          this.map.setCenter(bounds.getCenter());
          this.map.fitBounds(bounds);
        }
      }, 1
    );

  }

  renderShapeOnMap(data) {
    this.geoFenceCreateService.clearShapeOnMap.next();
    this.totalGeoCoordinates = [];
    if (this.selectedShape) {
      this.selectedShape.setMap(null);
      this.clearSelection();
    }
    data.forEach((element, index) => {
      if (element.shape === 'circle') {
        let obj = {
          'color': element.color,
          'isForDraw': true,
          'path': element.geoCoordinates,
          'radius': element.radius,
          'type': element.shape
        }
        this.totalCircle.push(this.setSelectedShapeAsCircle(obj));
        if (this.totalCircle[this.totalCircle.length - 1]) {
          this.totalCircle[this.totalCircle.length - 1].setMap(this.map);
        }
      } else if (element.shape === 'polygon') {
        let obj = {
          'color': element.color,
          'isForDraw': true,
          'path': element.geoCoordinates,
          'radius': element.radius,
          'type': element.shape
        }
        this.totalPolygon.push(this.setSelectedShapeAsPolygon(obj));
        if (this.totalPolygon[this.totalPolygon.length - 1])
          this.totalPolygon[this.totalPolygon.length - 1].setMap(this.map);
      }
      element.geoCoordinates.forEach(cor => {
        this.totalGeoCoordinates.push({
          'latitude': cor.latitude,
          'longitude': cor.longitude
        });
      });
    });
    if (this.totalCircle.length !== 0 && this.totalPolygon.length === 0) {
      let bounds = new google.maps.LatLngBounds();
      this.totalCircle.forEach(circle => {
        bounds.union(circle.getBounds());
      });
      this.map.fitBounds(bounds);
    } else if (this.totalGeoCoordinates.length === 0) {
      const bounds = new google.maps.LatLngBounds();
      bounds.extend({ lat: this.appService.defaultLat, lng: this.appService.defaultLng });
      this.map.setCenter(bounds.getCenter());
      this.map.fitBounds(bounds);
      this.map.setZoom(2);
    } else {
      this.setBoundsOnMap();
    }
  }

  setBoundsOnMap() {
    const bounds = new google.maps.LatLngBounds();
    this.totalGeoCoordinates.forEach(ast => {
      if (this.isFloat(ast.latitude) && this.isFloat(ast.longitude)) {
        bounds.extend({ lat: ast.latitude, lng: ast.longitude });
      }
      this.mapData.push(ast);
    });
    this.map.setCenter(bounds.getCenter());
    this.map.fitBounds(bounds);
  }

  isFloat(n) {
    return Number(n) === n && n % 1 !== 0;
  }

  setSelectedShapeAsCircle(data) {
    if (typeof google === 'object' && typeof google.maps === 'object') {
      return new google.maps.Circle({
        strokeColor: data.color ? data.color : this.selectedColor,
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: data.color ? data.color : this.selectedColor,
        fillOpacity: 0.35,
        map: this.map,
        center: { lat: data.path[0].latitude, lng: data.path[0].longitude },
        radius: data.radius
      });
    }
  }

  setSelectedShapeAsPolygon(data) {
    if (typeof google === 'object' && typeof google.maps === 'object') {
      let latlng = [];
      data.path.forEach(ltlng => {
        latlng.push({ lat: ltlng.latitude, lng: ltlng.longitude });
      });
      return new google.maps.Polygon({
        path: latlng,
        geodesic: true,
        strokeColor: data.color ? data.color : this.selectedColor,
        strokeOpacity: 1.0,
        strokeWeight: 2,
        fillColor: data.color ? data.color : this.selectedColor,
        fillOpacity: 0.35
      });
    }
  }

  circleRadiusSubscriber() {
    this.geoFenceCreateService.circleRadius.subscribe(
      (data) => {
        if (!data && this.selectedShape) {
          this.selectedShape.setMap(null);
        } else if (this.selectedShape) {
          this.selectedShape.setRadius(Number(data));
          if (this.editableItemOnMap['path'])
            this.setCircleToMapCenter(this.editableItemOnMap['path'][0].latitude, this.editableItemOnMap['path'][0].longitude);
        }
      }
    );
  }

  public editableItemOnMap = {};
  public selectedColor = '#FF0000';
  onChangeColorEventSubscriber() {
    this.geoFenceCreateService.onChangeColorEvent.subscribe(
      (data) => {
        this.selectedColor = data.color;
        this.polyOptions['fillColor'] = data.color;
        this.drawingManager.setOptions({
          rectangleOptions: this.polyOptions,
          circleOptions: this.polyOptions,
          polygonOptions: this.polyOptions,
        });
        if (this.selectedShape) {
          if (this.selectedShape.type === 'circle') {
            this.selectedShape.setMap(null);
            this.selectedShape = null;
            this.selectedShape = this.setSelectedShapeAsCircle(data);
            this.selectedShape.type = 'circle';
            this.selectedShape.setMap(this.map);
            this.setCircleToMapCenter(data['path'][0].latitude, data['path'][0].longitude);
          } else if (this.selectedShape.type === 'polygon') {
            this.selectedShape.setMap(null);
            this.selectedShape = null;
            this.selectedShape = this.setSelectedShapeAsPolygon(data);
            this.selectedShape.type = 'polygon';
            this.selectedShape.setMap(this.map);
          }
          this.totalGeoCoordinates = [];
        }
      }
    );
  }

  setAutoCompleteLocation() {
    this.mapsAPILoader.load().then(() => {
      const autocomplete: any = new google.maps.places.Autocomplete(this.elementRef.nativeElement.querySelector
        ('input[name=addressName]'), {
          types: ['geocode']
        });
      autocomplete.addListener('place_changed', () => {
        this._zone.run(() => {
          const place: any = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.map.fitBounds(place.geometry.viewport);
        });
      });
    });
  }

  mapRendered($event) {
    this.map = $event;
    const mapZoomOptions = { minZoom: 5 };
    this.map.setOptions(mapZoomOptions);
    this.initDrawingTools();
    this.mapTypeControlOptions = {
      style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
      mapTypeIds: ['roadmap', 'hybrid'],
      position: google.maps.ControlPosition.BOTTOM_LEFT
    }
    this.zoomControlOptions = {
      style: google.maps.ZoomControlStyle.DEFAULT,
      position: google.maps.ControlPosition.RIGHT_CENTER
    };
  }

  zoomChange(level: number) {
    this.zoom = level;
  }

  setDrawOptionsEvent(data) {
    setTimeout(() => {
      jQuery('.gmnoprint').css('pointer-events', data);
    });
  }

  public polyOptions = {};
  initDrawingTools() {
    this.polyOptions = {
      strokeWeight: 0,
      fillOpacity: 0.45,
      editable: true,
      draggable: true,
      fillColor: 'red'
    };
    if (this.drawingManager)
      this.drawingManager.setMap(null);
    this.setDrawingManagerConfig();
    this.drawingEventListener();
  }

  setDrawingManagerConfig() {
    this.drawingManager = new google.maps.drawing.DrawingManager();
    this.setDrawingManagerOptions();
    this.drawingManager.setMap(this.map);
  }

  setDrawingManagerOptions() {
    this.drawingManager.setOptions({
      drawingMode: null,
      drawingControl: true,
      drawingControlOptions: {
        position: google.maps.ControlPosition.RIGHT_BOTTOM,
        drawingModes: ['circle', 'polygon', 'polyline', 'rectangle'],
      },
      markerOptions: {
        draggable: true
      },
      polylineOptions: {
        editable: true,
        draggable: true
      },
      rectangleOptions: this.polyOptions,
      circleOptions: this.polyOptions,
      polygonOptions: this.polyOptions,
      map: this.map
    });
  }

  drawingEventListener() {
    this.addOverlayComplateListener();
    this.drawingmodeChangedListener();
    google.maps.event.addListener(this.map, 'click', (event) => {
      if (this.selectedShape) {
        if (this.selectedShape.type !== 'marker') {
          this.selectedShape.setEditable(false);
        }
        // this.selectedShape = null;
      } else {
      }
    });
    // google.maps.event.addListenerOnce(this.map, 'tilesloaded', () => {
    // this.setDrawOptionsEvent('none');
    // });
  }

  addOverlayComplateListener() {
    google.maps.event.addListener(this.drawingManager, 'overlaycomplete', (e) => {
      const newShape = e.overlay;
      newShape.type = e.type;
      if (e.type !== google.maps.drawing.OverlayType.MARKER) {
        // Switch back to non-drawing mode after drawing a shape.
        this.drawingManager.setDrawingMode(null);
        // Add an event listener that selects the newly-drawn shape when the user
        // mouses down on it.
        this.setClickEventToShape(newShape);
        this.setSelection(newShape);
        this.initListeners(newShape);
        this.saveSelectedShape();
        if (e.type === 'circle') {
          this.setCircleToMapCenter(newShape.getCenter().lat(), newShape.getCenter().lng());
        } else if (e.type === 'polygon') {
          let polygonArray = e.overlay.getPath().getArray();
          this.totalGeoCoordinates = [];
          polygonArray.forEach(polygon => {
            this.totalGeoCoordinates.push({
              'latitude': polygon.lat(),
              'longitude': polygon.lng()
            })
          });
          this.setBoundsOnMap();
          google.maps.event.addListener(newShape.getPath(), 'set_at', (index, obj) => {
            this.polygonBoundsChangeHandler(newShape);
          });

          google.maps.event.addListener(newShape.getPath(), 'insert_at', (index, obj) => {
            this.polygonBoundsChangeHandler(newShape);
          });
        }
      } else {
        google.maps.event.addListener(newShape, 'click', () => {
          this.setSelection(newShape);
        });
        this.setSelection(newShape);
      }
    });
  }

  setClickEventToShape(newShape) {
    google.maps.event.addListener(newShape, 'click', (e) => {
      if (e.vertex !== undefined) {
        if (newShape.type === google.maps.drawing.OverlayType.POLYGON) {
          const path = newShape.getPaths().getAt(e.path);
          path.removeAt(e.vertex);
          if (path.length < 3) {
            newShape.setMap(null);
          }
        }
        if (newShape.type === google.maps.drawing.OverlayType.POLYLINE) {
          const path = newShape.getPath();
          path.removeAt(e.vertex);
          if (path.length < 2) {
            newShape.setMap(null);
          }
        }
      }
      this.setSelection(newShape);
    });
  }
  polygonBoundsChangeHandler(newShape) {
    let polygonBounds = newShape.getPath();
    let boundsArray = [];
    polygonBounds.forEach((element, index) => {
      boundsArray.push({ 'latitude': polygonBounds.getAt(index).lat(), 'longitude': polygonBounds.getAt(index).lng() });
    });
    this.newShape = {
      type: 'polygon',
      radius: null,
      path: boundsArray
    }
    this.geoFenceCreateService.polygonBoundChange.next(this.newShape);
  }

  drawingmodeChangedListener() {
    google.maps.event.addListener(this.drawingManager, 'drawingmode_changed', () => {
      if (this.selectedShape) {
        if (this.selectedShape.type !== 'marker') {
          // this.selectedShape.setEditable(false);
          this.selectedShape.setMap(null);
        }
        this.selectedShape = null;
      }
      if (this.drawingManager && this.drawingManager.drawingMode) {
        this.geoFenceCreateService.setSelectedShap(this.drawingManager.drawingMode);
      }
    });
  }

  setSelection(shape) {
    if (shape.type !== 'marker') {
      this.clearSelection();
      shape.setEditable(true);
      // this.selectColor(shape.get('fillColor') || shape.get('strokeColor'));
    }
    this.selectedShape = shape;
  }

  clearSelection() {
    if (this.selectedShape) {
      if (this.selectedShape.type !== 'marker') {
        this.selectedShape.setEditable(false);
      }
      this.selectedShape = null;
    }
  }

  deleteSelectedShape() {
    if (this.selectedShape) {
      this.selectedShape.setMap(null);
      this.drawingmodeChanged.emit({ type: '', radius: '', path: [], isDeleted: true });
    }
  }

  saveSelectedShape() {
    if (this.selectedShape) {
      switch (this.selectedShape.type) {
        case Shape.CIRCLE:
          this.newShape = {
            type: this.selectedShape.type,
            radius: this.selectedShape.getRadius(),
            path: [{ latitude: this.selectedShape.getCenter().lat(), longitude: this.selectedShape.getCenter().lng() }]
          }
          this.editableItemOnMap = this.newShape;
          break;
        case Shape.POLYGON:
          this.newShape = {
            type: this.selectedShape.type,
            radius: null,
            path: this.getLatLngFromPath(this.selectedShape.getPaths().getArray()[0])
          }
          break;
        case Shape.POLYLINE:
          this.newShape = {
            type: this.selectedShape.type,
            radius: null,
            path: this.getLatLngFromPath(this.selectedShape.getPath())
          }
          break;
        case Shape.RECTANGLE:
          this.newShape = {
            type: this.selectedShape.type,
            radius: null,
            path: this.selectedShape.getBounds().toJSON()
          }
          break;
      }
      this.drawingmodeChanged.emit(this.newShape);
    }
  }

  getLatLngFromPath(paths: any) {
    const latlng: any = [];
    paths.forEach((path, key) => {
      latlng.push({ latitude: path.lat(), longitude: path.lng() });
    });
    return latlng;
  }

  initListeners(shape) {
    if (this.selectedShape) {
      this.initForCircle(this.selectedShape);
    }
  }

  initForCircle(shape) {
    google.maps.event.addListener(shape, 'bounds_changed', () => {
      this.saveSelectedShape();
    });
    google.maps.event.addListener(shape, 'dragend', () => {
      if (shape)
        this.setCircleToMapCenter(shape.getCenter().lat(), shape.getCenter().lng());
    });
  }

  setPositionToAutoComplate(prm) {
    if (prm === 'in') {
      jQuery('.pac-container').addClass('geo-fencing-create-pac-container');
    } else {
      jQuery('.pac-container').removeClass('geo-fencing-create-pac-container');
    }
  }
}
