import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeofenceTableViewComponent } from './geofence-table-view.component';

describe('GeofenceTableViewComponent', () => {
  let component: GeofenceTableViewComponent;
  let fixture: ComponentFixture<GeofenceTableViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeofenceTableViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeofenceTableViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
