import { Component, OnInit, SimpleChange, Input, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { Shape, newShape } from 'app/operation/geo-fence-create/geonfence-create';
import { ModalDirective } from 'ngx-bootstrap';
import { GeoFenceCreateService } from '../geo-fence-create.service';
import { AppService } from '../../../app.service';
import { AdalService } from '../../../shared-module';
import { GeofenceCreate } from '../geonfence-create';
import { ConvertToLocalDate } from '../../../shared-module/pipes/date-conversion';
import * as jQuery from 'jquery';
import * as _ from 'underscore';
import * as moment from 'moment';
import {TranslateService} from "@ngx-translate/core";


@Component({
  selector: 'app-geofence-table-view',
  templateUrl: './geofence-table-view.component.html',
  styleUrls: ['./geofence-table-view.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GeofenceTableViewComponent implements OnInit {
  @ViewChild('confirmModal') public confirmModal: ModalDirective;
  geofenceForm: FormGroup;
  public minInnerHeight = 0;
  public toggleFilterSearch: boolean = false;
  public selectedExcludedDays = [];
  public isCreateGeofenceError: boolean = false;
  public isActiveAllDays: boolean = false;
  public listFooterPosition = 0;
  public rowsOnPage = 10;
  public sortBy = 'assetId';
  public sortOrder = 'asc';
  public selectedGeoFence = {};
  public steps = [];
  public geoFenceDataMessage = 'Loading...';
  public mainBodyHeight: any;
  public isDirtyDaySchedule: boolean = false;
  public paginationInfo = {
    sortOrder: 'asc',
    sortBy: 'dataoccured',
    numberRowPerPage: 15
  };
  private filterWidthData = [{
    'width': 20,
    'id': 'name',
    'prm': true
  },
  {
    'width': 20,
    'id': 'shape',
    'prm': true
  },
  {
    'width': 30,
    'id': 'createdOn',
    'prm': true
  },
  {
    'width': 15,
    'id': 'status',
    'prm': true
  },
  {
    'width': 15,
    'id': 'action',
    'prm': false
  }];
  public isLoading: boolean = false;
  public filterValues = {};
  public geoFenceAvailable: any[] = [];
  public copyGeoFenceAvailable: any[] = [];
  public isAddNew: Boolean = false;
  public newGeoFence: any = {};
  public availableAssets: { id: string, itemName: string }[] = [];

  public dropdownSettings = {
    singleSelection: false,
    text: "Select Assets",
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    enableSearchFilter: true,
    classes: "assets-multiselect"
  };


  public startTimeDropdownSettings = {
    singleSelection: true,
    text: "Start Time",
    enableSearchFilter: false,
    classes: "start-time-select time-single-select"
  };

  public endTimeDropdownSettings = {
    singleSelection: true,
    text: "End Time",
    enableSearchFilter: false,
    classes: "end-time-select time-single-select"
  };

  public newGeofence: GeofenceCreate = new GeofenceCreate();
  public startTimeList: any[] = [];
  public endTimeList: any[] = [];
  public selectedAssets: any[] = [];
  public availableShape: string[] = ['circle', 'polygon'];
  public availableHosts: string[] = ['edge'];
  public company: any = null;
  private endTimeDisableRange = {};
  private endTimeReset = false;
  private startTimeDisableRange = {};
  private startTimeReset = false;
  public placeholder: string;
  public fullscreen: any = {
    map: false,
    list: false,
  };
  public minimize: any = {
    map: false,
    list: false
  };

  @Input() shape: newShape;
  constructor(public geoFenceCreateService: GeoFenceCreateService,
    public appService: AppService,
    private adalService: AdalService,
    private fb: FormBuilder,private translate: TranslateService) {
    this.geoFenceCreateService.geoFilter.subscribe((filterData) => {
      this.geoFenceAvailable = this.copyGeoFenceAvailable.filter((data) => {
        let status = filterData['status'] === 'Active' ? 1 : 0;
        return (filterData['category'] === '' || filterData['category'] === data['category']) &&
          (filterData['color'] === '' || filterData['color'] === data['color']) &&
          (filterData['shape'] === '' || filterData['shape'] === data['shape']) &&
          (filterData['status'] === '' || status === data['status']);
      });
    });

  }

  ngOnInit() {
    this.startTimeList = this.getTimeRangeArray(0, 23);
    this.endTimeList = this.getTimeRangeArray(1, 24);
    let company: any = this.appService.getActiveCompany();
    this.steps[0] = true;
    if (company) {
      this.company = company;
      this.getGeofence(company.companyId, false);
      this.getAssets(company.companyId);
      this.newGeofence.companyId = company.companyId;
      this.newGeofence.companyName = company.companyName;
    }
    this.appService.activeCompanyInfo.subscribe(
      (company: any) => {
        this.company = company;
        this.getAssets(company.companyId);
        this.getGeofence(company.companyId, false);
      }
    );

    this.geoFenceCreateService.selectedShap.subscribe(
      (data: any) => {
        this.geofenceForm.controls['shape'].setValue(data === 'circle' ? data : 'polygon');
        if (data !== 'circle') {
          this.geofenceForm.controls['radius'].reset({ value: null, disabled: true })
        }
      }
    );
    this.setFormData();
    this.resetFilterData();
    this.polygonBoundChangeHandler();
    this.minInnerHeight = document.getElementsByTagName('body')[0].clientHeight - 108;
    this.accordingClick();
    this.translate.stream("generic_boundary").subscribe(res => {
      if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
        this.placeholder = res;

      }
    });
  }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    if (changes.shape && changes.shape.currentValue && (!changes.shape.previousValue || changes.shape.currentValue.radius !== changes.shape.previousValue.radius)) {
      this.shapeChanged(changes.shape.currentValue);
    }
    if (changes.shape && changes.shape.currentValue && changes.shape.currentValue['path']) {
      this.geofenceForm.controls['geoCoordinates'].setValue(JSON.stringify(changes.shape.currentValue['path']));
    }
  }

  accordingClick() {
    jQuery(document).ready(function () {
      jQuery("#accordion section h1").click(function (e) {
        jQuery(this).parents().siblings("section").addClass("ac_hidden");
        jQuery(this).parents("section").removeClass("ac_hidden");
        e.preventDefault();
      });
    });
  }

  polygonBoundChangeHandler() {
    this.geoFenceCreateService.polygonBoundChange.subscribe((data) => {
      if (data && data.path.length > 0) {
        this.geofenceForm.controls['geoCoordinates'].setValue(JSON.stringify(data['path']));
      }
    })
  }

  resetFilterData() {
    this.toggleFilterSearch = false;
    this.filterValues = {
      'name': '',
      'shape': '',
      'createdOn': '',
      'status': '',
      'action': ''
    };
  }

  onUnselectAll(event) {
    setTimeout(() => {
      this.selectedAssets = event;
      this.geofenceForm.controls['selectedAssets'].setValue(event);
    });
  }

  toggleAddForm(data) {
    this.isAddNew = true;
    this.isCreateGeofenceError = false;
    if (data) {
      this.selectedAssets = [];
      this.newGeofence = new GeofenceCreate();
      this.setFormData();
      jQuery('.geofence-info').css('height', (jQuery(document).height() - 147) + 'px');
    }
    this.appService.setAllOptionPagination('geoCreateFenceTable');
    this.geoFenceCreateService.setDrawOptionsPointerEvent('auto');
    this.geoFenceCreateService.clearShapeOnMap.next();
    this.selectedExcludedDays = [];
    this.steps = [];
    this.steps[0] = true;
    this.isActiveAllDays = false;
  }

  cancelGeofence() {
    this.isAddNew = false;
    jQuery('.geofence-info').css('height', '0px');
    this.appService.setAllOptionPagination('geoCreateFenceTable');
    this.setMaxHeightToGeoFenceTable();
    this.geoFenceCreateService.setDrawOptionsPointerEvent('none');
    this.onRadiusChange(null);
  }

  changeMinMax() {
    this.geoFenceCreateService.applyFullscreen({ fullscreen: this.fullscreen, minimize: this.minimize });
  }

  setToggleFilterSearch() {
    this.toggleFilterSearch = !this.toggleFilterSearch;
    this.setMaxHeightToGeoFenceTable();
  }

  setMaxHeightToGeoFenceTable() {
    this.mainBodyHeight = jQuery(document).height() - (this.geoFenceAvailable.length > 15 ? 215 : 142);
  }

  getDayObject(key, value, checked) {
    return new FormGroup({
      key: new FormControl(key),
      value: new FormControl(value),
      checked: new FormControl(checked)
    });
  }

  changeGeoFenceStatus(prm) {
    if (prm === 'active') {
      this.geofenceForm.controls['inActiveStatus'].setValue(false);
    } else {
      this.geofenceForm.controls['activeStatus'].setValue(false);
    }
  }


  setFormData() {
    this.geofenceForm = this.fb.group({
      'name': new FormControl(this.newGeofence.name, [Validators.required, Validators.minLength(2), Validators.maxLength(64), Validators.pattern("[a-zA-Z][a-zA-Z ]*([']?[-]?)[a-zA-Z ]*")]),
      'shape': new FormControl({ value: !this.newGeofence.shape ? 'polygon' : this.newGeofence.shape, disabled: true }),
      'host': new FormControl(this.newGeofence['host'] ? this.newGeofence['host'] : '', Validators.required),
      'activeStatus': [this.newGeofence.status],
      'inActiveStatus': [!this.newGeofence.status],
      'scheduleDayOfWeek': [!this.newGeofence.scheduleDayOfWeek ? [] : this.newGeofence.scheduleDayOfWeek.split(','), [Validators.required]],
      'radius': new FormControl({ value: this.newGeofence.radius, disabled: this.newGeofence.shape != 'circle' }, [Validators.required, Validators.min(1)]),
      'geoCoordinates': new FormControl({ value: JSON.stringify(this.newGeofence.geoCoordinates), disabled: true }),
      'category': [this.newGeofence.category, [Validators.required]],
      'selectedAssets': [this.selectedAssets, [Validators.required]],
      'color': [this.newGeofence.color ? this.newGeofence.color : '#FFF'],
      'daySchedules': new FormArray([
        this.getDayObject('Sunday', 'Sunday', this.newGeofence['daySchedule'] ? this.newGeofence['daySchedule'].includes('Sunday') : false),
        this.getDayObject('Monday', 'Monday', this.newGeofence['daySchedule'] ? this.newGeofence['daySchedule'].includes('Monday') : false),
        this.getDayObject('Tuesday', 'Tuesday', this.newGeofence['daySchedule'] ? this.newGeofence['daySchedule'].includes('Tuesday') : false),
        this.getDayObject('Wednesday', 'Wednesday', this.newGeofence['daySchedule'] ? this.newGeofence['daySchedule'].includes('Wednesday') : false),
        this.getDayObject('Thursday', 'Thursday', this.newGeofence['daySchedule'] ? this.newGeofence['daySchedule'].includes('Thursday') : false),
        this.getDayObject('Friday', 'Friday', this.newGeofence['daySchedule'] ? this.newGeofence['daySchedule'].includes('Friday') : false),
        this.getDayObject('Saturday', 'Saturday', this.newGeofence['daySchedule'] ? this.newGeofence['daySchedule'].includes('Saturday') : false),
      ]),
      'selectedDays': [!this.newGeofence['daySchedule'] ? [] : this.newGeofence['daySchedule']],
      'startTimeSchedule': new FormControl(this.newGeofence['startTimeSchedule'] ? this.checkTimeFormat(moment().format('MM/DD/YYYY'), this.newGeofence['startTimeSchedule']).time : '', Validators.required),
      'endTimeSchedule': new FormControl(this.newGeofence['endTimeSchedule'] ? this.checkTimeFormat(moment().format('MM/DD/YYYY'), this.newGeofence['endTimeSchedule']).time : '', Validators.required),
      'tags': [this.newGeofence.tags && this.newGeofence.tags.join(',')],
      'onMaxSpeed': new FormGroup({
        'isMaxSpeed': new FormControl(this.newGeofence.maxSpeedAlert ? true : false),
        'maxSpeedAlertDesc': new FormControl(this.newGeofence.maxSpeedAlertDesc),
        'maxSpeedAlertLimit': new FormControl(this.newGeofence.maxSpeedAlertLimit)
      }),
      'onMinSpeed': new FormGroup({
        'isMinSpeed': new FormControl(this.newGeofence.minSpeedAlert ? true : false),
        'minSpeedAlertDesc': new FormControl(this.newGeofence.minSpeedAlertDesc),
        'minSpeedAlertLimit': new FormControl(this.newGeofence.minSpeedAlertLimit)
      }),
      'onMinStayTime': new FormGroup({
        'isMinStayTime': new FormControl(this.newGeofence.minStayTimeAlert ? true : false),
        'minStayTimeAlertDesc': new FormControl(this.newGeofence.minStayTimeAlertDesc),
        'minStayTimeAlertSeconds': new FormControl(this.newGeofence.minStayTimeAlertSeconds)
      }),
      'onMaxTimeAlert': new FormGroup({
        'isMaxTimeAlert': new FormControl(this.newGeofence.maxStayTimeAlert ? true : false),
        'maxStayTimeAlertDesc': new FormControl(this.newGeofence.maxStayTimeAlertDesc),
        'maxStayTimeAlertSeconds': new FormControl(this.newGeofence.maxStayTimeAlertSeconds)
      }),
      'onEntranceAlert': new FormGroup({
        'isEntranceAlert': new FormControl(this.newGeofence.onEntranceAlert ? true : false),
        'onEntranceAlertBoundary': new FormControl(this.newGeofence.onEntranceAlertBoundary),
        'onEntranceAlertDelay': new FormControl(this.newGeofence.onEntranceAlertDelay),
        'onEntranceAlertDesc': new FormControl(this.newGeofence.onEntranceAlertDesc)
      }),
      'onExitAlert': new FormGroup({
        'isExitAlert': new FormControl(this.newGeofence.onExitAlert ? true : false),
        'onExitAlertBoundary': new FormControl(this.newGeofence.onExitAlertBoundary),
        'onExitAlertDelay': new FormControl(this.newGeofence.onExitAlertDelay),
        'onExitAlertDesc': new FormControl(this.newGeofence.onExitAlertDesc)
      }),
    });
    this.onStartTimeModelChange(this.geofenceForm.value['startTimeSchedule']);
    this.onEndTimeModelChange(this.geofenceForm.value['endTimeSchedule']);

    this.onCheckChange('onMaxSpeed', 'isMaxSpeed');
    this.onCheckChange('onMinSpeed', 'isMinSpeed');
    this.onCheckChange('onMinStayTime', 'isMinStayTime');
    this.onCheckChange('onMaxTimeAlert', 'isMaxTimeAlert');
    this.onCheckChange('onEntranceAlert', 'isEntranceAlert');
    this.onCheckChange('onExitAlert', 'isExitAlert');
  }

  editItem(item: GeofenceCreate) {
    jQuery('.geofence-info').css('height', (jQuery(document).height() - 147) + 'px');
    this.drawOrRemoveShape(item);
    this.selectedAssets = [];
    this.newGeofence = Object.assign({}, item);
    this.newGeofence['daySchedule'] = this.newGeofence.scheduleDayOfWeek && this.newGeofence.scheduleDayOfWeek.split(',');
    this.newGeofence['startTimeSchedule'] = moment(this.newGeofence.scheduleStartTime).format("hh:mm");
    this.newGeofence['endTimeSchedule'] = moment(this.newGeofence.scheduleEndTime).format("hh:mm");
    this.newGeofence.assets.forEach((ast) => {
      this.selectedAssets.push({ itemName: ast.assetId, id: ast.gwSNO });
    });
    this.isCreateGeofenceError = false;
    this.toggleAddForm(false);
    this.setFormData();
    this.steps = [];
    this.steps[0] = true;
  }

  onCheckChange(control, value) {
    let prm = this.geofenceForm.controls[control].value[value];
    if (control === 'onMaxSpeed') {
      this.geofenceForm.get(control).get('maxSpeedAlertDesc').setValidators(prm ? [Validators.required] : null);
      this.geofenceForm.get(control).get('maxSpeedAlertDesc').updateValueAndValidity();
      this.geofenceForm.get(control).get('maxSpeedAlertLimit').setValidators(prm ? [Validators.required, Validators.min(5), Validators.max(100)] : null);
      this.geofenceForm.get(control).get('maxSpeedAlertLimit').updateValueAndValidity();
    } else if (control === 'onMinSpeed') {
      this.geofenceForm.get(control).get('minSpeedAlertDesc').setValidators(prm ? [Validators.required] : null);
      this.geofenceForm.get(control).get('minSpeedAlertDesc').updateValueAndValidity();
      this.geofenceForm.get(control).get('minSpeedAlertLimit').setValidators(prm ? [Validators.required, Validators.min(5), Validators.max(100)] : null);
      this.geofenceForm.get(control).get('minSpeedAlertLimit').updateValueAndValidity();
    } else if (control === 'onMinStayTime') {
      this.geofenceForm.get(control).get('minStayTimeAlertDesc').setValidators(prm ? [Validators.required] : null);
      this.geofenceForm.get(control).get('minStayTimeAlertDesc').updateValueAndValidity();
      this.geofenceForm.get(control).get('minStayTimeAlertSeconds').setValidators(prm ? [Validators.required, Validators.max(120), Validators.min(5)] : null);
      this.geofenceForm.get(control).get('minStayTimeAlertSeconds').updateValueAndValidity();
    } else if (control === 'onMaxTimeAlert') {
      this.geofenceForm.get(control).get('maxStayTimeAlertDesc').setValidators(prm ? [Validators.required] : null);
      this.geofenceForm.get(control).get('maxStayTimeAlertDesc').updateValueAndValidity();
      this.geofenceForm.get(control).get('maxStayTimeAlertSeconds').setValidators(prm ? [Validators.required, Validators.max(120), Validators.min(5)] : null);
      this.geofenceForm.get(control).get('maxStayTimeAlertSeconds').updateValueAndValidity();
    } else if (control === 'onEntranceAlert') {
      this.geofenceForm.get(control).get('onEntranceAlertDesc').setValidators(prm ? [Validators.required] : null);
      this.geofenceForm.get(control).get('onEntranceAlertDesc').updateValueAndValidity();
      this.geofenceForm.get(control).get('onEntranceAlertDelay').setValidators(prm ? [Validators.required, Validators.min(0), Validators.max(120)] : null);
      this.geofenceForm.get(control).get('onEntranceAlertDelay').updateValueAndValidity();
      this.geofenceForm.get(control).get('onEntranceAlertBoundary').setValidators(prm ? [Validators.required, Validators.min(0), Validators.max(100)] : null);
      this.geofenceForm.get(control).get('onEntranceAlertBoundary').updateValueAndValidity();
    } else if (control === 'onExitAlert') {
      this.geofenceForm.get(control).get('onExitAlertDesc').setValidators(prm ? [Validators.required] : null);
      this.geofenceForm.get(control).get('onExitAlertDesc').updateValueAndValidity();
      this.geofenceForm.get(control).get('onExitAlertDelay').setValidators(prm ? [Validators.required, Validators.min(0), Validators.max(120)] : null);
      this.geofenceForm.get(control).get('onExitAlertDelay').updateValueAndValidity();
      this.geofenceForm.get(control).get('onExitAlertBoundary').setValidators(prm ? [Validators.required, Validators.min(0), Validators.max(100)] : null);
      this.geofenceForm.get(control).get('onExitAlertBoundary').updateValueAndValidity();
    }
  }

  onDaySelect(key, value) {
    if (value) {
      let data = this.geofenceForm.controls['scheduleDayOfWeek'].value;
      data.push(key);
    } else {
      let data = _.filter(this.geofenceForm.controls['scheduleDayOfWeek'].value, (element) => {
        return element !== key;
      });
      this.geofenceForm.controls['scheduleDayOfWeek'].setValue(data);
    }
    this.geofenceForm.get('scheduleDayOfWeek').updateValueAndValidity();
  }

  onColorChange(color) {
    let shape = {
      type: this.newGeofence.shape,
      path: JSON.parse(this.geofenceForm.controls['geoCoordinates'].value),
      radius: this.geofenceForm.get('radius').value,
      isForDraw: true,
      color: color
    };
    this.geofenceForm.get('color').setValue(color);
    this.geoFenceCreateService.onChangeColor(shape);
  }

  drawOrRemoveShape(item) {
    let shape = {
      type: item.shape,
      path: item.geoCoordinates,
      radius: item.radius,
      isForDraw: true,
      color: item.color
    };
    this.geoFenceCreateService.drawOrRemoveShape(shape);
  }

  toggleAllDays(event) {
    this.isActiveAllDays = event.target.checked;
    if (this.isActiveAllDays) {
      this.geofenceForm.controls['endTimeSchedule'].setValue('');
      this.geofenceForm.controls['startTimeSchedule'].setValue('');
      this.geofenceForm.get('endTimeSchedule').reset();
      this.geofenceForm.get('startTimeSchedule').reset();
    }
  }

  getGeofence(companyId: string, prm) {
    this.appService.loader.next(true);
    this.geoFenceCreateService.getGeofence(companyId)
      .finally(() => {
        this.appService.loader.next(false);
      })
      .subscribe(
        (response) => {
          if (response) {
            this.toggleFilterSearch = false;
            this.geoFenceDataMessage = 'No Data Found.';
            this.geoFenceAvailable = [];
            response.forEach(data => {
              let model = new GeofenceCreate(data);
              data.createdOn = data.createdOn;
              data.tCreatedOn = this.getUserTimeZoneDate(data.createdOn);
              this.geoFenceAvailable.push(data);
            });
            this.copyGeoFenceAvailable = this.geoFenceAvailable;
            this.geoFenceCreateService.geoFenceAvailable.next(this.geoFenceAvailable);
            this.setMaxHeightToGeoFenceTable();
            this.appService.setAllOptionPagination('geoCreateFenceTable');
            let interval = setInterval(() => {
              if (typeof google === 'object' && typeof google.maps === 'object') {
                this.geoFenceCreateService.renderShapeOnMap.next(response);
                clearInterval(interval);
              }
            }, 100);
            if (prm)
              this.cancelGeofence();
          }
        }
      );
  }

  getUserTimeZoneDate(value) {
    let convertedTimezone = '';
    if (this.appService.userTimezone != null) {
      convertedTimezone = moment.tz(value, this.appService.userTimezone).format('MM/DD/YYYY, hh:mm A');
    } else {
      convertedTimezone = moment.utc(value).local().format('MM/DD/YYYY, hh:mm A');
    }
    return convertedTimezone;
  }

  getAssets(companyId: string) {
    this.geoFenceCreateService.getAsset(companyId).subscribe(
      (res) => {
        res.forEach((asset: any, index: any) => {
          const latLangObject: object = { lat: null, lang: null };
          let model = { id: asset.gwSNO, itemName: asset.assetId }
          this.availableAssets.push(model);
        });
      }
    );
  }

  getTimeRangeArray(startPoint, endPoint) {
    const timeArray = [];
    for (let i = startPoint; i <= endPoint; i++) {
      let pointValue = i;
      if (i < 10) {
        pointValue = '0' + i;
      }
      pointValue = pointValue + ':00';
      const timeObject = {
        id: i,
        itemName: pointValue
      }
      timeArray.push(timeObject);
    }
    return timeArray;
  }

  saveGeofence() {
    if (this.geofenceForm.valid) {
      this.setGeoFenceSaveData();
      if (this.newGeofence.geoCoordinates.length === 0) {
        this.isCreateGeofenceError = true;
        return;
      }
      this.isLoading = true;
      if (this.newGeofence.geofenceId) {
        this.newGeofence.updatedBy = this.adalService.userInfo.profile.name;
        this.geoFenceCreateService.upateGeofence(this.newGeofence)
          .finally(() => {
            this.isLoading = false;
          })
          .subscribe(
            (res) => {
              this.updateGeoFenceData();
            }
          ), (error) => {
            this.appService.showErrorMessage('Error', 'Something wrong!');
          };
      } else {
        this.newGeofence.geofenceIdentifier = this.geoFenceAvailable.length + 1;
        this.newGeofence.createdBy = this.adalService.userInfo.profile.name;
        let request = _.omit(this.newGeofence, ['geofenceId', 'version', 'updatedOn', 'updatedBy'])
        this.geoFenceCreateService.createGeofence(request)
          .finally(() => {
            this.isLoading = false;
          })
          .subscribe(
            (res) => {
              this.updateGeoFenceData();
            }
          ), (error) => {
            this.appService.showErrorMessage('Error', 'Something wrong!');
          };
      }
    }
  }

  selectedDayNotifyChange(data) {
    this.isDirtyDaySchedule = true;
    this.geofenceForm.controls['scheduleDayOfWeek'].setValue(data);
  }

  updateGeoFenceData() {
    this.getGeofence(this.company.companyId, true);
    if (this.newGeofence.geofenceId) {
      this.appService.showSuccessMessage('Success', 'Geo fence updated successfully');
    } else {
      this.appService.showSuccessMessage('Success', 'Geo fence created successfully');
    }
  }

  checkTimeFormat(date, time) {
    let tempDate = moment();
    if (this.appService.userTimezone != null) {
      tempDate = moment.tz(date + ' ' + time, this.appService.userTimezone);
      tempDate = moment(tempDate).utcOffset(-tempDate.utcOffset());
    } else {
      tempDate = moment(date + ' ' + time).utc();
    }
    date = tempDate.format('MM/DD/YYYY');
    time = tempDate.format("HH:mm");
    if (tempDate.hours() === 0) {
      tempDate = tempDate.subtract(1, "days");
      date = tempDate.format('MM/DD/YYYY');
      time = '23:59';
    }
    return { 'date': date, 'time': time };
  }

  getShortNameOfDay(name) {
    if (name === 'Sunday') {
      return 'Sun';
    } else if (name === 'Monday') {
      return 'Mon';
    } else if (name === 'Tuesday') {
      return 'Tue';
    } else if (name === 'Wednesday') {
      return 'Wed';
    } else if (name === 'Thursday') {
      return 'Thu';
    } else if (name === 'Friday') {
      return 'Fri';
    } else if (name === 'Saturday') {
      return 'Sat';
    }
  }

  setGeoFenceSaveData() {
    let geoFenceData = this.geofenceForm.value;
    let assets = [];
    let daySchedule = [];
    let today = new Date();
    this.selectedAssets.forEach((item) => {
      assets.push({ assetId: item.itemName, gwSNO: item.id });
    });
    let days = [];
    this.selectedExcludedDays.forEach((date) => {
      days.push(this.checkTimeFormat(date, '00:00').date);
    });
    let scheduleDayOfWeek = [];
    console.log(geoFenceData.geoFenceData);
    geoFenceData.scheduleDayOfWeek.forEach(day => {
      scheduleDayOfWeek.push(this.getShortNameOfDay(day.text));
    });
    // { latitude: 40.588987, longitude: -74.608660 }
    let scheduleStartTime = this.checkTimeFormat(moment().format('MM/DD/YYYY'), `${geoFenceData.startTimeSchedule}:00`);
    let scheduleEndTime = this.checkTimeFormat(moment().format('MM/DD/YYYY'), `${geoFenceData.endTimeSchedule}:00`);
    this.newGeofence.companyId = this.company.companyId;
    this.newGeofence.companyName = this.company.name;
    this.newGeofence.assets = assets;
    this.newGeofence.host = 'edge';
    this.newGeofence.name = geoFenceData.name;
    this.newGeofence.shape = this.geofenceForm.controls['shape'].value;
    this.newGeofence.host = this.geofenceForm.controls['host'].value;
    this.newGeofence.color = geoFenceData.color;
    this.newGeofence.category = geoFenceData.category;
    this.newGeofence.radius = geoFenceData.radius ? geoFenceData.radius : '';
    this.newGeofence.status = geoFenceData.activeStatus ? 1 : 0;
    this.newGeofence.geoCoordinates = this.geofenceForm.controls['geoCoordinates'].value === '[]' ? [] : JSON.parse(this.geofenceForm.controls['geoCoordinates'].value);
    this.newGeofence.scheduleDayOfWeek = scheduleDayOfWeek.join(',');
    this.newGeofence.scheduleStartTime = scheduleStartTime.date + ' ' + scheduleStartTime.time;
    this.newGeofence.scheduleEndTime = scheduleEndTime.date + ' ' + scheduleEndTime.time;
    this.newGeofence.onEntranceAlert = geoFenceData.onEntranceAlert.isEntranceAlert ? 1 : 0;
    this.newGeofence.onEntranceAlertDesc = geoFenceData.onEntranceAlert.isEntranceAlert ? geoFenceData.onEntranceAlert.onEntranceAlertDesc : '';
    this.newGeofence.onEntranceAlertDelay = geoFenceData.onEntranceAlert.isEntranceAlert ? geoFenceData.onEntranceAlert.onEntranceAlertDelay : '';
    this.newGeofence.onEntranceAlertBoundary = geoFenceData.onEntranceAlert.isEntranceAlert ? geoFenceData.onEntranceAlert.onEntranceAlertBoundary : '';
    this.newGeofence.onExitAlert = geoFenceData.onExitAlert.isExitAlert ? 1 : 0;
    this.newGeofence.onExitAlertDesc = geoFenceData.onExitAlert.isExitAlert ? geoFenceData.onExitAlert.onExitAlertDesc : '';
    this.newGeofence.onExitAlertDelay = geoFenceData.onExitAlert.isExitAlert ? geoFenceData.onExitAlert.onExitAlertDelay : '';
    this.newGeofence.onExitAlertBoundary = geoFenceData.onExitAlert.isExitAlert ? geoFenceData.onExitAlert.onExitAlertBoundary : '';
    this.newGeofence.maxSpeedAlert = geoFenceData.onMaxSpeed.isMaxSpeed ? 1 : 0;
    this.newGeofence.maxSpeedAlertDesc = geoFenceData.onMaxSpeed.isMaxSpeed ? geoFenceData.onMaxSpeed.maxSpeedAlertDesc : '';
    this.newGeofence.maxSpeedAlertLimit = geoFenceData.onMaxSpeed.isMaxSpeed ? geoFenceData.onMaxSpeed.maxSpeedAlertLimit : '';
    this.newGeofence.minSpeedAlert = geoFenceData.onMinSpeed.isMinSpeed ? 1 : 0;
    this.newGeofence.minSpeedAlertDesc = geoFenceData.onMinSpeed.isMinSpeed ? geoFenceData.onMinSpeed.minSpeedAlertDesc : '';
    this.newGeofence.minSpeedAlertLimit = geoFenceData.onMinSpeed.isMinSpeed ? geoFenceData.onMinSpeed.minSpeedAlertLimit : '';
    this.newGeofence.minStayTimeAlert = geoFenceData.onMinStayTime.isMinStayTime ? 1 : 0;
    this.newGeofence.minStayTimeAlertDesc = geoFenceData.onMinStayTime.isMinStayTime ? geoFenceData.onMinStayTime.minStayTimeAlertDesc : '';
    this.newGeofence.minStayTimeAlertSeconds = geoFenceData.onMinStayTime.isMinStayTime ? geoFenceData.onMinStayTime.minStayTimeAlertSeconds : '';
    this.newGeofence.maxStayTimeAlert = geoFenceData.onMaxTimeAlert.isMaxTimeAlert ? 1 : 0;
    this.newGeofence.maxStayTimeAlertDesc = geoFenceData.onMaxTimeAlert.isMaxTimeAlert ? geoFenceData.onMaxTimeAlert.maxStayTimeAlertDesc : '';
    this.newGeofence.maxStayTimeAlertSeconds = geoFenceData.onMaxTimeAlert.isMaxTimeAlert ? geoFenceData.onMaxTimeAlert.maxStayTimeAlertSeconds : '';
    this.newGeofence.tags = geoFenceData.tags.split(',');
    this.newGeofence['excludedDays'] = days;
  }

  onRadiusChange(event) {
    if (!event) return;
    this.geoFenceCreateService.setCircleRadius(event);
  }

  updateData(event) {
    this.geoFenceAvailable = event;
    this.appService.setAllOptionPagination('geoCreateFenceTable');
  }

  shapeChanged(shapeNew) {
    if (!shapeNew.isDeleted) {
      if (shapeNew.type == "circle") {
        this.newGeofence.shape = "circle";
        this.geofenceForm.controls['radius'].reset({ value: shapeNew.radius, disabled: false });
      } else {
        this.newGeofence.shape = "polygon";
        this.newGeofence.radius = null;
      }
      if (this.geofenceForm.controls['color'].value === '#FFF') {
        this.geofenceForm.controls['color'].setValue('#FF0000');
      }
      this.geofenceForm.controls['geoCoordinates'].setValue(JSON.stringify(shapeNew.path));
    } else {
      this.newGeofence.shape = "";
      this.newGeofence.radius = null;
      this.geofenceForm.controls['geoCoordinates'].setValue(JSON.stringify([]));
    }
    this.isCreateGeofenceError = false;
  }

  deleteGeoFence(item: GeofenceCreate) {
    this.confirmModal.hide();
    this.appService.loader.next(true);
    this.geoFenceCreateService.deleteGeofence(this.selectedGeoFence)
      .finally(() => {
        this.appService.loader.next(false);
        this.selectedGeoFence = null;
      })
      .subscribe(
        (res) => {
          if (res) {
            this.geoFenceCreateService.clearShapeOnMap.next();
            this.getGeofence(this.company.companyId, false);
            this.appService.showSuccessMessage('Success', 'Geo fence deleted successfully');
            this.appService.setAllOptionPagination('geoCreateFenceTable');
          }
        }, (err) => {
          this.appService.showErrorMessage('Error', 'Something wrong!');
        }
      );
  }

  onStartTimeModelChange(value) {
    if (value) {
      value = value.split(':');
      this.endTimeDisableRange = {
        start: 0,
        end: Number(value[0])
      };
      this.endTimeReset = false;
    } else {
      this.endTimeReset = true;
    }
    this.endTimeList = this.getTimeRangeArray(1, 24);
  }

  onEndTimeModelChange(value) {
    if (value) {
      value = value.split(':');
      this.startTimeDisableRange = {
        start: Number(value[0]),
        end: 23
      };
      this.startTimeReset = false;
    } else {
      this.startTimeReset = true;
    }
    this.startTimeList = this.getTimeRangeArray(0, 23);
  }

  selectedExcludeDays(data) {
    this.selectedExcludedDays = data;
  }


}
