export class GeofenceCreate {
  public geofenceId: string;
  public geofenceIdentifier: number;
  public companyId: string;
  public companyName: string;
  public assets: { assetId: string, gwSNO: string }[];
  public version: string;
  public host: string;
  public name: string;
  public shape: string;
  public color: string;
  public category: string;
  public radius: string;
  public createdOn: string;
  public createdBy: string;
  public updatedBy: string;
  public status: number;
  public geoCoordinates: { latitude: string, longitude: string }[];
  public scheduleDayOfWeek: string;
  public scheduleStartTime: string;
  public scheduleEndTime: string;
  public onEntranceAlert: number;
  public onEntranceAlertDesc: string;
  public onEntranceAlertDelay: string;
  public onEntranceAlertBoundary: string;
  public onExitAlert: number;
  public onExitAlertDesc: string;
  public onExitAlertDelay: string;
  public onExitAlertBoundary: string;
  public maxSpeedAlert: number;
  public maxSpeedAlertDesc: string;
  public maxSpeedAlertLimit: string;
  public minSpeedAlert: number;
  public minSpeedAlertDesc: string;
  public minSpeedAlertLimit: string;
  public minStayTimeAlert: number;
  public minStayTimeAlertDesc: string;
  public minStayTimeAlertSeconds: string;
  public maxStayTimeAlert: number;
  public maxStayTimeAlertDesc: string;
  public maxStayTimeAlertSeconds: string;
  public tags: string[];
  constructor(data: any = {}) {
    this.geofenceId = data.geofenceId || null;
    this.geofenceIdentifier = data.geofenceIdentifier || null;
    this.companyId = data.companyId || '';
    this.companyName = data.companyName || '';
    this.assets = data.assets || [];
    this.version = data.version || '';
    this.host = data.host || '';
    this.name = data.name || '';
    this.shape = data.shape || '';
    this.color = data.color || '';
    this.category = data.category || '';
    this.radius = data.radius || '';
    this.createdOn = data.createdOn || '';
    this.createdBy = data.createdBy || '';
    this.updatedBy = data.updatedBy || '';
    this.status = data.status || false;
    this.geoCoordinates = data.geoCoordinates || [];
    this.scheduleDayOfWeek = data.scheduleDayOfWeek || '';
    this.scheduleStartTime = data.scheduleStartTime || '';
    this.scheduleEndTime = data.scheduleEndTime || '';
    this.onEntranceAlert = data.onEntranceAlert || 0;
    this.onEntranceAlertDesc = data.onEntranceAlertDesc || '';
    this.onEntranceAlertDelay = data.onEntranceAlertDelay || '';
    this.onEntranceAlertBoundary = data.onEntranceAlertBoundary || '';
    this.onExitAlert = data.onExitAlert || 0;
    this.onExitAlertDesc = data.onExitAlertDesc || '';
    this.onExitAlertDelay = data.onExitAlertDelay || '';
    this.onExitAlertBoundary = data.onExitAlertBoundary || '';
    this.maxSpeedAlert = data.maxSpeedAlert || '';
    this.maxSpeedAlertDesc = data.maxSpeedAlertDesc || '';
    this.maxSpeedAlertLimit = data.maxSpeedAlertLimit || '';
    this.minSpeedAlert = data.minSpeedAlert || '';
    this.minSpeedAlertDesc = data.minSpeedAlertDesc || '';
    this.minSpeedAlertLimit = data.minSpeedAlertLimit || '';
    this.minStayTimeAlert = data.minStayTimeAlert || '';
    this.minStayTimeAlertDesc = data.minStayTimeAlertDesc || '';
    this.minStayTimeAlertSeconds = data.minStayTimeAlertSeconds || '';
    this.maxStayTimeAlert = data.maxStayTimeAlert || '';
    this.maxStayTimeAlertDesc = data.maxStayTimeAlertDesc || '';
    this.maxStayTimeAlertSeconds = data.maxStayTimeAlertSeconds || '';
    this.tags = data.tags || [];
  }
}

export enum Shape {
  CIRCLE = 'circle',
  POLYLINE = 'polyline',
  POLYGON = 'polygon',
  RECTANGLE = 'rectangle'
}


export class newShape {
  public type: string;
  public path: any[];
  public radius: string;
  public isDeleted?: Boolean = false;
}