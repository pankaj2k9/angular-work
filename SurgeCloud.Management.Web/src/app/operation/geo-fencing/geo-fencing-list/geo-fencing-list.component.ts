import { Component, OnDestroy, OnInit, HostListener, AfterViewInit, Input, ViewChild, TemplateRef, SimpleChange, EventEmitter, Output } from '@angular/core';
import { GeoFenceAsset } from '../../../app-common';
import { BsMapService } from '../../../shared-module/services/bsmap/bsmap.service';
import { AppService } from '../../../../app';
import * as moment from 'moment';
import { OperationService } from '../../operation.service';
import * as _ from 'underscore';
import { GeofenceFilter, GeoData } from '../../operation';
import { GeoFencingService } from '../geo-fencing.service';
import { Router } from '@angular/router';
import * as jQuery from 'jquery';
import { Socket } from 'ng-socket-io';
import { MapsAPILoader } from '@agm/core';
import { Subject } from 'rxjs/Subject';
import {TranslateService} from "@ngx-translate/core";
declare const google: any;


@Component({
  selector: 'app-geo-fencing-list',
  templateUrl: './geo-fencing-list.component.html',
  styleUrls: ['./geo-fencing-list.component.scss']
})
export class GeoFencingListComponent implements OnInit, OnDestroy, AfterViewInit {

  public minInnerHeight = 0;
  listFooterPosition = 0;
  listContainerInlineStyle: object;

  public data: GeoFenceAsset[] = [];
  public bsMap: BsMapService = new BsMapService();
  public availableAsset: GeoFenceAsset[] = [];
  locationDataFromGeocode = [];
  public filterQuery = '';
  public rowsOnPage = 10;
  public sortBy = 'assetId';
  public sortOrder = 'asc';
  public dateRangeValue = [0, 0];
  public timeRangeValue = [0, 2];
  public colorCodeIndex: number = 1;
  public locationInterval: any;
  public columns = [
    {
      name: '',
      cssClass: '',
    },
    {
      name: 'Asset Id',
      cssClass: 'text-left',
      sort: 'assetId',
      width: '30%'
    },
    {
      name: 'Location',
      cssClass: 'text-left',
      width: '25%',
      sort: 'location'
    },
    {
      name: 'Last Updated',
      cssClass: 'text-center',
      sort: 'tLatestSyncDate',
      width: '30%'
    },
    {
      name: 'Actions',
      cssClass: 'text-center',
      sort: 'status',
      width: '15%'
    }
  ];

  public availableColumns: any[] = [
    {
      name: 'Asset Id',
      cssClass: 'text-left',
      sort: 'assetId',
      width: '30%'
    },
    {
      name: 'VIN',
      cssClass: 'text-left',
      sort: 'vin',
      width: '30%'
    },
    {
      name: 'Make',
      cssClass: 'text-left',
      sort: 'make',
      width: '30%'
    },
    {
      name: 'Model',
      cssClass: 'text-left',
      sort: 'model',
      width: '30%'
    },
    {
      name: 'Serial Number',
      cssClass: 'text-left',
      sort: 'serialNumber',
      width: '30%'
    }
  ];

  public statusClass: string[] = [
    'inactive',
    'stopped',
    'idle',
    'moving',
    'alarm'
  ];
  public event: any = {
    idle: 0,
    inactive: 0,
    stopped: 0,
    alarm: 0,
    moving: 0
  }

  public paginationInfo = {
    sortOrder: 'asc',
    sortBy: 'dataoccured',
    numberRowPerPage: 10
  };

  public availableGeoData: GeoData[] = [];
  public sliderDate: { startDate: string, endDate: string } = { startDate: '', endDate: '' };
  public sliderTime: { startTime: string, endTime: string } = { startTime: '', endTime: '' };

  private keysForSubscription = [];

  public fullscreen: any = {
    map: false,
    list: false,
  };
  public minimize: any = {
    map: false,
    list: false
  };
  public socketConnectPrm: any;
  @Input() filterBy: any;
  @Output() public dataChange: EventEmitter<GeoFenceAsset[]> = new EventEmitter();
  @Output() public geoData: EventEmitter<GeoData[]> = new EventEmitter();
  @Output() public realTime: EventEmitter<any> = new EventEmitter();
  public todayDate = this.geoFencingService.getTodayDateAsPerTimeZone();
  public tripStarted: Boolean = false;
  toggleExpandClick = false;
  public windowResizeData = {};
  private onDestroy$ = new Subject();
  @Output() displayMarkerInfo: EventEmitter<number> = new EventEmitter<number>();
  constructor(private operationService: OperationService, private appService: AppService,
    private geoFencingService: GeoFencingService, private router: Router, private socket: Socket,
    private _mapsAPILoader: MapsAPILoader,private translate: TranslateService) { }
  @HostListener('window:resize') onWindowResize() {
    this.setHeigthToTableBody();
    this.onPageWindowResize();
  }

  setHeigthToTableBody() {
    setTimeout(() => {
      let minusLength = this.data.length > 10 ? 247 : 170;
      jQuery('#geoFenceTable .main-body').css('max-height', (jQuery(document).height() - minusLength) + 'px');
    });
  }

  onPageWindowResize() {
    this.setResizedDataOnDocumentLoad(true);
    this.minInnerHeight = this.windowResizeData['windowInnerHeight'] - (this.windowResizeData['headerHeight'] + this.windowResizeData['footerHeight']);
    if (window.innerWidth < 992) {
      const geogencingListingFooterHeight = document.getElementById('geogencing-listing-footer').offsetHeight;
      const pageFooterHeight = document.getElementById('appFooter').offsetHeight;
      const bottomPadding = geogencingListingFooterHeight + pageFooterHeight;
      this.listContainerInlineStyle = { 'min-height': '500px', 'padding-bottom': bottomPadding + 'px' };
    }
    else {
      this.listContainerInlineStyle = { height: this.minInnerHeight + 'px' };
    }

    if (this.windowResizeData['windowInnerWidth'] < 992) {
      this.listFooterPosition = this.windowResizeData['footerHeight'];
    } else {
      this.listFooterPosition = 1;
    }
  }

  ngOnInit() {
    if (this.appService.getActiveCompany()) {
      this.getAssets(this.appService.getActiveCompany().companyId);
    }
    this.appService.activeCompanyInfo
      .takeUntil(this.onDestroy$)
      .subscribe(
        (company) => {
          this.availableAsset = [];
          this.data = [];
          this.event = {
            idle: 0,
            inactive: 0,
            stopped: 0,
            alarm: 0,
            moving: 0
          };
          this.getAssets(company.companyId);
        }
      );
    // this.geoFencingService.minMaxApplied.subscribe(
    //   (data) => {
    //     this.fullscreen = data.fullscreen;
    //     this.minimize = data.minimize;
    //   }
    // );
    this.geoFencingService.oneTripCompleted
      .takeUntil(this.onDestroy$)
      .subscribe(
        (changed) => {
          let currentPlaying = _.findWhere(this.data, { expanded: true });
          if (currentPlaying) {
            currentPlaying.isPaused = !currentPlaying.isPaused;
          }
        }
      );

    this.geoFencingService.shapeChanged
      .takeUntil(this.onDestroy$)
      .subscribe(
        (insideCircleMarkers) => {
          this.data = insideCircleMarkers;
        }
      );

    this.geoFencingService.closeAllMarkerEvent
      .takeUntil(this.onDestroy$)
      .subscribe(
        (data) => {
          this.closeAllMarker();
        }
      );
    this.onPageWindowResize();

    this.translate.stream("ao_assetid").subscribe(res => {
      if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
        this.columns[0].name = res;


      }
    });
    this.translate.stream("generic_location").subscribe(res => {
      if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
        this.columns[1].name = res;

      }
    });
    this.translate.stream("generic_last_updated").subscribe(res => {
      if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
        this.columns[2].name = res;

      }
    });
    this.translate.stream("generic_actions").subscribe(res => {
      if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
        this.columns[3].name = res;

      }
    });
    this.translate.stream("ao_assetid").subscribe(res => {
      if(res.charAt(0) !== res.charAt(0).toLowerCase()) {

        this.availableColumns[0].name = res;
      }
    });
    this.translate.stream("ao_vin").subscribe(res => {
      if(res.charAt(0) !== res.charAt(0).toLowerCase()) {

        this.availableColumns[1].name = res;
      }
    });
    this.translate.stream("ao_make").subscribe(res => {
      if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
        this.availableColumns[2].name = res;
      }
    });
    this.translate.stream("ao_model").subscribe(res => {
      if(res.charAt(0) !== res.charAt(0).toLowerCase()) {

        this.availableColumns[3].name = res;
      }
    });
    this.translate.stream("ao_serial_number").subscribe(res => {
      if(res.charAt(0) !== res.charAt(0).toLowerCase()) {

        this.availableColumns[4].name = res;
      }
    });

  }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    let filter = changes.filterBy;
    if (filter) {
      this.applyFileter(filter.currentValue);
    }
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    clearInterval(this.locationInterval);
    this.destroySocketConnection();
    this.onDestroy$.next();
  }

  destroySocketConnection() {
    if (this.socketConnectPrm && this.socketConnectPrm != null) {
      this.socketConnectPrm.removeAllListeners();
      this.socketConnectPrm.disconnect();
    }
  }

  setResizedDataOnDocumentLoad(resize: boolean = false) {
    const windowInnerWidth = window.innerWidth;
    const windowInnerHeight = window.innerHeight;
    const headerHeight = document.getElementById('appHeader').offsetHeight;
    const footerHeight = document.getElementById('appFooter').offsetHeight;
    const gmapHeaderHeight = document.getElementById('gmap-header').offsetHeight;
    const gmapFooterHeight = document.getElementById('gmap-footer').offsetHeight;
    this.windowResizeData = {
      headerHeight: headerHeight,
      footerHeight: footerHeight,
      windowInnerWidth: windowInnerWidth,
      windowInnerHeight: windowInnerHeight,
      gmapHeaderHeight: gmapHeaderHeight,
      gmapFooterHeight: gmapFooterHeight,
      resize: resize
    };
  }

  getAssets(companyId: string) {
    this.operationService.getAssets(companyId).subscribe(
      (res) => {
        const company = this.appService.getActiveCompany();
        this.keysForSubscription = [];
        this.event = {
          idle: 0,
          inactive: 0,
          stopped: 0,
          alarm: 0,
          moving: 0
        };
        this.setDataToTable(res);
        this.availableAsset = this.data;
        if (this.data.length > 0) {
          this.getLocationInfo();
        }
        this.subscribeForAlarm();
        this.dataChange.next(this.availableAsset);
        this.appService.setAllOptionPagination('geoFenceMfBootstrapPaginator');
        this.setHeigthToTableBody();
      }
    )
  }

  setDataToTable(res) {
    this.data = [];
    res.forEach((asset: any, index: any) => {
      const latLangObject: object = { lat: null, lang: null };
      const tLatestSyncDate = asset.latestSyncDate;
      let date1 = moment(asset.latestSyncDate).utc();
      let date2 = this.todayDate;
      if (moment.duration((date2.diff(date1))).asDays() > 7) {
        asset.status = 0;
      }
      let model = new GeoFenceAsset(
        asset.aId,
        asset.altitude,
        asset.assetId,
        asset.assetLogo,
        asset.assetStatus,
        asset.assetType,
        asset.calibrationId,
        asset.companyId,
        asset.companyName,
        asset.controllerId,
        asset.customerEquipmentGroup,
        asset.customerEquipmentId,
        asset.facilityId,
        asset.facilityName,
        asset.iotHubName,
        asset.createDate,
        asset.latestSyncDate,
        asset.latitude,
        asset.logo,
        asset.longitude,
        asset.make,
        asset.model,
        asset.serialNumber,
        asset.softwareId,
        asset.status,
        asset.unitNumber,
        asset.vin,
        asset.gwAppBuild,
        asset.gwDCAver,
        asset.gwDescription,
        asset.gwIMEI,
        asset.gwOSBuild,
        asset.gwRSSI,
        asset.gwSIMICCID,
        asset.gwSNO,
        asset.gwTimezone,
        asset.gwWANIP,
        latLangObject,
        asset.pVer,
        asset.param
      );
      model['tLatestSyncDate'] = tLatestSyncDate;
      // model.icon = this.getIcon();
      model.zIndex = parseInt(`9${asset.status}`);
      this.data.push(model);
      this.keysForSubscription.push(`${asset.assetId}.${asset.gwSNO}.geo`);
      this.increaseCount(asset);
    });
  }

  getLocationInfo() {
    this.data.forEach((element, index) => {
      setTimeout((count) => {
        this.appService.getGeoCodeLocation(element.latitude, element.longitude).subscribe((response) => {
          let results = JSON.parse(response['_body']).results;
          const geocoderAddress = results[0] ? results[0].formatted_address.split(',') : '';
          if (geocoderAddress) {
            let locationAddress = {
              street: (geocoderAddress[0] && geocoderAddress[0].trim()) || '',
              city: (geocoderAddress[1] && geocoderAddress[1].trim()) || '',
              state: (geocoderAddress[2] && geocoderAddress[2].trim().split(' ')[0]) || '',
              zipcode: (geocoderAddress[2] && geocoderAddress[2].trim().split(' ')[1]) || '',
              country: (geocoderAddress[3] && geocoderAddress[3].trim()) || ''
            };
            const locationString: String = locationAddress['city'] + ', ' + locationAddress['state'] + ', ' + locationAddress['zipcode'];
            this.locationDataFromGeocode[this.data[count].aId] = locationString;
            this.data[count].location = locationString;
          }
        }, (err) => {
          //console.log(err);
        });
      }, true, index);
    });
  }

  applyFileter(filter: GeofenceFilter) {
    let query: any = {};
    if (filter.utilities) {
      this.columns[1] = this.availableColumns[Number(this.filterBy.utilities) - 1];
    }

    filter.facility ? query.facilityId = filter.facility : null;
    filter.event ? query.status = parseInt(filter.event) : null;
    filter.assetType ? query.assetType = filter.assetType : null;
    this.data = _.where(this.availableAsset, query);
    if (filter.vin) {
      this.data = _.filter(this.data, (item) => {
        return ((item[this.columns[1]['sort']] && item[this.columns[1]['sort']].toLocaleLowerCase().indexOf(filter.vin.toLocaleLowerCase()) !== -1) ||
          (item.make && item.make.toLocaleLowerCase().indexOf(filter.vin.toLocaleLowerCase()) !== -1) ||
          (item.model && item.model.toLocaleLowerCase().indexOf(filter.vin.toLocaleLowerCase()) !== -1) ||
          (item.vin && (item.vin.toLocaleLowerCase().indexOf(filter.vin.toLocaleLowerCase()) !== -1)) ||
          (item.serialNumber && (item.serialNumber.toLocaleLowerCase().indexOf(filter.vin.toLocaleLowerCase()) !== -1)));
      });
    }
    this.dataChange.next(this.data);
  }

  checkTimeFormat(time, date) {
    let tempDate = moment();
    if (this.appService.userTimezone != null) {
      tempDate = moment.tz(date + ' ' + time, this.appService.userTimezone);
      tempDate = moment(tempDate).utcOffset(-tempDate.utcOffset());
    } else {
      tempDate = moment(date + ' ' + time).utc();
    }
    date = tempDate.format('MM/DD/YYYY');
    time = tempDate.format("HH:mm");
    if (tempDate.hours() === 0) {
      tempDate = tempDate.subtract(1, "days");
      date = tempDate.format('MM/DD/YYYY');
      time = '23:59';
    }
    return { 'date': date, 'time': time };
  }

  getGeoData(asset: GeoFenceAsset, startDate, endDate, startTime, endTime) {
    this.geoFencingService.getGeoData(this.appService.getActiveCompany().companyId, asset.gwSNO, asset.assetId, startDate, endDate)
      .finally(() => {
        this.appService.loader.next(false);
      })
      .subscribe(
        (res) => {
          this.tripStarted = false;
          this.colorCodeIndex = 1;
          res.forEach(
            (geo, index) => {
              let model = new GeoData(
                geo.gwSno,
                geo.assetId,
                geo.occurrenceDate,
                'geo',
                geo.param
              );
              /* params.value holds - Latitude,Longitude,altitude,direction,speed,visibleSatellites,pdop,hdop,updateReason,geofenceID */
              if (model.values[8] && model.values[8].indexOf('Start') > -1) {
                this.tripStarted = true;
              } else if (model.values[8] && (model.values[8].toLowerCase().indexOf('stop') > -1 || (model.values[8].toLowerCase().indexOf('end')) > -1)) {
                this.colorCodeIndex++;
                this.tripStarted = false;
              }
              if (this.tripStarted) {
                model.colorCode = this.colorCodeIndex;
              }
              this.availableGeoData.push(model);
            }
          );
          this.geoData.next(this.availableGeoData);
        }
      );
  }

  timeChanged(time: any) {
    this.sliderTime.startTime = time[0] > 10 ? time[0] + ':00' : `0${time[0]}:00`;
    this.sliderTime.endTime = time[1] > 10 ? time[1] + ':00' : `0${time[1]}:00`;
    this.timeRangeValue = time;
    this.geoFencingService.timeRange = this.timeRangeValue;
  }

  dateChanged(data: any[]) {
    this.sliderDate.startDate = data[0];
    this.sliderDate.endDate = data[1];
    this.dateRangeValue = data[2];
    this.geoFencingService.dateRange = this.dateRangeValue;
  }

  navigateDashboard(asset) {
    this.router.navigate([`operation/asset-dashboard/${asset.assetId}`]);
  }

  toggleExpand(item) {
    this.data.forEach((itm, key) => {
      if (item.assetId === itm.assetId) {
        itm.expanded = !itm.expanded;
        this.stopAlarmData(item);
      } else {
        itm.expanded = false;
      }
    });
    this.geoFencingService.isRouteFromMap = false;
    this.geoFencingService.timeRange = [0, 2];
    this.geoFencingService.dateRange = [0, 0];
    this.timeRangeValue = this.geoFencingService.timeRange;
    this.dateRangeValue = this.geoFencingService.dateRange;
    this.toggleExpandClick = true;
    setTimeout(() => {
      this.geoFencingService.closeInfoWindows.next();
    }, 100);
  }

  closeAllMarker() {
    this.data.forEach((itm, key) => {
      itm.expanded = false;
    });
  }

  listItemClick(item) {
    if (!this.toggleExpandClick) {
      this.displayMarkerInfo.emit(item);
    }
    this.toggleExpandClick = false;
  }

  getAlarmData(asset: GeoFenceAsset, startDate = this.sliderDate.startDate, endDate = this.sliderDate.endDate, startTime = this.sliderTime.startTime, endTime = this.sliderTime.endTime) {
    this.appService.loader.next(true);
    asset.isPlaying = true;
    asset.expanded = true;
    this.availableGeoData = [];
    if (this.geoFencingService.isRouteFromMap) {
      this.dateRangeValue = this.geoFencingService.dateRange;
      this.timeRangeValue = this.geoFencingService.timeRange;
    }
    let start = this.checkTimeFormat(startTime, startDate);
    startDate = start.date + ' ' + start.time;
    startTime = start.time;
    this.geoFencingService.startTime = startTime;
    this.geoFencingService.startDate = start.date;
    let end = this.checkTimeFormat(endTime, endDate);
    endDate = end.date + ' ' + end.time;
    endTime = end.time;
    this.geoFencingService.endTime = endTime;
    this.geoFencingService.startDate = end.date;
    this.geoFencingService.getAlarmData(this.appService.getActiveCompany().companyId, asset.gwSNO, asset.assetId, startDate, endDate)
      .subscribe(
        (res) => {
          res.forEach(
            (geo) => {
              let model = new GeoData(
                geo.gwSno,
                geo.assetId,
                geo.occurrenceDate,
                'alarm',
                geo.param,
              );
              this.availableGeoData.push(model);
            }
          );
          this.getGeoData(asset, startDate, endDate, startTime, endTime);
          this.dataChange.next(res);
        }, (err) => {
          this.appService.loader.next(false);
          this.appService.showErrorMessage('Error', 'Something wrong!');
        }
      );
    this.geoFencingService.playOrPause(false);
  }

  checkClosestAlarm(available, current) {
    let closest = false;
    let data = null;
    available.forEach(element => {
      let alarmDate = Date.parse(element);
      let geoDate = Date.parse(current);

      if (Math.abs(alarmDate - geoDate) <= 5000 || Math.abs(geoDate - alarmDate) <= 5000) {
        closest = true;
        data = element;
      }

    });
    return { closest, data };
  }

  changeMinMax() {
    this.geoFencingService.applyFullscreen({ fullscreen: this.fullscreen, minimize: this.minimize });
  }

  subscribeForAlarm() {
    this.destroySocketConnection();
    setTimeout(() => {
      this.socketConnectPrm = this.socket.connect();
      this.socket.emit('subscribe', this.keysForSubscription);
      this.socketOn();
    }, 1000);
  }

  socketOn() {
    this.socket.on('data', (data) => {
      if (data.status === 'Geo') {
        this.realTime.next(JSON.parse(JSON.stringify(data)));
        const ind = _.indexOf(this.data, _.findWhere(this.data, { assetId: data.assetID }));
        let obj = typeof data.event_param === 'string' ? JSON.parse(data.event_param)[0] : data.event_param[0];
        if (this.data[ind] && obj.sts) {
          let date1 = moment(obj.TS).utc();
          if (moment.duration((this.todayDate.diff(date1))).asDays() > 7) {
            obj.sts = 'Inactive';
          }
        }
        const eventParams = data.event_param ? typeof data.event_param === 'string' ? JSON.parse(data.event_param)[0] : data.event_param[0] : {};
        if (this.data[ind] && moment(this.data[ind].latestSyncDate).diff(moment(data.occurrence_date_time)) < 0) {
          this.data[ind].latestSyncDate = data.occurrence_date_time;
          this.data[ind]['tLatestSyncDate'] = data.occurrence_date_time;
        }
        this.updateCounter();
      }
    });

    this.socketDisconnectHandler();
    this.socketErrorConectHandler();
    this.socketErrorHandler();
  }

  socketDisconnectHandler() {
    this.socket.on('disconnect', () => {
      if (this.router.url === '/operation/geo-fencing') {
        console.log('Got disconnect!');
        this.destroySocketConnection();
        this.subscribeForAlarm();
      }
    });
  }

  socketErrorConectHandler() {
    this.socket.on('connect_error', () => {
      console.log('connect_error');
      this.destroySocketConnection();
      this.subscribeForAlarm();
    });
  }

  socketErrorHandler() {
    this.socket.on('error', (err) => {
      console.log('Error connecting to server', err);
      this.destroySocketConnection();
      this.subscribeForAlarm();
    });
  }

  updateCounter() {
    this.event = {
      idle: 0,
      inactive: 0,
      stopped: 0,
      alarm: 0,
      moving: 0
    };
    this.data.forEach(asset => {
      if (asset['isAlarm']) {
        asset['svgFillEdge'] = '#eb3c33';
        this.event.alarm += 1;
      }
      this.increaseCount(asset);
    });
  }

  stopAlarmData(asset: GeoFenceAsset) {
    asset.isPlaying = false;
    this.subscribeForAlarm();
    this.geoFencingService.playOrPause('stop');
    this.dataChange.next(this.availableAsset);
  }

  decreaseCount(ast) {
    if (ast.status === 2) {
      this.event.idle -= 1;
    } else if (ast.status === 0) {
      this.event.inactive -= 1;
    } else if (ast.status === 1) {
      this.event.stopped -= 1;
    } else if (ast.status === 3) {
      this.event.moving -= 1;
    } else {
      this.event.alarm -= 1;
    }
  }

  increaseCount(ast) {
    if (ast.status === 2) {
      this.event.idle += 1;
    } else if (ast.status === 0) {
      this.event.inactive += 1;
    } else if (ast.status === 1) {
      this.event.stopped += 1;
    } else if (ast.status === 3) {
      this.event.moving += 1;
    } else {
      this.event.alarm += 1;
    }
  }

  getStatus(sts = '') {
    let status = sts ? sts.split(',') : [];
    let result = { map: 91, asset: 1 };
    if (status) {
      if (status[1] && status[1] === '1') {
        result = this.getMappedStatus(status[1]);
      } else if (status[0]) {
        result = this.getMappedStatus(status[0]);
      }
    }
    return result;
  }

  getMappedStatus(status = '') {
    let mapNumber = 91;
    let assetNumber = 1;
    switch (status.toString().toLowerCase()) {
      case 'geo':
        mapNumber = 93;
        assetNumber = 3;
        break;
      case 'alarm':
        mapNumber = 94;
        assetNumber = 4;
        break;
      case '1':
        mapNumber = 94;
        assetNumber = 4;
        break;
      case 'inactive':
        mapNumber = 90;
        assetNumber = 0;
        break;
      case 'stopped':
        mapNumber = 91;
        assetNumber = 1;
        break;
      case 'moving':
        mapNumber = 93;
        assetNumber = 3;
        break;
      case 'idle':
        mapNumber = 92;
        assetNumber = 2;
        break;
      default:
        mapNumber = 91;
        assetNumber = 1;
    }
    return { map: mapNumber, asset: assetNumber };
  }

  pauseOrReplayAlarmData(item) {
    item.isPaused = !item.isPaused;
    this.geoFencingService.playOrPause(item.isPaused);
  }

  setEndColorCode(idx) {
    for (let i = 0; i < idx; i++) {
      this.availableGeoData[i].colorCode = 2;
    }
  }
}


      /*  removed by kESAVAN to fix - stop to display markers */
      // ast.latitude = null;
      // ast.longitude = null;
      // ast.location = null;
      // if (data.latitude && data.longitude) {
      // let ad = this.bsMap.getLocationInfo(parseFloat(data.latitude), parseFloat(data.longitude));
      // ast.location = this.bsMap.getLocationInfo(parseFloat(data.latitude), parseFloat(data.longitude))
