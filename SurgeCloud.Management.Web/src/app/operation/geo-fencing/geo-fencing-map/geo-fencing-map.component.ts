import {
  Component, OnInit, Output, EventEmitter, AfterViewInit, Input, SimpleChange, ElementRef, NgZone, ViewChild, QueryList, ViewChildren,
  OnChanges
} from '@angular/core';
import { GeoFencingService } from '../geo-fencing.service';
import { Facility, Asset, GeoFenceAsset } from '../../../app-common';
import { AppService } from '../../../app.service';
import { GeofenceFilter } from '../../operation';
import { MapsAPILoader, GoogleMapsAPIWrapper, CircleManager, AgmMarker, AgmMap, AgmCircle } from '@agm/core';
import * as _ from 'underscore';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { concat } from 'rxjs/observable/concat';
import { Shape } from 'app/operation/geo-fence-create/geonfence-create';
import * as MomentTimezone from 'moment-timezone';
// import { clearInterval } from 'timers';
import * as jQuery from 'jquery';
import {TranslateService} from "@ngx-translate/core";
import {AssetType} from "../../../admin/asset-onboarding/asset-onboarding";
declare const google: any;

@Component({
  selector: 'app-geo-fencing-map',
  templateUrl: './geo-fencing-map.component.html',
  styleUrls: ['./geo-fencing-map.component.scss']
})

export class GeoFencingMapComponent implements OnInit, AfterViewInit {
  public mainContainerHeight: number;
  private filterBy = 1;
  private availableFacility: Facility[] = [];
  private assets: Asset[] = [];
  private geoFilter: GeofenceFilter = new GeofenceFilter();
  public availableGeoData = [];
  public displayRoute: any = [];
  public isPaused: Boolean = false;
  public activeCompanyInfoSubscriber: any;
  public geoHistoryData: Array<{ lat: number, lng: number, speed: any, altitude: any, direction: any, occurrenceDate: any }> = [];
  public geoHistoryDataFull: Array<{ lat: number, lng: number, speed: any, altitude: any, direction: any }> = [];
  private svgIcons: string[] = [
    'assets/icons/common/clear.svg#clear',
    'assets/icons/common/save.svg#save'
  ];
  private routeColors: string[] = [
    '#08b6ff',
    'green',
    'red',
    '#4800ff',
    '#ff6a00',
    '#c0c0c0',
    '#ff006e',
    '#7fffff',
    '#7f1610',
    '#08b6ff'
  ];
  public startingMarkerList = [];
  public endingMarkerList = [];
  public startingMarker: any = null;
  public endingMarker: any = null;
  public fitBounds: [{ lat: number, lng: number }] = null;
  public geoDataA: any;
  public alarmDataA: any
  public map: any;
  public polyline: any;
  public polyline1: any;
  public poly2: any;
  public routeColorsIndex = 0;
  public zoom = 19;
  public drawingManager: any;
  public selectedShape: any;
  public avilableaAssetType: any[] = [];
  public newShape: any = {};
  public selectedMarkerIndex: any;
  public selectedMarkerData: any;
  private statusIcons: string[] = [
    'assets/icons/Geofencing/Status_Stopped.svg#Ellipse_661_copy_3',
    'assets/icons/Geofencing/Status_Inactive.svg#Ellipse_661_copy_6',
    'assets/icons/Geofencing/Status_Idle.svg#Ellipse_661_copy',
    'assets/icons/Geofencing/Status_Moving.svg#Ellipse_661_copy_15',
    'assets/icons/Geofencing/Status_Alarm.svg#Ellipse_661_copy_4'
  ];
  public sliderDate: { startDate: string, endDate: string } = { startDate: '', endDate: '' };
  public sliderTime: { startTime: string, endTime: string } = { startTime: '', endTime: '' };
  public availStatus: string[] = ['Inactive', 'Active', 'Moving', 'Idle', 'Alarm'];
  public alarmData: any[] = [];
  public fullscreen: any = {
    map: false,
    list: false,
  };
  public minimize: any = {
    map: false,
    list: false
  };
  public mapInterval = null;
  public infowindow: any;
  public mapTypeControlOptions: any;
  public zoomControlOptions: any;
  public borderPolyline: any;
  private selectedInfoWindowIndex = null;
  private infoWindowArray = [];
  public placeholderone: string;
  public placeholdertwo: string;
  label = {
    text: 'test',
    color: '#ffffff',
    fontSize: '1px',
    fontWeight: 'bold'
  };
  gmarkers: any[] = [];
  public availableGeoFence = [];
  public todayDate = this.geoFencingService.getTodayDateAsPerTimeZone();
  @Input('data') public mapData: GeoFenceAsset[] = [];
  @Input() set geoData(data) {
    this.availableGeoData = [];
    this.availableGeoData = data;
    if (this.availableGeoData.length > 0) {
      this.geoDataA = this.availableGeoData.filter(x => x.dataType === 'geo');
      this.alarmDataA = this.availableGeoData.filter(x => x.dataType === 'alarm');
      if (this.geoDataA.length > 0) {
        this.drawLines();
        const bounds = new google.maps.LatLngBounds();
        if (this.map) {
          this.map.setCenter(bounds.getCenter());
          this.map.fitBounds(bounds);
        }
      }
    } else {
      const position = new google.maps.LatLng(this.appService.defaultLat, this.appService.defaultLng);
      this.map.panTo(position);
    }
  }
  @Input() set realtime(data) {
    this.updateAssetPosition(JSON.parse(JSON.stringify(data)));
  }

  @ViewChild(AgmCircle) circle: AgmCircle;
  @ViewChildren(AgmMarker) markers: QueryList<AgmMarker>;

  @Output() filter = new EventEmitter<GeofenceFilter>();
  @Output() action = new EventEmitter<GeoFenceAsset>();
  @Output() stopAlarmdata = new EventEmitter<GeoFenceAsset>();
  @Output() toggleExpand = new EventEmitter<GeoFenceAsset>();
  @Output() oneTripCompleted = new EventEmitter<any>();
  @Input() set displayMarkerInfo(item) {
    if (item) {
      this.mapData.forEach((element, index) => {
        if (element.aId === item.aId)
          this.clickedMarker(index);
      });
    }
  }
  public tripPolylines: any[] = [];
  public isPlayWithRoute: boolean = false;
  /* below variable are used for moving marker with animation - http://jsfiddle.net/rcravens/RFHKd/13/*/
  public numDeltas = 100;
  public delay = 10; //milliseconds
  public deltCount = 0;
  public deltaLat;
  public deltaLng;
  public moveMarkerLocation: any[] = [];
  public count: any = 0;
  public bounds: any;
  public availableMarkers: GeoFenceAsset[] = [];
  public dateRangeValue = [0, 0];
  public timeRangeValue = [0, 2];
  constructor(private geoFencingService: GeoFencingService,
    private elementRef: ElementRef,
    private appService: AppService,
    private mapsAPILoader: MapsAPILoader,
    private _zone: NgZone, private router: Router,private translate: TranslateService
  ) { }

  ngOnInit() {
    this.getAssetType();
    // this.setMainContainerHeight();

    this.activeCompanyInfoSubscriber = this.appService.activeCompanyInfo.subscribe(
      (company) => {
        this.geoFencingService.playOrPauseApplied.next('stop');
        this.getAvailableFacility();
        this.getGeofence(company['companyId']);
        this.geoFilter = new GeofenceFilter();
        this.changeFilter();
      }
    );
    let company = this.appService.getActiveCompany();
    if (company) {
      this.getGeofence(company.companyId);
      this.getAvailableFacility();
    }
    this.geoFencingService.filterApplied.subscribe(
      (data: GeoFenceAsset[]) => {
        // this.isPaused = true;
        this.mapData = [];
        if (data.length === 0 && this.map) {
          const position = new google.maps.LatLng(this.appService.defaultLat, this.appService.defaultLng);
          this.map.panTo(position);
        }
        if (this.mapInterval && this.selectedShape) {
          clearInterval(this.mapInterval);
          this.polyline.setMap(null);
          this.borderPolyline.setMap(null);
          this.resetAllMarkers();
          this.startingMarker.setMap(null);
          this.endingMarker.setMap(null);
          this.tripPolylines.forEach((line) => {
            line.setMap(null);
          });
          this.selectedShape.setMap(null);
        }
        let tooltiopEle: any = document.querySelectorAll('.gmap-tooltip');
        if (tooltiopEle.length) {
          tooltiopEle.forEach(element => {
            element.className = element.className.replace('gmap-tooltip', '');
          });
          this.infowindow.close();
        }
        for (let i = 0; i < this.gmarkers.length; i++) {
          this.gmarkers[i].setMap(null);
        }
        if (data[0]) {
          let mapTimeout = setTimeout(
            () => {
              if (typeof google == "object") {
                clearTimeout(mapTimeout);
                this.bounds = new google.maps.LatLngBounds();
                data.forEach((ast: GeoFenceAsset) => {
                  if (this.isFloat(ast.latitude) && this.isFloat(ast.longitude)) {
                    this.bounds.extend({ lat: ast.latitude, lng: ast.longitude });
                    ast.icon = this.getIcon(ast);
                    this.mapData.push(ast);
                  }
                });
                this.availableMarkers = Object.assign([], this.mapData);
                if (this.map) {
                  this.map.setCenter(this.bounds.getCenter());
                  this.map.fitBounds(this.bounds);
                  if (this.map.getZoom() > 17) {
                    this.map.setZoom(17);
                  }
                }

              }
            }, 100);
        } else {
          this.mapData = [];
          if (this.map) {
            const bounds = new google.maps.LatLngBounds();
            this.map.setZoom(1);
            bounds.extend({ lat: this.appService.defaultLat, lng: this.appService.defaultLng });
            this.map.setCenter({ lat: this.appService.defaultLat, lng: this.appService.defaultLng });
          }
        }
      }
    );
    this.geoFencingService.playOrPauseApplied.subscribe(
      (isPaused) => {
        if (isPaused === 'clear' || isPaused === 'stop') {
          this.isPlayWithRoute = false;
          if (this.mapInterval) {
            this.count = 0;
            clearInterval(this.mapInterval);
            this.polyline.setMap(null);
            this.borderPolyline.setMap(null);
            this.resetAllMarkers();
            this.startingMarker.setMap(null);
            this.endingMarker.setMap(null);
            this.tripPolylines.forEach((line) => {
              line.setMap(null);
            });
            // this.selectedShape.setMap(null);
          }
        } else {
          this.isPaused = isPaused;
          if (this.count == 0 && this.selectedShape) {
            this.moveIcon();
          }
        }
      }
    );
    this.geoFencingService.closeInfoWindows.subscribe(() => {
      this.mapData.forEach(element => {
        element.isOpen = false;
      });
    });
    this.translate.stream("geo_vin_sno").subscribe(res => {
      if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
        this.placeholderone = res;


      }
    });
    this.translate.stream("geo_location").subscribe(res => {
      if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
        this.placeholdertwo = res;

      }
    });
  }

  setPositionToAutoComplate(prm) {
    if (prm === 'in') {
      jQuery('.pac-container').addClass('geo-fencing-pac-container');
    } else {
      jQuery('.pac-container').removeClass('geo-fencing-pac-container');
    }
  }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
  }

  ngAfterViewInit() {
    this.mapsAPILoader.load().then(() => {
      // Place your code in here...
      const autocomplete: any = new google.maps.places.Autocomplete(this.elementRef.nativeElement.querySelector
        ('input[name=addressName]'), {
          types: ['geocode']
        });
      autocomplete.addListener('place_changed', () => {
        this._zone.run(() => {
          const place: any = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.map.fitBounds(place.geometry.viewport);
        });
      });
    });
  }

  getGeofence(companyId: string) {
    this.geoFencingService.getGeofence(companyId).subscribe(
      (response) => {
        this.availableGeoFence = response;
      }
    );
  }

  getShapeObject(data) {
    return {
      type: data['shape'],
      radius: data['radius'],
      path: data['geoCoordinates'],
      color: data['color']
    }
  }

  changeGeoFenceFilter(value) {
    if (this.geoFilter.geofence) {
      this.geoFilter.showZones = value;
      if (this.selectedShape) {
        this.selectedShape.setMap(null);
      }
      let geoFence = this.availableGeoFence.filter(element => {
        return element.geofenceId === this.geoFilter.geofence;
      });
      geoFence = geoFence[0];
      let shape = this.getShapeObject(geoFence);
      this.selectedShape ? this.selectedShape.setMap(null) : '';
      if (geoFence['shape'] === 'circle') {
        this.setSelectedShapeAsCircle(shape);
        this.selectedShape.type = 'circle';
      } else if (geoFence['shape'] === 'polygon') {
        this.setSelectedShapeAsPolygon(shape);
        this.selectedShape.type = 'polygon';
      }
      this.setSelection(this.selectedShape);
    } else {
      this.deleteSelectedShape();
      this.resetGeoFenceFilter(false);
      this.changeFilter();
    }
  }

  resetGeoFenceFilter(value) {
    if (!value) {
      this.geoFilter.showZones = false;
      this.selectedShape.setMap(null);
      if (this.map) {
        this.map.setCenter(this.bounds.getCenter());
        this.map.fitBounds(this.bounds);
      }
    }
    else {
      this.changeGeoFenceFilter(true);
    }
  }

  setSelectedShapeAsCircle(data) {
    this.selectedShape = new google.maps.Circle({
      strokeColor: data.color ? data.color : '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: data.color ? data.color : '#FF0000',
      fillOpacity: 0.35,
      map: this.map,
      center: { lat: data.path[0].latitude, lng: data.path[0].longitude },
      radius: data.radius
    });
    this.selectedShape.setMap(this.map);
    const marker = new google.maps.Marker({
      map: this.map,
      position: new google.maps.LatLng(data['path'][0].latitude, data['path'][0].longitude)
    });
    this.selectedShape.bindTo('center', marker, 'position');
    this.map.fitBounds(this.selectedShape.getBounds());
    marker.setMap(null);
  }

  setSelectedShapeAsPolygon(data) {
    let latlng = [];
    data.path.forEach(ltlng => {
      latlng.push({ lat: ltlng.latitude, lng: ltlng.longitude });
    });
    this.selectedShape = new google.maps.Polygon({
      path: latlng,
      geodesic: true,
      strokeColor: data.color ? data.color : '#FF0000',
      strokeOpacity: 1.0,
      strokeWeight: 2,
      fillColor: data.color ? data.color : '#FF0000',
      fillOpacity: 0.35
    });

    this.selectedShape.setMap(this.map);
    const bounds = new google.maps.LatLngBounds();
    data['path'].forEach((ast: GeoFenceAsset) => {
      if (this.isFloat(ast.latitude) && this.isFloat(ast.longitude)) {
        bounds.extend({ lat: ast.latitude, lng: ast.longitude });
      }
    });
    if (this.map) {
      this.map.setCenter(bounds.getCenter());
      this.map.fitBounds(bounds);
    }
  }

  changeFilter() {
    setTimeout(
      () => {
        this.filter.emit(this.geoFilter);
      }, 2);
  }

  getAvailableFacility() {
    this.geoFencingService.getFacility(this.appService.getActiveCompany().companyId).subscribe(
      (data) => {
        this.availableFacility = [];
        data.forEach(facility => {
          const model = new Facility(facility.logo, facility.name, facility.status, facility.address, facility.timezone,
            facility.facilityType, facility.facilityId);
          this.availableFacility.push(model);
        });
      }
    );
  }

  // setLocation(lat: number, lng: number) {
  //   this.lat = lat;
  //   this.lng = lng;

  //   this.circle.getBounds().then(
  //     (data) => {
  //       // this.getMarkersInsideCircle(data, this.markers);
  //     }
  //   );
  // }

  // getMarkersInsideCircle(circle, markers) {
  //   const result: AgmMarker[] = [];
  //   markers.forEach(marker => {
  //     if (circle.contains({ lat: marker.latitude, lng: marker.longitude })) {
  //       result.push(marker);
  //     }
  //   });
  // }

  initDrawingTools() {
    const polyOptions = {
      strokeWeight: 0,
      fillOpacity: 0.45,
      editable: true,
      draggable: true,
      fillColor: 'red'
    };

    this.drawingManager = new google.maps.drawing.DrawingManager({
      drawingMode: null,
      drawingControl: true,
      drawingControlOptions: {
        position: google.maps.ControlPosition.RIGHT_BOTTOM,
        drawingModes: ['circle', 'polygon', 'polyline', 'rectangle']
      },
      markerOptions: {
        draggable: true
      },
      polylineOptions: {
        editable: true,
        draggable: true
      },
      rectangleOptions: polyOptions,
      circleOptions: polyOptions,
      polygonOptions: polyOptions,
      map: this.map
    });
    this.drawingManager.setMap(this.map);
    this.drawingEventListen();
  }

  mapRendered($event) {
    this.map = $event;
    const mapZoomOptions = { minZoom: 5 };
    this.map.setOptions(mapZoomOptions);
    this.initDrawingTools();
    this.mapTypeControlOptions = {
      style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
      mapTypeIds: ['roadmap', 'hybrid'],
      position: google.maps.ControlPosition.BOTTOM_LEFT
    }
    this.zoomControlOptions = {
      style: google.maps.ZoomControlStyle.DEFAULT,
      position: google.maps.ControlPosition.RIGHT_CENTER
    };
    this.startingMarker = new google.maps.Marker({});
    this.endingMarker = new google.maps.Marker({});
  }

  zoomChange(level: number) {
    this.zoom = level;
  }

  setPinToPoint() {
    this.startingMarkerList = [];
    this.endingMarkerList = [];
    this.geoDataA.forEach((element, index) => {
      if (element.params[0].value.toLowerCase().includes('start')) {
        this.setStartingPointPin(index);
      }
      if (element.params[0].value.toLowerCase().includes('stop') || element.params[0].value.toLowerCase().includes('end')) {
        this.setEndingPointPin(index);
      }
    });
  }

  setStartingPointPin(index) {
    this.geoDataA[index].params[0].value = typeof this.geoDataA[index].params[0].value === 'string' && this.geoDataA[index].params[0].value.toLowerCase() === 'nan' ? '' : this.geoDataA[index].params[0].value;
    const startlatlng = this.geoDataA[index].params[0].value.split(',');
    if (startlatlng[0] && startlatlng[1]) {
      let startingMarker = this.getMarker(startlatlng, 'assets/images/flag_start.svg', 100 + index);
      this.addClickEventToStartingPointPin(startingMarker, index);
      this.startingMarkerList.push(startingMarker);
    }
  }

  addClickEventToStartingPointPin(startingMarker, index) {
    startingMarker.addListener('click', (event) => {
      let eventKey = Object.keys(event);
      if (event[eventKey[1]].target.parentNode.nextSibling && event[eventKey[1]].target.parentNode.nextSibling.className.indexOf('gmap-tooltip') == -1) {
        event[eventKey[1]].target.parentNode.nextSibling.className += ' gmap-tooltip';
      }
      let startposition = JSON.parse(JSON.stringify(this.geoDataA[index]));
      startposition.occurrenceDate = this.convertDateToZone(startposition.occurrenceDate);
      this.geoDataA[index].params[0].value = typeof this.geoDataA[index].params[0].value === 'string' && this.geoDataA[index].params[0].value.toLowerCase() === 'nan' ? '' : this.geoDataA[index].params[0].value;
      const endlatlng = this.geoDataA[index].params[0].value.split(',');
      this.infowindow.close();
      if (this.infoWindowArray[this.selectedInfoWindowIndex])
        this.infoWindowArray[this.selectedInfoWindowIndex].close();
      this.infowindow.setContent(`<div class="hover-info">
      <div class="header">${moment(startposition.occurrenceDate).format('MM/DD/YYYY, hh:mm A')}</div>
      <div class="info"><strong>Trip Started</strong></div>
      <div><strong>Latitude</strong> :  ${endlatlng[0]}</div>
      <div><strong>Longitude</strong> :  ${endlatlng[1]}</div>
      </div>
    `);
      this.infowindow.setPosition({
        lat: parseFloat(endlatlng[0]),
        lng: parseFloat(endlatlng[1])
      });
      this.infowindow.open(this.map);
    });
  }

  setEndingPointPin(index) {
    this.geoDataA[index].params[0].value = typeof this.geoDataA[index].params[0].value === 'string' && this.geoDataA[index].params[0].value.toLowerCase() === 'nan' ? '' : this.geoDataA[index].params[0].value;
    const endlatlng = this.geoDataA[index].params[0].value.split(',');
    if (endlatlng[0] && endlatlng[1]) {
      let endingMarker = this.getMarker(endlatlng, 'assets/images/flag_end.svg', 100 + index);
      this.addClickEventToEndingPointPin(endingMarker, index);
      this.endingMarkerList.push(endingMarker);
    }
  }

  addClickEventToEndingPointPin(endingMarker, index) {
    endingMarker.addListener('click', (event) => {
      let eventKey = Object.keys(event);
      if (event[eventKey[1]].target.parentNode.nextSibling && event[eventKey[1]].target.parentNode.nextSibling.className.indexOf('gmap-tooltip') == -1) {
        event[eventKey[1]].target.parentNode.nextSibling.className += ' gmap-tooltip';
      }
      let startposition = JSON.parse(JSON.stringify(this.geoDataA[index]));
      startposition.occurrenceDate = this.convertDateToZone(startposition.occurrenceDate);
      this.geoDataA[index].params[0].value = typeof this.geoDataA[index].params[0].value === 'string' && this.geoDataA[index].params[0].value.toLowerCase() === 'nan' ? '' : this.geoDataA[index].params[0].value;
      const startlatlng = this.geoDataA[index].params[0].value.split(',');
      this.infowindow.close();
      this.infowindow.setContent(`<div class="hover-info">
      <div class="header">${moment(startposition.occurrenceDate).format('MM/DD/YYYY, hh:mm A')}</div>
          <div class="info"><strong>Trip Ended</strong></div>
          <div><strong>Latitude</strong> :  ${startlatlng[0]}</div>
          <div><strong>Longitude</strong> :  ${startlatlng[1]}</div>
          </div>
        `);
      this.infowindow.setPosition({
        lat: parseFloat(startlatlng[0]),
        lng: parseFloat(startlatlng[1])
      });
      this.infowindow.open(this.map);
    });
  }

  setStartingAndEndingPointToTrip() {
    this.geoDataA[0].params[0].value = typeof this.geoDataA[0].params[0].value === 'string' && this.geoDataA[0].params[0].value.toLowerCase() === 'nan' ? '' : this.geoDataA[0].params[0].value;
    const startlatlng = this.geoDataA[0].params[0].value.split(',');
    const endlatlng = this.geoDataA[this.geoDataA.length - 1].params[0].value.split(',');
    if (startlatlng[0] && startlatlng[1]) {
      this.startingMarker = this.getMarker(startlatlng, 'assets/images/pin_start.png', 98);
      this.startingMarker.removeListener && this.startingMarker.removeListener('click', this.startingPointerInfo);
      this.startingMarker.addListener('click', this.startingPointerInfo);
    }
    if (endlatlng[0] && endlatlng[1]) {
      this.endingMarker = new google.maps.Marker({});
      this.endingMarker = this.getMarker(endlatlng, 'assets/images/pin_end.png', 99);
      this.endingMarker.removeListener && this.endingMarker.removeListener('click', this.endingPointerInfo);
      this.endingMarker.addListener('click', this.endingPointerInfo);
    }
  }

  getMarker(latlng, url, zIndex) {
    let marker = new google.maps.Marker({});
    marker.setOptions({
      position: {
        lat: parseFloat(latlng[0]),
        lng: parseFloat(latlng[1])
      },
      icon: {
        url: url,
        scaledSize: new google.maps.Size(30, 30)
      },
      zIndex: zIndex,
      label: '',
      map: this.map,
    });
    return marker;
  }

  drawLines() {
    this.zoom = 14;
    this.routeColorsIndex = 0;
    this.geoHistoryData = [];
    this.geoHistoryDataFull = [];
    this.polyline ? this.polyline.setMap(null) : null;
    this.infowindow = new google.maps.InfoWindow();
    let tripCheck = false;
    let color = this.routeColors[9];
    let lineSymbol = {
      path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
      strokeColor: '#fff',
      strokeWeight: 3,
      fillColor: '#00B3FD',
      fillOpacity: 1,
    };
    this.setPinToPoint();
    this.setStartingAndEndingPointToTrip();
    this.geoHistoryData = [];
    this.displayRouteTripsOnMap();
    this.polyline = new google.maps.Polyline({
      path: this.geoHistoryData,
      icons: [{
        icon: lineSymbol,
        offset: '100%'
      }],
      strokeColor: 'transparent',
      strokeWeight: 5,
      strokeOpacity: 0.001,
      map: this.map
    });

    this.borderPolyline = new google.maps.Polyline({
      path: this.geoHistoryData,
      strokeColor: color, // border color
      strokeOpacity: 0.0,
      strokeWeight: 20, // You can change the border weight here
      map: this.map
    });

    this.animateCircle(this.polyline);

    google.maps.event.addListener(this.borderPolyline, 'mousemove', (event) => {
      if (event.va && event.va.target.parentNode.nextSibling && event.va.target.parentNode.nextSibling.className.indexOf('gmap-tooltip') === -1) {
        event.va.target.parentNode.nextSibling.className += ' gmap-tooltip';
      }
      let foundData = _.find(this.geoHistoryData, function (data) {
        if (Math.abs(data.lat - event.latLng.lat()) < 0.001 && Math.abs(data.lng - event.latLng.lng()) < 0.001) {
          return data;
        }
      });
      if (this.selectedInfoWindowIndex !== null) {
        this.infoWindowArray[this.selectedInfoWindowIndex].close();
        this.selectedInfoWindowIndex = null;
      }
      this.infowindow.close();
      if (foundData) {
        let occurrenceDate = this.convertDateToZone(foundData.occurrenceDate);
        this.infowindow.setContent(`<div class="hover-info">
        <div class="header">${occurrenceDate}</div>
        <div><strong>Speed</strong> :  ${foundData.speed}</div>
        <div><strong>Altitude</strong> :  ${foundData.altitude}</div>
        <div><strong>Direction</strong> :  ${foundData.direction}</div>
        </div>
        `);
        this.infowindow.setPosition(event.latLng);
        window.setTimeout(
          () => {
            this.infowindow.open(this.map, this.polyline);
          }, 100
        );

      }
    }, this);

    google.maps.event.addListener(this.borderPolyline, 'mouseout', (event) => {
      this.infowindow.close();
    });
    google.maps.event.clearListeners(this.infowindow, 'domready');
    this.customizedCssToInfoWindow(this.infowindow);
  }

  customizedCssToInfoWindow(data) {
    google.maps.event.addListener(data, 'domready', function () {
      // Reference to the DIV that wraps the bottom of infowindow
      var iwOuter = jQuery('.gm-style-iw');
      iwOuter.addClass('top');
      var iwBackground = iwOuter.prev();
      // Removes background shadow DIV
      iwBackground.children(':nth-child(2)').css({ 'display': 'none' });
      // Removes white background DIV
      iwBackground.children(':nth-child(4)').css({ 'display': 'none' });
      var iwCloseBtn = iwOuter.next();
      // Apply the desired effect to the close button
      iwCloseBtn.css({ opacity: '1', right: '90px', top: '4px', 'border-radius': '13px', 'box-shadow': 'rgb(105, 110, 121) 0px 0px 5px' });
      // If the content of infowindow not exceed the set maximum height, then the gradient is removed.
      if (jQuery('.iw-content').height() < 140) {
        jQuery('.iw-bottom-gradient').css({ display: 'none' });
      }
    });
  }


  displayRouteTripsOnMap() {
    let count = 0;
    let array = [];
    let startTime = Number(this.geoFencingService.startTime.split(':')[0]);
    let endTime = Number(this.geoFencingService.endTime.split(':')[0]);
    let startDate = this.geoFencingService.startDate;
    endTime = endTime === 23 ? 24 : endTime;
    let isNextDate = false;
    let refEndTime = endTime;
    if (endTime < startTime) {
      endTime = 24;
      isNextDate = true;
    }
    const bounds = new google.maps.LatLngBounds();
    this.geoDataA.forEach((element, index) => {
      let date1 = this.geoDataA[index - 1] ? moment(this.geoDataA[index - 1].occurrenceDate).format('MM/DD/YYYY') : moment(this.geoDataA[index].occurrenceDate).format('MM/DD/YYYY');
      let date2 = moment(this.geoDataA[index].occurrenceDate).format('MM/DD/YYYY');
      let time = Number(moment(this.geoDataA[index].occurrenceDate).format('HH'));
      element.params[0].value = typeof element.params[0].value === 'string' && element.params[0].value.toLowerCase() === 'nan' ? '' : element.params[0].value;
      element.lat = element.params[0].value.split(',')[0];
      element.lng = element.params[0].value.split(',')[1];
      if (date1 !== date2) {
        count++;
      }
      if ((time >= startTime && time < endTime) ||
        (Number(moment(this.geoDataA[index].occurrenceDate).format('mm')) === 0 && time === endTime) ||
        startTime === endTime ||
        (isNextDate && startDate !== date2 && time < refEndTime)) {
        if (array[count] === undefined)
          array[count] = []
        array[count].push(element);
      }
    });
    array.forEach((childArray, index) => {
      childArray.forEach((prm, index) => {
        prm.params[0].value = typeof prm.params[0].value === 'string' && prm.params[0].value.toLowerCase() === 'nan' ? '' : prm.params[0].value;
        const latlng = prm.params[0].value.split(',');
        bounds.extend({ lat: parseFloat(latlng[0]), lng: parseFloat(latlng[1]) });
        if (latlng[0]) {
          this.geoHistoryData.push({
            lat: parseFloat(latlng[0]),
            lng: parseFloat(latlng[1]),
            speed: latlng[4],
            direction: latlng[3],
            altitude: latlng[2],
            occurrenceDate: prm.occurrenceDate
          });
        }
        prm.params[0].value = typeof prm.params[0].value === 'string' && prm.params[0].value.toLowerCase() === 'nan' ? '' : prm.params[0].value;
        let startlatlng = prm.params[0].value.split(',');
        let starting = {
          lat: parseFloat(startlatlng[0]),
          lng: parseFloat(startlatlng[1])
        };
        let nextlatlng = childArray[index + 1] ? childArray[index + 1].params[0].value.split(',') : childArray[index].params[0].value.split(',');
        let ending = {
          lat: parseFloat(nextlatlng[0]),
          lng: parseFloat(nextlatlng[1])
        }
        let strokeColor = this.routeColors[prm.colorCode % 10];
        if (starting && ending) {
          let poly = new google.maps.Polyline({
            path: [starting, ending],
            strokeColor: strokeColor,
            strokeWeight: 5,
            zIndex: prm.colorCode,
            map: this.map
          });
          this.tripPolylines.push(poly);
        }
      });
    });
  }

  endingPointerInfo = (event) => {
    let eventKey = Object.keys(event);
    if (event[eventKey[1]].target.parentNode.nextSibling && event[eventKey[1]].target.parentNode.nextSibling.className.indexOf('gmap-tooltip') == -1) {
      event[eventKey[1]].target.parentNode.nextSibling.className += ' gmap-tooltip';
    }
    let startposition = JSON.parse(JSON.stringify(this.geoDataA[this.geoDataA.length - 1]));
    startposition.occurrenceDate = this.convertDateToZone(startposition.occurrenceDate);
    this.geoDataA[this.geoDataA.length - 1].params[0].value = typeof this.geoDataA[this.geoDataA.length - 1].params[0].value === 'string' && this.geoDataA[this.geoDataA.length - 1].params[0].value.toLowerCase() === 'nan' ? '' : this.geoDataA[this.geoDataA.length - 1].params[0].value;
    const startlatlng = this.geoDataA[this.geoDataA.length - 1].params[0].value.split(',');
    this.infowindow.close();
    this.infowindow.setContent(`<div class="hover-info">
    <div class="header">${moment(startposition.occurrenceDate).format('MM/DD/YYYY, hh:mm A')}</div>
        <div><strong>Latitude</strong> :  ${startlatlng[0]}</div>
        <div><strong>Longitude</strong> :  ${startlatlng[1]}</div>
        </div>
      `);
    this.infowindow.setPosition({
      lat: parseFloat(startlatlng[0]),
      lng: parseFloat(startlatlng[1])
    });
    this.infowindow.open(this.map);
  }

  convertDateToZone(date) {
    date = MomentTimezone.utc(date).format('MM/DD/YYYY, hh:mm A');
    let convertedTimezone = '';
    if (this.appService.userTimezone != null) {
      convertedTimezone = MomentTimezone.tz(date, this.appService.userTimezone).format('MM/DD/YYYY, hh:mm A');
    } else {
      convertedTimezone = MomentTimezone.utc(date).local().format('MM/DD/YYYY, hh:mm A');
    }
    return convertedTimezone;
  }

  startingPointerInfo = (event) => {
    let eventKey = Object.keys(event);
    if (event[eventKey[1]].target.parentNode.nextSibling && event[eventKey[1]].target.parentNode.nextSibling.className.indexOf('gmap-tooltip') == -1) {
      event[eventKey[1]].target.parentNode.nextSibling.className += ' gmap-tooltip';
    }
    let startposition = JSON.parse(JSON.stringify(this.geoDataA[0]));
    startposition.occurrenceDate = this.convertDateToZone(startposition.occurrenceDate);
    this.geoDataA[0].params[0].value = typeof this.geoDataA[0].params[0].value === 'string' && this.geoDataA[0].params[0].value.toLowerCase() === 'nan' ? '' : this.geoDataA[0].params[0].value;
    const endlatlng = this.geoDataA[0].params[0].value.split(',');
    this.infowindow.close();
    if (this.infoWindowArray[this.selectedInfoWindowIndex])
      this.infoWindowArray[this.selectedInfoWindowIndex].close();
    this.infowindow.setContent(`<div class="hover-info">
    <div class="header">${moment(startposition.occurrenceDate).format('MM/DD/YYYY, hh:mm A')}</div>
    <div><strong>Latitude</strong> :  ${endlatlng[0]}</div>
    <div><strong>Longitude</strong> :  ${endlatlng[1]}</div>
  </div>
  `);
    this.infowindow.setPosition({
      lat: parseFloat(endlatlng[0]),
      lng: parseFloat(endlatlng[1])
    });
    this.infowindow.open(this.map);
  }

  animateCircle(line) {
    this.zoom = 15;
    this.count = 0;
    //alarm
    const that = this;
    this.infoWindowArray = [];
    for (let i = 0; i < this.alarmDataA.length; i++) {
      const latlng = this.alarmDataA[i].params[0].location.split(',');
      let marker = new google.maps.Marker({
        position: new google.maps.LatLng(parseFloat(latlng[0]), parseFloat(latlng[1])),
        icon: {
          url: `assets/images/pinend.png`,
          scaledSize: new google.maps.Size(30, 30)
        },
        map: this.map
      });
      let name = this.alarmDataA[i].params[0].name;
      let date = this.convertDateToZone(this.alarmDataA[i].params[0].ts);
      let value = this.alarmDataA[i].params[0].value;
      google.maps.event.addListener(marker, 'click', (function (marker, index) {
        return function () {
          if (that.infowindow.close())
            that.infowindow
          if (that.infoWindowArray[that.selectedInfoWindowIndex])
            that.infoWindowArray[that.selectedInfoWindowIndex].close();
          that.selectedInfoWindowIndex = index;
          that.infoWindowArray[that.selectedInfoWindowIndex] = new google.maps.InfoWindow();
          that.infoWindowArray[that.selectedInfoWindowIndex].setContent(`<div class="hover-info">
          <div class="header header-red">${date}</div>
          <div><strong>Name</strong> :  ${name}</div>
          <div><strong>Value</strong> :  ${value}</div>
        `);
          google.maps.event.clearListeners(that.infoWindowArray[that.selectedInfoWindowIndex], 'domready');
          that.customizedCssToInfoWindow(that.infoWindowArray[that.selectedInfoWindowIndex]);
          that.infoWindowArray[that.selectedInfoWindowIndex].open(that.map, marker);
        }
      })(marker, i));
      this.gmarkers.push(marker);
    }
    setTimeout(() => {
      this.isPaused = true;
      this.geoHistoryData[1] && this.map.setCenter(this.geoHistoryData[1]);
    }, 55);
    this.moveIcon();
  }

  moveIcon() {
    this.mapInterval = window.setInterval(() => {
      if (!this.isPaused) {
        if (this.polyline) {
          this.setDefaultZoomLevelToPolyline();
          this.count = (this.count + 0.01) % 100;
          let icons = this.polyline.get('icons');
          icons[0].offset = (this.count) + '%';
          this.polyline.set('icons', icons);
          if (this.count.toFixed(2) == 99.99) {
            clearInterval(this.mapInterval);
            this.count = 0;
            icons[0].offset = (this.count) + '%';
            this.geoFencingService.tripComplete();
          }
        }
      }
    });
  }

  setDefaultZoomLevelToPolyline() {
    let bounds = new google.maps.LatLngBounds();
    this.polyline.getPath().forEach(function (e) {//can't do polyline.getPath()[i] because it's a MVCArray
      bounds.extend(e);
    });
    setTimeout(() => {
      this.map.fitBounds(bounds);
    }, 100);
  }


  drawingEventListen() {
    google.maps.event.addListener(this.drawingManager, 'overlaycomplete', (e) => {
      const newShape = e.overlay;
      newShape.type = e.type;
      if (e.type !== google.maps.drawing.OverlayType.MARKER) {
        // Switch back to non-drawing mode after drawing a shape.
        this.drawingManager.setDrawingMode(null);
        // Add an event listener that selects the newly-drawn shape when the user
        // mouses down on it.
        google.maps.event.addListener(newShape, 'click', (e) => {
          if (e.vertex !== undefined) {
            if (newShape.type === google.maps.drawing.OverlayType.POLYGON) {
              const path = newShape.getPaths().getAt(e.path);
              path.removeAt(e.vertex);
              if (path.length < 3) {
                newShape.setMap(null);
              }
            }
            if (newShape.type === google.maps.drawing.OverlayType.POLYLINE) {
              const path = newShape.getPath();
              path.removeAt(e.vertex);
              if (path.length < 2) {
                newShape.setMap(null);
              }
            }
          }
          this.setSelection(newShape);
        });

        google.maps.event.addListener(newShape, 'bounds_changed', (e) => {
          this.setSelection(newShape);
        });

        this.setSelection(newShape);
      } else {
        google.maps.event.addListener(newShape, 'click', () => {
          this.setSelection(newShape);
        });
        this.setSelection(newShape);
      }
    });

    google.maps.event.addListener(this.drawingManager, 'drawingmode_changed', () => {
      if (this.selectedShape) {
        if (this.selectedShape.type !== 'marker') {
          // this.selectedShape.setEditable(false);
          this.selectedShape.setMap(null);
        }
        this.selectedShape = null;
      }
    });

    google.maps.event.addListener(this.map, 'click', () => {
      if (this.selectedShape) {
        if (this.selectedShape.type !== 'marker') {
          this.selectedShape.setEditable(false);
        }
        this.selectedShape = null;
      }
    });
  }

  setSelection(shape) {
    if (shape.type !== 'marker') {
      this.clearSelection();
      shape.setEditable(true);
      // this.selectColor(shape.get('fillColor') || shape.get('strokeColor'));
    }
    this.selectedShape = shape;
    this.getMarkersInsideShape();
  }

  getMarkersInsideShape() {
    this.mapData = [];
    if (this.selectedShape) {
      this.availableMarkers.forEach((ast) => {
        if (this.selectedShape.type == Shape.CIRCLE) {
          this.checkInsideCircle(ast)
        } else if (this.selectedShape.type == Shape.POLYGON || this.selectedShape.type == Shape.POLYLINE) {
          this.checkInsidePoly(ast)
        }
      });
      this.geoFencingService.shapeChange(this.mapData);
    }
  }

  checkInsideCircle(ast) {
    if (this.isFloat(ast.latitude) && this.isFloat(ast.longitude)) {
      if (google.maps.geometry && google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(ast.latitude, ast.longitude), this.selectedShape.getCenter()) <= this.selectedShape.getRadius()) {
        this.mapData.push(ast);
      }
    }
  }

  checkInsidePoly(ast) {
    if (this.isFloat(ast.latitude) && this.isFloat(ast.longitude)) {
      if (google.maps.geometry && google.maps.geometry.poly.containsLocation(new google.maps.LatLng(ast.latitude, ast.longitude), this.selectedShape)) {
        this.mapData.push(ast);
      }
    }
  }



  clearSelection() {
    if (this.selectedShape) {
      if (this.selectedShape.type !== 'marker') {
        this.selectedShape.setEditable(false);
      }
      this.selectedShape = null;
    }
  }

  deleteSelectedShape() {
    if (this.selectedShape) {
      this.selectedShape.setMap(null);
      this.mapData = this.availableMarkers;
      this.geoFencingService.shapeChange(this.availableMarkers);
    }
  }

  getAssetType() {
    this.geoFencingService.getAssetType().subscribe(
      (data) => {

        let avilableaAssetType:AssetType[] = [];
        if (data) {
          data.forEach(assettype => {
            let model = new AssetType(assettype.name, assettype.status);
            avilableaAssetType.push(model);
          });
        }
        avilableaAssetType.forEach(data => {
          let name=this.sentenceToKey(data.name);
          this.translate.stream(name).subscribe(res => {
            if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
              this.avilableaAssetType.push({
                name: res,
                status: data.status
              });
            }
          });

        });





      }
    );
  }

  saveSelectedShape() {
    if (this.selectedShape) {
      switch (this.selectedShape.type) {
        case 'circle':
          this.newShape = {
            type: this.selectedShape.type,
            radius: this.selectedShape.getRadius(),
            path: [{ lat: this.selectedShape.getCenter().lat(), lng: this.selectedShape.getCenter().lng() }]
          }
          break;
        case 'polygon':
          this.newShape = {
            type: this.selectedShape.type,
            radius: null,
            path: this.getLatLngFromPath(this.selectedShape.getPaths().getArray()[0])
          }
          break;
        case 'polyline':
          this.newShape = {
            type: this.selectedShape.type,
            radius: null,
            path: this.getLatLngFromPath(this.selectedShape.getPath())
          }
          break;
        case 'rectangle':
          this.newShape = {
            type: this.selectedShape.type,
            radius: null,
            path: this.selectedShape.getBounds().toJSON()
          }
          break;
      }
    }
  }

  getLatLngFromPath(paths: any) {
    const latlng: any = [];
    paths.forEach((path, key) => {
      latlng.push({ lat: path.lat(), lng: path.lng() });
    });
    return latlng;
  }

  triggerResize() {
    if (google) {
      google.maps.event.trigger(this.map, 'resize');
    }
  }

  dateChanged(data: any[]) {
    this.sliderDate.startDate = data[0];
    this.sliderDate.endDate = data[1];
    this.dateRangeValue = data[2];
    this.geoFencingService.dateRange = this.dateRangeValue;
  }

  timeChanged(time: any) {
    this.sliderTime.startTime = time[0] > 10 ? time[0] + ':00' : `0${time[0]}:00`;
    this.sliderTime.endTime = time[1] > 10 ? time[1] + ':00' : `0${time[1]}:00`;
    this.timeRangeValue = time;
    this.geoFencingService.timeRange = this.timeRangeValue;
  }

  navigateDashboard(asset) {
    this.router.navigate([`operation/asset-dashboard/${asset.assetId}`]);
  }

  changeMinMax() {
    this.geoFencingService.applyFullscreen({ fullscreen: this.fullscreen, minimize: this.minimize });
    setTimeout(
      () => {
        window.dispatchEvent(new Event('resize'));
        if (this.mapData.length) {
          const bounds = new google.maps.LatLngBounds();
          this.mapData.forEach((ast) => {
            if (this.isFloat(ast.latitude) && this.isFloat(ast.longitude)) {
              bounds.extend({ lat: ast.latitude, lng: ast.longitude });
            }
          });
          if (this.map) {
            this.map.setCenter(bounds.getCenter());
            this.map.fitBounds(bounds);
          }
        }
      }, 1
    );

  }

  alarmClicked(marker) {
    this.router.navigate([`operation/alarm/${marker.assetId}/${marker.gwSNO}`]);
  }


  updateAssetPosition(data) {
    if (data) {
      const ind = _.indexOf(this.mapData, _.findWhere(this.mapData, { assetId: data.assetID }));
      const eventParams = data.event_param ? typeof data.event_param === 'string' ? JSON.parse(data.event_param)[0] : data.event_param[0] : {};
      if (ind > -1) {

        const lat = data.status === 'Geo' ? Math.round(parseFloat(data.latitude) * 10000) / 10000 : this.mapData[ind].latitude;
        const lng = data.status === 'Geo' ? Math.round(parseFloat(data.longitude) * 10000) / 10000 : this.mapData[ind].longitude;

        if (data.status === 'Geo') {
          // this.mapData[ind].latitude = Math.round(parseFloat(data.latitude) * 10000) / 10000;
          // this.mapData[ind].longitude = Math.round(parseFloat(data.longitude) * 10000) / 10000;
          this.moveMarkerLocation = [lat, lng];
          this.transition(this.moveMarkerLocation, this.mapData[ind]);
        }
        if (eventParams) {
          // this.mapData[ind].zIndex = data.status === 'Geo' ? 93 : 94;
          let date1 = moment(eventParams.TS).utc();
          let isAlarmOnPastDays = false;
          if (moment.duration((this.todayDate.diff(date1))).asDays() > 7) {
            let status = eventParams.sts ? eventParams.sts.split(',') : [];
            if (status) {
              if (status[1] && status[1] === '1') {
                isAlarmOnPastDays = true;
              }
            }
            eventParams.sts = 'Inactive';
          }
          this.checkAndUpdateAlarmIconMarker(ind, JSON.parse(JSON.stringify(eventParams)).sts);
          this.mapData[ind].zIndex = this.getStatus(JSON.parse(JSON.stringify(eventParams)).sts).map;
          const bounds = new google.maps.LatLngBounds();
          bounds.extend({ lat: lat, lng: lng });
          const data = this.getStatus(eventParams.sts);
          this.mapData[ind].status = data['asset'];
          this.mapData[ind]['isAlarm'] = !isAlarmOnPastDays ? data['isAlarm'] : true;
        }
      }
    }
  }

  checkAndUpdateAlarmIconMarker(ind, sts) {
    let imageName = `assets/icons/assettype/${this.mapData[ind].assetType.toLocaleLowerCase().split(' ').join('')}`;
    let status = sts ? sts.split(',') : [];
    if (status) {
      if (status[1] && status[1] === '1' || this.mapData[ind]['isAlarm']) {
        imageName = `${imageName}-alarm.svg`;
      } else {
        imageName = `${imageName}.svg`;
      }
    }
    this.mapData[ind].icon = new google.maps.MarkerImage(imageName,
      new google.maps.Size(71, 71),
      new google.maps.Point(0, 0),
      new google.maps.Point(17, 34),
      new google.maps.Size(35, 35));
  }

  getIcon(data) {
    let imageName = 'assets/icons/assettype/semitrailer.svg';
    if (data && data.assetType) {
      imageName = `assets/icons/assettype/${data.assetType.toLocaleLowerCase().split(' ').join('')}.svg`;
    }
    return new google.maps.MarkerImage(imageName,
      new google.maps.Size(71, 71),
      new google.maps.Point(0, 0),
      new google.maps.Point(17, 34),
      new google.maps.Size(35, 35));
  }

  isFloat(n) {
    return Number(n) === n && n % 1 !== 0;
  }

  clickedMarker(index) {
    this.selectedMarkerIndex = index;
    this.mapData.forEach((value, key) => {
      value.isPlaying = false;
      if (key !== index) {
        value.isOpen = false;
      } else {
        value.isOpen = true;
      }
    });
    this.geoFencingService.dateRange = [0, 0];
    this.geoFencingService.timeRange = [0, 2];
    this.geoFencingService.closeAllMarkerEvent.next();
    const bounds = new google.maps.LatLngBounds();
    bounds.extend({ lat: this.mapData[index].latitude, lng: this.mapData[index].longitude });
    if (this.map) {
      this.map.setCenter(bounds.getCenter());
      this.map.fitBounds(bounds);
    }
    if (this.map.getZoom() > 10) {
      this.map.setZoom(10);
    }
  }

  getStatus(sts = '') {
    let status = sts ? sts.split(',') : [];
    let result = { map: 91, asset: 1 };
    if (status) {
      if (status[1] && status[1] === '1') {
        result = this.getMappedStatus(status[0]);
        result['isAlarm'] = true;
      } else if (status[0]) {
        result = this.getMappedStatus(status[0]);
      }
    }
    return result;
  }

  getMappedStatus(status = '') {
    let mapNumber = 91;
    let assetNumber = 1;
    switch (status.toString().toLowerCase()) {
      case 'inactive':
        mapNumber = 90;
        assetNumber = 0;
        break;
      case 'stopped':
        mapNumber = 91;
        assetNumber = 1;
        break;
      case 'moving':
        mapNumber = 93;
        assetNumber = 3;
        break;
      case 'idle':
        mapNumber = 92;
        assetNumber = 2;
        break;
      default:
        mapNumber = 91;
        assetNumber = 1;
    }
    return { map: mapNumber, asset: assetNumber };
  }

  ngOnDestroy() {
    if (this.activeCompanyInfoSubscriber) {
      this.activeCompanyInfoSubscriber.unsubscribe();
    }
    this.resetAllMarkers();
  }

  resetAllMarkers() {
    this.startingMarkerList.forEach(element => {
      element.setMap(null);
    });
    this.endingMarkerList.forEach(element => {
      element.setMap(null);
    });
  }

  transition(result, marker) {
    if (result[0] != marker.latitude && result[1] != marker.longitude) {
      this.deltCount = 0;
      this.deltaLat = (result[0] - this.moveMarkerLocation[0]) / this.numDeltas;
      this.deltaLng = (result[1] - this.moveMarkerLocation[1]) / this.numDeltas;
      this.moveMarker(marker);
    }
  }

  moveMarker(marker) {
    this.moveMarkerLocation[0] += this.deltaLat;
    this.moveMarkerLocation[1] += this.deltaLng;
    if (this.deltCount != this.numDeltas && this.moveMarkerLocation[0] && this.moveMarkerLocation[1]) {
      this.deltCount++;
      marker.latitude = this.moveMarkerLocation[0];
      marker.longitude = this.moveMarkerLocation[1];
      setTimeout(this.moveMarker(marker), this.delay);
    }
  }

  playWithRoute(index) {
    this.mapData[index].isPlaying = !this.mapData[index].isPlaying;
    this.mapData[index].isPaused = true;
    this.selectedMarkerData = this.mapData[index];
    this.selectedMarkerData.selectedIndex = index;
    this.dateRangeValue = this.geoFencingService.dateRange;
    this.timeRangeValue = this.geoFencingService.timeRange;
  }

  pauseOrReplayAlarm() {
    this.selectedMarkerData.isPaused = !this.selectedMarkerData.isPaused;
    this.geoFencingService.playOrPauseApplied.next(this.selectedMarkerData.isPaused);
  }

  stopAlarmRoute() {
    this.isPlayWithRoute = false;
    this.stopAlarmdata.next(this.selectedMarkerData);
    setTimeout(() => {
      if (this.mapData[this.selectedMarkerData.selectedIndex])
        this.playWithRoute(this.selectedMarkerData.selectedIndex)
    }, 100);
  }
  sentenceToKey(str){
    let strr=str.toLowerCase().trim().split(/\s+/).join('_');
    return strr;

  }
}



// previous code for route display on map
// for (let i = 0; i < this.geoDataA.length - 1; i++) {
//   let date1 = this.geoDataA[i - 1] ? moment(this.geoDataA[i - 1].occurrenceDate).format('MM/DD/YYYY') : moment(this.geoDataA[i].occurrenceDate).format('MM/DD/YYYY');
//   let date2 = moment(this.geoDataA[i].occurrenceDate).format('MM/DD/YYYY');
//   const latlng = this.geoDataA[i].params[0].value.split(',');
//   bounds.extend({ lat: parseFloat(latlng[0]), lng: parseFloat(latlng[1]) });
//   if (latlng[0] && date1 === date2) {
//     this.geoHistoryData.push({
//       lat: parseFloat(latlng[0]),
//       lng: parseFloat(latlng[1]),
//       speed: latlng[4],
//       direction: latlng[3],
//       altitude: latlng[2]
//     });
//   }
//   let startlatlng = date1 === date2 ? this.geoDataA[i].params[0].value.split(',') : this.geoDataA[i - 1] ? this.geoDataA[i - 1].params[0].value.split(',') : this.geoDataA[i].params[0].value.split(',');
//   let starting = {
//     lat: parseFloat(startlatlng[0]),
//     lng: parseFloat(startlatlng[1])
//   };
//   let nextlatlng = date1 === date2 ? this.geoDataA[i + 1].params[0].value.split(',') : this.geoDataA[i + 1].params[0].value.split(',');
//   let ending = {
//     lat: parseFloat(nextlatlng[0]),
//     lng: parseFloat(nextlatlng[1])
//   }
//   let strokeColor = this.routeColors[this.geoDataA[i].colorCode % 10];
//   if (date1 !== date2) {
//     strokeColor = 'black';
//   }
//   if (starting && ending) {
//     let poly = new google.maps.Polyline({
//       path: [starting, ending],
//       strokeColor: strokeColor,
//       strokeWeight: 5,
//       zIndex: this.geoDataA[i].colorCode,
//       map: this.map
//     });
//     this.tripPolylines.push(poly);
//   }
// }
