import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {GeoFencingComponent} from "./geo-fencing.component";
const routes: Routes = [
  {
    path: '',
    component: GeoFencingComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GeoFencingRoutingModule {

}
