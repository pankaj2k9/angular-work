import { AfterViewChecked, Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { GeoFenceAsset } from '../../app-common';
import { GeoFencingMapComponent } from './geo-fencing-map';
import { GeoFencingListComponent } from './geo-fencing-list';
import { GeoFencingService } from './geo-fencing.service';
declare var $: any;

@Component({
  selector: 'app-geo-fencing',
  templateUrl: './geo-fencing.component.html',
  styleUrls: ['./geo-fencing.component.scss']
})
export class GeoFencingComponent implements OnInit {
  firstViewCheckedExecuted = false;
  public windowResizeData: object;
  public filter: any = {};
  public geoDataAvailable: any[];
  public assetFiltered: GeoFenceAsset[] = [];
  public fullscreen: any = {
    map: false,
    list: false,
  };
  public minimize: any = {
    map: false,
    list: false
  }
  @ViewChild(GeoFencingMapComponent) map: GeoFencingMapComponent;
  @ViewChild(GeoFencingListComponent) table: GeoFencingListComponent;

  public realtimeDataForMap: any = null;
  displayMarkerIndex: number;
  constructor(private geoFencingService: GeoFencingService) { }

  ngOnInit() {
    this.geoFencingService.minMaxApplied.subscribe(
      (res) => {
        this.fullscreen = res.fullscreen;
        // this.minimize = res.minimize;
      }
    )
  }

  applyFilter(filter: number) {
    this.filter = Object.assign({}, filter);
  }

  dataChanged(data: GeoFenceAsset[]) {
    this.geoFencingService.setMapData(data);
  }

  geoData($event) {
    this.map.geoData = $event;
  }

  action(data: { item: GeoFenceAsset, startDate: any, endDate: any, startTime: any, endTime: any }) {
    this.table.getAlarmData(data.item, data.startDate, data.endDate, data.startTime, data.endTime);
  }

  toggleExpand(data) {
    this.table.toggleExpand(data);
  }

  stopAlarmdata(data) {
    this.table.stopAlarmData(data);
  }

  realTimeData(data) {
    if (data) {
      this.realtimeDataForMap = data;
    }
  }
  displayMarkerInfo(index: number) {
    this.displayMarkerIndex = index;
  }
}
