import { BsMapService } from '../../shared-module/services/bsmap/bsmap.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../../core';
import { FormsModule } from '@angular/forms';

import { DataTableModule } from 'angular2-datatable';
import { GeoFencingComponent } from './geo-fencing.component';
import { GeoFencingMapComponent } from './geo-fencing-map/geo-fencing-map.component';
import { GeoFencingListComponent } from './geo-fencing-list/geo-fencing-list.component';
import { GeoFencingService } from './geo-fencing.service';
import { AppCommonModule } from '../../app-common';
import { LocationPipe } from "./location.pipe";
import { SharedModule } from "../../shared-module/shared-module.module";
import { GeoFencingRoutingModule } from "./geo-fencing-routing.module";
import {TranslateModule,TranslateLoader,TranslateCompiler,TranslateParser,MissingTranslationHandler} from "@ngx-translate/core";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {HttpLoaderFactory} from "../../app.module";


@NgModule({
  imports: [
    GeoFencingRoutingModule,
    CommonModule,
    FormsModule,
    CoreModule,
    DataTableModule,
    AppCommonModule,
    SharedModule,
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    GeoFencingComponent,
    GeoFencingMapComponent,
    GeoFencingListComponent,
    LocationPipe
  ],
  providers: [GeoFencingService, BsMapService]
})
export class GeoFencingModule { }
