import { TestBed, inject } from '@angular/core/testing';

import { GeoFencingService } from './geo-fencing.service';

describe('GeoFencingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GeoFencingService]
    });
  });

  it('should ...', inject([GeoFencingService], (service: GeoFencingService) => {
    expect(service).toBeTruthy();
  }));
});
