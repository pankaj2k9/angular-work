import { Injectable } from '@angular/core';
import { HttpService } from '../../shared-module';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { GeoFenceAsset } from '../../app-common';
import { AppService } from '../../../app';
import * as moment from 'moment';

@Injectable()
export class GeoFencingService {

  public filterApplied: Subject<Array<GeoFenceAsset>> = new Subject();
  public minMaxApplied: Subject<{ fullscreen, minimize }> = new Subject();
  public playOrPauseApplied: Subject<any> = new Subject();
  public oneTripCompleted: Subject<any> = new Subject();
  public closeAllMarkerEvent: Subject<any> = new Subject();
  public closeInfoWindows: Subject<any> = new Subject();
  public shapeChanged: Subject<any> = new Subject();
  public isFirstTimePlay: boolean = false;
  public dateRangeValue = [0, 0];
  public timeRangeValue = [0, 2];
  public isRouteFromMap: boolean = false;
  public startTime: string;
  public startDate: string;
  public endDate: string;
  public endTime: string;
  constructor(private httpService: HttpService, private appService: AppService) { }

  getFacility(companyId): Observable<any> {
    return this.httpService.get(`facility/api/facility/get/${companyId}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getAsset(companyId: string): Observable<any> {
    return this.httpService.get(`device/api/iot/Get/${companyId}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getGeoData(companyId, gwSno, assetId, startDate, endDate) {
    return this.httpService.get(`device/api/iot/GetGeoData?companyId=${companyId}&gwSno=${gwSno}&assetId=${assetId}&startDate=${startDate}&endDate=${endDate}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getGeoDataRange(companyId, gwSno, assetId, startDate, endDate, startHour: string, endHour: string) {
    return this.httpService.get(`device/api/iot/GetGeoDataRange?companyId=${companyId}&gwSno=${gwSno}&assetId=${assetId}&startDate=${startDate}&endDate=${endDate}&startHour=${startHour}&endHour=${endHour}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getGeofence(companyId) {
    return this.httpService.get(`device/api/iot/GetGeofences/${companyId}`)
      .map(res => {
        return res.json();
      })
      .catch(err => Observable.throw(err));
  }

  getAssetType(): Observable<any> {
    return this.httpService.get('device/api/iot/getassettype')
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  setMapData(data: GeoFenceAsset[]) {
    this.filterApplied.next(data);
  }

  getAlarmData(companyId: string, gwSno: string, assetId: string, startDate: string, endDate: string) {
    return this.httpService.get(`device/api/iot/GetAlarmData?companyId=${companyId}&gwSno=${gwSno}&assetId=${assetId}&startDate=${startDate}&endDate=${endDate}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getAlarmDataRange(companyId: string, gwSno: string, assetId: string, startDate: string, endDate: string, startHour: string, endHour: string) {
    return this.httpService.get(`device/api/iot/GetAlarmDataRange?companyId=${companyId}&gwSno=${gwSno}&assetId=${assetId}&startDate=${startDate}&endDate=${endDate}&startHour=${startHour}&endHour=${endHour}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  applyFullscreen(data) {
    this.minMaxApplied.next(data);
  }

  playOrPause(isPause) {
    this.playOrPauseApplied.next(isPause);
  }

  tripComplete() {
    this.oneTripCompleted.next(true);
  }

  shapeChange(data: GeoFenceAsset[]) {
    this.shapeChanged.next(data);
  }

  set dateRange(value) {
    this.dateRangeValue = value;
  }

  get dateRange() {
    return this.dateRangeValue;
  }

  set timeRange(value) {
    this.timeRangeValue = value;
  }

  get timeRange() {
    return this.timeRangeValue
  }

  getTodayDateAsPerTimeZone() {
    let todayDate = moment();
    if (this.appService.userTimezone != null) {
      todayDate = moment.tz(todayDate, this.appService.userTimezone);
      todayDate = moment(todayDate).utcOffset(todayDate.utcOffset()).utc();
    } else {
      todayDate = moment(todayDate).utc();
    }
    return todayDate;
  }

}
