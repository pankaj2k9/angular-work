import {Pipe, PipeTransform} from '@angular/core';
import {BsMapService} from '../../shared-module/services/bsmap/bsmap.service';

@Pipe({
  name: 'locationFromLatLang'
})

export class LocationPipe implements PipeTransform {
  public bsMap: BsMapService = new BsMapService();
  transform(input) {
    return new Promise((resolve, reject) => {
      if (input && input.lat && input.lang) {
        const addr = this.bsMap.getLocationInfo(input.lat, input.lang).subscribe((data) => {
          if (data.status !== 'error') {
            const locationString = data['city'] + ', ' + data['state'];
            resolve(locationString);
          }
        });
      }
    });
  }
}
