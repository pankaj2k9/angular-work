import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { IndustrialBuildingComponent } from './industrial-building.component';

const routes: Routes = [
    {
        path: '',
        component: IndustrialBuildingComponent
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class IndustrialBuildingRoutingModule {

}
