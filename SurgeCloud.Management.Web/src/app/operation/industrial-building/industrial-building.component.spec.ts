import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndustrialBuildingComponent } from './industrial-building.component';

describe('IndustrialBuildingComponent', () => {
  let component: IndustrialBuildingComponent;
  let fixture: ComponentFixture<IndustrialBuildingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndustrialBuildingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndustrialBuildingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
