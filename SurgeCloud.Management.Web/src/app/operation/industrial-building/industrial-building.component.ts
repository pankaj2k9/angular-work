import { Component, OnInit } from '@angular/core';
import {IndustrialServiceService} from './service/industrial-service.service'
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Router } from '@angular/router';
import { AppService } from './../../app.service';
import { Subscription } from 'rxjs/Rx';

@Component({
  selector: 'app-industrial-building',
  templateUrl: './industrial-building.component.html',
  styleUrls: ['./industrial-building.component.scss'],
  providers:[IndustrialServiceService]
})
export class IndustrialBuildingComponent implements OnInit {
  companyId:string;
  IndustrialBuildingModule
  constructor( public _router: Router,private _industrialServiceService:IndustrialServiceService,private appService: AppService) { 
    this.companyId = this.appService.getActiveCompany().companyId;
    this.appService.activeCompanyInfo.subscribe((company) => {
      if (company) {
        this.companyId = company.companyId;
      }
    });

  }
  arrayCompanyList = []
  companyDetailsdata;
  ngOnInit() {
    this.getAllBulding()
  }

/** get All buildings  */
   getAllBulding()
   {
    this._industrialServiceService.GetCustomConfigList(this.companyId).subscribe(
      response => { 
      console.log("_industrialServiceService response",response);
      this.arrayCompanyList = response;
      },
      error => {
        console.log(error);

      })

   }

   companyDetails(companyDes)
   {
    this.companyDetailsdata = companyDes;
    this._router.navigate(['/operation/person-counter'],{ queryParams: companyDes,skipLocationChange: true});
   }

   ngOnDestroy() { 

    this._industrialServiceService.companyDetails = this.companyDetailsdata;

   }

}
