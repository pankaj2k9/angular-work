import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BsMapService } from '../../shared-module/services/bsmap/bsmap.service';
import { CoreModule } from '../../core';
import { FormsModule } from '@angular/forms';
import { DataTableModule } from 'angular2-datatable';
import { AppCommonModule } from '../../app-common';
import { SharedModule } from "../../shared-module/shared-module.module";
import { IndustrialBuildingComponent } from './industrial-building.component';
import { IndustrialBuildingRoutingModule } from "./industrial-building-routing.module";
import { NgxChartsModule } from '@swimlane/ngx-charts';
import {TranslateModule,TranslateLoader,TranslateCompiler,TranslateParser,MissingTranslationHandler} from "@ngx-translate/core";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {HttpLoaderFactory} from "../../app.module";



@NgModule({
    imports: [
        IndustrialBuildingRoutingModule,
        SharedModule,
        CommonModule,
        FormsModule,
        CoreModule,
        DataTableModule,
        AppCommonModule,
        NgxChartsModule,
      HttpClientModule,
      TranslateModule.forChild({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
      })
    ],
    declarations: [IndustrialBuildingComponent],
    exports: [IndustrialBuildingComponent],
    providers: [BsMapService]

})
export class IndustrialBuildingModule { }
