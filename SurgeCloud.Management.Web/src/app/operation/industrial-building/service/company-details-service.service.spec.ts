import { TestBed, inject } from '@angular/core/testing';

import { CompanyDetailsServiceService } from './company-details-service.service';

describe('CompanyDetailsServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CompanyDetailsServiceService]
    });
  });

  it('should ...', inject([CompanyDetailsServiceService], (service: CompanyDetailsServiceService) => {
    expect(service).toBeTruthy();
  }));
});
