import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions,Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HttpService } from './../../../shared-module';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from '../../../../environments/environment';

@Injectable()
export class CompanyDetailsServiceService {
  constructor(public httpService: HttpService, public http: Http)
  { 

  }
  //device/api/iot/getrealtimedata?companyId=4eabb792-03f6-4d8f-b0c6-25f52bbc2c68&gwSno=R115LLA0055&assetId=101
/** get all buildings */
  public getCompanyDesc(companyID:string,gwSno:string,assetId:String): Observable<any> {
         return this.httpService.get("device/api/iot/getrealtimedata?companyId="+companyID+"&gwSno="+gwSno+"&assetId="+assetId)
        .map(res => res.json())
        .catch(err => Observable.throw(err));
  }




}
