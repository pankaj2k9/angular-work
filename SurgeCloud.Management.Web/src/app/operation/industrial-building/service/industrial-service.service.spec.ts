import { TestBed, inject } from '@angular/core/testing';

import { IndustrialServiceService } from './industrial-service.service';

describe('IndustrialServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IndustrialServiceService]
    });
  });

  it('should ...', inject([IndustrialServiceService], (service: IndustrialServiceService) => {
    expect(service).toBeTruthy();
  }));
});
