import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions,Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HttpService } from './../../../shared-module';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from '../../../../environments/environment';

@Injectable()
export class IndustrialServiceService {
   companyDetails;
   activeCompanyInfoSubscription: any;

  constructor(public httpService: HttpService, public http: Http)
  { 

  }

/** get all buildings */
  public GetCustomConfigList(companyID:string): Observable<any> {
         return this.httpService.get("device/api/iot/get/"+companyID+"?assetType=Industrial Building")
        .map(res => res.json())
        .catch(err => Observable.throw(err));
  }

}
