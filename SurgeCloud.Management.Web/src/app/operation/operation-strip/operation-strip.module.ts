import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OperationStripComponent } from './operation-strip.component';
import {TranslateModule,TranslateLoader,TranslateCompiler,TranslateParser,MissingTranslationHandler} from "@ngx-translate/core";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {HttpLoaderFactory} from "../../app.module";

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [OperationStripComponent],
  exports : [OperationStripComponent]
})
export class OperationStripModule { }
