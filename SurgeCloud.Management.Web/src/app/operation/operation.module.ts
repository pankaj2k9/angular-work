import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OperationRoutingModule } from './operation-routing.module';
import { OperationComponent } from './operation.component';
import { GeoFencingModule } from './geo-fencing';
import { OperationService } from './operation.service';
import { AssetDashboardModule } from './asset-dashboard/asset-dashboard.module';
import { AlarmConsoleModule } from './alarm-console/alarm-console.module';
import { OperationStripModule } from './operation-strip/operation-strip.module';
import { RoutingDispatchModule } from './routing-dispatch/routing-dispatch.module';
import { PersonCounterModule } from './person-counter/person-counter.module';

//import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { environment } from '../../environments/environment';
import { GeneratorMonitoringModule } from './generator-monitoring/generator-monitoring.module';
import { GeoFenceCreateModule } from './geo-fence-create';
import { RuleEngineModule } from './rule-engine/rule-engine.module';
import { CreateRouteComponent } from './create-route/create-route.component';
import { CreateRouteModule } from './create-route/create-route.module';
import { PayloadDataComponent } from './payload-data/payload-data.component';
import { PayLoadDataModule } from "./payload-data/payload-data.module";
import {TranslateModule} from "@ngx-translate/core";

//const config: SocketIoConfig = { url: environment.socket.alarm, options: {} };

@NgModule({
  imports: [
    CommonModule,
    OperationRoutingModule,
    GeoFencingModule,
    AssetDashboardModule,
    AlarmConsoleModule,
    RuleEngineModule,
    OperationStripModule,
    GeneratorMonitoringModule,
    GeoFenceCreateModule,
    CreateRouteModule,
    PayLoadDataModule,
    RoutingDispatchModule,
    //SocketIoModule.forRoot(config),
    TranslateModule
  ],
  declarations: [OperationComponent, PayloadDataComponent],
  providers: [],
  exports: [OperationComponent,TranslateModule],
  bootstrap: [OperationComponent]
})
export class OperationModule { }
