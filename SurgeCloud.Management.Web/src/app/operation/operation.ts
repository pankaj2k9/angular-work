export class GeofenceFilter {
    public event: string = '';
    public facility: string = '';
    public geofence: string = '';
    public utilities: number = 1;
    public vin: string = '';
    public location: string = '';
    public showZones: Boolean = false;
    public assetType: string = '';
}

export class GeoData {
    public values: any;
    public colorCode: any = 0;
    constructor(
        public gwSno: string,
        public assetId: string,
        public occurrenceDate: string,
        public dataType: string,
        public params: any[]
    ) {
        this.gwSno = gwSno;
        this.assetId = assetId;
        this.occurrenceDate = occurrenceDate;
        this.dataType = dataType;
        this.params = params;
        this.values = params[0] && params[0].value.split(',')
    }
}
