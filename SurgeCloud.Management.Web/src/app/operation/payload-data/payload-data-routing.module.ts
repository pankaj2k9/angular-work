import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {PayloadDataComponent} from "./payload-data.component";
const routes: Routes = [
  {
    path: '',
    component: PayloadDataComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PayLoadDataRoutingModule {

}
