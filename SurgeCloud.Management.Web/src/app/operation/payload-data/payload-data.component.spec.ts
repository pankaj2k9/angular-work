import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayloadDataComponent } from './payload-data.component';

describe('PayloadDataComponent', () => {
  let component: PayloadDataComponent;
  let fixture: ComponentFixture<PayloadDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayloadDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayloadDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
