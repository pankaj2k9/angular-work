import {Component, OnInit, SimpleChanges} from '@angular/core';
import {PayLoadDataService} from "./payload-data.service";
import {ActivatedRoute} from "@angular/router";
import * as moment from 'moment';
import {AppService} from "../../app.service";
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import {NgForm, FormGroup} from "@angular/forms";


@Component({
  selector: 'app-payload-data',
  templateUrl: './payload-data.component.html',
  styleUrls: ['./payload-data.component.scss']
})
export class PayloadDataComponent implements OnInit {
  public apiUrl:any[] = [];
  public apiData:any[] = [];
  public apiDataa:any[] = [];
  public apiDataaRow:any[] = [];
  public props:any[] = [];
  public apiDataExp:any[]=[];
  public gwSno: string="R115LLA0072";
  public assetId: string = "U18DHP";
  public type:string;
  public asset:any;
  public startDate:string="";
  public endDate:string="";
  public isColumn:boolean=true;
  companyId: string;
  assets:any;
  assetID:any;
  public activeCompanyInfoSubscription: any;
  public routerParam: any;
  private paginationInfo = {
    sortOrder: 'asc',
    sortBy: 'tripStartTime',
    numberRowPerPage: 10
  };

  todayDate: Date = new Date();
  private myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'mm/dd/yyyy',
  };

  public headers:any[] = [];
  constructor(private appService: AppService, private payLoadDataService: PayLoadDataService, private route: ActivatedRoute,) { }

  ngOnInit() {
    if(this.companyId){
      this.getPayloadUrl();
    }
    this.routerParam = this.route.params.subscribe(params => {
      if (this.appService.getActiveCompany()) {
        this.companyId = this.appService.getActiveCompany().companyId;
        this.getPayloadUrl();
      }
    });

    this.activeCompanyInfoSubscription = this.appService.activeCompanyInfo.subscribe(
      (company) => {
        this.companyId = this.appService.getActiveCompany().companyId;
        this.getPayloadUrl();
      }
    );


    }

  ngOnChanges(changes: SimpleChanges) {
  }
  ngOnDestroy() {
     this.activeCompanyInfoSubscription.unsubscribe();
     this.routerParam.unsubscribe();
  }

  getPayloadUrl(){
    this.type="";
    this.apiData=[];
    this.apiDataa=[];
    this.apiDataExp=[];
    this.apiDataaRow=[];
    this.props=[];
    this.payLoadDataService.getPayLoadUrl().subscribe(response => {
         this.apiUrl=response;
         this.type=this.apiUrl[1].name;
      });
    this.payLoadDataService.getAsset(this.companyId).subscribe(response => {
         this.assets=response;
         this.asset=this.assets[0];
         this.assetID=this.asset.assetId;
         if(this.type){
           this.getPayLoadData();
         }

    });


    }
  setType(type:string) {
    this.type=type;
    this.apiData=[];
    this.apiDataa=[];
    this.apiDataExp=[];
    this.apiDataaRow=[];
    this.props=[];
    this.startDate="";
    this.endDate="";
    this.headers=[];
    this.getPayLoadData();
  }
  setAsset(assetId:string) {
    let assets= this.assets.filter(function( obj ) {
      return obj.assetId == assetId;
    }).map(function( obj ) {
      return obj;
    });
    this.asset=assets[0];
    this.apiData=[];
    this.apiDataa=[];
    this.apiDataExp=[];
    this.apiDataaRow=[];
    this.props=[];
    this.headers=[];
    this.startDate="";
    this.endDate="";
    this.getPayLoadData();
  }

    getPayLoadData(){
      this.payLoadDataService.getPayLoadData(this.type,this.asset.assetId,this.asset.gwSNO, this.startDate, this.endDate).subscribe(response => {
        let responsee  =response;
        responsee.forEach(data => {
          this.apiData.push({gwSNO: data.gwSNO,assetId: data.assetId,param: JSON.parse(data.param)});
          let dataParam =JSON.parse(data.param);
          let tempData:any={};
          let tempDataRow:any={};
          tempData.gwSNO=data.gw_sno;
          tempData.assetId=data.assetId;
          tempDataRow.gwSNO=data.gw_sno;
          tempDataRow.assetId=data.assetId;
          tempDataRow.param={};
          let property;
          let propertyrow;
          let properties:any[]=[];
          let paramData="";
          let paramDataExp="";
          for ( property in dataParam ) {
            if(property.includes("Time") || property.includes("TS")){
              let formatedValue=this.getReadableDate(dataParam[property]);
              tempData[property]=formatedValue;
            } else{
              let isValid = /^[0-9.]*$/.test(dataParam[property]);
              if(isValid && dataParam[property].trim().length){
                let roundVal=parseFloat(dataParam[property]).toFixed(3);
                let roundVall=roundVal.toString();
                let roundValll=parseFloat(roundVall);
                tempData[property]=roundValll;
              } else{
                tempData[property]=dataParam[property];
              }

            }

          }

          for ( propertyrow in dataParam ) {
            if(propertyrow.includes("TS")){
              let formatedValue=this.getReadableDate(dataParam[propertyrow]);
              tempDataRow[propertyrow]=formatedValue;
            } else if(propertyrow.includes("Time")){
              let formatedValue=this.getReadableDate(dataParam[propertyrow]);
              tempDataRow.param[propertyrow]=formatedValue;
            } else{
              let isValid = /^[0-9.]*$/.test(dataParam[propertyrow]);
              if(isValid && dataParam[propertyrow].trim().length){
                let roundVal=parseFloat(dataParam[propertyrow]).toFixed(3);
                let roundVall=roundVal.toString();
                let roundValll=parseFloat(roundVall);
                tempDataRow.param[propertyrow]=roundValll;
              } else{
                tempDataRow.param[propertyrow]=dataParam[propertyrow];
              }

            }

          }




          this.apiDataa.push(tempData);
          this.apiDataaRow.push(tempDataRow);
          this.apiDataExp.push({gwSNO: data.gw_sno,assetId: data.assetId,param: paramDataExp});
        });
        let apiDataProps=this.apiDataa[0];
        let property;
       if(apiDataProps){
          let expColumnWidth=600/(Object.keys(apiDataProps).length);
          for ( property in apiDataProps ) {
            if(property.includes("gwSNO") || property.includes("assetId")){
              this.props.push({prop: property, propName:property});
              this.headers.push({title: property, dataKey:property, width:expColumnWidth});
            } else if(property.includes("_") && property.charAt(0) === "_") {
              property = property.slice( 1 );
              this.props.push({prop: property, propName:this.camelToNormal(property)});
              this.headers.push({title: this.camelToNormal(property), dataKey:property, width:expColumnWidth});
            } else{
              this.props.push({prop: property, propName:this.camelToNormal(property)});
              this.headers.push({title: this.camelToNormal(property), dataKey:property, width:expColumnWidth});
            }

           }
        }

        //console.log(this.props);
        //console.log(this.apiDataa);
        console.log(this.apiDataaRow);


      });

    }

    getReadableDate(element){
      if (this.appService.userTimezone != null) {
        element = moment.tz(element, this.appService.userTimezone).format('MM/DD/YYYY, hh:mm A');
        return element;
      } else {
        element = moment.utc(element).local().format('MM/DD/YYYY, hh:mm A');
        return element;
      }


    }
  onSubmit(form: NgForm){
     this.startDate=form.value.startDate.formatted;
     this.endDate=form.value.endDate.formatted;
     this.apiData=[];
     this.apiDataa=[];
     this.apiDataExp=[];
     this.apiDataaRow=[];
     this.props=[];
     this.headers=[];
     this.getPayLoadData();
    if (form.valid) {
      form.form.reset();
    }
  }
  camelToNormal(camelCase){
    return camelCase
    // inject space before the upper case letters
      .replace(/([A-Z])/g, function(match) {
        return " " + match;
      })
      // replace first char with upper case
      .replace(/^./, function(match) {
        return match.toUpperCase();
      });

  }
  isColumnFn(rc){
    if(rc === "column"){
       this.isColumn=true;
    } else if(rc === "row"){
      this.isColumn=false;
    }

  }


}
