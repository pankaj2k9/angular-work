import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PayloadDataComponent} from "./payload-data.component";
import {AppCommonModule} from "../../app-common/app-common.module";
import {PayLoadDataRoutingModule} from "./payload-data-routing.module";
import { DataTableModule } from 'angular2-datatable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {CoreModule} from "../../core/core.module";
import { SqueezeBoxModule } from 'squeezebox';
import {PayLoadDataService} from "./payload-data.service";
import {SharedModule} from "../../shared-module/shared-module.module";
import {MyDatePickerModule} from "mydatepicker";
import {TranslateModule,TranslateLoader,TranslateCompiler,TranslateParser,MissingTranslationHandler} from "@ngx-translate/core";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {HttpLoaderFactory} from "../../app.module";

@NgModule({
  imports: [
    CommonModule,
    AppCommonModule,
    DataTableModule,
    FormsModule,
    ReactiveFormsModule,
    MyDatePickerModule,
    CoreModule,
    SqueezeBoxModule,
    PayLoadDataRoutingModule,
    SharedModule,
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })

  ],
  declarations: [PayloadDataComponent],
  providers: [PayLoadDataService],
  exports: [PayloadDataComponent]
})
export class PayLoadDataModule { }
