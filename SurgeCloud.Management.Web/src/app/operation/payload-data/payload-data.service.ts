import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http'
import { HttpService } from '../../shared-module';
import { Observable } from 'rxjs/Observable';
import { AppService } from '../../app.service';
import * as moment from 'moment';
import { Router } from '@angular/router';
@Injectable()
export class PayLoadDataService {


  constructor(private httpService: HttpService, private appService: AppService, private router: Router) {

  }

  getAsset(companyId: string): Observable<any>{
      return this.httpService.get(`device/api/iot/get/${companyId}?assetType=`)
        .map(res => res.json())
        .catch(err => Observable.throw(err));
  }

  getPayLoadUrl(){
    return this.httpService.get(`device/api/iot/GetPayload`).map((res) => {
      return res.json();
    })
  }

  getPayLoadData(type:string , assetId: string, gwSno: string, startDate: string, endDate: string){
     const url = `device/api/iot/GetControllerDataByType?type=${type}&assetId=${assetId}&gwsno=${gwSno}&startDate=${startDate}&endDate=${endDate}`;
     return this.httpService.get(encodeURI(url)).map((res) => {
       return res.json();
     })
  }
}
