import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PersonCounterComponent } from './person-counter.component';

const routes: Routes = [
    {
        path: '',
        component: PersonCounterComponent
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PersonCounterRoutingModule {

}
