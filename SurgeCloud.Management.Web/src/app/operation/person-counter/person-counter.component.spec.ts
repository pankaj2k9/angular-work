import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonCounterComponent } from './person-counter.component';

describe('PersonCounterComponent', () => {
  let component: PersonCounterComponent;
  let fixture: ComponentFixture<PersonCounterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonCounterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonCounterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
