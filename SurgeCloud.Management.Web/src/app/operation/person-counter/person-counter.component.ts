import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {CompanyDetailsServiceService} from '../industrial-building/service/company-details-service.service'
import { AppService } from './../../app.service';
import { Subscription } from 'rxjs/Rx';

declare var jQuery: any;   // not required
declare var $: any;

@Component({
  selector: 'app-person-counter',
  templateUrl: './person-counter.component.html',
  styleUrls: ['./person-counter.component.scss'],
  host: {
    '(window:resize)': 'onResize()'
  },
  providers:[CompanyDetailsServiceService]
})
export class PersonCounterComponent implements OnInit {

  public single: any[];
  public multi: any[];


  // options
  public showXAxis = true;
  public showYAxis = true;
  public gradient = false;
  public showLegend = true;
  public showXAxisLabel = true;
  public xAxisLabel = 'Time';
  public showYAxisLabel = true;
  public yAxisLabel = 'Count';

  public colorScheme = {
    domain: ['#5AA454', '#5AA454', '#5AA454', '#5AA454', '#5AA454', '#5AA454', '#5AA454', '#5AA454']
  };

  public barChartData = [
    {
      "name": "1:00",
      "value": 67
    },
    {
      "name": "2:00",
      "value": 34
    },
    {
      "name": "3:00",
      "value": 90
    },
    {
      "name": "4:00",
      "value": 45
    },
    {
      "name": "5:00",
      "value": 36
    },
    {
      "name": "6:00",
      "value": 68
    },
    {
      "name": "7:00",
      "value": 25
    },
    {
      "name": "8:00",
      "value": 55
    },
    {
      "name": "9:00",
      "value": 46
    },
    {
      "name": "10:00",
      "value": 40
    },
    {
      "name": "11:00",
      "value": 56
    },
    {
      "name": "12:00",
      "value": 48
    }
  ]

  public view: any[] = [300, 200];

  // Refrigerator barChart
  public refrigeratorBarChartData = [
    {
      "name": "1:00",
      "series": [
        {
          "name": "Temperature",
          "value": 50
        },
        {
          "name": "Humidity",
          "value": 45
        }
      ]
    },

    {
      "name": "2:00",
      "series": [
        {
          "name": "Temperature",
          "value": 60
        },
        {
          "name": "Humidity",
          "value": 45
        }
      ]
    },
    {
      "name": "3:00",
      "series": [
        {
          "name": "Temperature",
          "value": 33
        },
        {
          "name": "Humidity",
          "value": 65
        }
      ]
    },
    {
      "name": "4:00",
      "series": [
        {
          "name": "Temperature",
          "value": 48
        },
        {
          "name": "Humidity",
          "value": 95
        }
      ]
    },
    {
      "name": "5:00",
      "series": [
        {
          "name": "Temperature",
          "value": 58
        },
        {
          "name": "Humidity",
          "value": 56
        }
      ]
    },
  ]

  public userRatting = {
    total: 1799,
    positive: 1541,
    happy: 125,
    average: 50,
    negative: 83,
    current: "positive"
  }


  companyId:string;
  companyDetailData;
  storeData;
  peopleCount = "0";
  LastHourCount ="0";
  currentWeekCount = "0";
  montlyVisitorCount = "0";
  todayVisitorCount = "0";
  constructor(private elementRef: ElementRef ,public router:ActivatedRoute,private _companyDetailsServiceService:CompanyDetailsServiceService) { 
    //this.companyDetailData = industrialServiceService.companyDetails;
  }

  ngOnInit() {
    this.getScale();
    /** get company details*/
    this.router
    .queryParams
    .subscribe(params => {
      // Defaults to 0 if no query param provided.
      console.log("page number" +params);
      this.companyDetailData = params;
    });
    this.getStoreData()
  }

  /** get store data to show count */
  getStoreData()
  {
   this._companyDetailsServiceService.getCompanyDesc(this.companyDetailData.companyId,this.companyDetailData.gwSNO,this.companyDetailData.assetId).subscribe(
     response => { 
     console.log("_companyDetailsServiceService response",response);
     this.storeData = response;
     this.setPeopleCountByName()
     },
     error => {
       console.log(error);

     })
    // setTimeout(() => { console.log(); }, 10); 
  }

 /** get live people count and filter it out  */
  setPeopleCountByName()
  {
    if (this.storeData.length > 0)
    {
      let params = this.storeData[0]["param"];
      this.peopleCount = this.getObjectFilterByName(params,"In Count");
      this.todayVisitorCount = this.getObjectFilterByName(params,"Today's Visitor Count");
      this.currentWeekCount = this.getObjectFilterByName(params,"Current Week Visitor Count");
      this.montlyVisitorCount = this.getObjectFilterByName(params,"Current Month Visitor Count");
      this.LastHourCount = this.getObjectFilterByName(params,"Last Hour Visitor Count");

    }
  
  }
 /** filter it out  */
 getObjectFilterByName(arrayObject,name)
 {
    let arrayOfObject = arrayObject.filter((item, index) => item.name === name);
    if (arrayOfObject.length > 0)
    {
     let value = arrayOfObject[0]["value"];
     return value;
    }
  return "0";
 }

  onResize() {
    this.getScale();
  }

  getScale() {
    // let scale = document.getElementsByClassName("inner-view")[0].clientWidth / 1000;
    // $('.inner-view .image').css('transform', 'scale(' + scale + ')');
    // let scale1 = 300 / ($('.inner-view .pop-ups-container').outerWidth() * scale);
    // $('.inner-view .pop-ups-container').css('transform', 'scale(' + scale1 + ')');
    // $('.inner-view .objectRefrigerator').css('transform', 'scale(' + scale1 + ')');
  }

  updateRatting(type) {
    this.userRatting[this.userRatting.current] = this.userRatting[this.userRatting.current] - 1;
    this.userRatting[type] = this.userRatting[type] + 1;
    this.userRatting.current = type
  }
}
