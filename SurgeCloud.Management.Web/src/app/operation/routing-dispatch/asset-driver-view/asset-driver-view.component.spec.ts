import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetDriverViewComponent } from './asset-driver-view.component';

describe('AssetDriverViewComponent', () => {
  let component: AssetDriverViewComponent;
  let fixture: ComponentFixture<AssetDriverViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetDriverViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetDriverViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
