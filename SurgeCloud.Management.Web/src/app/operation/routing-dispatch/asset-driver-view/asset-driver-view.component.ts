import { Component, ViewEncapsulation, OnInit, Input, ElementRef, NgZone } from '@angular/core';
import { RoutingDispatchService } from "../routing-dispatch.service";
import { AppService } from '../../../app.service';
import * as jQuery from 'jquery';
import { select } from 'd3';


@Component({
  selector: 'app-asset-driver-view',
  templateUrl: './asset-driver-view.component.html',
  styleUrls: ['./asset-driver-view.component.scss']
})
export class AssetDriverViewComponent implements OnInit {

  @Input() currentActivity: any;
  public routes: any = [];
  public routesForDriver: any = [];
  public routesForAsset: any = [];
  public assets: any = [];
  public companyInfo: any = {};
  public currentView: any = {
    driver: false,
    asset: true
  }
  public drivers = [
    {
      driverId: 1,
      driverName: "Sanjay"
    },
    {
      driverId: 2,
      driverName: "Ajay"
    },
    {
      driverId: 3,
      driverName: "Sandip"
    },
    {
      driverId: 4,
      driverName: "Sanjay bhai"
    },
    {
      driverId: 5,
      driverName: "Ajay bhai"
    },
    {
      driverId: 6,
      driverName: "Sandip bhai"
    }
  ]

  constructor(private routingDispatchService: RoutingDispatchService, private appService: AppService) { }

  ngOnInit() {
    this.routingDispatchService.routes.subscribe((routes: any) => {
      this.routesForDriver = JSON.parse(JSON.stringify(routes));
      this.routesForAsset = JSON.parse(JSON.stringify(routes));
      console.log(this.routesForDriver, this.routesForAsset);
    });

    this.appService.activeCompanyInfo.subscribe((company) => {
      //get company info
      this.companyInfo = this.appService.getActiveCompany();

      //get all assetes
      this.routingDispatchService.getAsset(this.companyInfo.companyId).subscribe((data) => {
        this.assets = data;
        this.assets.forEach((asset, index) => {
          asset['id'] = index;
          asset['capacity'] = Math.floor(Math.random() * 100) + 1;
        });
        //width of div-table
        jQuery('.viewScroll.asset .div-table').width((this.assets.length + 1) * 140);
        jQuery('.viewScroll.driver .div-table').width((this.drivers.length + 1) * 140);
        console.log(this.assets);
      }, (err) => {
        console.log(err);
      })

    });
  }

  changeView() {
    this.currentView = {
      driver: !this.currentView.driver,
      asset: !this.currentView.asset
    }

    jQuery('.viewScroll.asset .div-table').width((this.assets.length + 1) * 140);
    jQuery('.viewScroll.driver .div-table').width((this.drivers.length + 1) * 140);
  }


  assignDriver(event, routeIndex, assetIndex) {
    this.drivers.forEach(driver => {
      if (driver['selectedFor'] == this.routesForAsset[routeIndex].routeId) {
        delete driver['selectedFor'];
        delete driver['selected'];
      }
      if (driver.driverName === event.target.value) {
        driver["selected"] = true;
        driver["selectedFor"] = this.routesForAsset[routeIndex].routeId;

        this.routesForAsset[routeIndex]['assignDriverName'] = driver.driverName;
      }
    });

    if (event.target.value == 'none') {
      delete this.routesForAsset[routeIndex]['assign'];
      delete this.routesForAsset[routeIndex]['assignAssetId'];
      delete this.routesForAsset[routeIndex]['assignAssetGwSNO'];
      delete this.routesForAsset[routeIndex]['assignDriverName'];

      delete this.assets[assetIndex]['assign'];
      delete this.assets[assetIndex]['assignRouteId'];

    } else {
      this.routesForAsset[routeIndex]['assign'] = true;
      this.routesForAsset[routeIndex]['assignAssetId'] = this.assets[assetIndex].aId;
      this.routesForAsset[routeIndex]['assignAssetGwSNO'] = this.assets[assetIndex].gwSNO;

      this.assets[assetIndex]['assign'] = true;
      this.assets[assetIndex]['assignRouteId'] = this.routesForAsset[routeIndex].routeId;
    }

    console.log(this.routesForAsset);
  }


  assignAsset(event, routeIndex, driverIndex) {
    this.assets.forEach(asset => {
      if (asset['selectedFor'] == this.routesForDriver[routeIndex].routeId) {
        delete asset['selectedFor'];
        delete asset['selected'];
      }
      if (asset.aId === event.target.value) {
        asset["selected"] = true;
        asset["selectedFor"] = this.routesForDriver[routeIndex].routeId;
        this.routesForDriver[routeIndex]['assignAssetId'] = asset.aId;
        this.routesForDriver[routeIndex]['assignAssetGwSNO'] = asset.gwSNO;
      }
    });

    if (event.target.value == 'none') {
      delete this.routesForDriver[routeIndex]['assign'];
      delete this.routesForDriver[routeIndex]['assignDriverId'];
      delete this.routesForDriver[routeIndex]['assignAssetId'];
      delete this.routesForDriver[routeIndex]['assignAssetGwSNO'];

      delete this.drivers[driverIndex]['assign'];
      delete this.drivers[driverIndex]['assignRouteId'];
    } else {
      this.routesForDriver[routeIndex]['assign'] = true;
      this.routesForDriver[routeIndex]['assignDriverId'] = this.drivers[driverIndex].driverId;
      this.routesForDriver[routeIndex]['assignDriverName'] = this.drivers[driverIndex].driverName;

      this.drivers[driverIndex]['assign'] = true;
      this.drivers[driverIndex]['assignRouteId'] = this.routesForDriver[routeIndex].routeId;
    }

  }

  saveRouteAssignment() {
    let routeAssignment = [];
    if (this.currentView.driver) {
      this.routesForDriver.forEach(route => {
        if (route.assign === true) {
          let data = {
            companyId: route.companyId,
            companyName: route.companyName,
            routeId: route.routeId,
            routeName: route.name,
            username: route.assignDriverName,
            assetId: route.assignAssetId,
            gwSNO: route.assignAssetGwSNO,
            status: 1
          }
          routeAssignment.push(data);
        }
      });
    } else {
      this.routesForAsset.forEach(route => {
        if (route.assign === true) {
          let data = {
            companyId: route.companyId,
            companyName: route.companyName,
            routeId: route.routeId,
            routeName: route.name,
            username: route.assignDriverName,
            assetId: route.assignAssetId,
            gwSNO: route.assignAssetGwSNO,
            status: 1
          }
          routeAssignment.push(data);
        }
      });
    }
    console.log(routeAssignment);
    this.routingDispatchService.manageRouteAssignment(routeAssignment).subscribe((data) => {
      console.log(data);
    }, (err) => {
      console.log(err);
    })
  }
}