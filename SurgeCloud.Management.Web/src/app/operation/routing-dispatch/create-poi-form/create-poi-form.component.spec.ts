import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePoiFormComponent } from './create-poi-form.component';

describe('CreatePoiFormComponent', () => {
  let component: CreatePoiFormComponent;
  let fixture: ComponentFixture<CreatePoiFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePoiFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePoiFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
