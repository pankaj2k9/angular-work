import { Component, ViewEncapsulation, OnInit, Input, ElementRef, NgZone } from '@angular/core';
import { MapsAPILoader, GoogleMapsAPIWrapper, CircleManager, AgmMarker, AgmMap, AgmCircle } from '@agm/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import * as moment from 'moment';
import { RoutingDispatchService } from "../routing-dispatch.service";
import { AppService } from '../../../app.service';
import * as _ from 'underscore';

@Component({
  selector: 'app-create-poi-form',
  templateUrl: './create-poi-form.component.html',
  styleUrls: ['./create-poi-form.component.scss']
})
export class CreatePoiFormComponent implements OnInit {

  @Input() currentActivity: any;
  public tabs = [
    {
      'tag': 'location_on',
      'name': 'Create Single POI',
      'img': 'assets/images/routing-dispatch/poi.png'
    }, {
      'tag': 'location_on',
      'name': 'Create Bulk POI',
      'img': 'assets/images/routing-dispatch/bulkpoi.png'
    }
  ];

  public currentTabs = [];
  public createPoiForm: any;
  public createPoiBulkForm: any;
  public geoFence = [];
  public selectedMarker = [];
  public samples: any = [];
  public poiMarker: any;
  public poiLocation: any = {};
  public useAsPoiLocation: any = {};
  public companyInfo: any = {};
  public userInfo: any = {};
  public autocompleteAddress: any = {};
  public createPoiData: any = {};
  public createBulkPoiData: any = [];
  public error: any = {
    poiLocation: false
  };
  public measurementCount = false;
  public selectedSample = [];
  public checkAllSample = false;

  constructor(private formBuilder: FormBuilder, private routingDispatchService: RoutingDispatchService, private mapsAPILoader: MapsAPILoader, private elementRef: ElementRef, private _zone: NgZone, private appService: AppService) {
    this.createPoiForm = this.formBuilder.group({
      'name': ['', [Validators.required]],
      'totalCapacity': ['', [Validators.required]],
      'waitingTime': ['', [Validators.required]],
      'tags': [''],
    });

    this.createPoiBulkForm = this.formBuilder.group({
      'totalCapacity': ['', [Validators.required]],
      'waitingTime': ['', [Validators.required]],
      'tags': [''],
    });
  }

  ngOnInit(): void {

    this.currentTabs[0] = true;

    this.routingDispatchService.unassignSample.subscribe((unassignSample) => {
      this.samples = unassignSample;
    });

    this.routingDispatchService.selectedMarker.subscribe((selectedMarker: Array<any>) => {
      this.selectedMarker = selectedMarker;
    })

    this.routingDispatchService.poiMarker.subscribe((poiMarker) => {
      this.poiMarker = poiMarker;
      this.autocompleteAddress = this.createAddressModel(this.poiMarker);
    })

    this.appService.activeCompanyInfo.subscribe((company) => {
      this.companyInfo = this.appService.getActiveCompany();
      this.appService.setLogInUserDetails().subscribe((userInfo) => {
        this.userInfo = userInfo;
      });
    });

    // get unassign sample
    this.routingDispatchService.unassignSample.subscribe((unassignSample) => {
      this.samples = unassignSample;
      if (this.samples.length > 0 && this.samples[0].measurement === "count") {
        this.measurementCount = true;
        this.createPoiForm.controls['totalCapacity'].setValidators(null);
        this.createPoiForm.controls['totalCapacity'].updateValueAndValidity();
        this.createPoiBulkForm.controls['totalCapacity'].setValidators(null);
        this.createPoiBulkForm.controls['totalCapacity'].updateValueAndValidity();
      }
    });
  }

  addPoints(index, event) {
    if (event.target.checked) {
      this.geoFence.push(this.samples[index]);
    } else {
      this.geoFence.splice(this.geoFence.indexOf(this.samples[index]), 1);
    }
  }
  removePoi(index) {
    this.selectedMarker.splice(index, 1);
  }

  createPoi() {
    if (this.createPoiForm.valid) {
      if (this.poiLocation.latitude && this.poiLocation.longitude) {

        //remove error for location
        this.error.poiLocation = false;

        // data for create poi
        if (this.measurementCount) {
          this.createPoiForm.controls['totalCapacity'].setValue(this.selectedMarker.length);
        }

        let data = Object.assign({}, this.createPoiForm.value);
        this.createPoiData = Object.assign({}, this.createPoiForm.value);
        this.createPoiData['address'] = this.poiLocation;
        this.createPoiData['tags'] = data.tags.length == 0 ? [] : data.tags.split(",");
        this.createPoiData['companyId'] = this.companyInfo.companyId;
        this.createPoiData['companyName'] = this.companyInfo.name;
        this.createPoiData['createdOn'] = moment().format('MM/DD/YYYY, hh:mm A');
        this.createPoiData['createdBy'] = this.userInfo.displayName;
        this.createPoiData['status'] = 1;

        //  post request for create poi
        this.routingDispatchService.createPoi([this.createPoiData]).subscribe((data) => {
          this.mapSamplePoi(data);
        }, (err) => {
          console.log(err);
        })
      } else {
        this.error.poiLocation = true;
      }
    } else {
      Object.keys(this.createPoiForm.controls).forEach(key => {
        this.createPoiForm.get(key).markAsDirty();
      });
    }
  }

  mapSamplePoi(poiData) {
    let mapSamplePoi = [];
    this.selectedMarker.forEach(marker => {
      mapSamplePoi.push({
        companyId: this.companyInfo.companyId,
        companyName: this.companyInfo.name,
        poiId: poiData.poiId,
        poiName: poiData.name,
        sampleId: marker.sampleId,
        sampleName: marker.name
      })
    });

    this.routingDispatchService.mapSamplePoi(mapSamplePoi).subscribe((data) => {
      this.clearAll();
    }, (err) => {
      console.log(err);
    })
  }

  changeActivity() {
    this.routingDispatchService.currentActivity.next(this.currentActivity);
  }

  useAsPoi(i, event) {
    this.selectedMarker.forEach((marker, index) => {
      if (event.target.checked) {
        if (i === index) {
          marker["useAsPoi"] = true;
          this.useAsPoiLocation = marker.address;
          this.poiLocation = marker.address;
        } else {
          marker["useAsPoi"] = false;
        }
      } else {
        marker["useAsPoi"] = false;
        this.useAsPoiLocation = {};
        this.poiLocation = this.autocompleteAddress;
      }
    });
  }

  clearAll() {
    this.selectedMarker = [];
    this.poiLocation = {};
    this.useAsPoiLocation = {};
    this.autocompleteAddress = {}
    this.createPoiForm.reset();
    this.routingDispatchService.clearMapCreatePoi.next();
  }

  useAsPoiBulk(i, event) {
    if (event.target.checked) {
      this.samples[i].selected = true;
      this.selectedSample.push(this.samples[i]);
    } else {
      this.checkAllSample = false;
      this.samples[i].selected = false;
      this.selectedSample.splice(this.selectedSample.indexOf(this.samples[i]), 1);
    }
  }

  allUseAsPoi(i, event) {
    if (event.target.checked) {
      this.samples.forEach(sample => {
        sample.selected = true;
      });
      this.checkAllSample = true;
      this.selectedSample = Object.assign([], this.samples);
    } else {
      this.samples.forEach(sample => {
        sample.selected = false;
      });
      this.checkAllSample = false;
      this.selectedSample = [];
    }
  }

  createSampleAsPoi() {
    if (this.createPoiBulkForm.valid) {
      this.createBulkPoiData = [];
      this.selectedSample.forEach(sample => {
        if (this.measurementCount) {
          this.createPoiBulkForm.controls['totalCapacity'].setValue(1);
        }
        let formData = Object.assign({}, this.createPoiBulkForm.value);
        let data: any = {};
        data = Object.assign({}, this.createPoiBulkForm.value);
        data['name'] = sample.name;
        data['address'] = sample.address;
        data['tags'] = formData.tags.length == 0 ? [] : formData.tags.split(",");
        data['tags'] = formData.tags.length == 0 ? [] : formData.tags.split(",");
        data['companyId'] = this.companyInfo.companyId;
        data['companyName'] = this.companyInfo.name;
        data['createdOn'] = moment().format('MM/DD/YYYY, hh:mm A');
        data['updatedOn'] = moment().format('MM/DD/YYYY, hh:mm A');
        data['createdBy'] = this.userInfo.displayName;
        data['updatedBy'] = this.userInfo.displayName;
        data['status'] = 1;
        this.createBulkPoiData.push(data);
      });
    } else {
      Object.keys(this.createPoiBulkForm.controls).forEach(key => {
        this.createPoiBulkForm.get(key).markAsDirty();
      });
    }
  }

  createAddressModel(autocompleteAddress) {
    let address = {};
    address = {
      "name": autocompleteAddress.name,
      "formattedAddress": autocompleteAddress.formatted_address,
      "latitude": autocompleteAddress.geometry.location.lat(),
      "longitude": autocompleteAddress.geometry.location.lng()
    }

    autocompleteAddress.address_components.forEach(address_component => {
      if (address_component.types.indexOf('street_number') === 0) {
        address['street1'] = address_component.short_name;
      }

      if (address_component.types.indexOf('route') === 0) {
        address['street2'] = address_component.short_name;
      }

      if (address_component.types.indexOf('administrative_area_level_2') === 0) {
        address['city'] = address_component.short_name;
      }

      if (address_component.types.indexOf('administrative_area_level_1') === 0) {
        address['stateCode'] = address_component.short_name;
      }

      if (address_component.types.indexOf('country') === 0) {
        address['countryCode'] = address_component.short_name;
      }

      if (address_component.types.indexOf('postal_code') === 0) {
        address['postcode'] = address_component.short_name;
      }
    });

    return address;
  }

}
