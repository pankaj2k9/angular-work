import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePoiMapComponent } from './create-poi-map.component';

describe('CreatePoiMapComponent', () => {
  let component: CreatePoiMapComponent;
  let fixture: ComponentFixture<CreatePoiMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePoiMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePoiMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
