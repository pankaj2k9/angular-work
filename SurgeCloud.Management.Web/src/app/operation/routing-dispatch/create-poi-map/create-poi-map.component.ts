import { Component, ViewEncapsulation, OnInit, Input, ElementRef, NgZone } from '@angular/core';
import { MapsAPILoader, GoogleMapsAPIWrapper, CircleManager, AgmMarker, AgmMap, AgmCircle } from '@agm/core';
import { RoutingDispatchService } from "../routing-dispatch.service";
import { AppService } from '../../../app.service';

import * as jQuery from 'jquery';
declare var google: any;

@Component({
  selector: 'app-create-poi-map',
  templateUrl: './create-poi-map.component.html',
  styleUrls: ['./create-poi-map.component.scss']
})
export class CreatePoiMapComponent implements OnInit {

  @Input() currentActivity: any;

  //map variables
  public map: any;
  public mapTypeControlOptions: any;
  public zoomControlOptions: any;
  public startingMarker: any = null;
  public endingMarker: any = null;
  public travelMode: String = 'DRIVING';
  public directions: any = [];
  public lat: number = 23.022505;
  public lng: number = 72.57136209999999;
  public geoFencePoints: any;
  public samples: any;
  public poiMarker: any = {};
  public address: any;
  public drawingManager: any;
  public drawingShapes: any = [];
  public companyInfo: any = {};

  constructor(private routingDispatchService: RoutingDispatchService, private mapsAPILoader: MapsAPILoader, private elementRef: ElementRef, private _zone: NgZone, private appService: AppService) { }

  ngOnInit() {

    // get unassign sample
    this.routingDispatchService.unassignSample.subscribe((unassignSample) => {
      this.samples = unassignSample;
      this.mapBounds(this.samples);
    });

    // change activity on resize so map load
    this.routingDispatchService.currentActivity.subscribe((currentActivity) => {
      setTimeout(() => {
        window.dispatchEvent(new Event("resize"));
        this.mapBounds(this.samples);
      }, 1);
    });

    // after create poi clear map
    this.routingDispatchService.clearMapCreatePoi.subscribe(() => {
      this.clearMap();
    });

    // get company info
    this.appService.activeCompanyInfo.subscribe((company) => {
      this.companyInfo = this.appService.getActiveCompany();
    });

    //map place api for auto complete
    this.setAutoCompleteLocation();
  }

  mapRendered($event) {
    this.map = $event;
    this.map.setOptions({
      streetViewControl: false,
      mapTypeControl: true
    });

    this.map.mapTypeControlOptions = {
      style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
      mapTypeIds: ['roadmap', 'hybrid'],
      position: google.maps.ControlPosition.TOP_LET
    }

    this.map.zoomControlOptions = {
      style: google.maps.ZoomControlStyle.DEFAULT,
      position: google.maps.ControlPosition.TOP_RIGHT
    };
    this.drawingTools();
  }


  drawingTools() {
    const polyOptions = {
      strokeWeight: 2,
      fillOpacity: 0.45,
      editable: true,
      draggable: true,
      fillColor: 'red'
    };

    this.drawingManager = new google.maps.drawing.DrawingManager({
      drawingMode: null,
      drawingControl: true,
      drawingControlOptions: {
        position: google.maps.ControlPosition.RIGHT_BOTTOM,
        drawingModes: ['circle', 'polygon', 'rectangle']
      },
      rectangleOptions: polyOptions,
      circleOptions: polyOptions,
      polygonOptions: polyOptions,
    });

    this.drawingManager.setMap(this.map);
    this.addOverlayComplateListener();
  }


  addOverlayComplateListener() {
    google.maps.event.addListener(this.drawingManager, 'overlaycomplete', (e) => {
      this.drawingShapes.push(e);
      this.drawingManager.setDrawingMode(null);
      this.drawingManager.setOptions({
        drawingControl: false
      });

      this.markerInsideOrOut(e)
      this.setClickEventToShape(e);
    });
  }

  setClickEventToShape(newShape) {
    google.maps.event.addListener(newShape.overlay, 'bounds_changed', () => {
      this.markerInsideOrOut(newShape)
    });
    google.maps.event.addListener(newShape.overlay, 'dragend', () => {
      this.markerInsideOrOut(newShape)
    });

    if (newShape.type === "polygon") {
      google.maps.event.addListener(newShape.overlay.getPath(), 'set_at', () => {
        this.markerInsideOrOut(newShape)
      });
    }
  }

  markerInsideOrOut(newShape) {
    let selectedMarker = [];
    this.samples.forEach(marker => {
      if (newShape.type == "rectangle" || newShape.type == "circle") {
        var insideOrOut = newShape.overlay.getBounds().contains({ lat: marker.address.latitude, lng: marker.address.longitude })
        if (insideOrOut) {
          selectedMarker.push(marker);
        }
        marker['insideGeoface'] = insideOrOut;
      } else if (newShape.type == "polygon") {
        var markerPosition = new google.maps.LatLng(marker.address.latitude, marker.address.longitude);
        var insideOrOut = google.maps.geometry.poly.containsLocation(markerPosition, newShape.overlay);
        if (insideOrOut) {
          selectedMarker.push(marker);
        }
      }
    });
    this.routingDispatchService.selectedMarker.next(selectedMarker);
  }

  clearAllShapes() {
    this.drawingShapes.forEach(drawingShape => {
      drawingShape.overlay.setMap(null);
    });
    this.drawingManager.setOptions({
      drawingControl: true
    });
  }

  setAutoCompleteLocation() {
    this.mapsAPILoader.load().then(() => {
      let locations = this.elementRef.nativeElement.querySelectorAll('input[name=addressName]');
      locations.forEach((location, index) => {
        const autocomplete: any = new google.maps.places.Autocomplete(location, {
          types: ['geocode'],
        });
        autocomplete.addListener('place_changed', () => {
          this._zone.run(() => {
            const place: any = autocomplete.getPlace();
            this[locations[index].id] = place;
            this.routingDispatchService.poiMarker.next(place);
            this.poiMarker = {
              lat: this.address.geometry.location.lat(),
              lng: this.address.geometry.location.lng(),
              icon: "https://cdn2.iconfinder.com/data/icons/mini-icon-set-map-location/91/Location_01-32.png",
              name: this.address.formatted_address
            }
            if (place.geometry === undefined || place.geometry === null) {
              return;
            }
          });
        });
      });
    });
  }

  mapBounds(markers) {
    var bounds = new google.maps.LatLngBounds();
    markers.forEach((marker) => {
      bounds.extend(new google.maps.LatLng(marker.address.latitude, marker.address.longitude));
    });
    this.map.fitBounds(bounds);
  }


  setPositionToAutoComplate(prm) {
    if (prm === 'in') {
      jQuery('.pac-container').addClass('create-poi-map-address');
    } else {
      jQuery('.pac-container').removeClass('create-poi-map-address');
    }
  }

  clearMap() {
    this.clearAllShapes();

    this.routingDispatchService.getUnassignedSamples(this.companyInfo.companyId).subscribe((data) => {
      this.samples = data;
      this.routingDispatchService.unassignSample.next(this.samples);
    }, (err) => {
      console.log(err);
    })

    this.poiMarker = {
      visible: false
    };
    let locations = this.elementRef.nativeElement.querySelectorAll('input[name=addressName]');
    locations.forEach((location, index) => {
      locations[index].value = '';
    });
  }
}




