import { Component, ViewEncapsulation, OnInit, Input, ElementRef, NgZone } from '@angular/core';
import { MapsAPILoader, GoogleMapsAPIWrapper, CircleManager, AgmMarker, AgmMap, AgmCircle } from '@agm/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import * as moment from 'moment';
import * as _ from 'underscore';
import { RoutingDispatchService } from "../routing-dispatch.service";
import { AppService } from '../../../app.service';
import { concat } from 'rxjs/operators/concat';
declare const google: any;


@Component({
  selector: 'app-create-route-form',
  templateUrl: './create-route-form.component.html',
  styleUrls: ['./create-route-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class CreateRouteFormComponent implements OnInit {

  @Input() currentActivity: any;

  public map: any;
  public mapTypeControlOptions: any;
  public zoomControlOptions: any;
  public startingMarker: any = null;
  public endingMarker: any = null;
  public lat: number = 23.022505;
  public lng: number = 72.57136209999999;
  public travelMode: String = 'DRIVING';
  public directions: any = [];
  public travelHours: number;
  public travelMinutes: number;
  public selectedExcludedDays = [];

  public selectedPoi: any = [];
  public checkAllPoi: any = false;

  public dropdownSettings = {
    disabled: false,
  }
  public dropdownOpen = false;

  public assets = [
    {
      id: 1,
      aId: 'Asset 1',
      assetId: 'Asset 1',
      capacity: 30,
      type: "Bus",
      index: 0
    },
    {
      id: 2,
      aId: 'Asset 2',
      assetId: 'Asset 2',
      capacity: 5,
      type: "Bus",
      index: 1
    },
    {
      id: 3,
      aId: 'Asset 3',
      assetId: 'Asset 3',
      capacity: 80,
      type: "Bus",
      index: 2
    },
    {
      id: 4,
      aId: 'Asset 4',
      assetId: 'Asset 4',
      capacity: 5000,
      type: "Truck",
      index: 3
    }
  ]

  public poi: any = [];

  public drivers = [
    {
      driverid: 1,
      driverName: "Sanjay"
    },
    {
      driverid: 2,
      driverName: "Ajay"
    },
    {
      driverid: 3,
      driverName: "Sandip"
    }
  ]

  private paginationInfo = {
    sortOrder: 'asc',
    sortBy: 'dataoccured',
    numberRowPerPage: 2
  };

  public startLocation: any = {};
  public endLocation: any = {};

  public selectedAssets = [];
  public allPossibility = [];
  public finalRoute = [];
  public selectedRoute = [];

  public lastIndex = 4;
  public startIndex = 0;

  // Time select box
  public startTimeDisableRange = {};
  private startTimeReset = false;
  private startTimeList = [];
  public createRouteForm: any;
  public companyInfo: any = {};
  public userInfo: any = {};
  public routeNameError: any = false;
  public poiError: any = false;
  public routeIndex: any = 0;

  constructor(private mapsAPILoader: MapsAPILoader, private elementRef: ElementRef, private _zone: NgZone, private routingDispatchService: RoutingDispatchService, private gmapsApi: GoogleMapsAPIWrapper, private formBuilder: FormBuilder, private appService: AppService) {
    this.createRouteForm = this.formBuilder.group({
      'startLocation': [{}, [Validators.required]],
      'endLocation': [{}, [Validators.required]],
      'startTimeSchedule': ['', [Validators.required]],
      'travelTimeHours': ['', [Validators.required]],
      'travelTimeMinutes': ['', []],
      'tags': [[], []],
      'daySchedule': [[], [Validators.required]],
      'selectedAssets': [[], [Validators.required]],
      'excludeDays': [[], []],
    });
  }

  ngOnInit() {
    this.gmapsApi.getNativeMap().then(map => {
      var directionsService = new google.maps.DirectionsService;
      var directionsDisplay = new google.maps.DirectionsRenderer;
    });

    this.routingDispatchService.unassignPOI.subscribe((unassignPOI) => {
      this.poi = unassignPOI;
    });

    this.createRouteForm.valueChanges.subscribe((newValue) => {
      if (this.createRouteForm.valid) {
        this.dropdownSettings.disabled = false;
      } else {
        this.dropdownSettings.disabled = true;
      }
    });

    this.routingDispatchService.createRouteMap.subscribe((map) => {
      this.map = map;
    });

    this.appService.activeCompanyInfo.subscribe((company) => {

      //get company info
      this.companyInfo = this.appService.getActiveCompany();

      //get userInfo info
      this.appService.setLogInUserDetails().subscribe((userInfo) => {
        this.userInfo = userInfo;
      });

      //get all assetes
      this.routingDispatchService.getAsset(this.companyInfo.companyId).subscribe((data) => {
        this.assets = data;
        this.assets.forEach((asset, index) => {
          asset['id'] = index;
          asset['capacity'] = Math.floor(Math.random() * 100) + 1;
        });
      }, (err) => {
        console.log(err);
      })
    });

    this.possibleRoutes(this.poi, this.selectedAssets, []);
    this.setAutoCompleteLocation();
    this.startTimeList = this.getTimeRangeArray(0, 23);
  }

  onBlurMethod(mainControl, secondControl) {
    if (!this.createRouteForm.controls[secondControl].valid) {
      this.createRouteForm.controls[mainControl].setValidators([Validators.required])
      this.createRouteForm.controls[mainControl].updateValueAndValidity()
      this.createRouteForm.controls[secondControl].setValidators(null)
      this.createRouteForm.controls[secondControl].updateValueAndValidity()
    }
  }

  possibleRoutes(counts, assetsCapacity, partial) {
    var sum = partial.reduce((a, b) => a + b.totalCapacity, 0);

    let possibleAssets = [];
    for (let i = 0; i < assetsCapacity.length; i++) {
      if (sum <= assetsCapacity[i].capacity) {
        possibleAssets.push(assetsCapacity[i]);
      }
    }

    if (possibleAssets.length > 0) {
      this.allPossibility.push({
        poi: partial,
        assets: possibleAssets
      })
    } else {
      return
    }

    for (let i = 0; i < counts.length; i++) {
      let countsCopy = counts.slice()
      let partialCopy = partial.slice()
      partialCopy.push(counts[i]);
      let newCounts = countsCopy.splice(i + 1);
      this.possibleRoutes(newCounts, assetsCapacity, partialCopy)
    }

    return this.allPossibility;
  }

  setAutoCompleteLocation() {
    this.mapsAPILoader.load().then(() => {
      let locations = this.elementRef.nativeElement.querySelectorAll('input[name=addressName]');
      locations.forEach((location, index) => {
        const autocomplete: any = new google.maps.places.Autocomplete(location, {
          types: ['geocode'],
        });

        autocomplete.addListener('place_changed', () => {
          this._zone.run(() => {
            const place: any = autocomplete.getPlace();
            this[locations[index].id] = place;
            this.createRouteForm.controls[locations[index].id].setValue(place);
            this.routingDispatchService[locations[index].id].next(this.createAddressModel(place));
            if (place.geometry === undefined || place.geometry === null) {
              return;
            }
          });
        });
      });
    });
  }

  intialrenderRoute() {
    if (this.createRouteForm.valid && this.selectedPoi.length > 0) {
      this.allPossibility = [];
      this.finalRoute = [];
      this.startIndex = 0;
      this.lastIndex = 4;
      this.poiError = false;
      this.possibleRoutes(this.selectedPoi, this.selectedAssets, [])
      this.allPossibility.shift();
      this.allPossibility.sort((a, b) => {
        return parseFloat(b.poi.length) - parseFloat(a.poi.length);
      });
      this.renderRoute();
    } else {
      console.log(this.selectedPoi.length);
      if (this.selectedPoi.length == 0) {
        this.poiError = true;
      } else {
        this.poiError = false;
      }
      Object.keys(this.createRouteForm.controls).forEach(key => {
        this.createRouteForm.get(key).markAsDirty();
      });
    }
  }

  getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  renderRoute() {
    if (this.startIndex < this.allPossibility.length) {
      this.routeIndex = this.routeIndex + 1;
      console.log(this.routeIndex);

      var directionsService = new google.maps.DirectionsService;
      var directionsDisplay = new google.maps.DirectionsRenderer(
        {
          draggable: true,
          polylineOptions: {
            strokeColor: this.getRandomColor()
          }
        }
      );

      directionsDisplay.setMap(this.map);

      var service_options = {
        waypoints: [],
        optimizeWaypoints: true,
        travelMode: 'DRIVING',
      };

      service_options['origin'] = {
        lat: this.startLocation.geometry.location.lat(),
        lng: this.startLocation.geometry.location.lng(),
        name: this.startLocation.name
      }

      service_options['destination'] = {
        lat: this.endLocation.geometry.location.lat(),
        lng: this.endLocation.geometry.location.lng(),
        name: this.endLocation.name
      }

      let poiTime = 0;

      this.allPossibility[this.startIndex].poi.forEach((poi) => {
        poiTime += poi.waitingTime;
        service_options['waypoints'].push({
          location: {
            lat: poi.address.latitude,
            lng: poi.address.longitude,
            name: poi.name
          }
        })
      });

      this.directions.push(directionsDisplay);

      directionsService.route(service_options, (response, status) => {
        if (status === 'OK') {
          directionsDisplay.addListener('directions_changed', () => {
            console.log(directionsDisplay);
          });
          this.compareTime(response, poiTime, directionsDisplay);
        } else {
          if (status === "OVER_QUERY_LIMIT") {
            setTimeout(() => {
              this.renderRoute();
            }, 2000);
          }
        }
      });
    }
  }

  compareTime(response, poiTime, directionsDisplay) {
    var totalDistance = 0;
    var totalTime = poiTime;

    // Total time and distance
    for (var i = 0; i < response.routes[0].legs.length; i++) {
      totalDistance += response.routes[0].legs[i].distance.value;
      totalTime += response.routes[0].legs[i].duration.value;
    }

    // get Travel time 
    if (this.createRouteForm.controls['travelTimeHours'].value == '') {
      this.createRouteForm.controls['travelTimeHours'].setValue(0)
    }
    if (this.createRouteForm.controls['travelTimeMinutes'].value == '') {
      this.createRouteForm.controls['travelTimeMinutes'].setValue(0)
    }

    // comprate Travel time and total route time
    if (totalTime < ((this.createRouteForm.controls['travelTimeHours'].value * 3600) + (this.createRouteForm.controls['travelTimeMinutes'].value * 60))) {

      // set value totalDistance, totalTime and status
      this.allPossibility[this.startIndex]['color'] = directionsDisplay.polylineOptions.strokeColor;
      this.allPossibility[this.startIndex]['totalDistance'] = totalDistance;
      this.allPossibility[this.startIndex]['totalTime'] = totalTime;
      this.allPossibility[this.startIndex]['status'] = true;
      this.allPossibility[this.startIndex]['response'] = response;
      this.getCoordinates(response);
      console.log(response);

      // push route to final route
      this.finalRoute.push(this.allPossibility[this.startIndex]);

      //set route into map
      directionsDisplay.setOptions({ suppressMarkers: true, preserveViewport: true });
      // directionsDisplay.setDirections(response);

      //check 5 route is avalible 
      if (this.startIndex < this.lastIndex) {
        this.startIndex += 1;
        this.renderRoute();
      }
    } else {

      // set value totalDistance, totalTime and status
      this.allPossibility[this.startIndex]['totalDistance'] = totalDistance;
      this.allPossibility[this.startIndex]['totalTime'] = totalTime;
      this.allPossibility[this.startIndex]['status'] = false;

      //increase start index and last index
      if (this.startIndex < this.lastIndex) {
        this.lastIndex += 1;
        this.startIndex += 1;
        this.renderRoute();
      }
    }
  }


  updateRoute(index, event) {
    if (event.target.checked) {

      // this.finalRoute[index]['response']['Index'] = index;
      this.selectedRoute.push(this.finalRoute[index]);
      this.directions[index].setDirections(this.finalRoute[index].response);
      this.selectedRoute[this.selectedRoute.indexOf(this.finalRoute[index])]["routeIndex"] = index;

    } else {
      this.directions[index].set('directions', null);
      this.selectedRoute.splice(this.selectedRoute.indexOf(this.finalRoute[index]), 1);
    }
    console.log(this.selectedRoute);
    this.finalRoute.forEach((route) => {
      delete route["reming"];
    });

    this.selectedRoute.forEach((selectedRoutePoi) => {
      selectedRoutePoi.poi.forEach((selectedPoi) => {
        this.finalRoute.forEach((route, routeIndex) => {
          if (selectedRoutePoi.routeIndex !== routeIndex && (route["reming"] == undefined || route["reming"])) {
            for (let i = 0; i < route.poi.length; i++) {
              if (route.poi[i] === selectedPoi) {
                route["reming"] = false;
                break;
              } else {
                route["reming"] = true;
              }
            }
          }
        })
      });
    })
  }


  saveRoutes() {
    if (this.selectedRoute.filter(route => (route.name == undefined || route.name == '')).length > 0) {
      this.routeNameError = true;
    } else {
      this.routeNameError = false;
      let saveRoutesData = [];
      this.selectedRoute.forEach(route => {
        let routeData = Object.assign({}, this.createRouteForm.value);
        let formData = Object.assign({}, this.createRouteForm.value);
        routeData = Object.assign({}, this.createRouteForm.value);
        routeData['companyId'] = this.companyInfo.companyId;
        routeData['companyName'] = this.companyInfo.name;
        routeData['name'] = route.name;
        routeData['start'] = this.createAddressModel(this.startLocation);
        routeData['end'] = this.createAddressModel(this.endLocation);
        routeData['tags'] = formData.tags.length == 0 ? [] : formData.tags.split(",");
        routeData['daySchedule'] = formData.daySchedule.map((day) => { return day.sort; }).join(",");
        routeData['maxDuration'] = (this.createRouteForm.controls['travelTimeHours'].value * 3600) + (this.createRouteForm.controls['travelTimeMinutes'].value * 60);
        routeData['coordinates'] = this.getCoordinates(route.response);
        routeData['timeInSecond'] = route.totalTime;
        routeData['distanceInMeter'] = route.totalDistance;
        routeData['color'] = route.color;
        routeData['possibleAssets'] = this.getPossibleAssets(route.assets);
        routeData['createdOn'] = moment().format('MM/DD/YYYY, hh:mm A');
        routeData['createdBy'] = this.userInfo.displayName;
        routeData['status'] = 1;
        saveRoutesData.push(routeData);
      });

      this.routingDispatchService.createRoutes(saveRoutesData).subscribe((data) => {
        this.mapPoiRoute(data);
      }, (err) => {
        console.log(err);
      })
    }
  }


  mapPoiRoute(routeData) {
    let mapPoiRoute = [];
    this.selectedRoute.forEach((route, index) => {
      route.poi.forEach(routePoi => {
        mapPoiRoute.push({
          companyId: this.companyInfo.companyId,
          companyName: this.companyInfo.name,
          routeId: routeData[index].routeId,
          routeName: routeData[index].name,
          poiId: routePoi.poiId,
          poiName: routePoi.name
        })
      });
    });

    console.log(this.selectedRoute);
    console.log(mapPoiRoute);

    this.routingDispatchService.mapPoiRoute(mapPoiRoute).subscribe((data) => {
      console.log(data);
    }, (err) => {
      console.log(err);
    })
  }

  getCoordinates(response) {
    let data = {};
    data['destination'] = {
      lat: response.request.destination.location.lat(),
      lng: response.request.destination.location.lng(),
    }

    data['origin'] = {
      lat: response.request.origin.location.lat(),
      lng: response.request.origin.location.lng(),
    }

    data['waypoints'] = [];

    response.request.waypoints.forEach(waypoint => {
      let waypointLatLong = {
        lat: waypoint.location.location.lat(),
        lng: waypoint.location.location.lng(),
      };
      data['waypoints'].push(waypointLatLong);
    });

    console.log(JSON.stringify(data));
    return JSON.stringify(data);
  }

  selectedDayNotifyChange(data) {
    this.createRouteForm.controls['daySchedule'].setValue(data);
  }

  getPossibleAssets(assets) {
    let assetsIds = [];
    assets.forEach(asset => {
      assetsIds.push(asset.aId);
    });
    return assetsIds;
  }

  getTimeRangeArray(startPoint, endPoint) {
    const timeArray = [];
    for (let i = startPoint; i <= endPoint; i++) {
      let pointValue = i;
      if (i < 10) {
        pointValue = '0' + i;
      }
      pointValue = pointValue + ':00';
      const timeObject = {
        displayValue: pointValue,
        value: i
      }
      timeArray.push(timeObject);
    }
    return timeArray;
  }

  selectSingelPoi(poiId, event) {
    console.log(poiId);
    var i = _.findIndex(this.poi, { poiId: poiId });
    if (event.target.checked) {
      this.poi[i]['selected'] = true;
      this.selectedPoi.push(this.poi[i]);
    } else {
      this.checkAllPoi = false;
      this.poi[i]['selected'] = false;
      this.selectedPoi.splice(this.selectedPoi.indexOf(this.poi[i]), 1);
    }
    console.log(this.selectedPoi);
  }

  selectAllPoi(i, event) {
    if (event.target.checked) {
      this.poi.forEach(poiSingle => {
        poiSingle['selected'] = true;
      });
      this.checkAllPoi = true;
      this.selectedPoi = Object.assign([], this.poi);
    } else {
      this.poi.forEach(sample => {
        sample['selected'] = false;
      });
      this.checkAllPoi = false;
      this.selectedPoi = [];
    }
    console.log(this.selectedPoi);
  }

  assignAsset(event, routeName) {
    this.assets.forEach(asset => {
      if (asset['selectedFor'] == routeName) {
        delete asset['selectedFor'];
        delete asset['selected']
      }

      if (asset.assetId === event.target.value) {
        asset["selected"] = true;
        asset["selectedFor"] = routeName;
      }
    });
  }

  assignDriver(event, routeName) {
    this.drivers.forEach(driver => {
      if (driver['selectedFor'] == routeName) {
        delete driver['selectedFor'];
        delete driver['selected']
      }

      if (driver.driverName === event.target.value) {
        driver["selected"] = true;
        driver["selectedFor"] = routeName;
      }
    });
  }

  createAddressModel(autocompleteAddress) {
    let address = {};
    address = {
      "name": autocompleteAddress.name,
      "formattedAddress": autocompleteAddress.formatted_address,
      "latitude": autocompleteAddress.geometry.location.lat(),
      "longitude": autocompleteAddress.geometry.location.lng()
    }

    autocompleteAddress.address_components.forEach(address_component => {
      if (address_component.types.indexOf('street_number') === 0) {
        address['street1'] = address_component.short_name;
      }

      if (address_component.types.indexOf('route') === 0) {
        address['street2'] = address_component.short_name;
      }

      if (address_component.types.indexOf('administrative_area_level_2') === 0) {
        address['city'] = address_component.short_name;
      }

      if (address_component.types.indexOf('administrative_area_level_1') === 0) {
        address['stateCode'] = address_component.short_name;
      }

      if (address_component.types.indexOf('country') === 0) {
        address['countryCode'] = address_component.short_name;
      }

      if (address_component.types.indexOf('postal_code') === 0) {
        address['postcode'] = address_component.short_name;
      }
    });

    return address;
  }

  selectedExcludeDays(data) {
    this.createRouteForm.controls['excludeDays'].setValue(data);
  }

  changeActivity() {
    this.routingDispatchService.currentActivity.next(this.currentActivity);
  }
}
