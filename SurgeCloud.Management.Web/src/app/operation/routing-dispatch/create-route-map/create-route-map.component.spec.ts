import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateRouteMapComponent } from './create-route-map.component';

describe('CreateRouteMapComponent', () => {
  let component: CreateRouteMapComponent;
  let fixture: ComponentFixture<CreateRouteMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateRouteMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateRouteMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
