import { Component, OnInit } from '@angular/core';
import { MapsAPILoader, GoogleMapsAPIWrapper, CircleManager, AgmMarker, AgmMap, AgmCircle } from '@agm/core';
import { RoutingDispatchService } from "../routing-dispatch.service";

@Component({
  selector: 'app-create-route-map',
  templateUrl: './create-route-map.component.html',
  styleUrls: ['./create-route-map.component.scss']
})
export class CreateRouteMapComponent implements OnInit {
  public map: any;
  public mapTypeControlOptions: any;
  public zoomControlOptions: any;
  public startingMarker: any = null;
  public endingMarker: any = null;
  public lat: number = 23.022505;
  public lng: number = 72.57136209999999;
  public travelMode: String = 'DRIVING';
  public directions: any = [];
  public poi: any = [];

  constructor(private routingDispatchService: RoutingDispatchService) { }

  ngOnInit() {
    this.routingDispatchService.currentActivity.subscribe((currentActivity) => {
      setTimeout(() => {
        window.dispatchEvent(new Event("resize"));
        this.mapBounds(this.poi);
      }, 1);
    });

    this.routingDispatchService.unassignPOI.subscribe((unassignPOI) => {
      this.poi = Object.assign([], unassignPOI);
      this.mapBounds(this.poi);
    });

    this.routingDispatchService.startLocation.subscribe((startLocation) => {
      this.poi.push({
        "name": "Start Location",
        "icon": "assets/images/routing-dispatch/startMarker.png",
        "address": startLocation
      })
      this.mapBounds(this.poi);
    });

    this.routingDispatchService.endLocation.subscribe((endLocation) => {
      this.poi.push({
        "name": "end Location",
        "icon": "assets/images/routing-dispatch/endMarker.png",
        "address": endLocation
      })
      this.mapBounds(this.poi);
    });
  }

  mapRendered($event) {
    this.map = $event;
    this.routingDispatchService.createRouteMap.next(this.map);

    const mapZoomOptions = { minZoom: 0 };
    this.map.setOptions(mapZoomOptions);
    this.mapTypeControlOptions = {
      style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
      mapTypeIds: ['roadmap', 'hybrid'],
      position: google.maps.ControlPosition.BOTTOM_LEFT
    }
    this.zoomControlOptions = {
      style: google.maps.ZoomControlStyle.DEFAULT,
      position: google.maps.ControlPosition.RIGHT_CENTER
    };


  }

  mapBounds(markers) {
    var bounds = new google.maps.LatLngBounds();
    markers.forEach(marker => {
      bounds.extend(new google.maps.LatLng(marker.address.latitude, marker.address.longitude));
    });
    this.map.fitBounds(bounds);
  }

}
