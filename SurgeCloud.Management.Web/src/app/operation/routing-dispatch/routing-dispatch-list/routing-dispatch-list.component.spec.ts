import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutingDispatchListComponent } from './routing-dispatch-list.component';

describe('RoutingDispatchListComponent', () => {
  let component: RoutingDispatchListComponent;
  let fixture: ComponentFixture<RoutingDispatchListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoutingDispatchListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutingDispatchListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
