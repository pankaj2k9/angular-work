import { Component, OnInit, Input } from '@angular/core';
import { RoutingDispatchService } from "../routing-dispatch.service";
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import * as moment from 'moment';
import { AppService } from '../../../app.service';

@Component({
  selector: 'app-routing-dispatch-list',
  templateUrl: './routing-dispatch-list.component.html',
  styleUrls: ['./routing-dispatch-list.component.scss']
})
export class RoutingDispatchListComponent implements OnInit {


  @Input() currentActivity: any;

  private paginationInfo = {
    sortOrder: 'asc',
    sortBy: 'dataoccured',
    numberRowPerPage: 4
  };

  public routes = [];
  public unassignPOI = [];
  public unassignSample = [];
  public selectedSample = [];
  public checkAllSample = false;
  public createPoiData = [];
  public activeCompanyInfoSubscription: any;
  public companyInfo: any = {};
  public userInfo: any;
  public currentWorkflow: any = {
    route: false,
    poi: false,
    sample: false
  }


  constructor(private routingDispatchService: RoutingDispatchService, private appService: AppService) {
  }

  ngOnInit() {
    this.getAllData();
  }

  checkWorkflow() {
    if (this.routes.length > 0) {
      this.currentWorkflow = {
        route: true,
        poi: false,
        sample: false
      }
    } else if (this.unassignPOI.length > 0) {
      this.currentWorkflow = {
        route: false,
        poi: true,
        sample: false
      }

    } else {
      this.currentWorkflow = {
        route: false,
        poi: false,
        sample: true
      }
    }
    this.routingDispatchService.checkWorkflow.next(this.currentWorkflow);
  }

  // Routes, poi and sample, compayn info data
  getAllData() {
    this.companyInfo = this.appService.getActiveCompany();
    if (this.companyInfo) {
      this.companyInfo = this.appService.getActiveCompany();
      this.allPromise();
    } else {
      this.activeCompanyInfoSubscription = this.appService.activeCompanyInfo.subscribe((company) => {
        this.companyInfo = this.appService.getActiveCompany();
        this.appService.setLogInUserDetails().subscribe((userInfo) => {
          this.userInfo = userInfo;
        });
        this.allPromise();
      });
    }
  }


  allPromise() {
    Promise.all([
      new Promise((resolve, reject) => {
        this.routingDispatchService.getAllRoutes(this.companyInfo.companyId).subscribe((data) => {
          this.routes = data;
          this.routingDispatchService.routes.next(this.routes);
          resolve(this.routes);
        }, (err) => {
          reject(err)
          console.log(err);
        })
      }),

      new Promise((resolve, reject) => {
        this.routingDispatchService.getUnassignedPoi(this.companyInfo.companyId).subscribe((data) => {
          this.unassignPOI = data;
          this.unassignPOI.forEach(poi => {
            poi['icon'] = "assets/images/routing-dispatch/poi.png";
          });
          this.routingDispatchService.unassignPOI.next(this.unassignPOI);
          resolve(this.unassignPOI);
        }, (err) => {
          reject(err)
          console.log(err);
        })
      }),

      new Promise((resolve, reject) => {
        this.routingDispatchService.getUnassignedSamples(this.companyInfo.companyId).subscribe((data) => {
          this.unassignSample = data;
          this.unassignSample.forEach(sample => {
            sample['icon'] = "assets/images/routing-dispatch/sample.png";
          });
          this.routingDispatchService.unassignSample.next(this.unassignSample);
          resolve(this.unassignSample);
        }, (err) => {
          reject(err)
          console.log(err);
        })
      }),
    ]).then(value => {
      this.checkWorkflow()
    });
  }

  changeActivity() {
    this.routingDispatchService.currentActivity.next(this.currentActivity);
  }
}
