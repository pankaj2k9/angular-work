import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutingDispatchMapComponent } from './routing-dispatch-map.component';

describe('RoutingDispatchMapComponent', () => {
  let component: RoutingDispatchMapComponent;
  let fixture: ComponentFixture<RoutingDispatchMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoutingDispatchMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutingDispatchMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
