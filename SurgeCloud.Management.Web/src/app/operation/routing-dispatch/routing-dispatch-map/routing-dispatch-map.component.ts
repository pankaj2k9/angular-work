import { Component, OnInit } from '@angular/core';
import { MapsAPILoader, GoogleMapsAPIWrapper, CircleManager, AgmMarker, AgmMap, AgmCircle } from '@agm/core';
import { RoutingDispatchService } from "../routing-dispatch.service";
import { materialize } from 'rxjs/operator/materialize';
import { sample } from 'rxjs/operator/sample';
import { concat } from 'rxjs/operators/concat';

declare const google: any;


@Component({
  selector: 'app-routing-dispatch-map',
  templateUrl: './routing-dispatch-map.component.html',
  styleUrls: ['./routing-dispatch-map.component.scss']
})
export class RoutingDispatchMapComponent implements OnInit {
  public map: any;
  public mapTypeControlOptions: any;
  public zoomControlOptions: any;
  public startingMarker: any = null;
  public endingMarker: any = null;
  public lat: number = 51.678418;
  public lng: number = 7.809007;
  public travelMode: String = 'DRIVING';
  public directions: any = [];
  public markers: any = [];

  public filter = {
    unAssign: true,
    assign: true,
    routes: true,
    poi: true,
    sample: true,
  }
  public routes: any = [];
  public unassignPOI: any = [];
  public unassignSample: any = [];
  public currentWorkflow: any = {};

  public fullscreen: any = {
    map: false,
    list: false,
  };
  public minimize: any = {
    map: false,
    list: false
  };

  constructor(private gmapsApi: GoogleMapsAPIWrapper, private routingDispatchService: RoutingDispatchService) { }
  public temp = [];
  ngOnInit() {
    this.gmapsApi.getNativeMap().then(map => {
      var directionsService = new google.maps.DirectionsService;
      var directionsDisplay = new google.maps.DirectionsRenderer;
    });

    this.routingDispatchService.routes.subscribe((routes) => {
      this.routes = routes;
      console.log(this.routes);
      this.renderRoute();
    });

    this.routingDispatchService.unassignPOI.subscribe((unassignPOI) => {
      this.unassignPOI = unassignPOI;
    });

    this.routingDispatchService.unassignSample.subscribe((unassignSample) => {
      this.unassignSample = unassignSample;
    });

    this.routingDispatchService.currentActivity.subscribe((currentActivity) => {
      setTimeout(() => {
        window.dispatchEvent(new Event("resize"));
      }, 1);
    });

    this.routingDispatchService.checkWorkflow.subscribe((checkWorkflow) => {
      this.currentWorkflow = checkWorkflow;
      this.renderMapMarker();
    });
  }


  mapRendered($event) {
    this.map = $event;
    const mapZoomOptions = { minZoom: 0 };
    this.map.setOptions(mapZoomOptions);
    this.mapTypeControlOptions = {
      style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
      mapTypeIds: ['roadmap', 'hybrid'],
      position: google.maps.ControlPosition.BOTTOM_LEFT
    }
    this.zoomControlOptions = {
      style: google.maps.ZoomControlStyle.DEFAULT,
      position: google.maps.ControlPosition.RIGHT_CENTER
    };


  }

  renderMapMarker() {
    if (this.currentWorkflow.route) {

      this.routes.forEach(route => {
        let data = JSON.parse(route.coordinates);

        let startMaker = this.markerData(data.origin.lat, data.origin.lng, route.name + " Star maker", "assets/images/routing-dispatch/startMarker.png");
        let endMarker = this.markerData(data.destination.lat, data.destination.lng, route.name + " End maker", "assets/images/routing-dispatch/endMarker.png");
        this.markers.push(startMaker);
        this.markers.push(endMarker);

        data.waypoints.forEach((waypoint, index) => {
          let poi = this.markerData(waypoint.lat, waypoint.lng, route.name + " Waypoint " + index, "assets/images/routing-dispatch/poi.png");
          this.markers.push(poi);
        });

      });
    } else if (this.currentWorkflow.poi) {
      this.markers = this.unassignPOI;
    } else if (this.currentWorkflow.sample) {
      this.markers = this.unassignSample;
    }
    this.mapBounds(this.markers);
  }

  markerData(lat, lng, markerName, icon) {
    let data = {
      address: {}
    };
    data['address']['latitude'] = lat;
    data['address']['longitude'] = lng;
    data['icon'] = icon;
    data['name'] = markerName;
    return data;
  }

  filterRoute() {
    if (this.filter.assign && this.filter.routes) {
      this.directions.forEach((route) => {
        route.setMap(this.map);
      });
    } else {
      this.directions.forEach((route) => {
        route.setMap(null);
      });
    }
  }

  renderRoute() {
    console.log(this.routes);
    var directionsService = new google.maps.DirectionsService;
    this.routes.forEach((route) => {
      var directionsDisplay = new google.maps.DirectionsRenderer({
        polylineOptions: {
          strokeColor: route.color
        }
      }
      );
      directionsDisplay.setMap(this.map);

      let data = JSON.parse(route.coordinates);
      var service_options = {
        waypoints: [],
        optimizeWaypoints: true,
        travelMode: 'DRIVING'
      };

      service_options['origin'] = {
        lat: data.origin.lat,
        lng: data.origin.lng,
        name: route.name + " Star maker"
      }

      service_options['destination'] = {
        lat: data.destination.lat,
        lng: data.destination.lng,
        name: route.name + " End maker"
      }

      data.waypoints.forEach((waypoint, index) => {
        service_options['waypoints'].push({
          location: {
            lat: waypoint.lat,
            lng: waypoint.lng,
            name: route.name + " Waypoint " + index
          }
        })
      });

      this.directions.push(directionsDisplay);

      console.log(service_options);

      directionsService.route(service_options, function (response, status) {
        if (status === 'OK') {
          directionsDisplay.setOptions({ suppressMarkers: true, preserveViewport: true });
          directionsDisplay.setDirections(response);
        } else {
          window.alert('Directions request failed due to ' + status);
        }
      });
    });
  }

  mapBounds(markers) {
    var bounds = new google.maps.LatLngBounds();
    markers.forEach(marker => {
      bounds.extend(new google.maps.LatLng(marker.address.latitude, marker.address.longitude));
    });
    this.map.fitBounds(bounds);
  }
}


