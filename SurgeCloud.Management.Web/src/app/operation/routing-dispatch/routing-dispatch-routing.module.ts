import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RoutingDispatchComponent } from './routing-dispatch.component';

const routes: Routes = [
    {
        path: '',
        component: RoutingDispatchComponent
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RoutingDispatchRoutingModule {

}
