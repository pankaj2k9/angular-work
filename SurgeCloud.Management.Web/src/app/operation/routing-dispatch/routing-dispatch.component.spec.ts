import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutingDispatchComponent } from './routing-dispatch.component';

describe('RoutingDispatchComponent', () => {
  let component: RoutingDispatchComponent;
  let fixture: ComponentFixture<RoutingDispatchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoutingDispatchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutingDispatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
