import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-routing-dispatch',
  templateUrl: './routing-dispatch.component.html',
  styleUrls: ['./routing-dispatch.component.scss']
})
export class RoutingDispatchComponent implements OnInit {
  public fullscreen: any = {
    map: false,
    list: false,
  };
  public minimize: any = {
    map: false,
    list: false
  }

  public currentActivity: any = {
    routeList: true,
    createRoute: false,
    createPOI: false,
    assetDriverView: false,
  }

  constructor() { }

  ngOnInit() {
  }

}
