import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsMapService } from '../../shared-module/services/bsmap/bsmap.service';
import { CoreModule } from '../../core';
import { DataTableModule } from 'angular2-datatable';
import { AppCommonModule } from '../../app-common';
import { SharedModule } from "../../shared-module/shared-module.module";
import { RoutingDispatchComponent } from './routing-dispatch.component';
import { RoutingDispatchRoutingModule } from "./routing-dispatch-routing.module";
import { RoutingDispatchMapComponent } from './routing-dispatch-map/routing-dispatch-map.component';
import { RoutingDispatchListComponent } from './routing-dispatch-list/routing-dispatch-list.component';
import { CreatePoiFormComponent } from './create-poi-form/create-poi-form.component';
import { CreatePoiMapComponent } from './create-poi-map/create-poi-map.component';
import { CreateRouteMapComponent } from './create-route-map/create-route-map.component';
import { CreateRouteFormComponent } from './create-route-form/create-route-form.component';

import { RoutingDispatchService } from './routing-dispatch.service';
import { AssetDriverViewComponent } from './asset-driver-view/asset-driver-view.component';
import {TranslateModule,TranslateLoader,TranslateCompiler,TranslateParser,MissingTranslationHandler} from "@ngx-translate/core";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {HttpLoaderFactory} from "../../app.module";


@NgModule({
    imports: [
        RoutingDispatchRoutingModule,
        SharedModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        CoreModule,
        DataTableModule,
        AppCommonModule,
      HttpClientModule,
      TranslateModule.forChild({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
      })
    ],
    declarations: [RoutingDispatchComponent, RoutingDispatchMapComponent, RoutingDispatchListComponent, CreatePoiFormComponent, CreatePoiMapComponent, CreateRouteMapComponent, CreateRouteFormComponent, AssetDriverViewComponent],
    exports: [RoutingDispatchComponent],
    providers: [BsMapService, RoutingDispatchService]

})
export class RoutingDispatchModule { }
