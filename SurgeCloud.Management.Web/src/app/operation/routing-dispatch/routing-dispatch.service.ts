import { Injectable } from '@angular/core';
import { HttpService } from '../../shared-module';
import { Observable } from 'rxjs/Observable';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class RoutingDispatchService {
    constructor(private httpService: HttpService) { }

    public selectedMarker = new Subject();
    public getGeoFencePoints = new Subject();
    public currentActivity = new Subject();
    public routes = new Subject();
    public unassignPOI = new Subject();
    public unassignSample = new Subject();
    public poiMarker = new Subject();
    public clearMapCreatePoi = new Subject();
    public createRouteMap = new Subject();
    public checkWorkflow = new Subject();
    public startLocation = new Subject();
    public endLocation = new Subject();


    getAllRoutes(companyId: string) {
        return this.httpService.get(`device/api/iot/getallroutes/${companyId}`)
            .map(res => res.json())
            .catch(err => Observable.throw(err));
    }


    getUnassignedSamples(companyId: string) {
        return this.httpService.get(`device/api/iot/getunassignedsamples/${companyId}`)
            .map(res => res.json())
            .catch(err => Observable.throw(err));
    }

    getUnassignedPoi(companyId: string) {
        return this.httpService.get(`device/api/iot/getunassignedpoi/${companyId}`)
            .map(res => res.json())
            .catch(err => Observable.throw(err));
    }

    createPoi(data: Array<any>) {
        return this.httpService.post(`device/api/iot/createpoi`, data)
            .map(res => res.json())
            .catch(err => Observable.throw(err));
    }

    mapSamplePoi(data: Array<any>) {
        return this.httpService.post(`device/api/iot/mapsamplepoi`, data)
            .map(res => res.json())
            .catch(err => Observable.throw(err));
    }

    getAsset(companyId: string): Observable<any> {
        return this.httpService.get(`device/api/iot/get/${companyId}`)
            .map(res => res.json())
            .catch(err => Observable.throw(err));
    }


    createRoutes(data: Array<any>) {
        return this.httpService.post(`device/api/iot/createroute`, data)
            .map(res => res.json())
            .catch(err => Observable.throw(err));
    }

    mapPoiRoute(data: Array<any>) {
        return this.httpService.post(`device/api/iot/mappoiroute`, data)
            .map(res => res.json())
            .catch(err => Observable.throw(err));
    }

    manageRouteAssignment(data: Array<any>) {
        return this.httpService.post(`device/api/iot/managerouteassignment`, data)
            .map(res => res.json())
            .catch(err => Observable.throw(err));
    }

}