import { Injectable } from '@angular/core';

@Injectable()
export class ModelService {
    arrowCount = 0;
    totalListHeight = 0;
    currentLiElement: any;
    copyArrowCount = 0;
    dropDownHeight = 0;

    constructor() {
    }

    arrowDown(length) {
        this.currentLiElement = document.getElementById('rulesDropDown').querySelectorAll('li');
        this.dropDownHeight = this.currentLiElement[0].parentElement.offsetHeight;
        if (this.arrowCount >= length) {
            return;
        }
        this.totalListHeight = this.totalListHeight + this.currentLiElement[this.arrowCount].offsetHeight;
        this.currentLiElement[this.arrowCount].classList.add('active');
        if (this.arrowCount != 0) {
            this.currentLiElement[this.arrowCount - 1].classList.remove('active');
        }
        this.arrowCount++;
        if (this.dropDownHeight >= 250 && (this.dropDownHeight + this.currentLiElement[0].parentElement.scrollTop) < this.totalListHeight) {
            this.copyArrowCount++;
            if (this.currentLiElement[this.arrowCount]) {
                this.currentLiElement[0].parentElement.scrollTop =
                    this.currentLiElement[0].parentElement.scrollTop +
                    this.currentLiElement[this.arrowCount].offsetHeight;
            }
        }
    }

    arrowUp() {
        if (this.arrowCount === 1 || this.arrowCount === 0 || !this.currentLiElement[this.arrowCount - 1]) {
            return;
        }
        this.arrowCount--;
        this.currentLiElement[this.arrowCount].classList.remove('active');
        this.totalListHeight = this.totalListHeight - this.currentLiElement[this.arrowCount].offsetHeight;
        this.currentLiElement[this.arrowCount - 1].classList.add('active');

        if (this.currentLiElement[this.arrowCount].parentElement && this.currentLiElement.length > 4 && (this.currentLiElement[this.arrowCount].parentElement.scrollTop > this.totalListHeight - 40)) {
            this.copyArrowCount--;
            this.currentLiElement[0].parentElement.scrollTop =
                this.currentLiElement[0].parentElement.scrollTop -
                this.currentLiElement[this.arrowCount + 1].offsetHeight;
        }
    }

    resetDropDown() {
        this.totalListHeight = 0;
        this.arrowCount = 0;
        this.copyArrowCount = 0;
        if (this.currentLiElement && this.currentLiElement[0].parentElement) {
            this.currentLiElement[0].parentElement.scrollTop = 0;
        }
    }

    set totalKeysUpDown(value) {
        this.arrowCount = value;
    }

    get totalKeysUpDown() {
        return this.arrowCount;
    }
}
