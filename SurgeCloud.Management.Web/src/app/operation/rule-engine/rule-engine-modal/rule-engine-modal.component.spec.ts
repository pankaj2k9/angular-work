import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RuleEngineModalComponent } from './rule-engine-modal.component';

describe('RuleEngineModalComponent', () => {
  let component: RuleEngineModalComponent;
  let fixture: ComponentFixture<RuleEngineModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RuleEngineModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RuleEngineModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
