import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild, Input, Output, OnChanges, SimpleChanges, EventEmitter, HostListener, AfterViewInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { FormArray, FormControl, FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { ModelService } from './rule-engine-modal.component.service';
import { RuleEngineService } from './rule-engine-modal.service';
import { AppService } from '../../../app.service';
import { AdalService } from '../../../shared-module';
import {
  RuleForList,
  ArithmaticOperation,
  DropdownSettings,
  AlarmPriorities,
  AndOrOperation,
  ConditionalOperation
} from './rule-engine-modal.constant';
import * as moment from 'moment';
import * as _ from 'underscore';
import * as jQuery from 'jquery';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-rule-engine-modal',
  templateUrl: './rule-engine-modal.component.html',
  styleUrls: ['./rule-engine-modal.component.scss'],
  providers: [ModelService, RuleEngineService],
  encapsulation: ViewEncapsulation.None
})
export class RuleEngineModalComponent implements OnInit, AfterViewInit, OnChanges {
  @ViewChild('ruleEnginedModal') public ruleEnginedModal: ModalDirective;
  @Input() selectedRule: string;
  @Output() public close = new EventEmitter();
  public selectedExcludedDays = [];
  private templateForm: FormGroup;
  private keyUpRuleInput = new Subject<string>();
  private ruleTypes = RuleForList;
  private dataPoints = [];
  private dropDownList;
  private copyDropDownList;
  private isShowDropDown: Boolean = false;
  private ruleParameterNumber = -1;
  private assets = [];
  private selectedAssets = [];
  private selectedIndex = 0;
  private company = {};
  private isOutPut: boolean = false;
  private alarmPriorities = AlarmPriorities;
  private endTimeDisableRange = {};
  private endTimeReset = false;
  private startTimeDisableRange = {};
  private startTimeReset = false;
  private startTimeList = [];
  private selectedStartTime = '';
  private endTimeList = [];
  private selectedEndTime = '';
  private dropdownSettings = DropdownSettings;
  private isLoading: boolean = false;
  private selectedConditions = [];
  private isInvalidConditionalOperation: boolean = false;
  private selectedParameters = [];
  private currentSpaceIndex = -1;
  public keyCodeArray = [40, 38, 13, 37, 39];
  public totalConditions = [];
  public conditionCount = 0;
  public IfElseBlockArray = [];
  public startBlockPosition = -1;
  public endBlockPosition = -1;
  public copyRuleText = '';
  public isDirtyselectedDays: boolean = false;
  public nestedBlockCondition = '';
  public placeholderpoint:string;
  public placeholderdesc:string;
  constructor(private appService: AppService,
    private modelService: ModelService,
    private adalService: AdalService,
    private formBuilder: FormBuilder,
    private ruleEngineService: RuleEngineService,private translate: TranslateService) {
  }

  @HostListener('keydown.esc')
  public onEsc(): void {
    this.cancelRule(false);
  }

  ngOnInit() {
    this.setTemplateForm();
    this.subscribeDaySchedules();
    this.setTimeForSchedule();
    setTimeout(() => {
      this.ruleEnginedModal.show();
    });
    this.company = this.appService.getActiveCompany();
    this.getAsset();
    this.getDataPoint();
    this.setInputChangeEvent();
    document.getElementsByTagName('body')[0].addEventListener('click', () => {
      this.isShowDropDown = false;
    });
    this.translate.stream("rule_enter_point").subscribe(res => {
      if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
        this.placeholderpoint = res;

      }
    });
    this.translate.stream("rule_enter_desc").subscribe(res => {
      if(res.charAt(0) !== res.charAt(0).toLowerCase()) {
        this.placeholderdesc= res;

      }
    });
  }

  ngAfterViewInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  ngOnDestroy() {
  }

  onUnselectAll(event) {
    setTimeout(() => {
      this.selectedAssets = event;
      this.templateForm.controls['selectedAssets'].setValue(event);
    });
  }

  setTemplateForm() {
    let outputData = null;
    if (this.selectedRule) {
      outputData = JSON.parse(this.selectedRule['output']);
      this.selectedRule['assets'].forEach((asset) => {
        const singleasset = {
          id: asset.assetId,
          itemName: asset.assetId,
          gwSNO: asset.gwSNO
        };
        this.selectedAssets.push(singleasset);
      });
    }
    this.templateForm = new FormGroup({
      'ruleName': new FormControl(this.selectedRule ? this.selectedRule['name'] : '', Validators.required),
      'ruleText': new FormControl(this.selectedRule ? this.selectedRule['rule'] : '', Validators.required),
      'ruleType': new FormControl(this.selectedRule ? this.selectedRule['ruleType'] : 'realtime'),
      'isOutPut': new FormControl(this.selectedRule ? true : false),
      'selectedAssets': new FormControl(this.selectedAssets, Validators.required),
      'status': new FormControl(this.selectedRule ? (this.selectedRule['status'] === 1 ? true : false) : true, Validators.required),
      'alarm': this.formBuilder.group({
        'pointName': [outputData ? outputData.pointName : ''],
        'description': [outputData ? outputData.description : ''],
        'priority': [outputData ? outputData.priority : 'low'],
        'value': [outputData ? outputData.value : ''],
        'sts': [outputData ? outputData.sts : 1],
        'ref': [outputData ? outputData.ref : '']
      }),
      'daySchedules': new FormArray([
        this.getDayObject('Sunday', 'Sun', this.selectedRule ? this.selectedRule['daySchedule'].includes('Sun') : false),
        this.getDayObject('Monday', 'Mon', this.selectedRule ? this.selectedRule['daySchedule'].includes('Mon') : false),
        this.getDayObject('Tuesday', 'Tue', this.selectedRule ? this.selectedRule['daySchedule'].includes('Tue') : false),
        this.getDayObject('Wednesday', 'Wed', this.selectedRule ? this.selectedRule['daySchedule'].includes('Wed') : false),
        this.getDayObject('Thursday', 'Thu', this.selectedRule ? this.selectedRule['daySchedule'].includes('Thu') : false),
        this.getDayObject('Friday', 'Fri', this.selectedRule ? this.selectedRule['daySchedule'].includes('Fri') : false),
        this.getDayObject('Saturday', 'Sat', this.selectedRule ? this.selectedRule['daySchedule'].includes('Sat') : false),
      ]),
      'selectedDays': new FormControl(this.selectedRule ? this.selectedRule['daySchedule'].split(',') : [], Validators.required),
      'startTimeSchedule': new FormControl(this.selectedRule ? this.selectedRule['startTimeSchedule'] : '', Validators.required),
      'endTimeSchedule': new FormControl(this.selectedRule ? this.selectedRule['endTimeSchedule'] : '', Validators.required)
    });
    // console.log(this.templateForm);
  }

  subscribeDaySchedules() {
    this.templateForm.controls['daySchedules'].valueChanges.subscribe((value) => {
      const filteredValue = value.filter((item) => {
        return item.checked;
      });
      let newData = filteredValue.map((item) => {
        return item.value;
      });
      this.templateForm.controls['selectedDays'].setValue(newData);
    });
  }

  setTimeForSchedule() {
    this.startTimeList = this.getTimeRangeArray(0, 23);
    this.endTimeList = this.getTimeRangeArray(1, 24);
    this.selectedStartTime = this.selectedRule ? this.selectedRule['startTimeSchedule'] : '';
    this.selectedEndTime = this.selectedRule ? this.selectedRule['endTimeSchedule'] : '';
    if (this.selectedRule) {
      this.onStartTimeModelChange(this.selectedStartTime);
      this.onEndTimeModelChange(this.selectedEndTime);
    }
  }

  getDayObject(key, value, checked) {
    return new FormGroup({
      key: new FormControl(key),
      value: new FormControl(value),
      checked: new FormControl(checked)
    });
  }

  onCheckChange() {
    let prm = this.templateForm.controls['isOutPut'].value;
    this.templateForm.get('alarm').get('pointName').setValidators(prm ? [Validators.required] : null);
    this.templateForm.get('alarm').get('pointName').updateValueAndValidity();
    this.templateForm.get('alarm').get('description').setValidators(prm ? [Validators.required] : null);
    this.templateForm.get('alarm').get('description').updateValueAndValidity();
    this.templateForm.get('alarm').get('priority').setValidators(prm ? [Validators.required] : null);
    this.templateForm.get('alarm').get('priority').updateValueAndValidity();
  }

  setInputChangeEvent() {
    const subscription = this.keyUpRuleInput
      .map(event => { return event })
      .debounceTime(200)
      .distinctUntilChanged()
      .subscribe(data => {
        if (data['target'].value === '') {
          this.selectedParameters = [];
          this.conditionCount = 0;
        }
        if (this.keyCodeArray.includes(data['keyCode'])) {
          return;
        }
        if (this.selectedConditions.length > 0) {
          this.inputEventForIfElse(data);
        } else if (ConditionalOperation.includes(data['target'].value)) {
          this.setConditionalOperationAsDropDown();
        } else {
          let index = this.selectedParameters.length % 4;
          this.isInvalidConditionalOperation = false;
          if (data['target'].value.charAt(0) === '$' && this.dropDownList !== AndOrOperation) {
            this.defaultInputEvent(data);
          } else if (this.dropDownList === AndOrOperation) {
            this.isShowDropDown = true;
          }
        }
      });
  }

  defaultInputEvent(data) {
    let subStr = data['target'].value.substring(this.currentSpaceIndex).trim();
    if (this.currentSpaceIndex !== -1) {
      if (subStr.length === 0) {
        this.selectedParameters.splice(-1, 1);
        this.currentSpaceIndex = this.templateForm.controls['ruleText'].value.replace(this.selectedParameters[this.selectedParameters.length - 1], '').length - 2;
      }
      if (data['keyCode'] === 8 && this.dropDownList.includes(subStr)) {
        this.conditionCount--;
      }
    }
    if (data['keyCode'] === 8) {
      this.selectedParameters[this.selectedParameters.length - 1] = subStr;
    }
    if (data['keyCode'] === 8 && this.selectedParameters.length > 0 && data['target'].value.substring(this.currentSpaceIndex).trim() == this.selectedParameters[this.selectedParameters.length - 1]) {
      this.ruleTextChanged((this.selectedParameters.length - 1) % 4);
      this.filterDropDownData(this.selectedParameters[this.selectedParameters.length - 1].replace('$', ''));
    } else {
      let index = this.selectedParameters.length % 4;
      let valueArray = data['target'].value.split(' ');
      if (index === 1 || index === 3 || data['target'].value.substring(data['target'].value.lastIndexOf(' ')).trim().charAt(0) === '$') {
        this.ruleTextChanged(this.selectedParameters.length % 4);
        this.filterDropDownData(data['target'].value.substring(data['target'].value.lastIndexOf(' ')).trim().replace(/\$/g, ''));
      } else if (data['keyCode'] !== 8 && (!this.templateForm.value.ruleText.includes('"') && this.templateForm.value.ruleText.charAt(0) === '$' && valueArray[valueArray.length - 1].trim().length === 0 && (valueArray[valueArray.length - 2] !== '' && !isNaN(valueArray[valueArray.length - 2]))) || (data['target'].value.split('\"') && data['target'].value.split('\"').length > 2 && data['target'].value.split('\"')[data['target'].value.split('\"').length - 1].trim().length === 0 && data['target'].value.split('\"')[data['target'].value.split('\"').length - 2])) {
        (<HTMLInputElement>document.getElementById('ruleTextArea')).placeholder = '&&/||';
        this.dropDownList = AndOrOperation;
        this.conditionCount = 0;
        this.totalConditions.push(this.templateForm.value.ruleText);
        this.templateForm.value.ruleText = '';
        this.setRuleTextAutoHeight();
      }
    }
  }

  selectedDayNotifyChange(data) {
    this.isDirtyselectedDays = true;
    this.templateForm.controls['selectedDays'].setValue(data);
  }

  inputEventForIfElse(data) {
    jQuery('#rulesDropDown li').removeClass('active');
    let value = data['target'].value;
    if (value === '') {
      this.selectedConditions = [];
      this.isShowDropDown = false;
    }
    this.isInvalidConditionalOperation = this.parenthesesAreBalanced(value) ? false : true;
    if (!this.isInvalidConditionalOperation) {
      value = value.trim();
      let cursorPosition = value.slice(0, document.getElementById('ruleTextArea')['selectionStart']).length;
      this.startBlockPosition = value.lastIndexOf('(', cursorPosition);
      this.endBlockPosition = value.indexOf(')', cursorPosition);
      if (this.startBlockPosition !== -1 && this.endBlockPosition !== -1) {
        let str = value.substring(value.lastIndexOf('(', cursorPosition), value.indexOf(')', cursorPosition));
        str = str.replace('(', '');
        str = str.trim();
        if (str) {
          this.isShowDropDown = true;
          let index = 0;
          if (str === '$') {
            this.setDropDownContent(index);
          }
        }
      } else if ((value.lastIndexOf('}') === value.length - 1) && (value.length - 1 === cursorPosition - 1)) {
        this.isShowDropDown = true;
        this.dropDownList = ConditionalOperation;
      } else if (value) {
        this.nestedBlockCondition = value.split(' ').join('');
        this.nestedBlockCondition = this.nestedBlockCondition.replace('if(){', '');
        this.nestedBlockCondition = this.nestedBlockCondition.replace('}', '');
        this.nestedBlockCondition = this.nestedBlockCondition.split(/\n/g).join('');
        if (this.nestedBlockCondition) {
          this.isShowDropDown = true;
          this.dropDownList = ConditionalOperation;
        }
      } else {
        this.isShowDropDown = false;
      }
    }
  }

  parenthesesAreBalanced(string) {
    let parentheses = "{}()",
      stack = [],
      i, character, bracePosition;

    for (i = 0; character = string[i]; i++) {
      bracePosition = parentheses.indexOf(character);
      if (bracePosition === -1) {
        continue;
      }
      if (bracePosition % 2 === 0) {
        stack.push(bracePosition + 1); // push next expected brace position
      } else {
        if (stack.length === 0 || stack.pop() !== bracePosition) {
          return false;
        }
      }
    }
    return stack.length === 0;
  }


  ruleTextChanged(index) {
    let isNew = this.templateForm.value.ruleText.charAt(this.templateForm.value.ruleText.length - 1) === '$';
    let isSpace = this.templateForm.value.ruleText.charAt(this.templateForm.value.ruleText.length - 1);
    this.isShowDropDown = true;
    this.setDropDownContent(index);
  }

  filterDropDownData(value) {
    this.modelService.totalKeysUpDown = 0;
    this.modelService.resetDropDown();
    if (value === '') {
      this.dropDownList = this.copyDropDownList;
    } else {
      this.dropDownList = _.filter(this.copyDropDownList, function (obj) {
        return obj.toLowerCase().includes(value.toLowerCase());
      });
    }
  }

  getAsset() {
    this.ruleEngineService.getAsset(this.company['companyId']).subscribe(data => {
      data.forEach((asset) => {
        const singleasset = {
          id: asset.assetId,
          itemName: asset.assetId,
          gwSNO: asset.gwSNO
        }
        this.assets.push(singleasset);
      });
    }), (error) => {
      console.log(error);
    };
  }

  getDataPoint() {
    this.ruleEngineService.getDataPoint(this.company['companyId'], this.templateForm.value.ruleType).subscribe(data => {
      this.dataPoints = data;
      this.dropDownList = data;
      this.copyDropDownList = data;
    }), (error) => {
      console.log(error);
    };
  }

  arrowDown() {
    if (this.isShowDropDown) {
      this.modelService.arrowDown(this.dropDownList.length);
    }
  }

  arrowUp(event) {
    event.preventDefault();
    if (this.isShowDropDown) {
      this.modelService.arrowUp();
    }
  }

  removeSearchCharacter() {
    let value = this.templateForm.value.ruleText.trim();
    this.currentSpaceIndex = this.templateForm.value.ruleText.lastIndexOf(' ');
    let subStr = this.templateForm.value.ruleText.substring(this.currentSpaceIndex);
    subStr = subStr.replace('{', '');
    subStr = subStr.replace('}', '');
    if (this.selectedParameters.length === 0) {
      this.templateForm.value.ruleText = this.templateForm.value.ruleText.replace(subStr, '');
    } else {
      this.templateForm.value.ruleText = this.templateForm.value.ruleText.replace(subStr, ' ');
    }
  }

  addToRuleText(index) {
    this.selectedIndex = index;
    if (this.isShowDropDown && this.dropDownList[this.selectedIndex]) {
      this.removeSearchCharacter();
      if (this.dropDownList === ConditionalOperation) {
        this.checkIsConditionalOperation(this.dropDownList[this.selectedIndex]);
      } else if (this.startBlockPosition !== -1 && this.endBlockPosition !== -1) {
        this.templateForm.value.ruleText.replace(this.templateForm.value.ruleText.substring(this.startBlockPosition, this.endBlockPosition), '');
        this.templateForm.value.ruleText = this.templateForm.value.ruleText.slice(0, this.startBlockPosition + 2) + ('$' + this.dropDownList[this.selectedIndex]) + this.templateForm.value.ruleText.slice(this.startBlockPosition);
        this.startBlockPosition = -1;
        this.endBlockPosition = -1;
      } else {
        let currentDropDownIndex = this.selectedParameters.length % 4;
        if (currentDropDownIndex !== 0 && currentDropDownIndex !== 2) {
          this.templateForm.value.ruleText = this.templateForm.value.ruleText + this.dropDownList[this.selectedIndex];
          this.selectedParameters.push(this.dropDownList[this.selectedIndex]);
        } else {
          this.templateForm.value.ruleText = this.templateForm.value.ruleText + '$' +
            this.dropDownList[this.selectedIndex];
          this.selectedParameters.push('$' + this.dropDownList[this.selectedIndex]);
        }
        this.conditionCount++;
      }
      this.isShowDropDown = false;
      this.modelService.resetDropDown();
    }
    this.setConditionAsTag();
  }

  setConditionAsTag() {
    if (this.conditionCount === 3 || this.dropDownList[this.selectedIndex] === '||' || this.dropDownList[this.selectedIndex] === '&&') {
      this.totalConditions.push(this.conditionCount === 3 ? this.templateForm.value.ruleText : this.dropDownList[this.selectedIndex]);
      this.conditionCount = 0;
      if (this.dropDownList[this.selectedIndex] === '||' || this.dropDownList[this.selectedIndex] === '&&') {
        this.dropDownList = this.dataPoints;
        (<HTMLInputElement>document.getElementById('ruleTextArea')).placeholder = '$';
      } else {
        this.dropDownList = AndOrOperation;
        (<HTMLInputElement>document.getElementById('ruleTextArea')).placeholder = '&&/||';
      }
      this.templateForm.value.ruleText = '';
      this.setRuleTextAutoHeight();
    }
  }

  setRuleTextAutoHeight() {
    setTimeout(() => {
      document.getElementById('ruleTextArea').style.paddingTop = jQuery('#totalConditions').innerHeight() + 5 + 'px';
      if ((Number(jQuery('#ruleTextArea').css('height').replace('px', '')) - Number(jQuery('#ruleTextArea').css('padding-top').replace('px', '')) < 50)) {
        jQuery('#ruleTextArea').css('height', (Number(jQuery('#ruleTextArea').css('height').replace('px', '')) + 100) + 'px');
      }
    });
    setTimeout(() => {
      jQuery('#ruleTypeDropdown').css('top', jQuery('#ruleTextArea').css('height'));
    });
  }

  checkIsConditionalOperation(value) {
    if (this.selectedConditions.length === 0 && this.selectedIndex !== 0 && this.selectedConditions.length === 0) {
      this.templateForm.value.ruleText = '';
      this.isInvalidConditionalOperation = true;
    } else if ((value === ConditionalOperation[2] && this.selectedConditions[this.selectedConditions.length - 1] === ConditionalOperation[2]) ||
      (value === ConditionalOperation[1] && this.selectedConditions[this.selectedConditions.length - 1] === ConditionalOperation[2])) {
      this.isInvalidConditionalOperation = true;
    } else {
      if (this.nestedBlockCondition) {
        this.templateForm.value.ruleText = this.copyRuleText.slice(0, this.currentSpaceIndex + 1) + value + ' () { \n }' + this.copyRuleText.slice(this.currentSpaceIndex + 1);
        this.nestedBlockCondition = '';
      } else if (this.selectedIndex === 2) {
        this.templateForm.value.ruleText = this.templateForm.value.ruleText + value + '{ \n }';
      } else {
        this.templateForm.value.ruleText = this.templateForm.value.ruleText + value + ' () { \n }';
      }
      this.selectedConditions.push(value);
      this.copyRuleText = this.templateForm.value.ruleText;
    }
  }

  setDropDownContent(index) {
    if (index === 1) {
      this.dropDownList = ArithmaticOperation;
      this.copyDropDownList = JSON.parse(JSON.stringify(ArithmaticOperation));
    } else if (index === 3) {
      this.dropDownList = AndOrOperation;
      this.copyDropDownList = JSON.parse(JSON.stringify(AndOrOperation));
    } else {
      this.dropDownList = this.dataPoints;
      this.copyDropDownList = JSON.parse(JSON.stringify(this.dataPoints));
    }
  }

  setConditionalOperationAsDropDown() {
    jQuery('#rulesDropDown li').removeClass('active');
    this.isShowDropDown = true;
    this.isInvalidConditionalOperation = false;
    this.dropDownList = ConditionalOperation;
    this.copyDropDownList = JSON.parse(JSON.stringify(ConditionalOperation));
  }

  deleteCondition() {
    this.totalConditions.splice(-1, 1);
    this.isShowDropDown = false;
    if ((<HTMLInputElement>document.getElementById('ruleTextArea')).placeholder === '$') {
      (<HTMLInputElement>document.getElementById('ruleTextArea')).placeholder = '&&/||';
      this.dropDownList = AndOrOperation;
    } else {
      (<HTMLInputElement>document.getElementById('ruleTextArea')).placeholder = '$';
      this.dropDownList = this.dataPoints;
    }
    setTimeout(() => {
      document.getElementById('ruleTextArea').style.paddingTop = jQuery('#totalConditions').innerHeight() + 5 + 'px';
    });
  }

  getRoleData() {
    let assets = [];
    this.selectedAssets.forEach(asset => {
      assets.push({
        'assetId': asset.id,
        'gwSNO': asset.gwSNO
      });
    });
    let daySchedules = '';
    this.templateForm.controls['selectedDays'].value.forEach((day, index) => {
      if (index != this.templateForm.controls['selectedDays'].value.length - 1) {
        daySchedules = daySchedules + day + ',';
      } else {
        daySchedules = daySchedules + day;
      }
    });
    let days = [];
    this.selectedExcludedDays.forEach((date) => {
      let tempDate = moment();
      if (this.appService.userTimezone != null) {
        tempDate = moment.tz(date, this.appService.userTimezone);
        tempDate = moment(tempDate).utcOffset(-tempDate.utcOffset());
      } else {
        tempDate = moment(date).utc();
      }
      days.push(tempDate.format('MM/DD/YYYY'));
    });
    return {
      'companyId': this.company['companyId'],
      'companyName': this.company['name'],
      'name': this.templateForm.value.ruleName,
      'ruleType': this.templateForm.value.ruleType,
      'rule': this.templateForm.value.ruleText,
      'output': JSON.stringify(this.templateForm.value.alarm),
      'status': this.templateForm.value.status === true ? 1 : 0,
      'assets': assets,
      'daySchedule': daySchedules,
      'excludedDays': days,
      'startTimeSchedule': this.templateForm.value.startTimeSchedule,
      'endTimeSchedule': this.templateForm.value.endTimeSchedule
    }
  }

  saveRule() {
    if (this.templateForm.valid) {
      let obj = this.getRoleData();
      let prm = '';
      if (this.selectedRule) {
        prm = 'UpdateRule';
        obj['ruleId'] = this.selectedRule['ruleId'];
        obj['updatedBy'] = this.adalService.userInfo.profile.name;
      } else {
        prm = 'CreateRule';
        obj['createdBy'] = this.adalService.userInfo.profile.name;
      }
      this.isLoading = true;
      this.ruleEngineService.saveRule(obj, prm)
        .finally(() => {
          this.isLoading = false;
        })
        .subscribe(response => {
          if (this.selectedRule) {
            this.appService.showSuccessMessage('Success', 'Rule updated successfully');
          } else {
            this.appService.showSuccessMessage('Success', 'Rule saved successfully');
          }
          this.cancelRule(true);
        }), (error) => {
          console.log(error);
        };
    }
  }

  testRule() {
    if (this.dropDownList === AndOrOperation) {
      let prm = 'GetRealTimeData';
      if (this.templateForm.value.ruleType === 'alarm') {
        prm = 'GetAlarmData';
      } else if (this.templateForm.value.ruleType === 'geo') {
        prm = 'GetGeoData';
      }
      this.isLoading = true;
      console.log(this.selectedAssets);
      let obj = { companyId: this.company['companyId'], gwSno: this.selectedAssets[0].gwSNO, assetId: this.selectedAssets[0].id };
      this.ruleEngineService.testRule(obj)
        .finally(() => {
          this.isLoading = false;
        })
        .subscribe(response => {
          console.log(response);
        }), (error) => {
          console.log(error);
        };
    } else {
      this.isInvalidConditionalOperation = true;
    }
  }

  cancelRule(prm) {
    //  ["$Absolute Fuel Rail Pressure:0 == $Absolute Fuel Rail Pressure:0", "&&", "$Absolute Fuel Rail Pressure:0 == $Absolute Fuel Rail Pressure:0"]
    console.log(this.totalConditions);
    this.totalConditions.forEach((element, index) => {
      if ((index + 1) % 2 === 0) {
        console.log(element);
      }
    });
    // this.close.emit(prm);
    // this.ruleEnginedModal.hide();
    // this.templateForm.reset();
  }

  getTimeRangeArray(startPoint, endPoint) {
    const timeArray = [];
    for (let i = startPoint; i <= endPoint; i++) {
      let pointValue = i;
      if (i < 10) {
        pointValue = '0' + i;
      }
      pointValue = pointValue + ':00';
      const timeObject = {
        displayValue: pointValue,
        value: i
      }
      timeArray.push(timeObject);
    }
    return timeArray;
  }

  onStartTimeModelChange(value) {
    if (value) {
      value = value.split(':');
      this.endTimeDisableRange = {
        start: 0,
        end: Number(value[0])
      };
      this.endTimeReset = false;
    } else {
      this.endTimeReset = true;
    }
    this.endTimeList = this.getTimeRangeArray(1, 24);
  }

  onStartTimeChange() {
    const startTimeValue = this.templateForm.controls['startTimeSchedule'].value;
    const endTimeValue = this.templateForm.controls['endTimeSchedule'].value;
    if (startTimeValue != '' && endTimeValue <= startTimeValue) {
      const splitValue = startTimeValue.split(':');
      let valueInNumber = Number(splitValue[0]);
      valueInNumber = valueInNumber + 1;
      let valueInString = '';
      if (valueInNumber < 10) {
        valueInString = '0' + valueInNumber;
      } else {
        valueInString = String(valueInNumber);
      }
      this.selectedEndTime = valueInString + ':00';
    }
  }

  onEndTimeModelChange(value) {
    if (value) {
      value = value.split(':');
      this.startTimeDisableRange = {
        start: Number(value[0]),
        end: 23
      };
      this.startTimeReset = false;
    } else {
      this.startTimeReset = true;
    }
    this.startTimeList = this.getTimeRangeArray(0, 23);
  }

  onRuleTypeChange(value) {
    this.templateForm.value.ruleType = value;
    this.templateForm.value.ruleText = '';
    this.ruleParameterNumber = 0;
    this.isShowDropDown = false;
    this.getDataPoint();
  }

  selectedExcludeDays(data) {
    this.selectedExcludedDays = data;
  }

  unpublishRule(rule) {

  }

  publishRule(rule) {

  }

}
