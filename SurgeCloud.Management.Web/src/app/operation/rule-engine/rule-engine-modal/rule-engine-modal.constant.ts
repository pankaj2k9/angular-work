export const RuleForList = ['realtime', 'alarm', 'geo'];

export const ArithmaticOperation = ['>', '>=', '==', '<', '<=']

export const AndOrOperation = ['&&', '||']

export const ConditionalOperation = ['if', 'else if', 'else']


export const DropdownSettings = {
    singleSelection: false,
    text: "Select Assets",
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    enableSearchFilter: true,
    classes: "assets-multiselect"
};

export const AlarmPriorities = ['low', 'medium', 'high']

