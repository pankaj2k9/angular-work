import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http'
import { HttpService } from '../../../shared-module';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class RuleEngineService {
    arrowCount = 0;
    totalListHeight = 0;
    currentLiElement: any;
    copyArrowCount = 0;
    dropDownHeight = 0;

    constructor(private httpService: HttpService) {
    }

    getAsset(companyId: string): Observable<any> {
        return this.httpService.get(`device/api/iot/get/${companyId}`)
            .map(res => res.json())
            .catch(err => Observable.throw(err));
    }

    getRules(id): Observable<any> {
        return this.httpService.get(`device/api/Iot/getrules/${id}`)
            .map(res => res.json())
            .catch(err => Observable.throw(err));
    }

    getDataPoint(id, pointType): Observable<any> {
        return this.httpService.get(`device/api/Iot/GetDatapoint/${id}?pointType=${pointType}`)
            .map(res => res.json())
            .catch(err => Observable.throw(err));
    }

    saveRule(data, prm): Observable<any> {
        return this.httpService.post(`device/api/Iot/${prm}`, data)
            .map(res => res.json())
            .catch(err => Observable.throw(err));
    }

    testRule(obj: { companyId: string, gwSno: string, assetId: string }): Observable<any> {
        return this.httpService.get(encodeURI(`device/api/iot/getrealtimedata?companyId=${obj.companyId}&gwSno=${obj.gwSno}&assetId=${obj.assetId}`))
            .map(res => res.json())
            .catch(err => Observable.throw(err));
    }

}
