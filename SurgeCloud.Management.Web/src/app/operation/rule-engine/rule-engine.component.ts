import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { FormArray, FormControl, FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { PaginationInfo, FilterWidthData } from './rule-engine.constant';
import { AppService } from './../../app.service';
import { AdalService } from '../../shared-module';
import { RuleEngineService } from './rule-engine.service';
import * as jQuery from 'jquery';
import * as _ from 'underscore';
import * as moment from 'moment';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-rule-engine',
  templateUrl: './rule-engine.component.html',
  styleUrls: ['./rule-engine.component.scss']
})
export class RuleEngineComponent implements OnInit, OnDestroy {
  @ViewChild('confirmModal') public confirmModal: ModalDirective;
  private data = [];
  private paginationInfo = PaginationInfo;
  private company = {};
  private selectedRule = null;
  private isRuleModal: boolean = false;
  private ruleMessage = 'Loading...';
  private userInfo = {};
  private toggleFilterSearch: boolean = false;
  private filterValues = {};
  private filterWidthData = FilterWidthData;
  public mainBodyHeight:any;
  private activeCompanyInfoSubscriber:any;
  constructor(private appService: AppService,
    private adalService: AdalService,
    private formBuilder: FormBuilder,
    private ruleEngineService: RuleEngineService,private translate: TranslateService) {
    this.activeCompanyInfoSubscriber = this.appService.activeCompanyInfo.subscribe(
      (company) => {
        this.company = company;
        this.getRuleList();
      }
    );
    this.appService.setLogInUserDetails().subscribe(
      (userInfo) => {
        this.userInfo = userInfo;
      }
    );
  }

  ngOnInit() {
    this.company = this.appService.getActiveCompany();
    if (this.company && this.company['companyId']) {
      this.getRuleList();
    }
    this.resetFilterData();
  }

  ngOnDestroy() {
    if(this.activeCompanyInfoSubscriber)
    this.activeCompanyInfoSubscriber.unsubscribe();
  }

  resetFilterData() {
    this.toggleFilterSearch = false;
    this.filterValues = {
      'name': '',
      'tOnDate': '',
      'publishedOn': '',
      'status': ''
    };
  }

  updateData(event) {
    this.data = event;
    this.appService.appExportData.next(event);
    this.appService.setAllOptionPagination('ruleMfBootstrapPaginator');
  }

  getRuleList() {

    this.ruleMessage = 'Loading...';
    this.data = [];
    this.ruleEngineService.getRules(this.company['companyId']).subscribe((data) => {
      if(this.translate.currentLang= "es"){
        this.ruleMessage = data.length === 0 ? 'No hay regla' : '';
      }else{
        this.ruleMessage = data.length === 0 ? 'No Rule exists' : '';
      }
      this.data = data;
      this.data.forEach(rule => {
        if (rule.updatedOn) {
          rule.tOnDate = rule.updatedOn;
        } else {
          rule.tOnDate = rule.createdOn;
        }
        rule.publishedOn = rule.publishedOn;
      });
      this.appService.setAllOptionPagination('ruleEngineTable');
      setTimeout(() => {
        jQuery('.rule-engine').css('min-height', (jQuery(document).height() - 100) + 'px');
        this.mainBodyHeight = jQuery(document).height() - 220;
      });
    }, (err) => {
      console.log(err);
      this.ruleMessage = 'Something Wrong...';
    })
  }

  openAddRuleModal() {
    this.isRuleModal = true;
    this.selectedRule = null;
  }

  updateRuleModal(event) {
    this.isRuleModal = false;
    if (event) {
      this.getRuleList();
    }
    this.resetFilterData();
  }

  editRule(rule) {
    this.isRuleModal = true;
    this.selectedRule = rule;
  }

  deleteRule() {
    console.log(this.selectedRule);
    this.appService.loader.next(true);
    this.confirmModal.hide();
    this.ruleEngineService.deleteRule(this.selectedRule)
      .finally(() => {
        this.appService.loader.next(false);
      })
      .subscribe((data) => {
        this.appService.showSuccessMessage('Success', 'Rule deleted successfully');
        this.getRuleList();
      }, (err) => {
        this.appService.showErrorMessage('Error', 'Something wrong!');
      })
  }

  toggleFilter() {
    this.toggleFilterSearch = !this.toggleFilterSearch;
  }



}
