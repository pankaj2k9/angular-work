export const PaginationInfo = {
    sortOrder: 'asc',
    sortBy: 'dataoccured',
    numberRowPerPage: 10
};

export const FilterWidthData = [
    {
        'width': 30,
        'id': 'name',
        'prm': true
    },
    {
        'width': 24,
        'id': 'tOnDate',
        'prm': true
    },
    {
        'width': 24,
        'id': 'publishedOn',
        'prm': true
    },
    {
        'width': 8,
        'id': 'status',
        'prm': true
    },
    {
        'width': 14,
        'id': 'action',
        'prm': false
    },
]