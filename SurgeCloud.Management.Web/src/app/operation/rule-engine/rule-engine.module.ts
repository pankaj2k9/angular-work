import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'angular2-datatable';
import { RuleEngineComponent } from './rule-engine.component';
import { RuleEngineService } from './rule-engine.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared-module/shared-module.module';
import { CoreModule } from '../../core';
import { ModalModule } from 'ngx-bootstrap';
import { RuleEngineModalComponent } from './rule-engine-modal/rule-engine-modal.component';
import { RuleEngineRoutingModule } from "./rule-engine-routing.module";
import {TranslateModule,TranslateLoader,TranslateCompiler,TranslateParser,MissingTranslationHandler} from "@ngx-translate/core";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {HttpLoaderFactory} from "../../app.module";

@NgModule({
  imports: [
    RuleEngineRoutingModule,
    CommonModule,
    DataTableModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
    CoreModule,
    ModalModule.forRoot(),
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [RuleEngineComponent, RuleEngineModalComponent],
  exports: [RuleEngineComponent],
  providers: [RuleEngineService]
})
export class RuleEngineModule { }
