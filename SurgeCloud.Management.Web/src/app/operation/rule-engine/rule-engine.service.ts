import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http'
import { HttpService } from '../../shared-module';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class RuleEngineService {
    arrowCount = 0;
    totalListHeight = 0;
    currentLiElement: any;
    copyArrowCount = 0;
    dropDownHeight = 0;

    constructor(private httpService: HttpService) {
    }

    getRules(id): Observable<any> {
        return this.httpService.get(`device/api/Iot/getrules/${id}`)
            .map(res => res.json())
            .catch(err => Observable.throw(err));
    }

    deleteRule(data): Observable<any> {
        return this.httpService.post(`device/api/Iot/DeleteRule`, data)
            .map(res => res.json())
            .catch(err => Observable.throw(err));
    }
}
