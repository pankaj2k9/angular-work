import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-day-scheduler',
  templateUrl: './day-scheduler.component.html',
  styleUrls: ['./day-scheduler.component.scss']
})
export class DaySchedulerComponent implements OnInit {

  @Output() notifyChange = new EventEmitter();
  @Input() data;
  public daySchedules = [
    { 'name': 'S', 'text': 'Sunday', 'sort': 'Sun' },
    { 'name': 'M', 'text': 'Monday', 'sort': 'Mon' },
    { 'name': 'T', 'text': 'Tuesday', 'sort': 'Tue' },
    { 'name': 'W', 'text': 'Wednesday', 'sort': 'Wed' },
    { 'name': 'T', 'text': 'Thursday', 'sort': 'Thu' },
    { 'name': 'F', 'text': 'Friday', 'sort': 'Fri' },
    { 'name': 'S', 'text': 'Saturday', 'sort': 'Sat' }];

  public daySortNameList = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  public selectedDayList = [];

  constructor() {
    this.selectedDayList = [];
  }

  ngOnInit() {
    this.data.forEach((item) => {
      let index = this.daySortNameList.indexOf(item);
      if (index > -1) {
        this.selectedDayList[index] = true;
      }
    });
  }

  toggleSelectedDay(index) {
    this.selectedDayList[index] = !this.selectedDayList[index];
    let selectedDay = [];
    this.selectedDayList.forEach((data, index) => {
      if (data) {
        selectedDay.push(this.daySchedules[index]);
      }
    });
    this.notifyChange.next(selectedDay);
  }

}
