import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExcludeDaysComponent } from './exclude-days.component';

describe('ExcludeDaysComponent', () => {
  let component: ExcludeDaysComponent;
  let fixture: ComponentFixture<ExcludeDaysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExcludeDaysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExcludeDaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
