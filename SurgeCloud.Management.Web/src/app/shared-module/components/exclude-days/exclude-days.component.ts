import { Component, OnInit, OnChanges, SimpleChanges, Input, EventEmitter, Output, Injector } from '@angular/core';
import { AppService } from '../../../app.service';
import * as jQuery from 'jquery';
import * as _ from 'underscore';
import * as moment from 'moment';

@Component({
  selector: 'app-exclude-days',
  templateUrl: './exclude-days.component.html',
  styleUrls: ['./exclude-days.component.scss']
})
export class ExcludeDaysComponent implements OnInit, OnChanges {

  @Input() days = [];
  @Input() excludedDays = [];
  @Output() public selected: EventEmitter<any> = new EventEmitter();

  private appService: AppService;
  constructor(injector: Injector) {
    this.appService = injector.get(AppService);
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.excludedDays.forEach((element, index) => {
      this.excludedDays[index] = this.getUserTimeZoneDate(element).format('MM/DD/YYYY');
    });
  }

  parseDate(dateString: string) {
    let date = moment(dateString).format('MM/DD/YYYY');
    if (!this.excludedDays.includes(date)) {
      this.excludedDays.push(date);
      this.selected.next(this.excludedDays);
    }
    (<HTMLInputElement>document.getElementById('exd-date-type')).value = null;
  }

  removeDate(index) {
    this.excludedDays.splice(index, 1);
    this.selected.next(this.excludedDays);
  }

  getUserTimeZoneDate(value) {
    let convertedTimezone;
    if (this.appService.userTimezone != null) {
      convertedTimezone = moment.tz(value, this.appService.userTimezone);
    } else {
      convertedTimezone = moment.utc(value).local();
    }
    return convertedTimezone;
  }

}
