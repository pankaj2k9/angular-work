import { Component, OnInit, OnDestroy, OnChanges, SimpleChanges, Input, Injector } from '@angular/core';
import { AppService } from '../../../app.service';
import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';
import * as _ from 'underscore';
import * as jQuery from 'jquery';
declare var moment: any;
declare var jsPDF: any; // Important

type AOA = Array<Array<any>>;
function s2ab(s: string): ArrayBuffer {
  const buf: ArrayBuffer = new ArrayBuffer(s.length);
  const view: Uint8Array = new Uint8Array(buf);
  for (let i = 0; i !== s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
  return buf;
}

@Component({
  selector: 'app-export-data',
  templateUrl: './export-data.component.html',
  styleUrls: ['./export-data.component.scss']
})
export class ExportDataComponent implements OnInit, OnChanges, OnDestroy {
  @Input() data;
  @Input() headers;
  @Input() src = '';
  @Input() fileTagName = '';

  private downloadableColumn = [];
  private appService: AppService;
  constructor(injector: Injector) {
    this.appService = injector.get(AppService);
  }
  private cloneData = []
  private dates = [];
  public exportDiv: any;
  public wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'binary' };
  public validExts = new Array(".xlsx", ".xls");
  public appExportDataSubscription: any;
  public sortByPrm: string;
  public sortByType: string;
  public copyHeaders = [];
  ngOnInit() {
    this.dates = [];
    this.copyHeaders = JSON.parse(JSON.stringify(this.headers));
    this.copyHeaders.forEach((element, index) => {
      if (element['dataKey'].indexOf('|date') > -1) {
        this.copyHeaders[index]['dataKey'] = element['dataKey'].replace('|date', '');
        this.dates.push(element['dataKey'].replace('|date', ''));
      }
      this.downloadableColumn.push(this.copyHeaders[index]['dataKey']);
    });
    this.appExportDataSubscription = this.appService.appExportData.subscribe((data) => {
      this.cloneData = JSON.parse(JSON.stringify(data));
    });
    jQuery('thead th mfdefaultsorter a').unbind('click');
    this.sortDataEvent();
  }

  ngOnDestroy() {
    this.appExportDataSubscription.unsubscribe();
  }

  sortDataEvent() {
    jQuery('thead th mfdefaultsorter a').click((event) => {
      let tableAttr = jQuery('table').attr('ng-reflect-input-data');
      if (typeof tableAttr !== typeof undefined && tableAttr) {
        this.sortByPrm = jQuery(event.target).parent().attr('by');
        this.sortByType = jQuery('table').attr('ng-reflect-sort-order');
        let data = this.getSortOrderData();
        this.appService.appExportData.next(data);
      }
    });
  }

  getSortOrderData() {
    let data = _.sortBy(this.cloneData, (item) => {
      return typeof item[this.sortByPrm] === 'string' ? item[this.sortByPrm].toLowerCase() : item[this.sortByPrm];
    });
    if (this.sortByType === 'desc') {
      data.reverse();
    }
    return data;
  }

  ngOnChanges(changes: SimpleChanges) {
    this.cloneData = JSON.parse(JSON.stringify(this.data));
  }

  showExportAsOptions(event) {
    event.stopPropagation();
    this.exportDiv = document.getElementById('exportAsOptions');
    if (this.exportDiv) {
      this.exportDiv.classList.add('export-as-options-show');
    }
  }

  getPdfObjetc() {
    let pdf = new jsPDF('p', 'pt', 'letter');
    pdf.cellInitialize();
    return pdf;
  }

  setRowForPdf() {
    let data = JSON.parse(JSON.stringify(this.cloneData));
    data.forEach(row => {
      _.each(row, (value, key, obj) => {
        let cellValue = value === null || value === '' || value === undefined ? " " : value.toString();
        cellValue = key === 'status' && value.toString().toLowerCase() !== 'ok' ? this.setStringAsStatus(!isNaN(cellValue) ? Number(cellValue) : cellValue) : cellValue;
        cellValue = (this.dates.includes(key) && moment(cellValue).isValid()) ? this.getDateFormat(cellValue) : cellValue;
        row[key] = cellValue;
      });
    });
    return data;
  }

  setColumnWidth() {
    let obj = {};
    this.copyHeaders.forEach((element) => {
      obj[element.dataKey] = { columnWidth: element.width };
    });
    return obj;
  }

  downloadAsPDF() {
    let pdf = this.getPdfObjetc();
    let data = this.setRowForPdf();
    pdf.autoTable(this.copyHeaders, data, {
      headerStyles: { textColor: '#ffffff', fillColor: [86, 96, 124] },
      styles: { fontSize: 7, overflow: 'linebreak', columnWidth: 'wrap' },
      columnStyles: this.setColumnWidth(),
      margin: { top: 20, right: 5, left: 5 },
    });
    pdf.save(this.fileTagName + '_' + this.appService.getActiveCompany().name + '_' + moment().format('YYYYMMDDHHMMSS') + '.pdf');
    // this.cloneData = JSON.parse(JSON.stringify(this.data));
  }

  downloadAsExcel() {
    let excelData = [];
    let excelHeader = [];
    this.copyHeaders.forEach(header => {
      excelHeader.push(header.title);
    });
    excelData.push(excelHeader);
    JSON.parse(JSON.stringify(this.cloneData)).forEach((element, index) => {
      excelData[index + 1] = [];
      _.each(JSON.parse(JSON.stringify(element)), (value, key, obj) => {
        if (this.downloadableColumn.includes(key)) {
          let cellValue = value === null || value === '' || value === undefined ? '' : value.toString();
          cellValue = key === 'status' && value.toString().toLowerCase() !== 'ok' ? this.setStringAsStatus(!isNaN(cellValue) ? Number(cellValue) : cellValue) : cellValue;
          cellValue = (this.dates.includes(key) && moment(cellValue).isValid()) ? this.getDateFormat(cellValue) : cellValue;
          let cellIndex = Number(this.downloadableColumn.indexOf(key));
          excelData[index + 1][cellIndex] = cellValue;
        }
      });
    });
    /* generate worksheet */
    const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(excelData);
    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    /* save to file */
    const wbout: string = XLSX.write(wb, this.wopts);
    saveAs(new Blob([s2ab(wbout)]), this.fileTagName + '_' + this.appService.getActiveCompany().name + '_' + moment().format('YYYYMMDDHHMMSS') + '.xlsx');
    // this.cloneData = JSON.parse(JSON.stringify(this.data));
  }

  getDateFormat(value) {
    if (!value.includes('Z')) value = value + 'Z';
    let convertedTimezone = '';
    if (this.appService.userTimezone != null) {
      convertedTimezone = moment.tz(value, this.appService.userTimezone).format('MM/DD/YYYY, hh:mm A');
    } else {
      convertedTimezone = moment.utc(value).local().format('MM/DD/YYYY, hh:mm A');
    }
    return convertedTimezone;
  }

  setStringAsStatus(str) {
    if (str === 0) {
      return this.cloneData[0]['isNormalStringAsStatus'] ? 'Normal' : 'Inactive';
    } else if (str === 1) {
      return 'Active';
    } else if (str === 2) {
      return 'Acknowledged'
    } else {
      return str;
    }
  }
}
