import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NativeColorPickerComponent } from './native-color-picker.component';

describe('NativeColorPickerComponent', () => {
  let component: NativeColorPickerComponent;
  let fixture: ComponentFixture<NativeColorPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NativeColorPickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NativeColorPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
