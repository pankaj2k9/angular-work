import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-native-color-picker',
  templateUrl: './native-color-picker.component.html',
  styleUrls: ['./native-color-picker.component.scss']
})
export class NativeColorPickerComponent implements OnInit {

  constructor() { }
  @Output() onColorChange = new EventEmitter();
  @Input() selectedColor;
  public colors = ['darkblue', 'blue', 'skyblue', 'powderblue', 'darkgreen', 'green', 'lightgreen', 'yellowgreen', 'darkred', 'red', 'indianred', 'palevioletred', 'yellow', 'orange', 'black', 'white'];
  public toggleDropDown: boolean = false;
  ngOnInit() {
    // this.onColorChange.emit();
    this.selectedColor = this.selectedColor ? this.selectedColor : '#FF4136';
    document.getElementsByTagName('body')[0].addEventListener('click', () => {
      this.toggleDropDown = false;
    });
  }

  onSelectColor(color) {
    this.selectedColor = color;
    this.onColorChange.emit(color);
    this.toggleDropDown = false;
  }

}
