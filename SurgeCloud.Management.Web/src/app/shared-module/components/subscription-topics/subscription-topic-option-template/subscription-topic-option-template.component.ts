import {
  AfterViewInit, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input,
  OnChanges,
  OnInit,
  Output, ViewChild
} from '@angular/core';
import { ModalDirective } from "ngx-bootstrap";
import { FormArray, FormControl, FormGroup, Validators } from "@angular/forms";
import { AssetDashboardService } from "../../../../operation/asset-dashboard/index";
import { CompanyAndRoleDataModel } from "../../../../admin/customer-onboarding/create-role-group/company-and-role-data-model";
import { CreateRoleGroupService } from "../../../../admin/customer-onboarding/create-role-group/create-role-group.service";
import * as jQuery from 'jquery';
import { AdalService } from "../../../index";
import { Router } from '@angular/router';
declare var CKEDITOR: any;
@Component({
  selector: 'app-subscription-topic-option-template',
  templateUrl: './subscription-topic-option-template.component.html',
  styleUrls: ['./subscription-topic-option-template.component.scss'],

})
export class SubscriptionTopicOptionTemplateComponent implements OnInit, AfterViewInit, OnChanges {
  @Input() templateData: object;
  @Input() companyAndRoleData: CompanyAndRoleDataModel;
  @Input() templateUsedFor: string;
  @Input() status: number;
  @Input() emailDataStorageForAllTopics: Array<object>;
  @Input() smsDataStorageForAllTopics: Array<object>;
  @Input() pushDataStorageForAllTopics: Array<object>;
  @Output() closeModal: EventEmitter<boolean> = new EventEmitter();
  @Input() updateProfile = false;
  @Input() userId: string;
  @ViewChild('subscriptionTopicOptionTemplate') subscriptionTopicOptionTemplateRef: ModalDirective;

  public copyEmailExcludedDays = [];
  public copySmsExcludedDays = [];
  emailTemplateForm: FormGroup;
  smsTemplateForm: FormGroup;
  pushTemplateForm: FormGroup;
  assetsList = [];
  dropdownSettings = {};
  selectedAssets = [];

  startTimeList = [];
  startTimeDropdownSettings = {};
  selectedStartTime = '00:00';

  endTimeList = [];
  endTimeDropdownSettings = {};
  selectedEndTime = '24:00';

  subjectInputBoxCaretPosition = 0;
  @ViewChild('subject') subject: ElementRef;
  editMode = false;
  //email data declarations
  emailDataArray: Array<object>;
  emailDataObject: object;
  smsDataArray: Array<object>;
  smsDataObject: object;
  pushDataArray: Array<object>;
  pushDataObject: object;
  suggestedWords: Array<string>;
  filteredWords: Array<string>;
  assetsGwSNOMappedData = {};
  userInfo: object;
  endTimeDisableRange = {};
  endTimeReset = false;
  startTimeDisableRange = {};
  startTimeReset = false;
  selectedExcludedDays = [];
  excludeDaysPristine = true;
  @Output() disableSelectedTopic: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private assetDashboardService: AssetDashboardService,
    private createRolGroupService: CreateRoleGroupService,
    private adalService: AdalService,
    private router: Router
  ) {
    this.userInfo = this.adalService.userInfo;
    createRolGroupService.getSuggestedWordsForTemplating().subscribe((res) => {
      this.suggestedWords = res;
    });
  }
  ngOnInit() {
    if (this.templateUsedFor == 'email') { //email form set up
      this.dropdownSettings = {
        singleSelection: false,
        text: "Select Assets",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "assets-multiselect"
      };
      this.startTimeDropdownSettings = {
        singleSelection: true,
        text: "Start Time",
        enableSearchFilter: false,
        classes: "start-time-select time-single-select"
      };
      this.startTimeList = this.getTimeRangeArray(0, 23);
      this.endTimeDropdownSettings = {
        singleSelection: true,
        text: "End Time",
        enableSearchFilter: false,
        classes: "end-time-select time-single-select"
      };
      this.endTimeList = this.getTimeRangeArray(1, 24);
      const defaultSubject = 'SurgeCloud&reg; Event Notification - $assetId - $alarm_name';
      const defaultBody = '<table align="center" border="0" cellpadding="5" cellspacing="0" style="background-color:#ffffff;' +
        ' border-collapse:collapse; border-color:gray; color:black; font-family:arial,helvetica,sans-serif; + font-size:14px;' +
        ' text-align:center; width:100%"><tbody><tr><td colspan="2" style="background-color:#57607d; text-align:center">' +
        '<span style="font-size:18px"><strong><span style="color:#ffffff">SurgeCloud&reg; Event Notification</span></strong></span>' +
        '</td></tr><tr><td style="background-color:#dbdddb; text-align:left"><span style="font-size:14px"><strong>Asset ID</strong>' +
        '</span></td><td style="background-color:#dbdddb; text-align:left"><span style="font-size:14px">$assetId</span></td></tr>' +
        '<tr><td style="background-color:white; text-align:left"><span style="font-size:14px"><strong>Gateway Serial No.</strong>' +
        '</span></td><td style="background-color:white; text-align:left"><span style="font-size:14px">$gw_Sno</span></td></tr><tr>' +
        '<td style="background-color:#dbdddb; text-align:left"><span style="font-size:14px"><strong>Event </strong></span></td>' +
        '<td style="background-color:#dbdddb; text-align:left">$alarm_name</td></tr><tr><td style="background-color:white; ' +
        'text-align:left"><span style="font-size:14px"><strong>Event Priority</strong></span></td><td style="background-color:white;' +
        ' text-align:left"><span style="font-size:14px">$priority</span></td></tr><tr><td style="background-color:#dbdddb; ' +
        'text-align:left"><span style="font-size:14px"><strong>Event Description</strong></span></td><td style="background-color:' +
        '#dbdddb; text-align:left"><span style="font-size:14px">$alarm_description</span></td></tr><tr><td style="background-color:' +
        'white; text-align:left"><span style="font-size:14px"><strong>Event OccurrenceTime</strong></span></td><td ' +
        'style="background-color:white; text-align:left"><span style="font-size:14px">$active_timestamp</span></td></tr><tr><td ' +
        'style="background-color:#dbdddb; text-align:left"><span style="font-size:14px"><strong>Event Normalized Time</strong></span>' +
        '</td><td style="background-color:#dbdddb; text-align:left"><span style="font-size:14px">$normal_timestamp</span></td></tr>' +
        '<tr><td style="background-color:white; text-align:left"><span style="font-size:14px"><strong>Location</strong></span></td>' +
        '<td style="background-color:white; text-align:left"><span style="font-size:14px">Click&nbsp;<a ' +
        'href="https://www.google.com/maps/?q=$latitude,$longitude" target="_blank">here</a>&nbsp;to view on map.&nbsp;</span></td>' +
        '</tr><tr><td colspan="2" style="background-color:#57607d; text-align:center"><span style="font-size:16px"><span ' +
        'style="color:#ffffff">Visit <a href="http://surgecloud.bluesurge.com" target="_blank">SurgeCloud </a>for more details.' +
        '</span></span></td></tr></tbody></table>';
      this.emailTemplateForm = new FormGroup({
        'subject': new FormControl(defaultSubject, Validators.required),
        'body': new FormControl(defaultBody, Validators.required),
        'assets': new FormControl('', Validators.required),
        'daySchedules': new FormArray([
          new FormGroup({
            key: new FormControl('Sunday'),
            value: new FormControl("Sun"),
            checked: new FormControl(true)
          }),
          new FormGroup({
            key: new FormControl('Monday'),
            value: new FormControl("Mon"),
            checked: new FormControl(true)
          }),
          new FormGroup({
            key: new FormControl('Tuesday'),
            value: new FormControl("Tue"),
            checked: new FormControl(true)
          }),
          new FormGroup({
            key: new FormControl('Wednesday'),
            value: new FormControl("Wed"),
            checked: new FormControl(true)
          }),
          new FormGroup({
            key: new FormControl('Thursday'),
            value: new FormControl("Thu"),
            checked: new FormControl(true)
          }),
          new FormGroup({
            key: new FormControl('Friday'),
            value: new FormControl("Fri"),
            checked: new FormControl(true)
          }),
          new FormGroup({
            key: new FormControl('Saturday'),
            value: new FormControl("Sat"),
            checked: new FormControl(true)
          })
        ]),
        'selectedDays': new FormControl([], Validators.required),
        'startTimeSchedule': new FormControl('', Validators.required),
        'endTimeSchedule': new FormControl('', Validators.required)
      });
      this.emailTemplateForm.controls['selectedDays'].setValue(this.emailTemplateForm.controls['daySchedules'].value);
      this.emailTemplateForm.controls['daySchedules'].valueChanges.subscribe((value) => {
        //console.log(value);
        const filteredValue = value.filter((item) => {
          return item.checked;
        });
        //console.log(filteredValue);
        var newData = filteredValue.map((item) => {
          return item.value;
        });
        this.emailTemplateForm.controls['selectedDays'].setValue(newData);
        //console.log(newData);
      });
      // email form default value set up for edit
      //console.log('emailDataStorageForAllTopics', this.emailDataStorageForAllTopics);
      //console.log("template data:", this.templateData);
      //console.log("template data:", this.templateData['data']);
      this.emailDataArray = this.templateData['data'];
      //console.log(this.emailDataArray);
      if (this.emailDataArray.length > 0) { //edit
        this.editMode = true;
        //console.log(this.editMode);
        this.emailDataObject = this.emailDataArray[0];
        this.emailTemplateForm.controls['subject'].setValue(this.emailDataObject['subject']);
        this.emailTemplateForm.controls['body'].setValue(this.emailDataObject['body']);
        //this.selectedStartTime.push({ id: this.emailDataObject['startTimeSchedule'], itemName: this.emailDataObject['startTimeSchedule'] });
        this.selectedStartTime = this.emailDataObject['startTimeSchedule'];
        //this.selectedEndTime.push({ id: this.emailDataObject['endTimeSchedule'], itemName: this.emailDataObject['endTimeSchedule'] });
        this.selectedEndTime = this.emailDataObject['endTimeSchedule'];
        const temporaryAssets = [];
        this.emailDataArray.forEach((item) => {
          if (temporaryAssets.indexOf(item['assetId']) == -1) {
            this.selectedAssets.push({ id: item['assetId'], itemName: item['assetId'] });
            temporaryAssets.push(item['assetId']);
          }
        });
        const dayScheduleArray = this.emailDataObject['daySchedule'].split(',');
        (<FormArray>this.emailTemplateForm.controls['daySchedules']).controls.forEach((item) => {
          if (dayScheduleArray.indexOf(item.get('value').value) > -1) {
            item.get('checked').setValue(true);
          } else {
            item.get('checked').setValue(false);
          }
        });
        if (this.emailDataObject['excludeDays']) {
          this.selectedExcludedDays = this.emailDataObject['excludeDays'];
          this.copyEmailExcludedDays = JSON.parse(JSON.stringify(this.selectedExcludedDays));
        }
      }
    }

    if (this.templateUsedFor == 'sms') { //sms form set up
      this.dropdownSettings = {
        singleSelection: false,
        text: "Select Assets",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "assets-multiselect"
      };
      this.startTimeDropdownSettings = {
        singleSelection: true,
        text: "Start Time",
        enableSearchFilter: false,
        classes: "start-time-select time-single-select"
      };
      this.startTimeList = this.getTimeRangeArray(0, 23);
      this.endTimeDropdownSettings = {
        singleSelection: true,
        text: "End Time",
        enableSearchFilter: false,
        classes: "end-time-select time-single-select"
      };
      this.endTimeList = this.getTimeRangeArray(1, 24);
      const defaultBody = '<p>$alarm_name Event Notification @ $active_timestamp</p>' +
        '<p>Asset ID : $assetId</p>' +
        '<p>Event Priority : $priority </p>' +
        '<p>Event Description : $alarm_description</p>';
      this.smsTemplateForm = new FormGroup({
        'body': new FormControl(defaultBody, Validators.required),
        'assets': new FormControl('', Validators.required),
        'daySchedules': new FormArray([
          new FormGroup({
            key: new FormControl('Sunday'),
            value: new FormControl("Sun"),
            checked: new FormControl(true)
          }),
          new FormGroup({
            key: new FormControl('Monday'),
            value: new FormControl("Mon"),
            checked: new FormControl(true)
          }),
          new FormGroup({
            key: new FormControl('Tuesday'),
            value: new FormControl("Tue"),
            checked: new FormControl(true)
          }),
          new FormGroup({
            key: new FormControl('Wednesday'),
            value: new FormControl("Wed"),
            checked: new FormControl(true)
          }),
          new FormGroup({
            key: new FormControl('Thursday'),
            value: new FormControl("Thu"),
            checked: new FormControl(true)
          }),
          new FormGroup({
            key: new FormControl('Friday'),
            value: new FormControl("Fri"),
            checked: new FormControl(true)
          }),
          new FormGroup({
            key: new FormControl('Saturday'),
            value: new FormControl("Sat"),
            checked: new FormControl(true)
          })
        ]),
        'selectedDays': new FormControl([], Validators.required),
        'startTimeSchedule': new FormControl('00:00', Validators.required),
        'endTimeSchedule': new FormControl('24:00', Validators.required)
      });
      this.smsTemplateForm.controls['selectedDays'].setValue(this.smsTemplateForm.controls['daySchedules'].value);
      this.smsTemplateForm.controls['daySchedules'].valueChanges.subscribe((value) => {
        const filteredValue = value.filter((item) => {
          return item.checked;
        });
        var newData = filteredValue.map((item) => {
          return item.value;
        });
        this.smsTemplateForm.controls['selectedDays'].setValue(newData);
      });
      // sms form default value set up for edit
      this.smsDataArray = this.templateData['data'];
      //console.log(this.smsDataArray);
      if (this.smsDataArray.length > 0) { //edit
        this.editMode = true;
        this.smsDataObject = this.smsDataArray[0];
        this.smsTemplateForm.controls['body'].setValue(this.smsDataObject['body']);
        //this.selectedStartTime.push({ id: this.smsDataObject['startTimeSchedule'], itemName: this.smsDataObject['startTimeSchedule'] });
        this.selectedStartTime = this.smsDataObject['startTimeSchedule'];
        //this.selectedEndTime.push({ id: this.smsDataObject['endTimeSchedule'], itemName: this.smsDataObject['endTimeSchedule'] });
        this.selectedEndTime = this.smsDataObject['endTimeSchedule'];
        const temporaryAssets = [];
        this.smsDataArray.forEach((item) => {
          if (temporaryAssets.indexOf(item['assetId']) == -1) {
            this.selectedAssets.push({ id: item['assetId'], itemName: item['assetId'] });
            temporaryAssets.push(item['assetId']);
          }
        });
        const dayScheduleArray = this.smsDataObject['daySchedule'].split(',');
        (<FormArray>this.smsTemplateForm.controls['daySchedules']).controls.forEach((item) => {
          if (dayScheduleArray.indexOf(item.get('value').value) > -1) {
            item.get('checked').setValue(true);
          } else {
            item.get('checked').setValue(false);
          }
        });
        if (this.smsDataObject['excludeDays']) {
          this.selectedExcludedDays = this.smsDataObject['excludeDays'];
          this.copySmsExcludedDays = JSON.parse(JSON.stringify(this.selectedExcludedDays));
        }
      }
    }
    if (this.templateUsedFor == 'push') { //sms form set up
      this.dropdownSettings = {
        singleSelection: false,
        text: "Select Assets",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "assets-multiselect"
      };
      this.startTimeDropdownSettings = {
        singleSelection: true,
        text: "Start Time",
        enableSearchFilter: false,
        classes: "start-time-select time-single-select"
      };
      this.startTimeList = this.getTimeRangeArray(0, 23);
      this.endTimeDropdownSettings = {
        singleSelection: true,
        text: "End Time",
        enableSearchFilter: false,
        classes: "end-time-select time-single-select"
      };
      this.endTimeList = this.getTimeRangeArray(1, 24);
      const defaultBody = '<p>$alarm_name Event Notification for $assetId @ $active_timestamp</p>';
      this.pushTemplateForm = new FormGroup({
        'body': new FormControl(defaultBody, Validators.required),
        'assets': new FormControl('', Validators.required)
      });
      // sms form default value set up for edit
      this.pushDataArray = this.templateData['data'];
      //console.log(this.pushDataArray);
      if (this.pushDataArray.length > 0) { //edit
        this.editMode = true;
        this.pushDataObject = this.pushDataArray[0];
        this.pushTemplateForm.controls['body'].setValue(this.pushDataObject['body']);
        const temporaryAssets = [];
        this.pushDataArray.forEach((item) => {
          if (temporaryAssets.indexOf(item['assetId']) == -1) {
            this.selectedAssets.push({ id: item['assetId'], itemName: item['assetId'] });
            temporaryAssets.push(item['assetId']);
          }
        });
      }
    }
    this.assetDashboardService.getAssetDesc(this.companyAndRoleData.companyId).subscribe((res) => {
      //console.log('Assets: ', res);
      res.forEach((asset) => {
        //console.log('single: ', asset);
        //console.log('asset id:', asset.assetId);
        const singleasset = {
          id: asset.assetId,
          itemName: asset.assetId
        }
        this.assetsList.push(singleasset);
        this.assetsGwSNOMappedData[asset.assetId] = asset.gwSNO;
      });
      //console.log('Mapped data:', this.assetsGwSNOMappedData);
    });
    const self = this;
    jQuery('.modal').on('click', () => {
      self.filteredWords = [];
    });
    jQuery(document).on('click', '.autocomplete', (event) => {
      event.stopPropagation();
    });
  }

  ngOnChanges() {
    //console.log('firstOnChange', this.templateData);
  }

  onUnselectAll(event) {
    setTimeout(() => {
      this.selectedAssets = event;
      if (this.templateUsedFor == 'email') {
        this.emailTemplateForm.controls['assets'].setValue(event);
      } else if (this.templateUsedFor == 'sms') {
        this.smsTemplateForm.controls['assets'].setValue(event);
      } else if (this.templateUsedFor == 'push') {
        this.pushTemplateForm.controls['assets'].setValue(event);
      }
    });
  }
  getTimeRangeArray(startPoint, endPoint) {
    const timeArray = [];
    for (let i = startPoint; i <= endPoint; i++) {
      let pointValue = i;
      if (i < 10) {
        pointValue = '0' + i;
      }
      pointValue = pointValue + ':00';
      const timeObject = {
        displayValue: pointValue,
        value: i
      }
      timeArray.push(timeObject);
    }
    return timeArray;
  }
  onStartTimeModelChange(value) {
    //console.log(value);
    if (value != '') {
      value = value.split(':');
      this.endTimeDisableRange = {
        start: 0,
        end: Number(value[0])
      };
      this.endTimeReset = false;
    } else {
      this.endTimeReset = true;
    }
    this.endTimeList = this.getTimeRangeArray(1, 24);
    //this.endTimeDisableRange.
  }
  onStartTimeChange() {
    const startTimeValue = this.emailTemplateForm.controls['startTimeSchedule'].value;
    const endTimeValue = this.emailTemplateForm.controls['endTimeSchedule'].value;
    //console.log(startTimeValue);
    if (startTimeValue != '' && endTimeValue <= startTimeValue) {
      const splitValue = startTimeValue.split(':');
      let valueInNumber = Number(splitValue[0]);
      valueInNumber = valueInNumber + 1;
      //const valueInString = String(valueInNumber);
      let valueInString = '';
      if (valueInNumber < 10) {
        valueInString = '0' + valueInNumber;
      } else {
        valueInString = String(valueInNumber);
      }
      //console.log(valueInString);
      this.selectedEndTime = valueInString + ':00';
    }

  }
  onEndTimeModelChange(value) {
    if (value != '') {
      value = value.split(':');
      this.startTimeDisableRange = {
        start: Number(value[0]),
        end: 23
      };
      this.startTimeReset = false;
    } else {
      this.startTimeReset = true;
    }
    this.startTimeList = this.getTimeRangeArray(0, 23);
  }
  emailTemplateSubmit() {
    let days = '';
    this.emailTemplateForm.value.daySchedules.forEach((item) => {
      if (item.checked) {
        days = days + item.value + ',';
      }
    });
    days = days.substring(0, days.length - 1);
    const formData = {
      "subscriptionId": null,
      "topic": this.templateData['topicName'],
      "companyId": this.companyAndRoleData.companyId,
      "companyName": this.companyAndRoleData.companyName,
      "roleId": this.companyAndRoleData.roleId,
      "groupId": this.companyAndRoleData.groupId,
      "assetId": '',
      "gwSNO": '',
      "subject": this.emailTemplateForm.value.subject,
      "body": this.emailTemplateForm.value.body,
      "daySchedule": days,
      "excludeDays": this.selectedExcludedDays,
      "startTimeSchedule": this.emailTemplateForm.value.startTimeSchedule,
      "endTimeSchedule": this.emailTemplateForm.value.endTimeSchedule,
      "createdBy": this.userInfo['profile']['name'],
      "createdOn": "",
      "updatedBy": "",
      "updatedOn": "",
      "status": this.status
    };
    if (this.updateProfile) {
      //console.log(this.userId);
      formData['userId'] = this.userId;
      formData['isUser'] = true;
      //console.log(formData);
    }
    if (this.editMode) {
      const sendData = [];
      this.emailTemplateForm.value.assets.forEach((item, key) => {
        //console.log(item, key);
        //console.log(formData);
        const editData = Object.assign({}, formData);
        //console.log(editData);
        if (key == 0) {
          editData.subscriptionId = this.emailDataObject['subscriptionId'];
        }
        else {
          editData.subscriptionId = null;
        }
        editData.assetId = item['id'];
        editData.gwSNO = this.assetsGwSNOMappedData[item['id']];
        editData.status = this.emailDataObject['status'];
        editData.updatedBy = this.userInfo['profile']['name'];
        //console.log('formData1', formData);
        //console.log(editData);
        sendData.push(editData);
      });

      const sendDataEmail = new Array();
      this.emailDataStorageForAllTopics.forEach((item, key) => {
        item['isUser'] = this.router.url === '/user/update-profile' ? true : false;
        if (item['topic'] != this.templateData['topicName']) {
          sendDataEmail.push(item);
        }
      });
      //console.log(sendData);
      sendData.forEach((item) => {
        sendDataEmail.push(item);
      });
      //console.log(JSON.stringify(sendDataEmail));
      this.createRolGroupService.updateTopicEmailSubscription(sendDataEmail).subscribe((res) => {
        this.closeModalBox(true);
      }, (error) => {
        console.log(error);
      });
    } else {

      //let formData = {};

      //formData.daySchedule = days;
      //console.log(this.emailTemplateForm.value.subject);
      const sendData = [];

      this.emailTemplateForm.value.assets.forEach((item) => {
        //console.log(item);
        const addData = Object.assign({}, formData);
        addData.assetId = item['id'];
        addData.gwSNO = this.assetsGwSNOMappedData[item['id']];
        //console.log('formData1', formData);
        sendData.push(addData);
      });
      //console.log(JSON.stringify(sendData));
      console.log('email submit', sendData);
      this.createRolGroupService.createTopicEmailSubscription(sendData).subscribe((res) => {
        //console.log("Create email:", res);
        this.closeModalBox(true);
      }, (error) => {
        console.log(error);
      });
    }
    //console.log(this.returnData);
  }

  public smsBodyCharactersError: boolean = false;
  smsTemplateSubmit() {
    let days = '';
    this.smsTemplateForm.value.daySchedules.forEach((item) => {
      if (item.checked) {
        days = days + item.value + ',';
      }
    });
    days = days.substring(0, days.length - 1);
    if (this.smsTemplateForm.value.body.length > 1500) {
      this.smsBodyCharactersError = true;
    } else {
      const formData = {
        "subscriptionId": null,
        "topic": this.templateData['topicName'],
        "companyId": this.companyAndRoleData.companyId,
        "companyName": this.companyAndRoleData.companyName,
        "roleId": this.companyAndRoleData.roleId,
        "groupId": this.companyAndRoleData.groupId,
        "assetId": '',
        "gwSNO": '',
        "body": this.smsTemplateForm.value.body,
        "daySchedule": days,
        "excludeDays": this.selectedExcludedDays,
        "startTimeSchedule": this.smsTemplateForm.value.startTimeSchedule,
        "endTimeSchedule": this.smsTemplateForm.value.endTimeSchedule,
        "createdBy": this.userInfo['profile']['name'],
        "createdOn": "",
        "updatedBy": "",
        "updatedOn": "",
        "status": this.status
      };
      console.log(this.updateProfile);
      if (this.updateProfile) {
        //console.log(this.userId);
        formData['userId'] = this.userId;
        formData['isUser'] = true;
      }
      if (this.editMode) {
        const sendData = [];
        this.smsTemplateForm.value.assets.forEach((item, key) => {
          //console.log(item, key);
          const editData = Object.assign({}, formData);
          if (key == 0) {
            editData.subscriptionId = this.smsDataObject['subscriptionId'];
          }
          else {
            editData.subscriptionId = null;
          }
          editData.assetId = item['id'];
          editData.gwSNO = this.assetsGwSNOMappedData[item['id']];
          editData.status = this.smsDataObject['status'];
          editData.updatedBy = this.userInfo['profile']['name'];
          //console.log('formData1', formData);
          sendData.push(editData);
        });
        //console.log(JSON.stringify(sendData));
        //marging existing topic data for email with edited data
        const sendDataSms = new Array();
        this.smsDataStorageForAllTopics.forEach((item, key) => {
          item['isUser'] = this.router.url === '/user/update-profile' ? true : false;
          if (item['topic'] != this.templateData['topicName']) {
            sendDataSms.push(item);
          }
        });
        sendData.forEach((item) => {
          sendDataSms.push(item);
        });
        //console.log(sendDataSms);
        this.createRolGroupService.updateTopicSmsSubscription(sendDataSms).subscribe((res) => {
          //console.log("Edit sms:", res);
          this.closeModalBox(true);
        }, (error) => {
          console.log(error);
        });
      } else {

        //let formData = {};

        //formData.daySchedule = days;
        //console.log(this.emailTemplateForm.value.subject);
        const sendData = [];

        this.smsTemplateForm.value.assets.forEach((item) => {
          //console.log(item);
          const addData = Object.assign({}, formData);
          addData.assetId = item['id'];
          addData.gwSNO = this.assetsGwSNOMappedData[item['id']];

          //console.log('formData1', formData);
          sendData.push(addData);
        });
        //console.log(JSON.stringify(sendData));
        this.createRolGroupService.createTopicSmsSubscription(sendData).subscribe((res) => {
          //console.log("Create sms:", res);
          this.closeModalBox(true);
        }, (error) => {
          console.log(error);
        });
      }
    }
  }

  pushTemplateSubmit() {
    const formData = {
      "subscriptionId": null,
      "topic": this.templateData['topicName'],
      "companyId": this.companyAndRoleData.companyId,
      "companyName": this.companyAndRoleData.companyName,
      "roleId": this.companyAndRoleData.roleId,
      "groupId": this.companyAndRoleData.groupId,
      "assetId": '',
      "gwSNO": '',
      "body": this.pushTemplateForm.value.body,
      "createdBy": this.userInfo['profile']['name'],
      "createdOn": "",
      "updatedBy": "",
      "updatedOn": "",
      "status": this.status
    };
    if (this.updateProfile) {
      //console.log(this.userId);
      formData['userId'] = this.userId;
      formData['isUser'] = true;
    }
    if (this.editMode) {
      const sendData = [];
      this.pushTemplateForm.value.assets.forEach((item, key) => {
        //console.log(item, key);
        const editData = Object.assign({}, formData);
        if (key == 0) {
          editData.subscriptionId = this.pushDataObject['subscriptionId'];
        }
        else {
          editData.subscriptionId = null;
        }
        editData.assetId = item['id'];
        editData.gwSNO = this.assetsGwSNOMappedData[item['id']];
        editData.status = this.pushDataObject['status'];
        editData.updatedBy = this.userInfo['profile']['name'];
        //console.log('formData1', formData);
        sendData.push(editData);
      });
      console.log(sendData);
      console.log(this.pushDataStorageForAllTopics);
      //console.log(JSON.stringify(sendData));

      //marging existing topic data for email with edited data
      const sendDataPush = new Array();
      this.pushDataStorageForAllTopics.forEach((item, key) => {
        item['isUser'] = this.router.url === '/user/update-profile' ? true : false;
        if (item['topic'] != this.templateData['topicName']) {
          sendDataPush.push(item);
        }
      });
      sendData.forEach((item) => {
        sendDataPush.push(item);
      });
      console.log(sendDataPush);
      this.createRolGroupService.updateTopicPushSubscription(sendDataPush).subscribe((res) => {
        //console.log("Edit push:", res);
        this.closeModalBox(true);
      }, (error) => {
        console.log(error);
      });

    } else {

      //let formData = {};

      //formData.daySchedule = days;
      //console.log(this.emailTemplateForm.value.subject);
      const sendData = [];

      this.pushTemplateForm.value.assets.forEach((item) => {
        //console.log(item);
        const addData = Object.assign({}, formData);
        addData.assetId = item['id'];
        addData.gwSNO = this.assetsGwSNOMappedData[item['id']];
        //console.log('formData1', formData);
        sendData.push(addData);
      });
      //console.log(JSON.stringify(sendData));
      this.createRolGroupService.createTopicPushSubscription(sendData).subscribe((res) => {
        //console.log("Create push:", res);
        this.closeModalBox(true);
      }, (error) => {
        console.log(error);
      });
    }
    //console.log(this.returnData);
  }
  bindWithInput(value: string, subjectRef) {
    let subject = this.subject['nativeElement'].value;
    const result = /\S+$/.exec(subject.slice(0, this.subjectInputBoxCaretPosition));
    const lastWord = result ? result[0] : null;
    const startingPosition = (this.subjectInputBoxCaretPosition - lastWord.length);
    subject = subject.substring(0, startingPosition) + value + subject.substring(this.subjectInputBoxCaretPosition);
    const updatedStringLength = value.length;
    this.emailTemplateForm.controls['subject'].setValue(subject);
    const caretMovingPosition = (updatedStringLength + this.subjectInputBoxCaretPosition) - lastWord.length;
    var input = document.getElementById("subject");
    subjectRef.focus();
    subjectRef.setSelectionRange(caretMovingPosition, caretMovingPosition);
    this.subjectInputBoxCaretPosition = caretMovingPosition;
    this.filteredWords = [];
  }
  stripHtmlTagsFromString(content) {
    var div = document.createElement("div");
    div.innerHTML = content;
    var text = div.textContent || div.innerText || "";
    return text;
  }
  getCaretPos(subjectRef) {
    //console.log('selectionStart', subjectRef.selectionStart);
    if (subjectRef.selectionStart || subjectRef.selectionStart == '0') {
      this.subjectInputBoxCaretPosition = subjectRef.selectionStart;
      this.autoSuggest(subjectRef);
    }
  }
  autoSuggest(subjectRef) {
    // console.log('works.', subjectRef);
    const subject = this.subject['nativeElement'].value;
    //console.log(subject);
    //const result = /\S+$/.exec(subject.slice(0, subject.indexOf(' ', this.subjectInputBoxCaretPosition)));
    const result = /\S+$/.exec(subject.slice(0, this.subjectInputBoxCaretPosition));
    const lastWord = result ? result[0] : null;
    //console.log('lastWord', lastWord);
    if (lastWord != null && lastWord.indexOf('$') !== -1) {
      //console.log('run search');
      const filteredWords = this.suggestedWords.filter((item) => {
        return item.indexOf(lastWord) > -1;
      });
      //console.log(filteredWords);
      this.filteredWords = filteredWords;
    }
    else {
      this.filteredWords = [];
    }
  }

  ngAfterViewInit() {
    this.subscriptionTopicOptionTemplateRef.show();
    //console.log('view child1', this.subscriptionTopicOptionTemplateRef);
    this.setUpCkEditor();
  }

  setUpCkEditor() {
    const self = this;
    if (document.getElementById('editor') != undefined) {
      const ck = CKEDITOR.replace('editor', {
        on: {
          pluginsLoaded: function () {
            const editor = this,
              config = editor.config;
            function colorfulSpan(color) {
              return '<span style="color:' + color + '">' + color + '</span>';
            }
            // See: http://docs.ckeditor.com/#!/guide/dev_advanced_content_filter
            const acfRules = 'span{color}';
            editor.ui.addRichCombo('DataPoints', {
              label: 'Data points',
              title: 'Data points',
              toolbar: 'styles',
              // Registers a certain type of contents in the editor.
              allowedContent: acfRules,
              // The minimal content that must be allowed in the editor for this combo to work.
              requiredContent: acfRules,
              // Combo iherits from the panel. Set up it first so all styles
              // of the contents are available in the panel.
              panel: {
                css: [CKEDITOR.skin.getPath('editor')].concat(config.contentsCss),
                multiSelect: false
              },
              // Let's populate the list of available items.
              init: function () {
                this.startGroup('Data points');
                for (let i = 0; i < self.suggestedWords.length; i++) {
                  const dataPoint = self.suggestedWords[i];
                  // Add entry to the panel.
                  //this.add( item, colorfulSpan( item ), item );
                  this.add(dataPoint);
                }
              },
              // This is what happens when the item is clicked.
              onClick: function (value) {
                editor.focus();
                // Register undo snapshot.
                editor.fire('saveSnapshot');
                // Insert a colorful <span>.
                //editor.insertHtml( colorfulSpan( value ) );
                editor.insertHtml(value);
                // Register another undo snapshot. The insertion becomes undoable.
                editor.fire('saveSnapshot');
              },

              // The value of the combo may need to be updated, i.e. according to the selection.
              // This is where such listener is supposed to be created.
              onRender: function () {
                //editor.on( 'selectionChange', function( ev ) {
                //    var currentValue = this.getValue();
                //    ...
                //    this.setValue( value );
                //}, this );
              },
              // The contents of the combo may change, i.e. according to the selection.
              // This is where it supposed to be updated.
              onOpen: function () {
              },

              // Disable the combo if the current editor's filter does not allow
              // the type of contents that the combo delivers (i.e. widget's nested editable).
              // See: http://docs.ckeditor.com/#!/guide/dev_widgets.
              refresh: function () {
                if (!editor.activeFilter.check(acfRules)) {
                  this.setState(CKEDITOR.TRISTATE_DISABLED);
                }
              }
            });
          },
          'change': () => {
            //console.log('data change5', ck.getData());
            //this.emailTemplateForm.get('body') = 5;
            if (this.templateUsedFor == 'email') {
              this.emailTemplateForm.controls['body'].setValue(ck.getData());
              this.changeDetectorRef.detectChanges();
            }
            if (this.templateUsedFor == 'sms') {
              this.smsTemplateForm.controls['body'].setValue(ck.getData());
              this.changeDetectorRef.detectChanges();
            }
            if (this.templateUsedFor == 'push') {
              this.pushTemplateForm.controls['body'].setValue(ck.getData());
              this.changeDetectorRef.detectChanges();
            }
          }
        }
      });
      ck.on('instanceReady', () => {
        if (this.templateUsedFor === 'sms') {
          jQuery("span").find(`[title='About CKEditor']`).parent().parent().append('<div id="remove-tag-sms-template" class="remove-tag-sms-template"><button class="remove-tab-btn">Remove Tag</button></div>');
          jQuery('#remove-tag-sms-template').on('click', (event) => {
            event.stopPropagation();
            this.removeHTMLtagFromDescription();
          });
        }
      });
    }
  }

  public copySMStemplateFormBodyValue: any;
  removeHTMLtagFromDescription() {
    this.copySMStemplateFormBodyValue = this.smsTemplateForm.value.body;
    let bodyRex = /(<([^>]+)>)/ig;
    let bodyText = this.smsTemplateForm.value.body.replace(bodyRex, "");
    this.smsTemplateForm.controls['body'].setValue(bodyText);
  }

  closeModalBox(reloadParent: boolean) {
    this.selectedExcludedDays = [];
    if (this.emailDataObject)
      this.emailDataObject['excludeDays'] = JSON.parse(JSON.stringify(this.copyEmailExcludedDays));
    if (this.smsDataObject)
      this.smsDataObject['excludeDays'] = JSON.parse(JSON.stringify(this.copySmsExcludedDays));
    this.closeModal.emit(reloadParent);
    if (this.templateData['data'].length == 0) {
      this.disableSelectedTopic.emit(true);
    }
  }

  onSelectedExcludedDay(excludedDays: Array<string>) {
    this.selectedExcludedDays = excludedDays;
    this.excludeDaysPristine = false;
  }
}
