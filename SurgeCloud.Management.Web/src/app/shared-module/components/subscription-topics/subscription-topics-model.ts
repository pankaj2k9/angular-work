export class SubscriptionTopicsModel {
  public topicName: string;
  public email: boolean;
  public sms: boolean;
  public push: boolean;
  constructor(topicName: any, email = null, sms = null, push = null) {
    this.topicName = topicName;
    this.email = email;
    this.sms = sms;
    this.push = push;
  }
}
