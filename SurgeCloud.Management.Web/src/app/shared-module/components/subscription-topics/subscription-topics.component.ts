import { Component, Input, OnChanges, OnDestroy, OnInit, Output, EventEmitter } from '@angular/core';
import { CompanyAndRoleDataModel } from "../../../admin/customer-onboarding/create-role-group/company-and-role-data-model";
import { SubscriptionTopicsModel } from "./subscription-topics-model";
import { CreateRoleGroupService } from "../../../admin/customer-onboarding/create-role-group/create-role-group.service";
import { AdalService } from "../../index";
import { Subject } from "rxjs/Subject";
import { Router } from '@angular/router';

@Component({
  selector: 'app-subscription-topics',
  templateUrl: './subscription-topics.component.html',
  styleUrls: ['./subscription-topics.component.scss']
})
export class SubscriptionTopicsComponent implements OnInit, OnChanges, OnDestroy {
  @Input() companyAndRoleData: object;
  @Input() tableBodyMaxHeight: object;
  @Input() groupId: string;
  @Input() userId: string;
  @Output() subscriptionTopicsSubscriber = new EventEmitter();
  templateUsedFor: string;
  @Input() updateProfile = false;
  openSubscriptionTopicOptionModal = false;
  subscriptionTopicsPermission = {
    email: false,
    sms: false,
    push: false,
    emailDisable: true,
    smsDisable: true,
    pushDisable: true
  }
  subscriptionTopics: SubscriptionTopicsModel[] = [];
  companyAndRoleDataForSubscription: CompanyAndRoleDataModel;
  emailData = {};
  templateData: object;
  emailDataStorage: Array<object>;
  smsData = {};
  smsDataStorage: Array<object>;
  pushData = {};
  pushDataStorage: Array<object>;
  userInfo: object;
  topicOptionStatus: number;
  topicEditElementLocalRef: HTMLElement;
  topicDataLoad: Subject<object[]> = new Subject<object[]>();
  adminEditUserProfile: boolean = true;
  constructor(private createRoleGroupService: CreateRoleGroupService,
    private adalService: AdalService,
    private router: Router) {
    this.userInfo = this.adalService.userInfo;
    this.topicDataLoad.subscribe((res) => {
      this.loadTopicOptionsData(res);
    });
  }

  ngOnInit() {
    //console.log(this.companyAndRoleData);
    // this.companyAndRoleDataForSubscription = new CompanyAndRoleDataModel(this.companyAndRoleData['companyId'], this.companyAndRoleData['companyName'], this.companyAndRoleData['id'], this.groupId, this.subscriptionTopics);
    //this.loadTopicOptionsData();
    //console.log(this.updateProfile);
  }

  ngOnDestroy() {
    this.topicDataLoad.unsubscribe();
  }

  ngOnChanges() {
    if (this.companyAndRoleData != undefined) {
      //console.log(this.companyAndRoleData);
      this.companyAndRoleDataForSubscription = new CompanyAndRoleDataModel(this.companyAndRoleData['companyId'], this.companyAndRoleData['companyName'], this.companyAndRoleData['id'], this.groupId, this.subscriptionTopics);
      //this.loadTopicOptionsData();
      this.emitTopicData();
    }
  }

  emitTopicData() {
    if (this.updateProfile) {
      this.topicDataLoad.next([]);
    } else {
      this.createRoleGroupService.getSubscriptionTopics(this.companyAndRoleDataForSubscription.companyId).subscribe((response) => {
        this.topicDataLoad.next(response);
      });
    }
  }

  getMatchedTopicKeyFromSubscriptionTopics(data: SubscriptionTopicsModel[], niddle: string) {
    let returnValue = null;
    data.forEach((value, key) => {
      if (value.topicName === niddle) {
        returnValue = key;
        return false;
      }
    });
    //console.log(returnValue);
    return returnValue;
  }

  loadTopicOptionsData(topicData = null) {
    this.subscriptionTopics = [];
    this.companyAndRoleDataForSubscription.topics = [];
    console.log(this.companyAndRoleDataForSubscription, this.subscriptionTopics);
    this.companyAndRoleDataForSubscription.topics = this.subscriptionTopics;
    console.log(this.subscriptionTopics, topicData);
    if (topicData && topicData.length > 0) {
      topicData.forEach((item) => {
        this.subscriptionTopics.push(new SubscriptionTopicsModel(item, false, false, false));
      });
    }
    this.createRoleGroupService.getTopicEmailSubscription(this.companyAndRoleDataForSubscription.companyId,
      this.companyAndRoleDataForSubscription.roleId, this.updateProfile).subscribe((responseEmail) => {
        console.log('responseEmail', responseEmail);
        //console.log(this.userId);
        if (this.userId) {
          const userId = this.userId;
          let tempEmailData = [];
          responseEmail.forEach((item) => {
            if (item.userId == userId) {
              tempEmailData.push(item);
              //this.subscriptionTopics.push(new SubscriptionTopicsModel(item.topic, true));
              if (this.updateProfile) {
                const matchedSubscriptionTopicKey = this.getMatchedTopicKeyFromSubscriptionTopics(this.subscriptionTopics, item.topic);
                //console.log(matchedSubscriptionTopicKey);
                if (matchedSubscriptionTopicKey != null) {
                  this.subscriptionTopics[matchedSubscriptionTopicKey].email = true;
                  //console.log(matchedSubscriptionTopicKey, this.subscriptionTopics);
                } else {
                  this.subscriptionTopics.push(new SubscriptionTopicsModel(item.topic, true));
                }
              }
            }
          });
          responseEmail = tempEmailData;
        }
        if (responseEmail.length > 0) {
          this.subscriptionTopicsPermission.email = true;
          this.subscriptionTopicsPermission.emailDisable = false;
        }
        this.emailDataStorage = responseEmail;
        this.companyAndRoleDataForSubscription.topics.forEach((topic) => {
          this.emailData[topic.topicName] = {
            email: []
          };

          responseEmail.forEach((emailItem) => {
            if (emailItem.topic == topic.topicName) {
              this.emailData[topic.topicName].email.push(emailItem);
              //looping for status property value change
              this.companyAndRoleDataForSubscription.topics.forEach((topic, key) => {
                if (topic.topicName == emailItem.topic) {
                  if (emailItem.status) {
                    this.companyAndRoleDataForSubscription.topics[key].email = true;
                  }
                  else {
                    this.companyAndRoleDataForSubscription.topics[key].email = false;
                  }
                }
              });
            }
          });
        });
      }, (error) => {
        console.log(error);
      });
    this.createRoleGroupService.getTopicSmsSubscription(this.companyAndRoleDataForSubscription.companyId,
      this.companyAndRoleDataForSubscription.roleId, this.updateProfile).subscribe((responseSms) => {
        if (this.userId) {
          let tempSmsData = [];
          responseSms.forEach((item) => {
            if (item.userId == this.userId) {
              tempSmsData.push(item);
              if (this.updateProfile) {
                const matchedSubscriptionTopicKey = this.getMatchedTopicKeyFromSubscriptionTopics(this.subscriptionTopics, item.topic);
                if (matchedSubscriptionTopicKey != null) {
                  this.subscriptionTopics[matchedSubscriptionTopicKey].sms = true;
                } else {
                  this.subscriptionTopics.push(new SubscriptionTopicsModel(item.topic, null, true));
                }
              }
            }
          });
          responseSms = tempSmsData;
        }
        if (responseSms.length > 0) {
          this.subscriptionTopicsPermission.sms = true;
          this.subscriptionTopicsPermission.smsDisable = false;
        }
        this.smsDataStorage = responseSms;
        this.companyAndRoleDataForSubscription.topics.forEach((topic) => {
          this.smsData[topic.topicName] = {
            sms: []
          };

          //this.emailData[topic.topicName]
          responseSms.forEach((smsItem) => {
            if (smsItem.topic == topic.topicName) {
              this.smsData[topic.topicName].sms.push(smsItem);
              //looping for status property value change
              this.companyAndRoleDataForSubscription.topics.forEach((topic, key) => {
                if (topic.topicName == smsItem.topic) {
                  if (smsItem.status) {
                    this.companyAndRoleDataForSubscription.topics[key].sms = true;
                  }
                  else {
                    this.companyAndRoleDataForSubscription.topics[key].sms = false;
                  }
                }
              });
            }
          });
        });
      }, (error) => {
        console.log(error);
      });
    this.createRoleGroupService.getTopicPushSubscription(this.companyAndRoleDataForSubscription.companyId,
      this.companyAndRoleDataForSubscription.roleId, this.updateProfile).subscribe((responsePush) => {
        if (this.userId) {
          const userId = this.userId;
          let tempPushData = [];
          responsePush.forEach((item) => {
            if (item.userId == userId) {
              tempPushData.push(item);
              if (this.updateProfile) {
                const matchedSubscriptionTopicKey = this.getMatchedTopicKeyFromSubscriptionTopics(this.subscriptionTopics, item.topic);
                if (matchedSubscriptionTopicKey != null) {
                  this.subscriptionTopics[matchedSubscriptionTopicKey].push = true;
                } else {
                  this.subscriptionTopics.push(new SubscriptionTopicsModel(item.topic, null, null, true));
                }
              }
            }
          });
          responsePush = tempPushData;
        }
        if (responsePush.length > 0) {
          this.subscriptionTopicsPermission.push = true;
          this.subscriptionTopicsPermission.pushDisable = false;
        }
        this.pushDataStorage = responsePush;
        this.companyAndRoleDataForSubscription.topics.forEach((topic) => {
          this.pushData[topic.topicName] = {
            push: []
          };

          //this.emailData[topic.topicName]
          responsePush.forEach((pushItem) => {
            if (pushItem.topic == topic.topicName) {
              this.pushData[topic.topicName].push.push(pushItem);
              //looping for status property value change
              this.companyAndRoleDataForSubscription.topics.forEach((topic, key) => {
                if (topic.topicName == pushItem.topic) {
                  if (pushItem.status) {
                    this.companyAndRoleDataForSubscription.topics[key].push = true;
                  }
                  else {
                    this.companyAndRoleDataForSubscription.topics[key].push = false;
                  }
                }
              });
            }
          });

        });
        //console.log('push data:', this.pushData);
      }, (error) => {
        console.log(error);
      });
    //}
    //this.companyAndRoleDataForSubscription.topics = this.subscriptionTopics;
    //this.setTabBoxHeight(this.selectedTabId);
    //console.log('companyAndRoleData:', this.companyAndRoleDataForSubscription);
    /*}, (error) => {
      console.log(error);
    });*/
    this.subscriptionTopicsSubscriber.next(this.subscriptionTopics);
  }

  topicHeadingOptionChange(propertyName: string, value: boolean) {
    if (propertyName === 'email') {
      if (value) {
        this.subscriptionTopicsPermission.emailDisable = false;
      } else {
        this.subscriptionTopicsPermission.emailDisable = true;
      }
      this.onTopicOptionStatusChange('email');
    }
    if (propertyName === 'sms') {
      if (value) {
        this.subscriptionTopicsPermission.smsDisable = false;
      } else {
        this.subscriptionTopicsPermission.smsDisable = true;
      }
      this.onTopicOptionStatusChange('sms');
    }
    if (propertyName === 'push') {
      if (value) {
        this.subscriptionTopicsPermission.pushDisable = false;
      } else {
        this.subscriptionTopicsPermission.pushDisable = true;
      }
      this.onTopicOptionStatusChange('push');
    }
  }

  onTopicOptionStatusChange(forWhich: string, fromTopic: boolean = false, topicName = null, topicDataEditElement = null, topicCheckbox = null) {
    console.log(topicCheckbox);
    this.topicEditElementLocalRef = topicCheckbox;
    let data = [];
    if (forWhich == 'email') {
      data = this.emailDataStorage;
      //console.log(forWhich, data, fromTopic, topicName);
      if (topicName) {
        let topicNameExist = false;
        data.forEach((item) => {
          //console.log(item.topic, topicName);
          if (item.topic == topicName) {
            topicNameExist = true;
            return false;
          }
        });
        if (!topicNameExist) {
          //console.log('open Edit');
          topicDataEditElement.click();
        } else {
          this.updateTopicOtionsStatus(forWhich, data, fromTopic);
        }
      }
      if (!fromTopic && topicName == null && topicDataEditElement == null) { //condition for heading checkbox status change
        this.updateTopicOtionsStatus(forWhich, data, fromTopic);
      }

    }
    if (forWhich == 'sms') {
      data = this.smsDataStorage;
      //console.log(forWhich, data, fromTopic);
      if (topicName) {
        let topicNameExist = false;
        data.forEach((item) => {
          //console.log(item.topic, topicName);
          if (item.topic == topicName) {
            topicNameExist = true;
            return false;
          }
        });
        if (!topicNameExist) {
          //console.log('open Edit');
          topicDataEditElement.click();
        } else {
          this.updateTopicOtionsStatus(forWhich, data, fromTopic);
        }
      }
      if (!fromTopic && topicName == null && topicDataEditElement == null) {
        this.updateTopicOtionsStatus(forWhich, data, fromTopic);
      }
    }
    if (forWhich == 'push') {
      data = this.pushDataStorage;
      //console.log(forWhich, data, fromTopic);
      if (topicName) {
        let topicNameExist = false;
        data.forEach((item) => {
          //console.log(item.topic, topicName);
          if (item.topic == topicName) {
            topicNameExist = true;
            return false;
          }
        });
        if (!topicNameExist) {
          //console.log('open Edit');
          topicDataEditElement.click();
        } else {
          this.updateTopicOtionsStatus(forWhich, data, fromTopic);
        }
      }
      if (!fromTopic && topicName == null && topicDataEditElement == null) {
        this.updateTopicOtionsStatus(forWhich, data, fromTopic);
      }
    }
  }

  updateTopicOtionsStatus(forWhich: string, data: Array<object>, fromTopic: boolean) {
    if (forWhich == 'email') {
      this.emailDataStorage.forEach((emailItem, key) => {
        const topicObject = this.getSingleTopicStatusObject(emailItem['topic']);
        //console.log(topicObject[forWhich]);
        this.emailDataStorage[key]['groupId'] = this.companyAndRoleDataForSubscription.groupId;
        if (fromTopic) {
          if (topicObject[forWhich]) {
            this.emailDataStorage[key]['status'] = 1;
          }
          else {
            this.emailDataStorage[key]['status'] = 0;
          }
        } else {
          if (this.subscriptionTopicsPermission.emailDisable) {
            this.emailDataStorage[key]['status'] = 0;
          } else {
            this.emailDataStorage[key]['status'] = 1;
          }
        }
        this.emailDataStorage[key]['updatedBy'] = this.userInfo['profile']['name'];
        this.emailDataStorage[key]['isUser'] = this.router.url === '/user/update-profile' ? true : false;
      });
      console.log('Before send:', this.emailDataStorage);
      if (this.emailDataStorage.length > 0) {
        this.createRoleGroupService.updateTopicEmailSubscription(this.emailDataStorage).subscribe((res) => {
          //console.log(res);
          if (res) {
            //this.loadTopicOptionsData();
            this.emailDataStorage.forEach((emailItem, key) => {
              this.updateTopicOptionStatusOnUpdate(emailItem['topic'], emailItem['status'], 'email');
            });
          }
        }, (error) => {
          console.log(error);
        });
      }
    }
    if (forWhich == 'sms') {
      this.smsDataStorage.forEach((smsItem, key) => {
        const topicObject = this.getSingleTopicStatusObject(smsItem['topic']);
        //console.log(topicObject[forWhich]);
        this.smsDataStorage[key]['groupId'] = this.companyAndRoleDataForSubscription.groupId;
        if (fromTopic) {
          if (topicObject[forWhich]) {
            this.smsDataStorage[key]['status'] = 1;
          }
          else {
            this.smsDataStorage[key]['status'] = 0;
          }
        } else {
          if (this.subscriptionTopicsPermission.smsDisable) {
            this.smsDataStorage[key]['status'] = 0;
          } else {
            this.smsDataStorage[key]['status'] = 1;
          }
        }
        this.smsDataStorage[key]['updatedBy'] = this.userInfo['profile']['name'];
        this.smsDataStorage[key]['isUser'] = this.router.url === '/user/update-profile' ? true : false;
      });
      //console.log('Before send:', this.smsDataStorage.length);
      if (this.smsDataStorage.length > 0) {
        this.createRoleGroupService.updateTopicSmsSubscription(this.smsDataStorage).subscribe((res) => {
          //console.log(res);
          if (res) {
            this.smsDataStorage.forEach((smsItem, key) => {
              this.updateTopicOptionStatusOnUpdate(smsItem['topic'], smsItem['status'], 'sms');
            });
          }
        }, (error) => {
          console.log(error);
        });
      }
    }
    if (forWhich == 'push') {
      this.pushDataStorage.forEach((pushItem, key) => {
        const topicObject = this.getSingleTopicStatusObject(pushItem['topic']);
        //console.log(topicObject[forWhich]);
        this.pushDataStorage[key]['groupId'] = this.companyAndRoleDataForSubscription.groupId;
        if (fromTopic) {
          if (topicObject[forWhich]) {
            this.pushDataStorage[key]['status'] = 1;
          }
          else {
            this.pushDataStorage[key]['status'] = 0;
          }
        } else {
          if (this.subscriptionTopicsPermission.pushDisable) {
            this.pushDataStorage[key]['status'] = 0;
          } else {
            this.pushDataStorage[key]['status'] = 1;
          }
        }
        this.pushDataStorage[key]['updatedBy'] = this.userInfo['profile']['name'];
        this.pushDataStorage[key]['isUser'] = this.router.url === '/user/update-profile' ? true : false;
      });
      //console.log('Before send:', this.pushDataStorage);
      if (this.pushDataStorage.length > 0) {
        this.createRoleGroupService.updateTopicPushSubscription(this.pushDataStorage).subscribe((res) => {
          //console.log(res);
          if (res) {
            this.pushDataStorage.forEach((pushItem, key) => {
              this.updateTopicOptionStatusOnUpdate(pushItem['topic'], pushItem['status'], 'push');
            });
          }
        }, (error) => {
          console.log(error);
        });
      }
    }
    //callback(this.emailDataStorage);
  }
  updateTopicOptionStatusOnUpdate(topic: string, status: number, statusFor: string) {
    this.companyAndRoleDataForSubscription.topics.forEach((topicObject, key) => {
      if (topicObject.topicName == topic) {
        this.companyAndRoleDataForSubscription.topics[key][statusFor] = status == 1 ? true : false;
        return false;
      }
    });
  }
  getSingleTopicStatusObject(topicName: string) {
    let matchedTopic = {};
    this.companyAndRoleDataForSubscription.topics.forEach((topic, key) => {
      if (topicName == topic.topicName) {
        //console.log(this.companyAndRoleDataForSubscription.topics[key]);
        matchedTopic = this.companyAndRoleDataForSubscription.topics[key];
        return false;
      }
    });
    return matchedTopic;
  }
  topicRowDisplay(emailDataLength: number, smsDataLength: number, pushDataLength: number) {
    if (emailDataLength == 0 && smsDataLength == 0 && pushDataLength == 0) {
      return false;
    } else {
      return true;
    }
  }
  openTopicOptionModal(templateFor: string, disable: boolean, templateData: Array<object>, topicName: string, checkboxStatus: boolean): void {
    //console.log(disable, checkboxStatus);
    if (checkboxStatus) {
      this.templateUsedFor = templateFor;
      //console.log(this.templateUsedFor);
      this.templateData = {
        topicName: topicName,
        data: templateData
      };
      this.openSubscriptionTopicOptionModal = true;
      let status = 0;
      if (checkboxStatus) {
        status = 1;
      }
      this.topicOptionStatus = status;
      //console.log(this.openSubscriptionTopicOptionModal);
      //console.log('templateData: ', templateData);
    }
  }
  closeTopicOptionModal(data: boolean): void {
    this.openSubscriptionTopicOptionModal = false;
    if (data) {
      this.emitTopicData();
    }
  }
  disableSelectedTopicCheckbox(disabled: boolean) {
    //console.log(disabled);
    this.topicEditElementLocalRef.click();
  }
}
