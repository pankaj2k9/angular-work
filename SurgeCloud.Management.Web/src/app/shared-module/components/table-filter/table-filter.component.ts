import { Component, OnInit, Input, Output, SimpleChanges, EventEmitter, Injector } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import * as _ from 'underscore';
import { AppService } from '../../../app.service';
declare var moment: any;

@Component({
  selector: 'app-table-filter',
  templateUrl: './table-filter.component.html',
  styleUrls: ['./table-filter.component.scss']
})
export class TableFilterComponent implements OnInit {
  @Input() widthData = [];
  @Input() filterValues = {};
  @Input() data = [];
  @Output() notifyChange = new EventEmitter();
  private copyData = [];
  public keyUp = new Subject<string>();
  private appService: AppService;
  constructor(injector: Injector) {
    this.copyData = [];
    this.appService = injector.get(AppService);
  }

  ngOnInit() {
    this.setSearchFilter();
  }

  setSearchFilter() {
    const subscription = this.keyUp
      .map((event) => {
        return {
          'value': event['target'].value,
          'prm': event['target'].id
        }
      })
      .debounceTime(300)
      .distinctUntilChanged()
      .subscribe(value => {
        if (this.copyData.length === 0) {
          this.copyData = JSON.parse(JSON.stringify(this.data))
        }
        this.filterValues[value.prm] = value.value.toLowerCase();
        this.data = _.filter(this.copyData, (data) => {
          if (data[value.prm] === null || data[value.prm] === undefined) {
            data[value.prm] = '';
          };
          let arr = [];
          _.each(this.filterValues, (objValue, key, obj) => {
            data[key] = (data[key] === null || data[key] === undefined) ? '' : data[key];
            let str = isNaN(data[key]) && moment(data[key]).isValid() ? this.getDateFormat(data[key].trim()) : data[key];
            if (value.prm === 'status' && key === 'status' && isNaN(this.filterValues[key])) {
              if (!isNaN(str))
                str = this.setStringAsStatus(Number(str));
            }
            let isStatusActive = false;
            if (key === 'status' && isNaN(this.filterValues['status'])) {
              if (this.copyData[0]['isNormalStringAsStatus']) {
                str = str == 0 ? 'normal' : str == 1 ? 'active' : str == 2 ? 'acknowledged' : str;
                this.filterValues[key] = this.filterValues[key] == 0 ? 'normal' : this.filterValues[key] == 1 ? 'active' : this.filterValues[key] == 2 ? 'acknowledged' : this.filterValues[key];
              } else {
                str = str == 0 ? 'inactive' : str == 1 ? 'active' : str;
                this.filterValues[key] = this.filterValues[key] == 0 ? 'inactive' : this.filterValues[key] == 1 ? 'active' : this.filterValues[key];
              }
              if (this.filterValues['status'].toLowerCase() === 'active')
                isStatusActive = true;
            }
            if ((value.value.toLowerCase() === 'active' || isStatusActive) && key === 'status')
              arr.push((this.filterValues[key] === '' || (str.toString().toLowerCase() === this.filterValues[key])));
            else
              arr.push((this.filterValues[key] === '' || (str.toString().toLowerCase().includes(this.filterValues[key]))));
          });
          return arr.every(x => x === true);
        });
        this.notifyChange.emit(this.data);
      });
  }

  setStringAsStatus(str) {
    if (str === 0) {
      return this.copyData[0]['isNormalStringAsStatus'] ? 'normal' : 'inactive';
    } else if (str === 1) {
      return 'active';
    } else if (str === 2) {
      return 'acknowledged'
    } else {
      return str;
    }
  }

  getDateFormat(value) {
    if (value.slice(-1).toLowerCase() === 'z')
      value = value.slice(0, -1);
    value = moment(value).format('MM/DD/YYYY, hh:mm A');
    let convertedTimezone = '';
    if (this.appService.userTimezone != null) {
      convertedTimezone = moment.tz(value, this.appService.userTimezone).format('MM/DD/YYYY, hh:mm A');
    } else {
      convertedTimezone = moment.utc(value).local().format('MM/DD/YYYY, hh:mm A');
    }
    return convertedTimezone;
  }
}



