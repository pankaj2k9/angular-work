import {
  Directive,
  OnInit, OnDestroy, DoCheck,
  Input, Output, HostListener, HostBinding,
  TemplateRef, EventEmitter
} from '@angular/core';
import { NgClass } from '@angular/common';
import { NgTransclude, IAttribute } from './utils.directive';
import { TabsComponent } from './tabs.component';

@Directive({
  selector: 'tab, [tab]'
})
export class TabDirective implements OnDestroy {

  @Input() public heading: string;
  @Input() public disabled: boolean;
  @Input() public removable: boolean;

  @HostBinding('class.active')
  // @Input() public get active() {
  //   return this._active;
  // }
  public active : Boolean = true;
  @Output() public select: EventEmitter<TabDirective> = new EventEmitter();
  @Output() public deselect: EventEmitter<TabDirective> = new EventEmitter();
  @Output() public removed: EventEmitter<TabDirective> = new EventEmitter();
  private _active: boolean;
  public headingRef: TemplateRef<any>;

  @HostBinding('class.tab-pane') private addClass = true;

  constructor(public tabset: TabsComponent) {
    this.tabset.addTab(this);
  }

  ngOnInit() {
    this.removable = !!this.removable;
  }

  ngOnDestroy() {
    this.tabset.removeTab(this);
  }

}
