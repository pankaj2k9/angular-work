import {Directive, ElementRef, HostListener, Input, OnChanges, Renderer2} from '@angular/core';

@Directive({
  selector: '[appAdminViewStructure]'
})
export class AdminViewStructureDirective implements OnChanges{
  viewCheckedInitialized = false;
  @Input() appAdminViewStructure: boolean;
  @HostListener('window:resize') onWindowResize() {
    setTimeout(() => {
      this.applyAdminViewStyle();
    }, 0)
  }
  constructor(private elementRef: ElementRef, private renderer: Renderer2) {

  }
  
  ngOnChanges() {
    setTimeout(() => {
      this.applyAdminViewStyle();
    }, 0)
  }

  applyAdminViewStyle() {
    if(this.appAdminViewStructure) {
      const windowInnerHeight = window.innerHeight;
      const headerHeight = document.getElementById('appHeader').offsetHeight;
      const footerHeight = document.getElementById('appFooter').offsetHeight;
      const containerHeight = (windowInnerHeight - (headerHeight + footerHeight));
      this.viewCheckedInitialized = true;
      this.renderer.setStyle(this.elementRef.nativeElement, 'max-height', containerHeight + 'px');
      this.renderer.setStyle(this.elementRef.nativeElement, 'overflow-y', 'auto');
      this.renderer.setStyle(this.elementRef.nativeElement, 'margin-top', headerHeight + 'px');
    }
    else{
      this.renderer.setStyle(this.elementRef.nativeElement, 'max-height', 'none');
      this.renderer.setStyle(this.elementRef.nativeElement, 'overflow-y', 'unset');
      this.renderer.setStyle(this.elementRef.nativeElement, 'margin-top',  0);
    }
  }
}
