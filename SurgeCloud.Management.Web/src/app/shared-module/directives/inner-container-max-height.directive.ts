import {Directive, ElementRef, HostListener, Input, Renderer2} from '@angular/core';

@Directive({
  selector: '[appInnerContainerMaxHeight]'
})
export class InnerContainerMaxHeightDirective {
  @Input() borderWidth = 0;
  @HostListener('window:resize') onWindowResize() {
    this.setElementContainerHeight();
  }
  constructor(private elementRef: ElementRef, private renderer: Renderer2) {
  }
  ngOnInit() {
    setTimeout(() => {
      this.setElementContainerHeight();
    }, 0);
  }
  ngOnChanges() {
    setTimeout(() => {
      this.setElementContainerHeight();
    }, 0);
  }
  setElementContainerHeight() {
    const windowHeight = window.innerHeight;
    //const elementPositionFromTop = this.elementRef.nativeElement.offsetTop;
    const headerHeight = document.getElementById('appHeader').offsetHeight;
    const footerHeight = document.getElementById('appFooter').offsetHeight;
    const height = windowHeight - (headerHeight + footerHeight + this.borderWidth);
    //const maxHeight = windowHeight;
    //console.log(windowHeight, headerHeight, footerHeight, height);
    this.renderer.setStyle(this.elementRef.nativeElement, 'min-height', 'unset');
    this.renderer.setStyle(this.elementRef.nativeElement, 'height', height + 'px');
    this.renderer.setStyle(this.elementRef.nativeElement, 'overflow-y', 'scroll');
  }
}
