import {Directive, ElementRef, HostListener, Input, OnChanges, OnInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[appInnerContainer]'
})
export class InnerContainerDirective implements OnInit, OnChanges{
  @Input() borderWidth = 0;
  @HostListener('window:resize') onWindowResize() {
    this.setElementContainerHeight();
  }
  constructor(private elementRef: ElementRef, private renderer: Renderer2) {
  }
  ngOnInit() {
    setTimeout(() => {
        this.setElementContainerHeight();
    }, 0);
  }
  ngOnChanges() {
    setTimeout(() => {
      this.setElementContainerHeight();
    }, 0);
  }
  setElementContainerHeight() {
    const windowHeight = window.innerHeight;
    const elementPositionFromTop = this.elementRef.nativeElement.offsetTop;
    const footerHeight = document.getElementById('appFooter').offsetHeight;
    const minHeight = windowHeight - (elementPositionFromTop + footerHeight + this.borderWidth);
    this.renderer.setStyle(this.elementRef.nativeElement, 'min-height', minHeight + 'px');
  }
}
