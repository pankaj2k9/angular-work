import { Directive, ElementRef, Input, Output, EventEmitter, OnInit, AfterViewChecked, SimpleChanges, HostListener } from '@angular/core';
declare const google: any;

@Directive({
  selector: '[appMapWindowResize]'
})

export class MapWindowResizeDirective implements OnInit, AfterViewChecked {
  private firstViewCheckedExecuted = false;
  private windowResizeData: object;
  private mainContainerHeight: number;

  @Input() map;
  @Input() data = [];
  @Output() public onChangeHeight: EventEmitter<any> = new EventEmitter();

  constructor() { }

  @HostListener('window:resize') onWindowResize() {
    this.setResizedDataOnDocumentLoad(true);
    this.setResizeEvent();
  }

  ngOnInit() {
    this.setResizedDataOnDocumentLoad(true);
    this.setMainContainerHeight();
  }

  ngOnChanges(SimpleChanges) {
  }

  setMainContainerHeight() {
    if (window.innerWidth < 992) {
      this.mainContainerHeight = 500;
    }
    else {
      this.mainContainerHeight = (this.windowResizeData['windowInnerHeight'] -
        (this.windowResizeData['headerHeight'] + this.windowResizeData['footerHeight'] + this.windowResizeData['gmapHeaderHeight'] + this.windowResizeData['gmapFooterHeight'] + 3));
    }
    this.onChangeHeight.next(this.mainContainerHeight);
  }

  setResizeEvent() {
    this.setMainContainerHeight();
    if (this.windowResizeData['resize']) {
      if (this.data.length > 0) {
        const bounds = new google.maps.LatLngBounds();
        this.data.forEach((ast) => {
          if (this.isFloat(ast.latitude) && this.isFloat(ast.longitude)) {
            bounds.extend({ lat: ast.latitude, lng: ast.longitude });
          }
        });
        this.map.setCenter(bounds.getCenter());
        this.map.fitBounds(bounds);
      } else {
        this.triggerResize();
      }
    }
  }

  isFloat(n) {
    return Number(n) === n && n % 1 !== 0;
  }

  triggerResize() {
    if (google) {
      google.maps.event.trigger(this.map, 'resize');
    }
  }

  setResizedDataOnDocumentLoad(resize: boolean = false) {
    const windowInnerWidth = window.innerWidth;
    const windowInnerHeight = window.innerHeight;
    const headerHeight = document.getElementById('appHeader').offsetHeight;
    const footerHeight = document.getElementById('appFooter').offsetHeight;
    const gmapHeaderHeight = document.getElementById('gmap-header').offsetHeight;
    const gmapFooterHeight = document.getElementById('gmap-footer').offsetHeight;
    this.windowResizeData = {
      headerHeight: headerHeight,
      footerHeight: footerHeight,
      windowInnerWidth: windowInnerWidth,
      windowInnerHeight: windowInnerHeight,
      gmapHeaderHeight: gmapHeaderHeight,
      gmapFooterHeight: gmapFooterHeight,
      resize: resize
    };
  }

  ngAfterViewChecked() {
    if (!this.firstViewCheckedExecuted) {
      //console.log("-----------**------------", document.getElementById('appHeader').offsetHeight);
      this.setResizedDataOnDocumentLoad();
      this.firstViewCheckedExecuted = true;
    }
  }

}
