import {Directive, HostBinding, Input, OnChanges} from '@angular/core';

@Directive({
  selector: '[startTimeOptionsDisable]'
})

export class StartTimeOptionsDisableDirective implements OnChanges {
  @Input() optionValue: number;
  @Input() timeRange: object;
  @Input() optionReset: boolean;
  @HostBinding('disabled') disabled = false;
  constructor() { }
  ngOnChanges() {
    if(this.optionReset) {
      this.disabled = false;
    }
    else {
      if(this.optionValue >= this.timeRange['start'] && this.optionValue <= this.timeRange['end']) {
        this.disabled = true;
      }
    }
  }
}
