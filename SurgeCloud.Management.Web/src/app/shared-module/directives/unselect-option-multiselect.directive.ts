import { Directive, ElementRef, HostListener, Input, Output, OnChanges, OnInit, Renderer2, EventEmitter } from '@angular/core';
import * as jQuery from 'jquery';

@Directive({
    selector: '[appUnselectOptionMultiselect]'
})

export class UnselectOptionMultiselectDirective implements OnInit {

    selectedId: string;
    @Input() data: object;
    @Output() public onUnselectAll: EventEmitter<any> = new EventEmitter();

    constructor(private elementRef: ElementRef, private renderer: Renderer2) {
        this.selectedId = elementRef.nativeElement.getAttribute('id');
    }

    ngOnInit() {
        setTimeout(() => {
            this.setUnselectOptionToMultiselect();
        }, true);
    }

    ngOnDestroy() {
        jQuery(`#${this.selectedId} .unselectAll-checkbox`).unbind('click');
        jQuery(`#${this.selectedId} .dropdown-list`).unbind('click');
    }

    setUnselectOptionToMultiselect() {
        let ref = jQuery(`#${this.selectedId} .cuppa-dropdown .dropdown-list .list-area .pure-checkbox.select-all`);
        ref.append(`<div class="unselectAll-checkbox"><input id="${this.selectedId}-checkbox" type="checkbox"><label><span>UnSelect All</span></label></div>`);
        jQuery(`#${this.selectedId} .unselectAll-checkbox`).on('click', (event) => {
            event.stopPropagation();
            jQuery(`#${this.selectedId}-checkbox`).click();
            setTimeout(() => {
                this.onUnselectAll.next([]);
            });
        });

        jQuery(`#${this.selectedId} .dropdown-list`).on('click', (event) => {
            jQuery(`#${this.selectedId} .unselectAll-checkbox #${this.selectedId}-checkbox`).prop('checked', false);
        });
    }

}
