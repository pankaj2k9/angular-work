import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'cameltotitle'})
export class CameltotitlePipe implements PipeTransform {
  public transform(input:string): string{
    if (!input) {
      return '';
    } else {
      return input
        .replace(/([A-Z])/g, function(match) {
          return " " + match;
        })
        .replace(/^./, function(match) {
          return match.toUpperCase();
        });
    }
  }

}
