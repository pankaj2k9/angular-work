import {Injector, Pipe, PipeTransform} from '@angular/core';
//import * as moment from 'moment';
import {AppService} from "../../app.service";
declare var moment: any;
@Pipe({
  name: 'convertToLocalDate'
})
export class ConvertToLocalDate implements PipeTransform {
  private appService: AppService;
  constructor(injector:Injector) {
    this.appService = injector.get(AppService);
  }
  transform(value: any, args: string[]): any {
    //console.log(this.appService.userTimezone);
    if (value != null) {
      /*if (args) {
          return moment.utc(value).format('MM/DD/YYYY, hh:mm A');
      }*/
      //moment.tz(value, "MM/DD/YYYY, hh:mm A", this.appService.userTimezone);
      //
      //console.log(value);
      value = moment.utc(value).format('MM/DD/YYYY, hh:mm A');
      //console.log(value);
      let convertedTimezone = '';
      if (this.appService.userTimezone != null) {
        convertedTimezone = moment.tz(value, this.appService.userTimezone).format('MM/DD/YYYY, hh:mm A');
      } else {
        convertedTimezone = moment.utc(value).local().format('MM/DD/YYYY, hh:mm A');
      }
      //console.log(convertedTimezone);
      return convertedTimezone;

      //return moment.tz(value, this.appService.userTimezone).format('MM/DD/YYYY, hh:mm A');
      //return moment.utc(value).local().format('MM/DD/YYYY, hh:mm A');
      //return moment().tz("America/Los_Angeles").format();
    }
    return '';
  }
}
