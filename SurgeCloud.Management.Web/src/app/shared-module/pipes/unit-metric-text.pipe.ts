import { UnitConversionFormulaService } from './../../unit-conversion-formula.service';
import {Injector, Pipe, PipeTransform} from '@angular/core';
import {AppService} from '../../app.service';

@Pipe({
  name: 'unitMetricText'
})
export class UnitMetricTextPipe implements PipeTransform {
  private appService: AppService;
  private unitService: UnitConversionFormulaService;
  constructor(injector: Injector) {
    this.appService = injector.get(AppService);
    this.unitService = injector.get(UnitConversionFormulaService);
  }
  transform(value: any, args?: any): any {
    return this.unitService.convertUnitText(value, this.appService.userProfileDetails.measurementUnit);
  }
}
