import {Injector, Pipe, PipeTransform} from '@angular/core';
import {AppService} from '../../app.service';
import { UnitConversionFormulaService } from './../../unit-conversion-formula.service';

@Pipe({
  name: 'unitMetric'
})
export class UnitMetricPipe implements PipeTransform {
  private appService: AppService;
  private unitService: UnitConversionFormulaService;
  constructor(injector: Injector) {
    this.appService = injector.get(AppService);
    this.unitService = injector.get(UnitConversionFormulaService);
  }
  transform(value: any, args?: any): any {
    let updatedVal = this.unitService.convertUnit(args, this.appService.userProfileDetails.measurementUnit, value);
    if (Number(updatedVal) <= Number.MAX_SAFE_INTEGER) {
      if (updatedVal < 0.001 && updatedVal > 0) {
        updatedVal = Number(value).toExponential(2);
      } else {
        updatedVal = parseFloat(Number(value).toFixed(3));
      }
    }
    return updatedVal;
  }

}
