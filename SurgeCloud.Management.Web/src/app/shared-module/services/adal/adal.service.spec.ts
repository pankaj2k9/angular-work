import { TestBed, inject } from '@angular/core/testing';

import { AdalService } from './adal.service';

describe('AdalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdalService]
    });
  });

  it('should ...', inject([AdalService], (service: AdalService) => {
    expect(service).toBeTruthy();
  }));
});
