import { TestBed, inject } from '@angular/core/testing';

import { BsMapService } from './bsmap.service';

describe('WebsocketService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BsMapService]
    });
  });

  it('should ...', inject([BsMapService], (service: BsMapService) => {
    expect(service).toBeTruthy();
  }));
});
