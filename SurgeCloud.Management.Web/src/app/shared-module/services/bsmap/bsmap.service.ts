import { Injectable } from '@angular/core';
import * as jQuery from 'jquery';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { environment } from '../../../../environments/environment';
declare let google: any;

@Injectable()
export class BsMapService {

  public locationAddress: any;
  constructor() { }
  public getLocationInfo(latitude: number, longitude: number) {
    const geocoder = (typeof google == "object") ? new google.maps.Geocoder() : '';
    const latlng = { lat: latitude, lng: longitude };
    const that = this;
    return Observable.create((observer: Observer<object>) => {
      if (geocoder) {
        new google.maps.Geocoder().geocode({ 'location': latlng }, function (results, status) {
          if (status === 'OK') {
            const geocoderAddress = results[0].formatted_address.split(',');
            /* added exception condition by kesavan */
            if (geocoderAddress) {
              this.locationAddress = {
                street: (geocoderAddress[0] && geocoderAddress[0].trim()) || '',
                city: (geocoderAddress[1] && geocoderAddress[1].trim()) || '',
                state: (geocoderAddress[2] && geocoderAddress[2].trim().split(' ')[0]) || '',
                zipcode: (geocoderAddress[2] && geocoderAddress[2].trim().split(' ')[1]) || '',
                country: (geocoderAddress[3] && geocoderAddress[3].trim()) || ''
              };
              observer.next(this.locationAddress);
            }

          } else if (status === 'OVER_QUERY_LIMIT') {
            observer.next({ status: 'error' });
          }
          else {
            observer.next({ status: 'error' });
          }
        });
      } else {
        observer.next({ status: 'error' });
      }
    });
  }
}
