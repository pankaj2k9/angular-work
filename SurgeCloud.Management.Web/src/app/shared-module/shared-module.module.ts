import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule, XHRBackend, RequestOptions } from '@angular/http';
import { HttpService } from './services/http/http.service';
import { WebsocketService } from './services/websocket/websocket.service';
import { BsMapService } from './services/bsmap/bsmap.service';
import { SortByPipe } from './pipes/sort-by.pipe';
import { ConvertToLocalDate } from './pipes/date-conversion'
import { WizardComponent } from './components';
import { AgmCoreModule } from '@agm/core';
// import { MaterialModule } from '@angular/material';
import { TabsComponent } from './components/tabs/tabs.component';
import { TabHeadingDirective } from './components/tabs/tab-heading.directive';
import { TabDirective } from './components/tabs/tab.directive';
import { NgTransclude } from './components/tabs/utils.directive';
import { NgProgressService } from 'ng2-progressbar';
import { AdalService } from './services/adal/adal.service';
import { AdminViewStructureDirective } from './directives/admin-view-structure.directive';
import { InnerContainerDirective } from './directives/inner-container.directive';
import { EndTimeOptionsDisableDirective } from './directives/end-time-options-disable.directive';
import { InnerContainerMaxHeightDirective } from './directives/inner-container-max-height.directive';
import { StartTimeOptionsDisableDirective } from './directives/start-time-options-disable.directive';
import { UnselectOptionMultiselectDirective } from './directives/unselect-option-multiselect.directive';
import { SubscriptionTopicsComponent } from './components/subscription-topics/subscription-topics.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SubscriptionTopicOptionTemplateComponent } from './components/subscription-topics/subscription-topic-option-template/subscription-topic-option-template.component';
import { ModalModule } from 'ngx-bootstrap';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { UnitMetricPipe } from './pipes/unit-metric.pipe';
import { UnitMetricTextPipe } from './pipes/unit-metric-text.pipe';
import { TableFilterComponent } from './components/table-filter/table-filter.component';
import { ExportDataComponent } from './components/export-data/export-data.component';
import { ExcludeDaysComponent } from './components/exclude-days/exclude-days.component';
import { NativeColorPickerComponent } from './components/native-color-picker/native-color-picker.component';
import { MapWindowResizeDirective } from './directives/map-window-resize.directive';
import { DaySchedulerComponent } from './components/day-scheduler/day-scheduler.component';
import { ObjKeysPipe } from './pipes/obj-keys.pipe';
import { CameltotitlePipe } from './pipes/cameltotitle.pipe';
import {TranslateModule} from "@ngx-translate/core";

export function httpServiceFactory(backend: XHRBackend, defaultOptions: RequestOptions, NgProgressService: NgProgressService) {
  return new HttpService(backend, defaultOptions, NgProgressService);
}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ModalModule.forRoot(),
    ReactiveFormsModule,
    AgmCoreModule,
    AngularMultiSelectModule,
    TranslateModule
    // MaterialModule
  ],
  declarations: [
    SortByPipe,
    ConvertToLocalDate,
    WizardComponent,
    TabsComponent,
    TabHeadingDirective,
    TabDirective,
    NgTransclude,
    AdminViewStructureDirective,
    InnerContainerDirective,
    EndTimeOptionsDisableDirective,
    InnerContainerMaxHeightDirective,
    StartTimeOptionsDisableDirective,
    UnselectOptionMultiselectDirective,
    SubscriptionTopicsComponent,
    SubscriptionTopicOptionTemplateComponent,
    UnitMetricPipe,
    UnitMetricTextPipe,
    TableFilterComponent,
    ExportDataComponent,
    ExcludeDaysComponent,
    NativeColorPickerComponent,
    MapWindowResizeDirective,
    DaySchedulerComponent,
    ObjKeysPipe,
    CameltotitlePipe
  ],
  exports: [
    SortByPipe,
    ConvertToLocalDate,
    WizardComponent,
    NgTransclude,
    TabHeadingDirective,
    TabsComponent,
    TabDirective,
    AdminViewStructureDirective,
    InnerContainerDirective,
    EndTimeOptionsDisableDirective,
    InnerContainerMaxHeightDirective,
    StartTimeOptionsDisableDirective,
    UnselectOptionMultiselectDirective,
    SubscriptionTopicsComponent,
    TableFilterComponent,
    ExportDataComponent,
    ExcludeDaysComponent,
    NativeColorPickerComponent,
    SubscriptionTopicOptionTemplateComponent,
    UnitMetricPipe,
    UnitMetricTextPipe,
    AngularMultiSelectModule,
    MapWindowResizeDirective,
    DaySchedulerComponent,
    AgmCoreModule,
    ObjKeysPipe,
    CameltotitlePipe,
    TranslateModule
  ],
  providers: [
    HttpService,
    WebsocketService,
    BsMapService,
    NgProgressService,
    {
      provide: HttpService,
      useFactory: httpServiceFactory,
      deps: [XHRBackend, RequestOptions, NgProgressService]
    }
  ]
})
export class SharedModule { }
