// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,

  apiBaseUrl: 'https://surgecloud-dev.azurewebsites.net/',
  // apiBaseUrl: 'http://intelligeninsight.azurewebsites.net/',
  // apiBaseUrl: 'https://surgecloud.bluesurge.com/',
  googleApiKey: 'AIzaSyCcoEdexIhtOtEQoCOoqk1UQrud_KisRes',
  // googleApiKey: 'AIzaSyC2M9mZet7ZI24632ugbnRc3gFHJGMYMrI',
  defaultImage: 'assets/images/org_default.svg',
  defaultAssetImage: 'assets/images/default_asset.svg',
  adal: {
    // tenant: 'intelligeninsight.onmicrosoft.com',
    tenant: 'bsdevad.onmicrosoft.com',
    // tenant: 'bluesurgead.onmicrosoft.com',
    // clientId: '379f6d12-7fea-4e23-907e-355887fe136d' // AD B2C Staging
    // clientId: '9f860bb8-0593-4484-82cc-a0be72c214a8' // AD B2C Local
   // clientId: '7ff95bad-ca15-4352-99f1-f309fbd46cfb' // Galpower local
   clientId: 'e533e6cc-fe9b-491a-a6c4-2413ea5be932' // AD Local
   // clientId: 'e78244fe-9742-4854-b1f2-c17666cdae00' // AD Dev
   // clientId: '3852743f-7a5f-4e7b-af59-075cd21a7c09' // '8df10808-cc89-4ba8-b4e5-23e7dcdfc64b' // AD Staging
   // clientId: 'b84ffcde-1910-451b-b6e1-099c9216e911' // Galpower Prod
  },
  socket: {
    googleMap: 'wss://40.71.192.34:3006',
    alarm: 'wss://streaming.bluesurge.com'
    // alarm: 'http://localhost:3000'
  }
};
